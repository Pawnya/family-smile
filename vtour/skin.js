// Garden Gnome Software - Skin
// Pano2VR 5.2.3/15990
// Filename: FS-2018.04.22.ggsk
// Generated вс апр. 22 15:03:29 2018

function pano2vrSkin(player,base) {
	var ggSkinVars = [];
	ggSkinVars['ht_ani'] = false;
	ggSkinVars['ht_ani_1'] = false;
	ggSkinVars['gallery_show_hide'] = false;
	ggSkinVars['gallery_pictures'] = 0;
	var me=this;
	var flag=false;
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	this.ggUserdata=me.player.userdata;
	this.lastSize={ w: -1,h: -1 };
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=[];
	this.elementMouseOver=[];
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	if (typeof document.body.style['transform'] == 'undefined') {
		for(var i=0;i<prefixes.length;i++) {
			if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
				cssPrefix='-' + prefixes[i].toLowerCase() + '-';
				domTransition=prefixes[i] + 'Transition';
				domTransform=prefixes[i] + 'Transform';
			}
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=[];
		var stack=[];
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		var hs='';
		this.ggCurrentTime=new Date().getTime();
		this._hotspots=document.createElement('div');
		this._hotspots.ggId="hotspots";
		this._hotspots.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._hotspots.ggVisible=true;
		this._hotspots.className='ggskin ggskin_container ';
		this._hotspots.ggType='container';
		hs ='';
		hs+='height : 17px;';
		hs+='left : 505px;';
		hs+='position : absolute;';
		hs+='top : 437px;';
		hs+='visibility : inherit;';
		hs+='width : 10px;';
		hs+='pointer-events:none;';
		this._hotspots.setAttribute('style',hs);
		this._hotspots.style[domTransform + 'Origin']='50% 50%';
		me._hotspots.ggIsActive=function() {
			return false;
		}
		me._hotspots.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._hotspots.ggUpdatePosition=function (useTransition) {
		}
		this.divSkin.appendChild(this._hotspots);
		this._hide_template=document.createElement('div');
		this._hide_template.ggId="hide_template";
		this._hide_template.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._hide_template.ggVisible=false;
		this._hide_template.className='ggskin ggskin_container ';
		this._hide_template.ggType='container';
		hs ='';
		hs+='height : 45px;';
		hs+='left : 278px;';
		hs+='overflow : hidden;';
		hs+='position : absolute;';
		hs+='top : 11px;';
		hs+='visibility : hidden;';
		hs+='width : 187px;';
		hs+='pointer-events:none;';
		this._hide_template.setAttribute('style',hs);
		this._hide_template.style[domTransform + 'Origin']='50% 50%';
		me._hide_template.ggIsActive=function() {
			return false;
		}
		me._hide_template.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._hide_template.ggUpdatePosition=function (useTransition) {
		}
		this.divSkin.appendChild(this._hide_template);
		this._virtushka=document.createElement('div');
		this._virtushka__img=document.createElement('img');
		this._virtushka__img.className='ggskin ggskin_svg';
		this._virtushka__img.setAttribute('src',basePath + 'images/virtushka.svg');
		this._virtushka__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this._virtushka__img['ondragstart']=function() { return false; };
		this._virtushka.appendChild(this._virtushka__img);
		this._virtushka.ggId="virtushka";
		this._virtushka.ggLeft=-88;
		this._virtushka.ggTop=-22;
		this._virtushka.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._virtushka.ggVisible=true;
		this._virtushka.className='ggskin ggskin_svg ';
		this._virtushka.ggType='svg';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 8px;';
		hs+='left : -88px;';
		hs+='position : absolute;';
		hs+='top : -22px;';
		hs+='visibility : inherit;';
		hs+='width : 68px;';
		hs+='pointer-events:auto;';
		this._virtushka.setAttribute('style',hs);
		this._virtushka.style[domTransform + 'Origin']='50% 50%';
		me._virtushka.ggIsActive=function() {
			return false;
		}
		me._virtushka.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._virtushka.onclick=function (e) {
			me.player.openUrl("http:\/\/virtushka.com","");
		}
		this._virtushka.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this._rectangle_4=document.createElement('div');
		this._rectangle_4.ggId="Rectangle 4";
		this._rectangle_4.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._rectangle_4.ggVisible=true;
		this._rectangle_4.className='ggskin ggskin_rectangle ';
		this._rectangle_4.ggType='rectangle';
		hs ='';
		hs+='background : rgba(255,255,255,0);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 32px;';
		hs+='left : -11px;';
		hs+='position : absolute;';
		hs+='top : -7px;';
		hs+='visibility : inherit;';
		hs+='width : 96px;';
		hs+='pointer-events:auto;';
		this._rectangle_4.setAttribute('style',hs);
		this._rectangle_4.style[domTransform + 'Origin']='50% 50%';
		me._rectangle_4.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._rectangle_4.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._rectangle_4.ggUpdatePosition=function (useTransition) {
		}
		this._virtushka.appendChild(this._rectangle_4);
		this.divSkin.appendChild(this._virtushka);
		this._fullmenu=document.createElement('div');
		this._fullmenu.ggId="fullmenu";
		this._fullmenu.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._fullmenu.ggVisible=true;
		this._fullmenu.className='ggskin ggskin_container ';
		this._fullmenu.ggType='container';
		hs ='';
		hs+='height : 447px;';
		hs+='left : 12px;';
		hs+='position : absolute;';
		hs+='top : 19px;';
		hs+='visibility : inherit;';
		hs+='width : 256px;';
		hs+='pointer-events:none;';
		this._fullmenu.setAttribute('style',hs);
		this._fullmenu.style[domTransform + 'Origin']='50% 50%';
		me._fullmenu.ggIsActive=function() {
			return false;
		}
		me._fullmenu.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me._fullmenu.ggCurrentLogicStatePosition = -1;
		this._fullmenu.ggUpdateConditionResize=function () {
			var newLogicStatePosition;
			if (
				(me.player.getViewerSize().height < 460)
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._fullmenu.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._fullmenu.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._fullmenu.style[domTransition]='left none, top none';
				if (me._fullmenu.ggCurrentLogicStatePosition == 0) {
					me._fullmenu.style.left='-360px';
					me._fullmenu.style.top='19px';
				}
				else {
					me._fullmenu.style.left='12px';
					me._fullmenu.style.top='19px';
				}
			}
		}
		this._fullmenu.ggUpdatePosition=function (useTransition) {
			me._fullmenu.ggUpdateConditionResize();
		}
		this._svg_10=document.createElement('div');
		this._svg_10__img=document.createElement('img');
		this._svg_10__img.className='ggskin ggskin_svg';
		this._svg_10__img.setAttribute('src',basePath + 'images/svg_10.svg');
		this._svg_10__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this._svg_10__img['ondragstart']=function() { return false; };
		this._svg_10.appendChild(this._svg_10__img);
		this._svg_10.ggId="Svg 1";
		this._svg_10.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._svg_10.ggVisible=true;
		this._svg_10.className='ggskin ggskin_svg ';
		this._svg_10.ggType='svg';
		hs ='';
		hs+='height : 70px;';
		hs+='left : 30px;';
		hs+='position : absolute;';
		hs+='top : -11px;';
		hs+='visibility : inherit;';
		hs+='width : 151px;';
		hs+='pointer-events:auto;';
		this._svg_10.setAttribute('style',hs);
		this._svg_10.style[domTransform + 'Origin']='50% 50%';
		me._svg_10.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._svg_10.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._svg_10.ggUpdatePosition=function (useTransition) {
		}
		this._fullmenu.appendChild(this._svg_10);
		this._rectangle_5=document.createElement('div');
		this._rectangle_5.ggId="Rectangle 5";
		this._rectangle_5.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._rectangle_5.ggVisible=true;
		this._rectangle_5.className='ggskin ggskin_rectangle ';
		this._rectangle_5.ggType='rectangle';
		hs ='';
		hs+='background : rgba(255,255,255,0);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 51px;';
		hs+='left : 58px;';
		hs+='position : absolute;';
		hs+='top : -2px;';
		hs+='visibility : inherit;';
		hs+='width : 119px;';
		hs+='pointer-events:auto;';
		this._rectangle_5.setAttribute('style',hs);
		this._rectangle_5.style[domTransform + 'Origin']='50% 50%';
		me._rectangle_5.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._rectangle_5.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._rectangle_5.onclick=function (e) {
			me.player.openUrl("http:\/\/family-smile.ru","");
		}
		this._rectangle_5.onmouseover=function (e) {
			me.elementMouseOver['rectangle_5']=true;
		}
		this._rectangle_5.onmouseout=function (e) {
			me._tooltiplogo.style[domTransition]='none';
			me._tooltiplogo.style.visibility='hidden';
			me._tooltiplogo.ggVisible=false;
			me.elementMouseOver['rectangle_5']=false;
		}
		this._rectangle_5.ontouchend=function (e) {
			me.elementMouseOver['rectangle_5']=false;
		}
		this._rectangle_5.ggUpdatePosition=function (useTransition) {
		}
		this._tooltiplogo=document.createElement('div');
		this._tooltiplogo.ggId="tooltip-logo";
		this._tooltiplogo.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._tooltiplogo.ggVisible=false;
		this._tooltiplogo.className='ggskin ggskin_rectangle ';
		this._tooltiplogo.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 6px;';
		hs+='border-radius : 6px;';
		hs+='background : rgba(194,194,194,0.784314);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 21px;';
		hs+='left : -9px;';
		hs+='position : absolute;';
		hs+='top : -16px;';
		hs+='visibility : hidden;';
		hs+='width : 131px;';
		hs+='pointer-events:auto;';
		this._tooltiplogo.setAttribute('style',hs);
		this._tooltiplogo.style[domTransform + 'Origin']='50% 50%';
		me._tooltiplogo.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._tooltiplogo.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._tooltiplogo.ggUpdatePosition=function (useTransition) {
		}
		this._tooltiplogo0=document.createElement('div');
		this._tooltiplogo0__text=document.createElement('div');
		this._tooltiplogo0.className='ggskin ggskin_textdiv';
		this._tooltiplogo0.ggTextDiv=this._tooltiplogo0__text;
		this._tooltiplogo0.ggId="tooltip-logo-";
		this._tooltiplogo0.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._tooltiplogo0.ggVisible=true;
		this._tooltiplogo0.className='ggskin ggskin_text ';
		this._tooltiplogo0.ggType='text';
		hs ='';
		hs+='z-index: 100;';
		hs+='height : 19px;';
		hs+='left : 7px;';
		hs+='position : absolute;';
		hs+='top : -1px;';
		hs+='visibility : inherit;';
		hs+='width : 104px;';
		hs+='pointer-events:auto;';
		this._tooltiplogo0.setAttribute('style',hs);
		this._tooltiplogo0.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='background: #f9b233;';
		hs+='background: rgba(249,178,51,0);';
		hs+='border: 2px solid #f9b233;';
		hs+='border: 2px solid rgba(249,178,51,0);';
		hs+=cssPrefix + 'background-clip: padding-box;';
		hs+='background-clip: padding-box;';
		hs+='border-radius: 5px;';
		hs+=cssPrefix + 'border-radius: 5px;';
		hs+='color: rgba(0,0,0,1);';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._tooltiplogo0__text.setAttribute('style',hs);
		this._tooltiplogo0__text.innerHTML="<span style=\"font-family: 'Open Sans', sans-serif; font-size: 12px\">\u0432\u0435\u0440\u043d\u0443\u0442\u044c\u0441\u044f \u043d\u0430 \u0441\u0430\u0439\u0442<\/span>";
		this._tooltiplogo0.appendChild(this._tooltiplogo0__text);
		me._tooltiplogo0.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._tooltiplogo0.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._tooltiplogo0.ggUpdatePosition=function (useTransition) {
		}
		this._tooltiplogo.appendChild(this._tooltiplogo0);
		this._rectangle_5.appendChild(this._tooltiplogo);
		this._fullmenu.appendChild(this._rectangle_5);
		this.divSkin.appendChild(this._fullmenu);
		this._controlmenu=document.createElement('div');
		this._controlmenu.ggId="control-menu";
		this._controlmenu.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._controlmenu.ggVisible=true;
		this._controlmenu.className='ggskin ggskin_container ';
		this._controlmenu.ggType='container';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 37px;';
		hs+='left : 5px;';
		hs+='position : absolute;';
		hs+='top : 8px;';
		hs+='visibility : inherit;';
		hs+='width : 38px;';
		hs+='pointer-events:none;';
		this._controlmenu.setAttribute('style',hs);
		this._controlmenu.style[domTransform + 'Origin']='50% 50%';
		me._controlmenu.ggIsActive=function() {
			return false;
		}
		me._controlmenu.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._controlmenu.onmouseover=function (e) {
			if (me.player.transitionsDisabled) {
				me._controlmenu.style[domTransition]='none';
			} else {
				me._controlmenu.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._controlmenu.ggParameter.rx=0;me._controlmenu.ggParameter.ry=3;
			me._controlmenu.style[domTransform]=parameterToTransform(me._controlmenu.ggParameter);
		}
		this._controlmenu.onmouseout=function (e) {
			if (me.player.transitionsDisabled) {
				me._controlmenu.style[domTransition]='none';
			} else {
				me._controlmenu.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._controlmenu.ggParameter.rx=0;me._controlmenu.ggParameter.ry=0;
			me._controlmenu.style[domTransform]=parameterToTransform(me._controlmenu.ggParameter);
		}
		me._controlmenu.ggCurrentLogicStatePosition = -1;
		this._controlmenu.ggUpdateConditionResize=function () {
			var newLogicStatePosition;
			if (
				(me.player.getViewerSize().height < 460)
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._controlmenu.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._controlmenu.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._controlmenu.style[domTransition]='left none, top none';
				if (me._controlmenu.ggCurrentLogicStatePosition == 0) {
					me._controlmenu.style.left='-360px';
					me._controlmenu.style.top='8px';
				}
				else {
					me._controlmenu.style.left='5px';
					me._controlmenu.style.top='8px';
				}
			}
		}
		this._controlmenu.ggUpdatePosition=function (useTransition) {
			me._controlmenu.ggUpdateConditionResize();
		}
		this._svg_3=document.createElement('div');
		this._svg_3__img=document.createElement('img');
		this._svg_3__img.className='ggskin ggskin_svg';
		this._svg_3__img.setAttribute('src',basePath + 'images/svg_3.svg');
		this._svg_3__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this._svg_3__img['ondragstart']=function() { return false; };
		this._svg_3.appendChild(this._svg_3__img);
		this._svg_3.ggId="Svg 3";
		this._svg_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._svg_3.ggVisible=true;
		this._svg_3.className='ggskin ggskin_svg ';
		this._svg_3.ggType='svg';
		hs ='';
		hs+='height : 22px;';
		hs+='left : 6px;';
		hs+='position : absolute;';
		hs+='top : 4px;';
		hs+='visibility : inherit;';
		hs+='width : 22px;';
		hs+='pointer-events:auto;';
		this._svg_3.setAttribute('style',hs);
		this._svg_3.style[domTransform + 'Origin']='50% 50%';
		me._svg_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._svg_3.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._svg_3.ggUpdatePosition=function (useTransition) {
		}
		this._controlmenu.appendChild(this._svg_3);
		this._menu_close=document.createElement('div');
		this._menu_close.ggId="menu_close";
		this._menu_close.ggParameter={ rx:0,ry:0,a:0,sx:0,sy:0 };
		this._menu_close.ggVisible=true;
		this._menu_close.className='ggskin ggskin_rectangle ';
		this._menu_close.ggType='rectangle';
		hs ='';
		hs+='background : rgba(85,85,0,0);';
		hs+='border : 0px solid #0000ff;';
		hs+='cursor : pointer;';
		hs+='height : 47px;';
		hs+='left : -10px;';
		hs+='position : absolute;';
		hs+='top : -5px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		this._menu_close.setAttribute('style',hs);
		this._menu_close.style[domTransform + 'Origin']='50% 50%';
		this._menu_close.style[domTransform]=parameterToTransform(this._menu_close.ggParameter);
		me._menu_close.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._menu_close.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._menu_close.onclick=function (e) {
			if (me.player.transitionsDisabled) {
				me._fullmenu.style[domTransition]='none';
			} else {
				me._fullmenu.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._fullmenu.ggParameter.rx=0;me._fullmenu.ggParameter.ry=0;
			me._fullmenu.style[domTransform]=parameterToTransform(me._fullmenu.ggParameter);
			me._menu_close.style[domTransition]='none';
			me._menu_close.ggParameter.sx=0;me._menu_close.ggParameter.sy=0;
			me._menu_close.style[domTransform]=parameterToTransform(me._menu_close.ggParameter);
			me._menu_open.style[domTransition]='none';
			me._menu_open.ggParameter.sx=1;me._menu_open.ggParameter.sy=1;
			me._menu_open.style[domTransform]=parameterToTransform(me._menu_open.ggParameter);
			if (me.player.transitionsDisabled) {
				me._scrollarea_1.style[domTransition]='none';
			} else {
				me._scrollarea_1.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._scrollarea_1.ggParameter.rx=0;me._scrollarea_1.ggParameter.ry=0;
			me._scrollarea_1.style[domTransform]=parameterToTransform(me._scrollarea_1.ggParameter);
		}
		me._menu_close.ggCurrentLogicStateVisible = -1;
		this._menu_close.ggUpdateConditionResize=function () {
			var newLogicStateVisible;
			if (
				(me.player.getViewerSize().height < 460)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._menu_close.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._menu_close.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._menu_close.style[domTransition]='';
				if (me._menu_close.ggCurrentLogicStateVisible == 0) {
					me._menu_close.style.visibility="hidden";
					me._menu_close.ggVisible=false;
				}
				else {
					me._menu_close.style.visibility=(Number(me._menu_close.style.opacity)>0||!me._menu_close.style.opacity)?'inherit':'hidden';
					me._menu_close.ggVisible=true;
				}
			}
		}
		this._menu_close.ggUpdatePosition=function (useTransition) {
			me._menu_close.ggUpdateConditionResize();
		}
		this._controlmenu.appendChild(this._menu_close);
		this._menu_open=document.createElement('div');
		this._menu_open.ggId="menu_open";
		this._menu_open.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._menu_open.ggVisible=true;
		this._menu_open.className='ggskin ggskin_rectangle ';
		this._menu_open.ggType='rectangle';
		hs ='';
		hs+='background : rgba(255,255,255,0);';
		hs+='border : 0px solid #0000ff;';
		hs+='cursor : pointer;';
		hs+='height : 46px;';
		hs+='left : -6px;';
		hs+='position : absolute;';
		hs+='top : -5px;';
		hs+='visibility : inherit;';
		hs+='width : 43px;';
		hs+='pointer-events:auto;';
		this._menu_open.setAttribute('style',hs);
		this._menu_open.style[domTransform + 'Origin']='50% 50%';
		me._menu_open.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._menu_open.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._menu_open.onclick=function (e) {
			if (me.player.transitionsDisabled) {
				me._fullmenu.style[domTransition]='none';
			} else {
				me._fullmenu.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._fullmenu.ggParameter.rx=-364;me._fullmenu.ggParameter.ry=0;
			me._fullmenu.style[domTransform]=parameterToTransform(me._fullmenu.ggParameter);
			me._menu_open.style[domTransition]='none';
			me._menu_open.ggParameter.sx=0;me._menu_open.ggParameter.sy=0;
			me._menu_open.style[domTransform]=parameterToTransform(me._menu_open.ggParameter);
			me._menu_close.style[domTransition]='none';
			me._menu_close.ggParameter.sx=1;me._menu_close.ggParameter.sy=1;
			me._menu_close.style[domTransform]=parameterToTransform(me._menu_close.ggParameter);
			if (me.player.transitionsDisabled) {
				me._scrollarea_1.style[domTransition]='none';
			} else {
				me._scrollarea_1.style[domTransition]='all 1000ms ease-out 0ms';
			}
			me._scrollarea_1.ggParameter.rx=-364;me._scrollarea_1.ggParameter.ry=0;
			me._scrollarea_1.style[domTransform]=parameterToTransform(me._scrollarea_1.ggParameter);
		}
		this._menu_open.ggUpdatePosition=function (useTransition) {
		}
		this._controlmenu.appendChild(this._menu_open);
		this.divSkin.appendChild(this._controlmenu);
		this._screen_tint=document.createElement('div');
		this._screen_tint.ggId="screen_tint";
		this._screen_tint.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._screen_tint.ggVisible=false;
		this._screen_tint.className='ggskin ggskin_rectangle ';
		this._screen_tint.ggType='rectangle';
		hs ='';
		hs+='background : rgba(0,0,0,0.588235);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 100%;';
		hs+='left : 0.02%;';
		hs+='position : absolute;';
		hs+='top : 0.02%;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:auto;';
		this._screen_tint.setAttribute('style',hs);
		this._screen_tint.style[domTransform + 'Origin']='50% 50%';
		me._screen_tint.ggIsActive=function() {
			return false;
		}
		me._screen_tint.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._screen_tint.onclick=function (e) {
			ggSkinVars['gallery_show_hide'] = false;
			ggSkinVars['gallery_pictures'] = Number("0");
		}
		me._screen_tint.ggCurrentLogicStateVisible = -1;
		this._screen_tint.ggUpdateConditionTimer=function () {
			var newLogicStateVisible;
			if (
				(ggSkinVars['gallery_show_hide'] == true)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._screen_tint.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._screen_tint.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._screen_tint.style[domTransition]='';
				if (me._screen_tint.ggCurrentLogicStateVisible == 0) {
					me._screen_tint.style.visibility=(Number(me._screen_tint.style.opacity)>0||!me._screen_tint.style.opacity)?'inherit':'hidden';
					me._screen_tint.ggVisible=true;
				}
				else {
					me._screen_tint.style.visibility="hidden";
					me._screen_tint.ggVisible=false;
				}
			}
		}
		this._screen_tint.ggUpdatePosition=function (useTransition) {
		}
		this.divSkin.appendChild(this._screen_tint);
		this._gallery_picture=document.createElement('div');
		this._gallery_picture__img=document.createElement('img');
		this._gallery_picture__img.className='ggskin ggskin_external';
		this._gallery_picture__img.onload=function() {me._gallery_picture.ggUpdatePosition();}
		this._gallery_picture__img.setAttribute('src',basePath + '');
		this._gallery_picture__img['ondragstart']=function() { return false; };
		hs ='';
		this._gallery_picture.appendChild(this._gallery_picture__img);
		this._gallery_picture.ggId="gallery_picture";
		this._gallery_picture.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._gallery_picture.ggVisible=false;
		this._gallery_picture.className='ggskin ggskin_external ';
		this._gallery_picture.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 81.25%;';
		hs+='left : 3.76%;';
		hs+='position : absolute;';
		hs+='top : 10.67%;';
		hs+='visibility : hidden;';
		hs+='width : 91.875%;';
		hs+='pointer-events:auto;';
		this._gallery_picture.setAttribute('style',hs);
		this._gallery_picture.style[domTransform + 'Origin']='50% 50%';
		me._gallery_picture.ggIsActive=function() {
			return false;
		}
		me._gallery_picture.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._gallery_picture.onclick=function (e) {
			ggSkinVars['gallery_show_hide'] = false;
			ggSkinVars['gallery_pictures'] = Number("0");
		}
		me._gallery_picture.ggCurrentLogicStateVisible = -1;
		me._gallery_picture.ggCurrentLogicStateExternalUrl = -1;
		this._gallery_picture.ggUpdateConditionTimer=function () {
			var newLogicStateVisible;
			if (
				(ggSkinVars['gallery_show_hide'] == true)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._gallery_picture.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._gallery_picture.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._gallery_picture.style[domTransition]='';
				if (me._gallery_picture.ggCurrentLogicStateVisible == 0) {
					me._gallery_picture.style.visibility=(Number(me._gallery_picture.style.opacity)>0||!me._gallery_picture.style.opacity)?'inherit':'hidden';
					me._gallery_picture.ggVisible=true;
				}
				else {
					me._gallery_picture.style.visibility="hidden";
					me._gallery_picture.ggVisible=false;
				}
			}
			var newLogicStateExternalUrl;
			if (
				(ggSkinVars['gallery_pictures'] == 0)
			)
			{
				newLogicStateExternalUrl = 0;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 1)
			)
			{
				newLogicStateExternalUrl = 1;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 2)
			)
			{
				newLogicStateExternalUrl = 2;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 3)
			)
			{
				newLogicStateExternalUrl = 3;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 4)
			)
			{
				newLogicStateExternalUrl = 4;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 5)
			)
			{
				newLogicStateExternalUrl = 5;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 6)
			)
			{
				newLogicStateExternalUrl = 6;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 7)
			)
			{
				newLogicStateExternalUrl = 7;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 8)
			)
			{
				newLogicStateExternalUrl = 8;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 9)
			)
			{
				newLogicStateExternalUrl = 9;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 10)
			)
			{
				newLogicStateExternalUrl = 10;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 11)
			)
			{
				newLogicStateExternalUrl = 11;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 12)
			)
			{
				newLogicStateExternalUrl = 12;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 13)
			)
			{
				newLogicStateExternalUrl = 13;
			}
			else if (
				(ggSkinVars['gallery_pictures'] == 14)
			)
			{
				newLogicStateExternalUrl = 14;
			}
			else {
				newLogicStateExternalUrl = -1;
			}
			if (me._gallery_picture.ggCurrentLogicStateExternalUrl != newLogicStateExternalUrl) {
				me._gallery_picture.ggCurrentLogicStateExternalUrl = newLogicStateExternalUrl;
				me._gallery_picture.style[domTransition]='';
				if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 0) {
					me._gallery_picture.ggText="gallery/img_00.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 1) {
					me._gallery_picture.ggText="gallery/img_01.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 2) {
					me._gallery_picture.ggText="gallery/img_02.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 3) {
					me._gallery_picture.ggText="gallery/img_03.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 4) {
					me._gallery_picture.ggText="gallery/img_04.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 5) {
					me._gallery_picture.ggText="gallery/img_05.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 6) {
					me._gallery_picture.ggText="gallery/img_06.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 7) {
					me._gallery_picture.ggText="gallery/img_07.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 8) {
					me._gallery_picture.ggText="gallery/img_08.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 9) {
					me._gallery_picture.ggText="gallery/img_09.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 10) {
					me._gallery_picture.ggText="gallery/img_10.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 11) {
					me._gallery_picture.ggText="gallery/img_11.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 12) {
					me._gallery_picture.ggText="gallery/img_12.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 13) {
					me._gallery_picture.ggText="gallery/img_13.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else if (me._gallery_picture.ggCurrentLogicStateExternalUrl == 14) {
					me._gallery_picture.ggText="gallery/img_14.jpg";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
				else {
					me._gallery_picture.ggText="";
					me._gallery_picture__img.src=me._gallery_picture.ggText;
				}
			}
		}
		this._gallery_picture.ggUpdatePosition=function (useTransition) {
			var parentWidth = me._gallery_picture.clientWidth;
			var parentHeight = me._gallery_picture.clientHeight;
			var aspectRatioDiv = me._gallery_picture.clientWidth / me._gallery_picture.clientHeight;
			var aspectRatioImg = me._gallery_picture__img.naturalWidth / me._gallery_picture__img.naturalHeight;
			if (me._gallery_picture__img.naturalWidth < parentWidth) parentWidth = me._gallery_picture__img.naturalWidth;
			if (me._gallery_picture__img.naturalHeight < parentHeight) parentHeight = me._gallery_picture__img.naturalHeight;
			var currentWidth = me._gallery_picture__img.naturalWidth;
			var currentHeight = me._gallery_picture__img.naturalHeight;
			if (aspectRatioDiv > aspectRatioImg) {
			currentHeight = parentHeight;
			currentWidth = parentHeight * aspectRatioImg;
			me._gallery_picture__img.setAttribute('style','position: absolute; left: 50%; margin-left: -' + currentWidth/2 + 'px; top: 50%; margin-top: -' + currentHeight/2 + 'px;height:' + parentHeight + 'px;-webkit-user-drag:none;pointer-events:none;border-radius:0px;');
			} else {
			currentWidth = parentWidth;
			currentHeight = parentWidth / aspectRatioImg;
			me._gallery_picture__img.setAttribute('style','position: absolute; left: 50%; margin-left: -' + currentWidth/2 + 'px; top: 50%; margin-top: -' + currentHeight/2 + 'px;width:' + parentWidth + 'px;-webkit-user-drag:none;pointer-events:none;border-radius:0px;');
			};
		}
		this.divSkin.appendChild(this._gallery_picture);
		this._gallery_close=document.createElement('div');
		this._gallery_close__img=document.createElement('img');
		this._gallery_close__img.className='ggskin ggskin_svg';
		this._gallery_close__img.setAttribute('src',basePath + 'images/gallery_close.svg');
		this._gallery_close__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this._gallery_close__img['ondragstart']=function() { return false; };
		this._gallery_close.appendChild(this._gallery_close__img);
		this._gallery_close.ggId="gallery_close";
		this._gallery_close.ggLeft=-74;
		this._gallery_close.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._gallery_close.ggVisible=false;
		this._gallery_close.className='ggskin ggskin_svg ';
		this._gallery_close.ggType='svg';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 36px;';
		hs+='left : -74px;';
		hs+='position : absolute;';
		hs+='top : 15px;';
		hs+='visibility : hidden;';
		hs+='width : 37px;';
		hs+='pointer-events:auto;';
		this._gallery_close.setAttribute('style',hs);
		this._gallery_close.style[domTransform + 'Origin']='50% 50%';
		me._gallery_close.ggIsActive=function() {
			return false;
		}
		me._gallery_close.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._gallery_close.onclick=function (e) {
			ggSkinVars['gallery_show_hide'] = false;
			ggSkinVars['gallery_pictures'] = Number("0");
		}
		me._gallery_close.ggCurrentLogicStateVisible = -1;
		this._gallery_close.ggUpdateConditionTimer=function () {
			var newLogicStateVisible;
			if (
				(ggSkinVars['gallery_show_hide'] == true)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._gallery_close.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._gallery_close.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._gallery_close.style[domTransition]='';
				if (me._gallery_close.ggCurrentLogicStateVisible == 0) {
					me._gallery_close.style.visibility=(Number(me._gallery_close.style.opacity)>0||!me._gallery_close.style.opacity)?'inherit':'hidden';
					me._gallery_close.ggVisible=true;
				}
				else {
					me._gallery_close.style.visibility="hidden";
					me._gallery_close.ggVisible=false;
				}
			}
		}
		this._gallery_close.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w) + 'px';
			}
		}
		this.divSkin.appendChild(this._gallery_close);
		this._timer_1=document.createElement('div');
		this._timer_1.ggTimestamp=this.ggCurrentTime;
		this._timer_1.ggLastIsActive=true;
		this._timer_1.ggTimeout=250;
		this._timer_1.ggId="Timer 1";
		this._timer_1.ggLeft=-132;
		this._timer_1.ggTop=-57;
		this._timer_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._timer_1.ggVisible=false;
		this._timer_1.className='ggskin ggskin_timer ';
		this._timer_1.ggType='timer';
		hs ='';
		hs+='height : 20px;';
		hs+='left : -132px;';
		hs+='position : absolute;';
		hs+='top : -57px;';
		hs+='visibility : hidden;';
		hs+='width : 100px;';
		hs+='pointer-events:none;';
		this._timer_1.setAttribute('style',hs);
		this._timer_1.style[domTransform + 'Origin']='50% 50%';
		me._timer_1.ggIsActive=function() {
			return (me._timer_1.ggTimestamp==0 ? false : (Math.floor((me.ggCurrentTime - me._timer_1.ggTimestamp) / me._timer_1.ggTimeout) % 2 == 0));
		}
		me._timer_1.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._timer_1.ggActivate=function () {
			ggSkinVars['ht_ani'] = !ggSkinVars['ht_ani'];
		}
		this._timer_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.divSkin.appendChild(this._timer_1);
		this._opscreen_tint=document.createElement('div');
		this._opscreen_tint.ggId="op-screen_tint";
		this._opscreen_tint.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._opscreen_tint.ggVisible=false;
		this._opscreen_tint.className='ggskin ggskin_rectangle ';
		this._opscreen_tint.ggType='rectangle';
		hs ='';
		hs+='background : rgba(255,255,255,0.784314);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 100%;';
		hs+='left : 0%;';
		hs+='position : absolute;';
		hs+='top : 0%;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:auto;';
		this._opscreen_tint.setAttribute('style',hs);
		this._opscreen_tint.style[domTransform + 'Origin']='50% 50%';
		me._opscreen_tint.ggIsActive=function() {
			return false;
		}
		me._opscreen_tint.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._opscreen_tint.onclick=function (e) {
			me._op_close.style[domTransition]='none';
			me._op_close.style.visibility='hidden';
			me._op_close.ggVisible=false;
			me._optext_1.style[domTransition]='none';
			me._optext_1.style.visibility='hidden';
			me._optext_1.ggVisible=false;
			me._opscreen_tint.style[domTransition]='none';
			me._opscreen_tint.style.visibility='hidden';
			me._opscreen_tint.ggVisible=false;
		}
		this._opscreen_tint.ggUpdatePosition=function (useTransition) {
		}
		this.divSkin.appendChild(this._opscreen_tint);
		this._optext_1=document.createElement('div');
		this._optext_1__text=document.createElement('div');
		this._optext_1.className='ggskin ggskin_textdiv';
		this._optext_1.ggTextDiv=this._optext_1__text;
		this._optext_1.ggId="op-Text 1";
		this._optext_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._optext_1.ggVisible=false;
		this._optext_1.className='ggskin ggskin_text ';
		this._optext_1.ggType='text';
		hs ='';
		hs+='height : 80%;';
		hs+='left : 9.53%;';
		hs+='position : absolute;';
		hs+='top : 11.04%;';
		hs+='visibility : hidden;';
		hs+='width : 80%;';
		hs+='pointer-events:auto;';
		this._optext_1.setAttribute('style',hs);
		this._optext_1.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 100%;';
		hs+='height: 100%;';
		hs+='background: #ffffff;';
		hs+='background: rgba(255,255,255,0);';
		hs+='border: 0px solid #000000;';
		hs+='color: #000000;';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		this._optext_1__text.setAttribute('style',hs);
		this._optext_1__text.innerHTML=" \u0441 3 \u0430\u0432\u0433\u0443\u0441\u0442\u0430  \u043f\u043e 3 \u0441\u0435\u043d\u0442\u044f\u0431\u0440\u044f 2016 \u0432 \u041d\u0430\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u043e\u043c \u0446\u0435\u043d\u0442\u0440\u0435 \u0441\u043e\u0432\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u0445 \u0438\u0441\u043a\u0443\u0441\u0441\u0442\u0432 \u0420\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0438 \u0411\u0435\u043b\u0430\u0440\u0443\u0441\u044c (\u0443\u043b. \u041d\u0435\u043a\u0440\u0430\u0441\u043e\u0432\u0430, 3) \u043f\u0440\u043e\u0448\u0435\u043b \u0432\u044b\u0441\u0442\u0430\u0432\u043e\u0447\u043d\u044b\u0439 \u043f\u0440\u043e\u0435\u043a\u0442 \u201c\u041e\u0434\u0438\u0441\u0441\u0435\u044f\u201d, \u0432\u043a\u043b\u044e\u0447\u0430\u044e\u0449\u0438\u0439 \u0440\u0430\u0431\u043e\u0442\u044b \u0438\u0437\u0432\u0435\u0441\u0442\u043d\u044b\u0445 \u0431\u0435\u043b\u043e\u0440\u0443\u0441\u0441\u043a\u0438\u0445 \u0444\u043e\u0442\u043e\u0445\u0443\u0434\u043e\u0436\u043d\u0438\u043a\u043e\u0432 \u0410\u043b\u044c\u0431\u0435\u0440\u0442\u0430 \u0426\u0435\u0445\u0430\u043d\u043e\u0432\u0438\u0447\u0430, \u041c\u0430\u0440\u0438\u043d\u044b \u0411\u0430\u0442\u044e\u043a\u043e\u0432\u043e\u0439, \u041c\u0430\u0440\u0438\u0438 \u0411\u043e\u043d\u0435, \u0410\u043d\u0434\u0440\u0435\u044f \u0412\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u0441\u043a\u043e\u0433\u043e, \u0415\u043a\u0430\u0442\u0435\u0440\u0438\u043d\u044b \u0413\u0443\u0440\u0442\u043e\u0432\u043e\u0439, \u041d\u0430\u0442\u0430\u043b\u0438\u0438 \u0415\u0432\u043c\u0435\u043d\u0435\u043d\u043a\u043e, \u041e\u043b\u044c\u0433\u0438 \u0421\u0435\u0440\u0433\u0435\u0435\u0432\u043e\u0439 \u0438 \u0444\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0430 \u0438\u0437 \u0421\u043c\u043e\u043b\u0435\u043d\u0441\u043a\u0430 \u0410\u043d\u043d\u044b \u0417\u0438\u043c\u0438\u043d\u043e\u0439. <br\/><br\/>\u201c\u041e\u0434\u0438\u0441\u0441\u0435\u044f\u201d \u2013 \u0440\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442 \u043c\u043d\u043e\u0433\u043e\u043b\u0435\u0442\u043d\u0435\u0439 \u0440\u0430\u0431\u043e\u0442\u044b \u0433\u0440\u0443\u043f\u043f\u044b \u0444\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u043e\u0432, \u043f\u0440\u0435\u0434\u0441\u0442\u0430\u0432\u0438\u0442\u0435\u043b\u0435\u0439 \u041c\u0438\u043d\u0441\u043a\u043e\u0439 \u0448\u043a\u043e\u043b\u044b \u0444\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438, \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u044e\u0449\u0438\u0445 \u0432 \u0441\u0432\u043e\u0435\u0439 \u0445\u0443\u0434\u043e\u0436\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0439 \u043f\u0440\u0430\u043a\u0442\u0438\u043a\u0435 \u043c\u0435\u0442\u043e\u0434\u044b \u043a\u043b\u0430\u0441\u0441\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u0447\u0435\u0440\u043d\u043e-\u0431\u0435\u043b\u043e\u0439 \u0444\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438 \u0438 \u0440\u0430\u0437\u043d\u043e\u043e\u0431\u0440\u0430\u0437\u043d\u044b\u0435 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u044b \u043f\u0435\u0447\u0430\u0442\u0438, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u043f\u0440\u0438\u043d\u044f\u0442\u043e \u043d\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0430\u043b\u044c\u0442\u0435\u0440\u043d\u0430\u0442\u0438\u0432\u043d\u044b\u043c\u0438. \u042d\u0442\u043e \u043f\u0440\u043e\u0435\u043a\u0442-\u043f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u0435 \u0432\u043e \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u0438 \u043f\u0440\u043e\u0441\u0442\u0440\u0430\u043d\u0441\u0442\u0432\u0435, \u043e\u0441\u043d\u043e\u0432\u0430\u043d\u043d\u044b\u0439 \u043d\u0430 \u0432\u0435\u0441\u044c\u043c\u0430 \u0432\u043e\u043b\u044c\u043d\u043e\u0439 \u0438\u043d\u0442\u0435\u0440\u043f\u0440\u0435\u0442\u0430\u0446\u0438\u0438 \u043c\u043e\u0442\u0438\u0432\u043e\u0432 \u0438 \u043e\u0431\u0440\u0430\u0437\u043e\u0432 \u044d\u043f\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u043f\u043e\u044d\u043c\u044b \u0413\u043e\u043c\u0435\u0440\u0430 \u0438 \u201c\u0423\u043b\u0438\u0441\u0441\u0430\u201d \u0414\u0436. \u0414\u0436\u043e\u0439\u0441\u0430. <br\/><br\/>\u0422\u0435\u043c\u044b \u0443\u0445\u043e\u0434\u0430 \u0438 \u0432\u043e\u0437\u0432\u0440\u0430\u0449\u0435\u043d\u0438\u044f, \u043f\u043e\u0442\u0435\u0440\u0438 \u0438 \u043e\u0431\u0440\u0435\u0442\u0435\u043d\u0438\u044f, \u0437\u0430\u0445\u0432\u0430\u0442\u044b\u0432\u0430\u044e\u0449\u0435\u0433\u043e \u043f\u043b\u0430\u0432\u0430\u043d\u0438\u044f, \u0434\u043e\u0431\u0440\u043e\u0432\u043e\u043b\u044c\u043d\u043e\u0433\u043e \u0438\u043b\u0438 \u0432\u044b\u043d\u0443\u0436\u0434\u0435\u043d\u043d\u043e\u0433\u043e, \u0432\u043b\u0435\u043a\u0443\u0449\u0435\u0433\u043e, \u043d\u043e \u0438 \u043e\u043f\u0430\u0441\u043d\u043e\u0433\u043e \u0441\u0442\u043e\u043b\u043a\u043d\u043e\u0432\u0435\u043d\u0438\u044f \u0441 \u043d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u044b\u043c, \u0434\u0438\u043d\u0430\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0433\u043e \u043f\u0440\u043e\u0442\u0438\u0432\u043e\u0431\u043e\u0440\u0441\u0442\u0432\u0430 \u0440\u0430\u0437\u0443\u043c\u0430 \u0438 \u0441\u0442\u0438\u0445\u0438\u0438, \u0440\u0430\u0432\u043d\u043e\u0437\u043d\u0430\u0447\u043d\u043e\u0433\u043e \u043f\u0440\u0438\u0441\u0443\u0442\u0441\u0442\u0432\u0438\u044f \u0432 \u0436\u0438\u0437\u043d\u0438 \u0440\u0430\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0438 \u0438\u0440\u0440\u0430\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u043d\u0430\u0447\u0430\u043b \u0434\u0430\u044e\u0442 \u0434\u043e\u0441\u0442\u0430\u0442\u043e\u0447\u043d\u043e \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430 \u0434\u043b\u044f \u0440\u0430\u0437\u043c\u044b\u0448\u043b\u0435\u043d\u0438\u0439 \u0438 \u043c\u043d\u043e\u0433\u043e\u0437\u043d\u0430\u0447\u043d\u044b\u0445 \u0438\u043d\u0442\u0435\u0440\u043f\u0440\u0435\u0442\u0430\u0446\u0438\u0439 \u0438 \u0442\u0430\u043a \u0438\u043b\u0438 \u0438\u043d\u0430\u0447\u0435 \u043f\u0440\u0438\u0441\u0443\u0442\u0441\u0442\u0432\u0443\u044e\u0442 \u0432 \u043a\u043e\u043d\u0442\u0435\u043a\u0441\u0442\u0435 \u0435\u0432\u0440\u043e\u043f\u0435\u0439\u0441\u043a\u043e\u0439 \u0438 \u043c\u0438\u0440\u043e\u0432\u043e\u0439 \u043a\u0443\u043b\u044c\u0442\u0443\u0440\u044b.<br\/><br\/>\u0412\u043e\u0441\u0435\u043c\u044c \u0430\u0432\u0442\u043e\u0440\u043e\u0432 \u2013 \u0432\u043e\u0441\u0435\u043c\u044c \u043f\u0440\u043e\u0447\u0442\u0435\u043d\u0438\u0439, \u0432\u043e\u0441\u0435\u043c\u044c \u0432\u044b\u0431\u0440\u0430\u043d\u043d\u044b\u0445 \u043f\u043e \u043d\u0435\u043a\u043e\u043c\u0443 \u0432\u043d\u0443\u0442\u0440\u0435\u043d\u043d\u0435\u043c\u0443 \u0438\u043c\u043f\u0443\u043b\u044c\u0441\u0443 \u043f\u0443\u0442\u0435\u0439, \u0434\u0438\u043a\u0442\u0443\u044e\u0449\u0438\u0445 \u0442\u043e \u0438\u043b\u0438 \u0438\u043d\u043e\u0435 \u0441\u0442\u0438\u043b\u0438\u0441\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u0435 \u0438 \u0441\u043c\u044b\u0441\u043b\u043e\u0432\u043e\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u0435. \u041e\u0441\u043a\u043e\u043b\u043a\u0438 \u0430\u043d\u0442\u0438\u0447\u043d\u043e\u0441\u0442\u0438, \u0432\u0435\u0447\u043d\u044b\u0435 \u044d\u043b\u0435\u043c\u0435\u043d\u0442\u044b \u043f\u0440\u0438\u0440\u043e\u0434\u043d\u043e\u0433\u043e \u0431\u044b\u0442\u0438\u044f \u043f\u0435\u0440\u0435\u043f\u043b\u0435\u0442\u0430\u044e\u0442\u0441\u044f \u0441 \u043e\u0431\u0440\u0430\u0437\u0430\u043c\u0438 \u0438 \u0437\u043d\u0430\u043a\u0430\u043c\u0438 \u0441\u043e\u0432\u0440\u0435\u043c\u0435\u043d\u043d\u043e\u0441\u0442\u0438, \u0434\u0430\u0432\u043d\u0435\u0433\u043e \u0438 \u043d\u0435 \u043e\u0447\u0435\u043d\u044c \u0434\u0430\u0432\u043d\u0435\u0433\u043e \u043f\u0440\u043e\u0448\u043b\u043e\u0433\u043e, \u043e\u0442\u043f\u0435\u0447\u0430\u0442\u044b\u0432\u0430\u044e\u0442\u0441\u044f \u0432 \u043f\u0430\u043c\u044f\u0442\u0438 \u0438 \u0432\u043e\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0438, \u0442\u043e\u043d\u0443\u0442 \u043d\u0430 \u043a\u0430\u043a\u043e\u0435-\u0442\u043e \u0432\u0440\u0435\u043c\u044f \u0432 \u043f\u043e\u0432\u0441\u0435\u0434\u043d\u0435\u0432\u043d\u043e\u0441\u0442\u0438, \u043f\u0440\u043e\u043f\u0430\u0434\u0430\u044e\u0442 \u0438\u0437 \u043e\u0431\u043b\u0430\u0441\u0442\u0438 \u0441\u043e\u0437\u043d\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0433\u043e, \u0437\u0430\u0442\u0435\u043c \u0432\u043d\u043e\u0432\u044c \u0432\u0441\u043f\u043b\u044b\u0432\u0430\u044e\u0442 \u0438\u0437 \u043f\u043e\u0434\u0441\u043e\u0437\u043d\u0430\u043d\u0438\u044f, \u0438, \u0441\u043e\u0435\u0434\u0438\u043d\u044f\u044f\u0441\u044c \u043f\u0440\u0438\u0447\u0443\u0434\u043b\u0438\u0432\u043e, \u043f\u043e\u0440\u043e\u0436\u0434\u0430\u044e\u0442 \u043d\u043e\u0432\u044b\u0435 \u043d\u0435\u043e\u0436\u0438\u0434\u0430\u043d\u043d\u044b\u0435 \u0430\u0441\u0441\u043e\u0446\u0438\u0430\u0446\u0438\u0438 \u0438 \u043e\u0431\u0440\u0430\u0437\u044b.<br\/><br\/>\u041c\u0438\u0440 \u0433\u043e\u043c\u0435\u0440\u043e\u0432\u0441\u043a\u0438\u0445 \u043f\u043e\u044d\u043c \u2013 \u043c\u0438\u0440 \u043e\u0441\u0442\u0440\u043e\u0432\u043e\u0432, \u0440\u0430\u0437\u0434\u0435\u043b\u0435\u043d\u043d\u044b\u0445 \u043c\u043e\u0440\u0435\u043c, \u043e\u0431\u043e\u0441\u043e\u0431\u043b\u0435\u043d\u043d\u044b\u0445 \u043c\u0438\u043d\u0438-\u0432\u0441\u0435\u043b\u0435\u043d\u043d\u044b\u0445, \u043d\u0435 \u043f\u043e\u0445\u043e\u0436\u0438\u0445 \u043e\u0434\u043d\u0430 \u043d\u0430 \u0434\u0440\u0443\u0433\u0443\u044e, \u043d\u0430\u0441\u0435\u043b\u0435\u043d\u043d\u044b\u0445 \u043b\u044e\u0434\u044c\u043c\u0438 \u0438 \u043d\u0435\u043e\u0431\u044b\u0447\u043d\u044b\u043c\u0438 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0430\u043c\u0438, \u0443\u043f\u0440\u0430\u0432\u043b\u044f\u0435\u043c\u044b\u0445 \u0441\u0432\u043e\u0438\u043c\u0438 \u0432\u043b\u0430\u0441\u0442\u0438\u0442\u0435\u043b\u044f\u043c\u0438 \u0438 \u0437\u0430\u043a\u043e\u043d\u0430\u043c\u0438. \u041e\u043d\u0438 \u0440\u0430\u0437\u0434\u0435\u043b\u0435\u043d\u044b, \u043d\u043e \u0432\u0441\u0435 \u0436\u0435 \u0434\u043e\u0441\u0442\u0438\u0436\u0438\u043c\u044b, \u0445\u043e\u0442\u044f \u0438 \u043d\u0435 \u0432\u0441\u0435\u0433\u0434\u0430 \u043b\u0435\u0433\u043a\u043e, \u0438 \u043f\u043b\u0430\u0432\u0430\u043d\u0438\u0435 \u043c\u043e\u0436\u0435\u0442 \u043e\u0442\u043d\u044f\u0442\u044c \u043c\u0435\u0441\u044f\u0446\u044b \u0438 \u0433\u043e\u0434\u044b. \u0412 \u044d\u0442\u043e\u043c \u0441\u043c\u044b\u0441\u043b\u0435 \u043a\u0430\u0436\u0434\u044b\u0439 \u0430\u0432\u0442\u043e\u0440 \u043f\u0440\u043e\u0435\u043a\u0442\u0430 \u043f\u0440\u043e\u0445\u043e\u0434\u0438\u0442 \u0441\u0432\u043e\u0439 \u043f\u0443\u0442\u044c \u043d\u0430\u043f\u0440\u044f\u0436\u0435\u043d\u043d\u044b\u0445 \u043f\u043e\u0438\u0441\u043a\u043e\u0432 \u0432\u043e \u0438\u043c\u044f \u043e\u0431\u0440\u0435\u0442\u0435\u043d\u0438\u044f \u0441\u0432\u043e\u0435\u0439 \u201c\u0418\u0442\u0430\u043a\u0438\u201d \u2013 \u0441\u043e\u0431\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0433\u043e \u043c\u0438\u0440\u0430, \u043f\u0443\u0441\u0442\u044c \u043d\u0435\u0431\u043e\u043b\u044c\u0448\u043e\u0433\u043e, \u043d\u043e \u043f\u043e\u0441\u0442\u0440\u043e\u0435\u043d\u043d\u043e\u0433\u043e \u0432 \u0441\u043e\u043e\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0438\u0438 \u0441 \u043b\u0438\u0447\u043d\u044b\u043c\u0438 \u0446\u0435\u043d\u043d\u043e\u0441\u0442\u044f\u043c\u0438 \u0438 \u044d\u0441\u0442\u0435\u0442\u0438\u0447\u0435\u0441\u043a\u0438\u043c\u0438 \u043f\u0440\u0435\u0434\u043f\u043e\u0447\u0442\u0435\u043d\u0438\u044f\u043c\u0438.";
		this._optext_1.appendChild(this._optext_1__text);
		me._optext_1.ggIsActive=function() {
			return false;
		}
		me._optext_1.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._optext_1.onclick=function (e) {
			me._op_close.style[domTransition]='none';
			me._op_close.style.visibility='hidden';
			me._op_close.ggVisible=false;
			me._optext_1.style[domTransition]='none';
			me._optext_1.style.visibility='hidden';
			me._optext_1.ggVisible=false;
			me._opscreen_tint.style[domTransition]='none';
			me._opscreen_tint.style.visibility='hidden';
			me._opscreen_tint.ggVisible=false;
		}
		this._optext_1.ggUpdatePosition=function (useTransition) {
		}
		this.divSkin.appendChild(this._optext_1);
		this._op_close=document.createElement('div');
		this._op_close__img=document.createElement('img');
		this._op_close__img.className='ggskin ggskin_svg';
		this._op_close__img.setAttribute('src',basePath + 'images/op_close.svg');
		this._op_close__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this._op_close__img['ondragstart']=function() { return false; };
		this._op_close.appendChild(this._op_close__img);
		this._op_close.ggId="op_close";
		this._op_close.ggLeft=-70;
		this._op_close.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._op_close.ggVisible=false;
		this._op_close.className='ggskin ggskin_svg ';
		this._op_close.ggType='svg';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 36px;';
		hs+='left : -70px;';
		hs+='position : absolute;';
		hs+='top : 13px;';
		hs+='visibility : hidden;';
		hs+='width : 37px;';
		hs+='pointer-events:auto;';
		this._op_close.setAttribute('style',hs);
		this._op_close.style[domTransform + 'Origin']='50% 50%';
		me._op_close.ggIsActive=function() {
			return false;
		}
		me._op_close.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._op_close.onclick=function (e) {
			me._op_close.style[domTransition]='none';
			me._op_close.style.visibility='hidden';
			me._op_close.ggVisible=false;
			me._optext_1.style[domTransition]='none';
			me._optext_1.style.visibility='hidden';
			me._optext_1.ggVisible=false;
			me._opscreen_tint.style[domTransition]='none';
			me._opscreen_tint.style.visibility='hidden';
			me._opscreen_tint.ggVisible=false;
		}
		this._op_close.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w) + 'px';
			}
		}
		this.divSkin.appendChild(this._op_close);
		this._screentint_image=document.createElement('div');
		this._screentint_image.ggId="screentint_image";
		this._screentint_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._screentint_image.ggVisible=false;
		this._screentint_image.className='ggskin ggskin_rectangle ';
		this._screentint_image.ggType='rectangle';
		hs ='';
		hs+='background : rgba(0,0,0,0.392157);';
		hs+='border : 1px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 100%;';
		hs+='left : 0.02%;';
		hs+='position : absolute;';
		hs+='top : 0.05%;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:auto;';
		this._screentint_image.setAttribute('style',hs);
		this._screentint_image.style[domTransform + 'Origin']='50% 50%';
		me._screentint_image.ggIsActive=function() {
			return false;
		}
		me._screentint_image.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._screentint_image.onclick=function (e) {
			me._screentint_image.style[domTransition]='none';
			me._screentint_image.style.visibility='hidden';
			me._screentint_image.ggVisible=false;
			me._popup_image.ggText="";
			me._popup_image__img.src=me._popup_image.ggText;
			me._image_popup.style[domTransition]='none';
			me._image_popup.style.visibility='hidden';
			me._image_popup.ggVisible=false;
			me._popup_image.style[domTransition]='none';
			me._popup_image.style.visibility='hidden';
			me._popup_image.ggVisible=false;
		}
		this._screentint_image.ggUpdatePosition=function (useTransition) {
		}
		this.divSkin.appendChild(this._screentint_image);
		this._image_popup=document.createElement('div');
		this._image_popup.ggId="image_popup";
		this._image_popup.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._image_popup.ggVisible=false;
		this._image_popup.className='ggskin ggskin_container ';
		this._image_popup.ggType='container';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 80%;';
		hs+='left : 10.44%;';
		hs+='position : absolute;';
		hs+='top : 8.98%;';
		hs+='visibility : hidden;';
		hs+='width : 80%;';
		hs+='pointer-events:none;';
		this._image_popup.setAttribute('style',hs);
		this._image_popup.style[domTransform + 'Origin']='50% 50%';
		me._image_popup.ggIsActive=function() {
			return false;
		}
		me._image_popup.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._image_popup.onclick=function (e) {
			me._image_popup.style[domTransition]='none';
			me._image_popup.style.visibility='hidden';
			me._image_popup.ggVisible=false;
			me._screentint_image.style[domTransition]='none';
			me._screentint_image.style.visibility='hidden';
			me._screentint_image.ggVisible=false;
			me._image_popup.style[domTransition]='none';
			me._image_popup.style.visibility='hidden';
			me._image_popup.ggVisible=false;
			me._popup_image.style[domTransition]='none';
			me._popup_image.style.visibility='hidden';
			me._popup_image.ggVisible=false;
			me._popup_image.ggText="";
			me._popup_image__img.src=me._popup_image.ggText;
		}
		this._image_popup.ggUpdatePosition=function (useTransition) {
		}
		this._loading_image=document.createElement('div');
		this._loading_image__img=document.createElement('img');
		this._loading_image__img.className='ggskin ggskin_svg';
		this._loading_image__img.setAttribute('src',basePath + 'images/loading_image.svg');
		this._loading_image__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this._loading_image__img['ondragstart']=function() { return false; };
		this._loading_image.appendChild(this._loading_image__img);
		this._loading_image.ggId="loading_image";
		this._loading_image.ggLeft=-20;
		this._loading_image.ggTop=-20;
		this._loading_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading_image.ggVisible=true;
		this._loading_image.className='ggskin ggskin_svg ';
		this._loading_image.ggType='svg';
		hs ='';
		hs+='height : 40px;';
		hs+='left : -20px;';
		hs+='position : absolute;';
		hs+='top : -20px;';
		hs+='visibility : inherit;';
		hs+='width : 40px;';
		hs+='pointer-events:auto;';
		this._loading_image.setAttribute('style',hs);
		this._loading_image.style[domTransform + 'Origin']='50% 50%';
		me._loading_image.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._loading_image.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._loading_image.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h/2) + 'px';
			}
		}
		this._image_popup.appendChild(this._loading_image);
		this._popup_image=document.createElement('div');
		this._popup_image__img=document.createElement('img');
		this._popup_image__img.className='ggskin ggskin_external';
		this._popup_image__img.onload=function() {me._popup_image.ggUpdatePosition();}
		this._popup_image__img.setAttribute('src',basePath + '');
		this._popup_image__img['ondragstart']=function() { return false; };
		hs ='';
		this._popup_image.appendChild(this._popup_image__img);
		this._popup_image.ggId="popup_image";
		this._popup_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._popup_image.ggVisible=false;
		this._popup_image.className='ggskin ggskin_external ';
		this._popup_image.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 100%;';
		hs+='left : -0.15%;';
		hs+='position : absolute;';
		hs+='top : 0%;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:auto;';
		this._popup_image.setAttribute('style',hs);
		this._popup_image.style[domTransform + 'Origin']='50% 50%';
		me._popup_image.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._popup_image.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._popup_image.ggUpdatePosition=function (useTransition) {
			var parentWidth = me._popup_image.clientWidth;
			var parentHeight = me._popup_image.clientHeight;
			var aspectRatioDiv = me._popup_image.clientWidth / me._popup_image.clientHeight;
			var aspectRatioImg = me._popup_image__img.naturalWidth / me._popup_image__img.naturalHeight;
			if (me._popup_image__img.naturalWidth < parentWidth) parentWidth = me._popup_image__img.naturalWidth;
			if (me._popup_image__img.naturalHeight < parentHeight) parentHeight = me._popup_image__img.naturalHeight;
			var currentWidth = me._popup_image__img.naturalWidth;
			var currentHeight = me._popup_image__img.naturalHeight;
			if (aspectRatioDiv > aspectRatioImg) {
			currentHeight = parentHeight;
			currentWidth = parentHeight * aspectRatioImg;
			me._popup_image__img.setAttribute('style','position: absolute; left: 50%; margin-left: -' + currentWidth/2 + 'px; top: 50%; margin-top: -' + currentHeight/2 + 'px;height:' + parentHeight + 'px;-webkit-user-drag:none;pointer-events:none;border-radius:0px;');
			} else {
			currentWidth = parentWidth;
			currentHeight = parentWidth / aspectRatioImg;
			me._popup_image__img.setAttribute('style','position: absolute; left: 50%; margin-left: -' + currentWidth/2 + 'px; top: 50%; margin-top: -' + currentHeight/2 + 'px;width:' + parentWidth + 'px;-webkit-user-drag:none;pointer-events:none;border-radius:0px;');
			};
		}
		this._image_popup.appendChild(this._popup_image);
		this.divSkin.appendChild(this._image_popup);
		this._scrollarea_1=document.createElement('div');
		this._scrollarea_1__content=document.createElement('div');
		this._scrollarea_1.ggContent=this._scrollarea_1__content;
		this._scrollarea_1.appendChild(this._scrollarea_1__content);
		hs ='';
		hs+='background : rgba(255,255,255,0);';
		hs+='left : 0px;';
		hs+='overflow : visible;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		this._scrollarea_1__content.setAttribute('style',hs);
		this._scrollarea_1.ggId="Scrollarea 1";
		this._scrollarea_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._scrollarea_1.ggVisible=true;
		this._scrollarea_1.className='ggskin ggskin_scrollarea ';
		this._scrollarea_1.ggType='scrollarea';
		hs ='';
		hs+='-webkit-overflow-scrolling : touch;';
		hs+='background : rgba(255,255,255,0);';
		hs+='border : 1px solid rgba(0, 0, 0, 0);';
		hs+='height : 86.9565%;';
		hs+='left : 5px;';
		hs+='overflow-x : hidden;';
		hs+='overflow-y : auto;';
		hs+='position : absolute;';
		hs+='top : 76px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		this._scrollarea_1.setAttribute('style',hs);
		this._scrollarea_1.style[domTransform + 'Origin']='50% 50%';
		me._scrollarea_1.ggIsActive=function() {
			return false;
		}
		me._scrollarea_1.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me._scrollarea_1.ggCurrentLogicStatePosition = -1;
		this._scrollarea_1.ggUpdateConditionResize=function () {
			var newLogicStatePosition;
			if (
				(me.player.getViewerSize().height < 460)
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._scrollarea_1.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._scrollarea_1.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._scrollarea_1.style[domTransition]='left none, top none';
				if (me._scrollarea_1.ggCurrentLogicStatePosition == 0) {
					me._scrollarea_1.style.left='-360px';
					me._scrollarea_1.style.top='76px';
				}
				else {
					me._scrollarea_1.style.left='5px';
					me._scrollarea_1.style.top='76px';
				}
			}
		}
		this._scrollarea_1.ggUpdatePosition=function (useTransition) {
			{
				this.ggContent.style.left = '0px';
				this.ggContent.style.marginLeft = '0px';
				this.ggContent.style.top = '0px';
				this.ggContent.style.marginTop = '0px';
			}
			me._scrollarea_1.ggUpdateConditionResize();
		}
		this._dropdown_cloner=document.createElement('div');
		this._dropdown_cloner.ggNumRepeat = 1;
		this._dropdown_cloner.ggWidth = 184;
		this._dropdown_cloner.ggHeight = 58;
		this._dropdown_cloner.ggUpdating = false;
		this._dropdown_cloner.ggFilter = [];
		this._dropdown_cloner.ggUpdate = function(filter) {
			if(me._dropdown_cloner.ggUpdating == true) return;
			me._dropdown_cloner.ggUpdating = true;
			if (typeof filter=='object') {
				me._dropdown_cloner.ggFilter = filter;
			} else {
				filter = me._dropdown_cloner.ggFilter;
			};
			if (me._dropdown_cloner.hasChildNodes() == true) {
				while (me._dropdown_cloner.firstChild) {
					me._dropdown_cloner.removeChild(me._dropdown_cloner.firstChild);
				}
			}
			var tourNodes = me.player.getNodeIds();
			var row = 0;
			var column = 0;
			var numCols = me._dropdown_cloner.ggNumRepeat;
			if (numCols < 1) numCols = 1;
			for (i=0; i < tourNodes.length; i++) {
				var nodeId = tourNodes[i];
				var passed = false;
				if (filter.length > 0) {
					var nodeData = me.player.getNodeUserdata(nodeId);
					for (j=0; j < filter.length; j++) {
						if (nodeData['tags'].indexOf(filter[j]) != -1) passed = true;
					}
				}
				else passed = true;
				if (passed) {
					me._dropdown_cloner__node = document.createElement('div');
					me._dropdown_cloner.appendChild(me._dropdown_cloner__node);
					me._dropdown_cloner__node.setAttribute('style','position: absolute; top: ' + (row * me._dropdown_cloner.ggHeight) + 'px; left:' + (column * me._dropdown_cloner.ggWidth) + 'px; height: ' + me._dropdown_cloner.ggHeight + 'px; width: ' + me._dropdown_cloner.ggWidth + 'px; overflow: hidden;');
					var inst = new SkinCloner_dropdown_cloner_Class(nodeId, me);
					me._dropdown_cloner__node.appendChild(inst.__div);
					me._dropdown_cloner__node.ggObj=inst;
					me.updateSize(inst.__div);
					column++;
					if (column >= numCols) {
						column = 0;
						row++;
					}
				}
			}
			me._dropdown_cloner.ggClonerCallChildFunctions('ggUpdateConditionTimer');
			me._dropdown_cloner.ggUpdating = false;
		}
		this._dropdown_cloner.ggClonerCallChildFunctions = function(functionname){
			var stack = [];
			var i;
			for(i=0; i<me._dropdown_cloner.childNodes.length; i++) {
				stack.push(me._dropdown_cloner.childNodes[i]);
			}
			while (stack.length > 0) {
				var e = stack.pop();
				if (typeof e[functionname] == 'function')
					e[functionname]();
				if(e.hasChildNodes()) {
					for(i=0; i<e.childNodes.length; i++) {
						stack.push(e.childNodes[i]);
					}
				}
			}
		}
		this._dropdown_cloner.ggTags = [];
		this._dropdown_cloner.ggId="Dropdown Cloner";
		this._dropdown_cloner.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._dropdown_cloner.ggVisible=true;
		this._dropdown_cloner.className='ggskin ggskin_cloner ';
		this._dropdown_cloner.ggType='cloner';
		hs ='';
		hs+='height : 57px;';
		hs+='left : 4px;';
		hs+='overflow : visible;';
		hs+='position : absolute;';
		hs+='top : 6px;';
		hs+='visibility : inherit;';
		hs+='width : 183px;';
		hs+='pointer-events:none;';
		this._dropdown_cloner.setAttribute('style',hs);
		this._dropdown_cloner.style[domTransform + 'Origin']='50% 50%';
		me._dropdown_cloner.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._dropdown_cloner.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._dropdown_cloner.ggUpdatePosition=function (useTransition) {
			var w=me.player.getViewerSize().width;
			var h=me.player.getViewerSize().height
			if ((!me._dropdown_cloner.ggLastSize) || (me._dropdown_cloner.ggLastSize.w!=w) || (me._dropdown_cloner.ggLastSize.h!=h)) {
				me._dropdown_cloner.ggLastSize={ w:w, h:h };
				me._dropdown_cloner.ggUpdate();
			}
		}
		this._scrollarea_1__content.appendChild(this._dropdown_cloner);
		this.divSkin.appendChild(this._scrollarea_1);
		this._loading=document.createElement('div');
		this._loading.ggId="loading";
		this._loading.ggLeft=-41;
		this._loading.ggTop=20;
		this._loading.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loading.ggVisible=true;
		this._loading.className='ggskin ggskin_container ';
		this._loading.ggType='container';
		hs ='';
		hs+='height : 25px;';
		hs+='left : -41px;';
		hs+='position : absolute;';
		hs+='top : 20px;';
		hs+='visibility : inherit;';
		hs+='width : 89px;';
		hs+='pointer-events:none;';
		this._loading.setAttribute('style',hs);
		this._loading.style[domTransform + 'Origin']='50% 50%';
		me._loading.ggIsActive=function() {
			return false;
		}
		me._loading.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._loading.onclick=function (e) {
			me._loading.style[domTransition]='none';
			me._loading.style.visibility='hidden';
			me._loading.ggVisible=false;
		}
		me._loading.ggCurrentLogicStatePosition = -1;
		this._loading.ggUpdateConditionResize=function () {
			var newLogicStatePosition;
			if (
				(me.player.getViewerSize().width < 300)
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._loading.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._loading.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._loading.style[domTransition]='left none, top none';
				if (me._loading.ggCurrentLogicStatePosition == 0) {
					me._loading.ggLeft=-315;
					me._loading.ggTop=-195;
					me._loading.ggUpdatePosition(true);
				}
				else {
					me._loading.ggLeft=-41;
					me._loading.ggTop=20;
					me._loading.ggUpdatePosition(true);
				}
			}
		}
		this._loading.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h/2) + 'px';
			}
			me._loading.ggUpdateConditionResize();
		}
		this._loadingtext=document.createElement('div');
		this._loadingtext__text=document.createElement('div');
		this._loadingtext.className='ggskin ggskin_textdiv';
		this._loadingtext.ggTextDiv=this._loadingtext__text;
		this._loadingtext.ggId="loadingtext";
		this._loadingtext.ggLeft=-19;
		this._loadingtext.ggTop=-3;
		this._loadingtext.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loadingtext.ggVisible=true;
		this._loadingtext.className='ggskin ggskin_text ';
		this._loadingtext.ggType='text';
		hs ='';
		hs+='height : 35px;';
		hs+='left : -19px;';
		hs+='position : absolute;';
		hs+='top : -3px;';
		hs+='visibility : inherit;';
		hs+='width : 71px;';
		hs+='pointer-events:auto;';
		this._loadingtext.setAttribute('style',hs);
		this._loadingtext.style[domTransform + 'Origin']='0% 50%';
		hs ='position:absolute;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(0,0,0,1);';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._loadingtext__text.setAttribute('style',hs);
		this._loadingtext.ggUpdateText=function() {
			var hs=(me.player.getPercentLoaded()*100.0).toFixed(0)+" %";
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._loadingtext.ggUpdateText();
		this._loadingtext.appendChild(this._loadingtext__text);
		me._loadingtext.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._loadingtext.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._loadingtext.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h/2) + 'px';
			}
		}
		this._loading.appendChild(this._loadingtext);
		this._loadingbar=document.createElement('div');
		this._loadingbar.ggId="loadingbar";
		this._loadingbar.ggLeft=-83;
		this._loadingbar.ggTop=-35;
		this._loadingbar.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._loadingbar.ggVisible=true;
		this._loadingbar.className='ggskin ggskin_rectangle ';
		this._loadingbar.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 5px;';
		hs+='border-radius : 5px;';
		hs+='background : #42c8c8;';
		hs+='border : 0px solid #808080;';
		hs+='cursor : default;';
		hs+='height : 10px;';
		hs+='left : -83px;';
		hs+='position : absolute;';
		hs+='top : -35px;';
		hs+='visibility : inherit;';
		hs+='width : 232.71%;';
		hs+='pointer-events:auto;';
		this._loadingbar.setAttribute('style',hs);
		this._loadingbar.style[domTransform + 'Origin']='0% 50%';
		me._loadingbar.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._loadingbar.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._loadingbar.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h/2) + 'px';
			}
		}
		this._loading.appendChild(this._loadingbar);
		this._svg_1=document.createElement('div');
		this._svg_1__img=document.createElement('img');
		this._svg_1__img.className='ggskin ggskin_svg';
		this._svg_1__img.setAttribute('src',basePath + 'images/svg_1.svg');
		this._svg_1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this._svg_1__img['ondragstart']=function() { return false; };
		this._svg_1.appendChild(this._svg_1__img);
		this._svg_1.ggId="Svg 1";
		this._svg_1.ggLeft=-83;
		this._svg_1.ggTop=-139;
		this._svg_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._svg_1.ggVisible=true;
		this._svg_1.className='ggskin ggskin_svg ';
		this._svg_1.ggType='svg';
		hs ='';
		hs+='height : 92px;';
		hs+='left : -83px;';
		hs+='position : absolute;';
		hs+='top : -139px;';
		hs+='visibility : inherit;';
		hs+='width : 196px;';
		hs+='pointer-events:auto;';
		this._svg_1.setAttribute('style',hs);
		this._svg_1.style[domTransform + 'Origin']='50% 50%';
		me._svg_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._svg_1.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._svg_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h/2) + 'px';
			}
		}
		this._loading.appendChild(this._svg_1);
		this.divSkin.appendChild(this._loading);
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
			me._dropdown_cloner.ggUpdate(me._dropdown_cloner.ggTags);
		}
		this.divSkin.ggLoaded=function() {
			me._dropdown_cloner.ggText="list";
			if (me._dropdown_cloner.ggText=='') {
				me._dropdown_cloner.ggUpdate([]);
			} else {
				me._dropdown_cloner.ggUpdate(me._dropdown_cloner.ggText.split(','));
			}
			me.updateSize(me.divSkin);
			me._loading.style[domTransition]='none';
			me._loading.style.visibility='hidden';
			me._loading.ggVisible=false;
		}
		this.divSkin.ggReLoaded=function() {
			me._loading.style[domTransition]='none';
			me._loading.style.visibility=(Number(me._loading.style.opacity)>0||!me._loading.style.opacity)?'inherit':'hidden';
			me._loading.ggVisible=true;
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
		}
		this.divSkin.ggExitFullscreen=function() {
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.ggHotspotCallChildFunctions=function(functionname) {
		var stack = me.player.getCurrentPointHotspots();
		while (stack.length > 0) {
			var e = stack.pop();
			if (typeof e[functionname] == 'function') {
				e[functionname]();
			}
			if(e.hasChildNodes()) {
				for(var i=0; i<e.childNodes.length; i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	this.changeActiveNode=function(id) {
		me.ggUserdata=me.player.userdata;
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		me.ggCurrentTime=new Date().getTime();
		if (me.elementMouseOver['rectangle_5']) {
			me._tooltiplogo.style[domTransition]='none';
			me._tooltiplogo.style.visibility=(Number(me._tooltiplogo.style.opacity)>0||!me._tooltiplogo.style.opacity)?'inherit':'hidden';
			me._tooltiplogo.ggVisible=true;
		}
		me._screen_tint.ggUpdateConditionTimer();
		me._gallery_picture.ggUpdateConditionTimer();
		me._gallery_close.ggUpdateConditionTimer();
		if (me._timer_1.ggLastIsActive!=me._timer_1.ggIsActive()) {
			me._timer_1.ggLastIsActive=me._timer_1.ggIsActive();
			if (me._timer_1.ggLastIsActive) {
				ggSkinVars['ht_ani'] = !ggSkinVars['ht_ani'];
			} else {
			}
		}
		me._dropdown_cloner.ggClonerCallChildFunctions('ggUpdateConditionTimer');
		me._loadingtext.ggUpdateText();
		var hs='';
		if (me._loadingbar.ggParameter) {
			hs+=parameterToTransform(me._loadingbar.ggParameter) + ' ';
		}
		hs+='scale(' + (1 * me.player.getPercentLoaded() + 0) + ',1.0) ';
		me._loadingbar.style[domTransform]=hs;
		me.ggHotspotCallChildFunctions('ggUpdateConditionTimer');
	};
	function SkinHotspotClass(skinObj,hotspot) {
		var me=this;
		var flag=false;
		this.player=skinObj.player;
		this.skin=skinObj;
		this.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		this.ggUserdata=this.skin.player.getNodeUserdata(nodeId);
		this.elementMouseDown=[];
		this.elementMouseOver=[];
		
		this.findElements=function(id,regex) {
			return me.skin.findElements(id,regex);
		}
		
		if (hotspot.skinid=='ht_image') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_image";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 284px;';
			hs+='position : absolute;';
			hs+='top : 312px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.skin._popup_image.ggText=me.player.getBasePath()+""+me.hotspot.url;
				me.skin._popup_image__img.src=me.skin._popup_image.ggText;
				me.skin._popup_image.style[domTransition]='none';
				me.skin._popup_image.style.visibility=(Number(me.skin._popup_image.style.opacity)>0||!me.skin._popup_image.style.opacity)?'inherit':'hidden';
				me.skin._popup_image.ggVisible=true;
				me.skin._image_popup.style[domTransition]='none';
				me.skin._image_popup.style.visibility=(Number(me.skin._image_popup.style.opacity)>0||!me.skin._image_popup.style.opacity)?'inherit':'hidden';
				me.skin._image_popup.ggVisible=true;
				me.skin._screentint_image.style[domTransition]='none';
				me.skin._screentint_image.style.visibility=(Number(me.skin._screentint_image.style.opacity)>0||!me.skin._screentint_image.style.opacity)?'inherit':'hidden';
				me.skin._screentint_image.ggVisible=true;
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image_image=document.createElement('div');
			this._ht_image_image__img=document.createElement('img');
			this._ht_image_image__img.className='ggskin ggskin_svg';
			this._ht_image_image__img.setAttribute('src',basePath + 'images/ht_image_image.svg');
			this._ht_image_image__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._ht_image_image__img['ondragstart']=function() { return false; };
			this._ht_image_image.appendChild(this._ht_image_image__img);
			this._ht_image_image.ggId="ht_image_image";
			this._ht_image_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._ht_image_image.ggVisible=true;
			this._ht_image_image.className='ggskin ggskin_svg ';
			this._ht_image_image.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='opacity : 0.29999;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._ht_image_image.setAttribute('style',hs);
			this._ht_image_image.style[domTransform + 'Origin']='50% 50%';
			me._ht_image_image.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image_image.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image_image.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._ht_image_image);
		} else
		if (hotspot.skinid=='ht_node-old') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node-old";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.elementMouseOver['_div']=true;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.elementMouseOver['_div']=false;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ontouchend=function (e) {
				me.elementMouseOver['_div']=false;
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image0=document.createElement('div');
			this._ht_image0.ggId="ht_image";
			this._ht_image0.ggParameter={ rx:0,ry:0,a:0,sx:1.2,sy:1.2 };
			this._ht_image0.ggVisible=true;
			this._ht_image0.className='ggskin ggskin_container ';
			this._ht_image0.ggType='container';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -15px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:none;';
			this._ht_image0.setAttribute('style',hs);
			this._ht_image0.style[domTransform + 'Origin']='50% 50%';
			this._ht_image0.style[domTransform]=parameterToTransform(this._ht_image0.ggParameter);
			me._ht_image0.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image0.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image0.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_2=document.createElement('div');
			this._rectangle_2.ggId="Rectangle 2";
			this._rectangle_2.ggLeft=-16;
			this._rectangle_2.ggTop=-16;
			this._rectangle_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._rectangle_2.ggVisible=true;
			this._rectangle_2.className='ggskin ggskin_rectangle ';
			this._rectangle_2.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_2.setAttribute('style',hs);
			this._rectangle_2.style[domTransform + 'Origin']='50% 50%';
			me._rectangle_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_2.ggCurrentLogicStateScaling = -1;
			me._rectangle_2.ggCurrentLogicStateAlpha = -1;
			this._rectangle_2.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_2.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_2.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_2.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2.ggCurrentLogicStateScaling == 0) {
						me._rectangle_2.ggParameter.sx = 0.5;
						me._rectangle_2.ggParameter.sy = 0.5;
						me._rectangle_2.style[domTransform]=parameterToTransform(me._rectangle_2.ggParameter);
					}
					else {
						me._rectangle_2.ggParameter.sx = 1;
						me._rectangle_2.ggParameter.sy = 1;
						me._rectangle_2.style[domTransform]=parameterToTransform(me._rectangle_2.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_2.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_2.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_2.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_2.style.visibility=me._rectangle_2.ggVisible?'inherit':'hidden';
						me._rectangle_2.style.opacity=1;
					}
					else {
						me._rectangle_2.style.visibility=me._rectangle_2.ggVisible?'inherit':'hidden';
						me._rectangle_2.style.opacity=1;
					}
				}
			}
			this._rectangle_2.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg2=document.createElement('div');
			this._svg2__img=document.createElement('img');
			this._svg2__img.className='ggskin ggskin_svg';
			this._svg2__img.setAttribute('src',basePath + 'images/svg2.svg');
			this._svg2__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg2__img['ondragstart']=function() { return false; };
			this._svg2.appendChild(this._svg2__img);
			this._svg2.ggId="Svg2";
			this._svg2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg2.ggVisible=true;
			this._svg2.className='ggskin ggskin_svg ';
			this._svg2.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._svg2.setAttribute('style',hs);
			this._svg2.style[domTransform + 'Origin']='50% 50%';
			me._svg2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg2.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_2.appendChild(this._svg2);
			this._ht_image0.appendChild(this._rectangle_2);
			this._rectangle_1=document.createElement('div');
			this._rectangle_1.ggId="Rectangle 1";
			this._rectangle_1.ggLeft=-16;
			this._rectangle_1.ggTop=-16;
			this._rectangle_1.ggParameter={ rx:0,ry:0,a:0,sx:0.5,sy:0.5 };
			this._rectangle_1.ggVisible=true;
			this._rectangle_1.className='ggskin ggskin_rectangle ';
			this._rectangle_1.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_1.setAttribute('style',hs);
			this._rectangle_1.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_1.style[domTransform]=parameterToTransform(this._rectangle_1.ggParameter);
			me._rectangle_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_1.ggCurrentLogicStateScaling = -1;
			me._rectangle_1.ggCurrentLogicStateAlpha = -1;
			this._rectangle_1.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_1.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_1.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_1.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1.ggCurrentLogicStateScaling == 0) {
						me._rectangle_1.ggParameter.sx = 1;
						me._rectangle_1.ggParameter.sy = 1;
						me._rectangle_1.style[domTransform]=parameterToTransform(me._rectangle_1.ggParameter);
					}
					else {
						me._rectangle_1.ggParameter.sx = 0.5;
						me._rectangle_1.ggParameter.sy = 0.5;
						me._rectangle_1.style[domTransform]=parameterToTransform(me._rectangle_1.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_1.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_1.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_1.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_1.style.visibility="hidden";
						me._rectangle_1.style.opacity=0;
					}
					else {
						me._rectangle_1.style.visibility=me._rectangle_1.ggVisible?'inherit':'hidden';
						me._rectangle_1.style.opacity=1;
					}
				}
			}
			this._rectangle_1.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg_1_1=document.createElement('div');
			this._svg_1_1__img=document.createElement('img');
			this._svg_1_1__img.className='ggskin ggskin_svg';
			this._svg_1_1__img.setAttribute('src',basePath + 'images/svg_1_1.svg');
			this._svg_1_1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg_1_1__img['ondragstart']=function() { return false; };
			this._svg_1_1.appendChild(this._svg_1_1__img);
			this._svg_1_1.ggId="Svg 1_1";
			this._svg_1_1.ggLeft=-16;
			this._svg_1_1.ggTop=-16;
			this._svg_1_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg_1_1.ggVisible=true;
			this._svg_1_1.className='ggskin ggskin_svg ';
			this._svg_1_1.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._svg_1_1.setAttribute('style',hs);
			this._svg_1_1.style[domTransform + 'Origin']='50% 50%';
			me._svg_1_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg_1_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg_1_1.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_1.appendChild(this._svg_1_1);
			this._ht_image0.appendChild(this._rectangle_1);
			this.__div.appendChild(this._ht_image0);
			this._hotspot_preview=document.createElement('div');
			this._hotspot_preview.ggId="hotspot_preview";
			this._hotspot_preview.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspot_preview.ggVisible=false;
			this._hotspot_preview.className='ggskin ggskin_container ';
			this._hotspot_preview.ggType='container';
			hs ='';
			hs+='height : 103px;';
			hs+='left : -81px;';
			hs+='position : absolute;';
			hs+='top : -139px;';
			hs+='visibility : hidden;';
			hs+='width : 153px;';
			hs+='pointer-events:none;';
			this._hotspot_preview.setAttribute('style',hs);
			this._hotspot_preview.style[domTransform + 'Origin']='50% 50%';
			me._hotspot_preview.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._hotspot_preview.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._hotspot_preview.ggCurrentLogicStateVisible = -1;
			this._hotspot_preview.ggUpdateConditionTimer=function () {
				var newLogicStateVisible;
				if (
					(me.elementMouseOver['_div'] == true)
				)
				{
					newLogicStateVisible = 0;
				}
				else {
					newLogicStateVisible = -1;
				}
				if (me._hotspot_preview.ggCurrentLogicStateVisible != newLogicStateVisible) {
					me._hotspot_preview.ggCurrentLogicStateVisible = newLogicStateVisible;
					me._hotspot_preview.style[domTransition]='';
					if (me._hotspot_preview.ggCurrentLogicStateVisible == 0) {
						me._hotspot_preview.style.visibility=(Number(me._hotspot_preview.style.opacity)>0||!me._hotspot_preview.style.opacity)?'inherit':'hidden';
						me._hotspot_preview.ggVisible=true;
					}
					else {
						me._hotspot_preview.style.visibility="hidden";
						me._hotspot_preview.ggVisible=false;
					}
				}
			}
			this._hotspot_preview.ggUpdatePosition=function (useTransition) {
			}
			this._preview_picture_frame_=document.createElement('div');
			this._preview_picture_frame_.ggId="preview_picture_frame ";
			this._preview_picture_frame_.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_picture_frame_.ggVisible=true;
			this._preview_picture_frame_.className='ggskin ggskin_rectangle ';
			this._preview_picture_frame_.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 5px;';
			hs+='border-radius : 5px;';
			hs+='background : #f9b233;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : default;';
			hs+='height : 100px;';
			hs+='left : 2px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 150px;';
			hs+='pointer-events:auto;';
			this._preview_picture_frame_.setAttribute('style',hs);
			this._preview_picture_frame_.style[domTransform + 'Origin']='50% 50%';
			me._preview_picture_frame_.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._preview_picture_frame_.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._preview_picture_frame_.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview.appendChild(this._preview_picture_frame_);
			this._preview_nodeimage=document.createElement('div');
			this._preview_nodeimage__img=document.createElement('img');
			this._preview_nodeimage__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage__img.setAttribute('src',basePath + "images/preview_nodeimage_1_" + nodeId + ".png");
			this._preview_nodeimage.ggNodeId=nodeId;
			this._preview_nodeimage__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._preview_nodeimage__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage__img['ondragstart']=function() { return false; };
			this._preview_nodeimage.appendChild(this._preview_nodeimage__img);
			this._preview_nodeimage.ggId="Preview NodeImage";
			this._preview_nodeimage.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_nodeimage.ggVisible=true;
			this._preview_nodeimage.className='ggskin ggskin_nodeimage ';
			this._preview_nodeimage.ggType='nodeimage';
			hs ='';
			hs+='height : 90px;';
			hs+='left : 7px;';
			hs+='position : absolute;';
			hs+='top : 5px;';
			hs+='visibility : inherit;';
			hs+='width : 140px;';
			hs+='pointer-events:auto;';
			this._preview_nodeimage.setAttribute('style',hs);
			this._preview_nodeimage.style[domTransform + 'Origin']='50% 50%';
			me._preview_nodeimage.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me._preview_nodeimage.ggElementNodeId=function() {
				return this.ggNodeId;
			}
			this._preview_nodeimage.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview.appendChild(this._preview_nodeimage);
			this._tooltip=document.createElement('div');
			this._tooltip__text=document.createElement('div');
			this._tooltip.className='ggskin ggskin_textdiv';
			this._tooltip.ggTextDiv=this._tooltip__text;
			this._tooltip.ggId="tooltip";
			this._tooltip.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._tooltip.ggVisible=true;
			this._tooltip.className='ggskin ggskin_text ';
			this._tooltip.ggType='text';
			hs ='';
			hs+='height : 16px;';
			hs+='left : 45px;';
			hs+='position : absolute;';
			hs+='top : 90px;';
			hs+='visibility : inherit;';
			hs+='width : 50px;';
			hs+='pointer-events:auto;';
			this._tooltip.setAttribute('style',hs);
			this._tooltip.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #f9b233;';
			hs+='border: 5px solid #f9b233;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(0,0,0,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 1px 2px 1px 2px;';
			hs+='overflow: hidden;';
			this._tooltip__text.setAttribute('style',hs);
			this._tooltip__text.innerHTML=me.hotspot.title;
			this._tooltip.appendChild(this._tooltip__text);
			me._tooltip.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._tooltip.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._tooltip.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((62-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this._hotspot_preview.appendChild(this._tooltip);
			this.__div.appendChild(this._hotspot_preview);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				if (me.elementMouseOver['_div']) {
				}
				me._rectangle_2.ggUpdateConditionTimer();
				me._rectangle_1.ggUpdateConditionTimer();
				me._hotspot_preview.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_node') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.elementMouseOver['_div']=true;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.elementMouseOver['_div']=false;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ontouchend=function (e) {
				me.elementMouseOver['_div']=false;
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image_6=document.createElement('div');
			this._ht_image_6.ggId="ht_image_6";
			this._ht_image_6.ggParameter={ rx:0,ry:0,a:0,sx:1.2,sy:1.2 };
			this._ht_image_6.ggVisible=true;
			this._ht_image_6.className='ggskin ggskin_container ';
			this._ht_image_6.ggType='container';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -15px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:none;';
			this._ht_image_6.setAttribute('style',hs);
			this._ht_image_6.style[domTransform + 'Origin']='50% 50%';
			this._ht_image_6.style[domTransform]=parameterToTransform(this._ht_image_6.ggParameter);
			me._ht_image_6.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image_6.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image_6.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_1_6=document.createElement('div');
			this._rectangle_1_6.ggId="Rectangle 1_6";
			this._rectangle_1_6.ggLeft=-16;
			this._rectangle_1_6.ggTop=-16;
			this._rectangle_1_6.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_1_6.ggVisible=true;
			this._rectangle_1_6.className='ggskin ggskin_rectangle ';
			this._rectangle_1_6.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_1_6.setAttribute('style',hs);
			this._rectangle_1_6.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_1_6.style[domTransform]=parameterToTransform(this._rectangle_1_6.ggParameter);
			me._rectangle_1_6.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_1_6.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_1_6.ggCurrentLogicStateScaling = -1;
			me._rectangle_1_6.ggCurrentLogicStateAlpha = -1;
			this._rectangle_1_6.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_1_6.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_1_6.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_1_6.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_6.ggCurrentLogicStateScaling == 0) {
						me._rectangle_1_6.ggParameter.sx = 1;
						me._rectangle_1_6.ggParameter.sy = 1;
						me._rectangle_1_6.style[domTransform]=parameterToTransform(me._rectangle_1_6.ggParameter);
					}
					else {
						me._rectangle_1_6.ggParameter.sx = 0.8;
						me._rectangle_1_6.ggParameter.sy = 0.8;
						me._rectangle_1_6.style[domTransform]=parameterToTransform(me._rectangle_1_6.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_1_6.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_1_6.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_1_6.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_6.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_1_6.style.visibility="hidden";
						me._rectangle_1_6.style.opacity=0;
					}
					else {
						me._rectangle_1_6.style.visibility=me._rectangle_1_6.ggVisible?'inherit':'hidden';
						me._rectangle_1_6.style.opacity=1;
					}
				}
			}
			this._rectangle_1_6.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_3=document.createElement('div');
			this._rectangle_3.ggId="Rectangle 3";
			this._rectangle_3.ggLeft=-24;
			this._rectangle_3.ggTop=-14;
			this._rectangle_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._rectangle_3.ggVisible=true;
			this._rectangle_3.className='ggskin ggskin_rectangle ';
			this._rectangle_3.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 42px;';
			hs+='border-radius : 42px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : pointer;';
			hs+='height : 46px;';
			hs+='left : -24px;';
			hs+='position : absolute;';
			hs+='top : -14px;';
			hs+='visibility : inherit;';
			hs+='width : 46px;';
			hs+='pointer-events:auto;';
			this._rectangle_3.setAttribute('style',hs);
			this._rectangle_3.style[domTransform + 'Origin']='50% 50%';
			me._rectangle_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._rectangle_3.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_1_6.appendChild(this._rectangle_3);
			this._ht_image_6.appendChild(this._rectangle_1_6);
			this._rectangle_2_6=document.createElement('div');
			this._rectangle_2_6.ggId="Rectangle 2_6";
			this._rectangle_2_6.ggLeft=-16;
			this._rectangle_2_6.ggTop=-16;
			this._rectangle_2_6.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_2_6.ggVisible=true;
			this._rectangle_2_6.className='ggskin ggskin_rectangle ';
			this._rectangle_2_6.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_2_6.setAttribute('style',hs);
			this._rectangle_2_6.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_2_6.style[domTransform]=parameterToTransform(this._rectangle_2_6.ggParameter);
			me._rectangle_2_6.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_2_6.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_2_6.ggCurrentLogicStateScaling = -1;
			me._rectangle_2_6.ggCurrentLogicStateAlpha = -1;
			this._rectangle_2_6.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_2_6.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_2_6.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_2_6.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_6.ggCurrentLogicStateScaling == 0) {
						me._rectangle_2_6.ggParameter.sx = 0.7;
						me._rectangle_2_6.ggParameter.sy = 0.7;
						me._rectangle_2_6.style[domTransform]=parameterToTransform(me._rectangle_2_6.ggParameter);
					}
					else {
						me._rectangle_2_6.ggParameter.sx = 0.8;
						me._rectangle_2_6.ggParameter.sy = 0.8;
						me._rectangle_2_6.style[domTransform]=parameterToTransform(me._rectangle_2_6.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_2_6.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_2_6.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_2_6.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_6.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_2_6.style.visibility=me._rectangle_2_6.ggVisible?'inherit':'hidden';
						me._rectangle_2_6.style.opacity=1;
					}
					else {
						me._rectangle_2_6.style.visibility=me._rectangle_2_6.ggVisible?'inherit':'hidden';
						me._rectangle_2_6.style.opacity=1;
					}
				}
			}
			this._rectangle_2_6.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg_4=document.createElement('div');
			this._svg_4__img=document.createElement('img');
			this._svg_4__img.className='ggskin ggskin_svg';
			this._svg_4__img.setAttribute('src',basePath + 'images/svg_4.svg');
			this._svg_4__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg_4__img['ondragstart']=function() { return false; };
			this._svg_4.appendChild(this._svg_4__img);
			this._svg_4.ggId="Svg 4";
			this._svg_4.ggLeft=-17;
			this._svg_4.ggTop=-5;
			this._svg_4.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg_4.ggVisible=true;
			this._svg_4.className='ggskin ggskin_svg ';
			this._svg_4.ggType='svg';
			hs ='';
			hs+='height : 23px;';
			hs+='left : -17px;';
			hs+='position : absolute;';
			hs+='top : -5px;';
			hs+='visibility : inherit;';
			hs+='width : 31px;';
			hs+='pointer-events:auto;';
			this._svg_4.setAttribute('style',hs);
			this._svg_4.style[domTransform + 'Origin']='50% 50%';
			me._svg_4.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg_4.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg_4.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_2_6.appendChild(this._svg_4);
			this._ht_image_6.appendChild(this._rectangle_2_6);
			this.__div.appendChild(this._ht_image_6);
			this._hotspot_preview_3=document.createElement('div');
			this._hotspot_preview_3.ggId="hotspot_preview_3";
			this._hotspot_preview_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspot_preview_3.ggVisible=false;
			this._hotspot_preview_3.className='ggskin ggskin_container ';
			this._hotspot_preview_3.ggType='container';
			hs ='';
			hs+='height : 103px;';
			hs+='left : -81px;';
			hs+='position : absolute;';
			hs+='top : -139px;';
			hs+='visibility : hidden;';
			hs+='width : 153px;';
			hs+='pointer-events:none;';
			this._hotspot_preview_3.setAttribute('style',hs);
			this._hotspot_preview_3.style[domTransform + 'Origin']='50% 50%';
			me._hotspot_preview_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._hotspot_preview_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._hotspot_preview_3.ggCurrentLogicStateVisible = -1;
			this._hotspot_preview_3.ggUpdateConditionTimer=function () {
				var newLogicStateVisible;
				if (
					(me.elementMouseOver['_div'] == true)
				)
				{
					newLogicStateVisible = 0;
				}
				else {
					newLogicStateVisible = -1;
				}
				if (me._hotspot_preview_3.ggCurrentLogicStateVisible != newLogicStateVisible) {
					me._hotspot_preview_3.ggCurrentLogicStateVisible = newLogicStateVisible;
					me._hotspot_preview_3.style[domTransition]='';
					if (me._hotspot_preview_3.ggCurrentLogicStateVisible == 0) {
						me._hotspot_preview_3.style.visibility=(Number(me._hotspot_preview_3.style.opacity)>0||!me._hotspot_preview_3.style.opacity)?'inherit':'hidden';
						me._hotspot_preview_3.ggVisible=true;
					}
					else {
						me._hotspot_preview_3.style.visibility="hidden";
						me._hotspot_preview_3.ggVisible=false;
					}
				}
			}
			this._hotspot_preview_3.ggUpdatePosition=function (useTransition) {
			}
			this._preview_picture_frame_3=document.createElement('div');
			this._preview_picture_frame_3.ggId="preview_picture_frame _3";
			this._preview_picture_frame_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_picture_frame_3.ggVisible=true;
			this._preview_picture_frame_3.className='ggskin ggskin_rectangle ';
			this._preview_picture_frame_3.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 5px;';
			hs+='border-radius : 5px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : default;';
			hs+='height : 100px;';
			hs+='left : 2px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 150px;';
			hs+='pointer-events:auto;';
			this._preview_picture_frame_3.setAttribute('style',hs);
			this._preview_picture_frame_3.style[domTransform + 'Origin']='50% 50%';
			me._preview_picture_frame_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._preview_picture_frame_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._preview_picture_frame_3.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview_3.appendChild(this._preview_picture_frame_3);
			this._preview_nodeimage_3=document.createElement('div');
			this._preview_nodeimage_3__img=document.createElement('img');
			this._preview_nodeimage_3__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage_3__img.setAttribute('src',basePath + "images/preview_nodeimage_1_" + nodeId + ".png");
			this._preview_nodeimage_3.ggNodeId=nodeId;
			this._preview_nodeimage_3__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._preview_nodeimage_3__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage_3__img['ondragstart']=function() { return false; };
			this._preview_nodeimage_3.appendChild(this._preview_nodeimage_3__img);
			this._preview_nodeimage_3.ggId="Preview NodeImage_3";
			this._preview_nodeimage_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_nodeimage_3.ggVisible=true;
			this._preview_nodeimage_3.className='ggskin ggskin_nodeimage ';
			this._preview_nodeimage_3.ggType='nodeimage';
			hs ='';
			hs+='height : 90px;';
			hs+='left : 7px;';
			hs+='position : absolute;';
			hs+='top : 5px;';
			hs+='visibility : inherit;';
			hs+='width : 140px;';
			hs+='pointer-events:auto;';
			this._preview_nodeimage_3.setAttribute('style',hs);
			this._preview_nodeimage_3.style[domTransform + 'Origin']='50% 50%';
			me._preview_nodeimage_3.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me._preview_nodeimage_3.ggElementNodeId=function() {
				return this.ggNodeId;
			}
			this._preview_nodeimage_3.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview_3.appendChild(this._preview_nodeimage_3);
			this._tooltip_3=document.createElement('div');
			this._tooltip_3__text=document.createElement('div');
			this._tooltip_3.className='ggskin ggskin_textdiv';
			this._tooltip_3.ggTextDiv=this._tooltip_3__text;
			this._tooltip_3.ggId="tooltip_3";
			this._tooltip_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._tooltip_3.ggVisible=true;
			this._tooltip_3.className='ggskin ggskin_text ';
			this._tooltip_3.ggType='text';
			hs ='';
			hs+='height : 16px;';
			hs+='left : 45px;';
			hs+='position : absolute;';
			hs+='top : 90px;';
			hs+='visibility : inherit;';
			hs+='width : 50px;';
			hs+='pointer-events:auto;';
			this._tooltip_3.setAttribute('style',hs);
			this._tooltip_3.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #42c8c8;';
			hs+='border: 5px solid #42c8c8;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(255,255,255,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 1px 2px 1px 2px;';
			hs+='overflow: hidden;';
			this._tooltip_3__text.setAttribute('style',hs);
			this._tooltip_3__text.innerHTML=me.hotspot.title;
			this._tooltip_3.appendChild(this._tooltip_3__text);
			me._tooltip_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._tooltip_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._tooltip_3.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((62-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this._hotspot_preview_3.appendChild(this._tooltip_3);
			this.__div.appendChild(this._hotspot_preview_3);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				if (me.elementMouseOver['_div']) {
				}
				me._rectangle_1_6.ggUpdateConditionTimer();
				me._rectangle_2_6.ggUpdateConditionTimer();
				me._hotspot_preview_3.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_node-l') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node-l";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.elementMouseOver['_div']=true;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.elementMouseOver['_div']=false;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ontouchend=function (e) {
				me.elementMouseOver['_div']=false;
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image_5=document.createElement('div');
			this._ht_image_5.ggId="ht_image_5";
			this._ht_image_5.ggParameter={ rx:0,ry:0,a:-90,sx:1.2,sy:1.2 };
			this._ht_image_5.ggVisible=true;
			this._ht_image_5.className='ggskin ggskin_container ';
			this._ht_image_5.ggType='container';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -15px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:none;';
			this._ht_image_5.setAttribute('style',hs);
			this._ht_image_5.style[domTransform + 'Origin']='50% 50%';
			this._ht_image_5.style[domTransform]=parameterToTransform(this._ht_image_5.ggParameter);
			me._ht_image_5.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image_5.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image_5.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_1_5=document.createElement('div');
			this._rectangle_1_5.ggId="Rectangle 1_5";
			this._rectangle_1_5.ggLeft=-16;
			this._rectangle_1_5.ggTop=-16;
			this._rectangle_1_5.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_1_5.ggVisible=true;
			this._rectangle_1_5.className='ggskin ggskin_rectangle ';
			this._rectangle_1_5.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_1_5.setAttribute('style',hs);
			this._rectangle_1_5.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_1_5.style[domTransform]=parameterToTransform(this._rectangle_1_5.ggParameter);
			me._rectangle_1_5.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_1_5.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_1_5.ggCurrentLogicStateScaling = -1;
			me._rectangle_1_5.ggCurrentLogicStateAlpha = -1;
			this._rectangle_1_5.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_1_5.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_1_5.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_1_5.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_5.ggCurrentLogicStateScaling == 0) {
						me._rectangle_1_5.ggParameter.sx = 1;
						me._rectangle_1_5.ggParameter.sy = 1;
						me._rectangle_1_5.style[domTransform]=parameterToTransform(me._rectangle_1_5.ggParameter);
					}
					else {
						me._rectangle_1_5.ggParameter.sx = 0.8;
						me._rectangle_1_5.ggParameter.sy = 0.8;
						me._rectangle_1_5.style[domTransform]=parameterToTransform(me._rectangle_1_5.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_1_5.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_1_5.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_1_5.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_5.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_1_5.style.visibility="hidden";
						me._rectangle_1_5.style.opacity=0;
					}
					else {
						me._rectangle_1_5.style.visibility=me._rectangle_1_5.ggVisible?'inherit':'hidden';
						me._rectangle_1_5.style.opacity=1;
					}
				}
			}
			this._rectangle_1_5.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_3_3=document.createElement('div');
			this._rectangle_3_3.ggId="Rectangle 3_3";
			this._rectangle_3_3.ggLeft=-24;
			this._rectangle_3_3.ggTop=-14;
			this._rectangle_3_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._rectangle_3_3.ggVisible=true;
			this._rectangle_3_3.className='ggskin ggskin_rectangle ';
			this._rectangle_3_3.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 42px;';
			hs+='border-radius : 42px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : pointer;';
			hs+='height : 46px;';
			hs+='left : -24px;';
			hs+='position : absolute;';
			hs+='top : -14px;';
			hs+='visibility : inherit;';
			hs+='width : 46px;';
			hs+='pointer-events:auto;';
			this._rectangle_3_3.setAttribute('style',hs);
			this._rectangle_3_3.style[domTransform + 'Origin']='50% 50%';
			me._rectangle_3_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_3_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._rectangle_3_3.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_1_5.appendChild(this._rectangle_3_3);
			this._ht_image_5.appendChild(this._rectangle_1_5);
			this._rectangle_2_5=document.createElement('div');
			this._rectangle_2_5.ggId="Rectangle 2_5";
			this._rectangle_2_5.ggLeft=-17;
			this._rectangle_2_5.ggTop=-16;
			this._rectangle_2_5.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_2_5.ggVisible=true;
			this._rectangle_2_5.className='ggskin ggskin_rectangle ';
			this._rectangle_2_5.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -17px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_2_5.setAttribute('style',hs);
			this._rectangle_2_5.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_2_5.style[domTransform]=parameterToTransform(this._rectangle_2_5.ggParameter);
			me._rectangle_2_5.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_2_5.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_2_5.ggCurrentLogicStateScaling = -1;
			me._rectangle_2_5.ggCurrentLogicStateAlpha = -1;
			this._rectangle_2_5.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_2_5.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_2_5.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_2_5.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_5.ggCurrentLogicStateScaling == 0) {
						me._rectangle_2_5.ggParameter.sx = 0.7;
						me._rectangle_2_5.ggParameter.sy = 0.7;
						me._rectangle_2_5.style[domTransform]=parameterToTransform(me._rectangle_2_5.ggParameter);
					}
					else {
						me._rectangle_2_5.ggParameter.sx = 0.8;
						me._rectangle_2_5.ggParameter.sy = 0.8;
						me._rectangle_2_5.style[domTransform]=parameterToTransform(me._rectangle_2_5.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_2_5.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_2_5.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_2_5.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_5.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_2_5.style.visibility=me._rectangle_2_5.ggVisible?'inherit':'hidden';
						me._rectangle_2_5.style.opacity=1;
					}
					else {
						me._rectangle_2_5.style.visibility=me._rectangle_2_5.ggVisible?'inherit':'hidden';
						me._rectangle_2_5.style.opacity=1;
					}
				}
			}
			this._rectangle_2_5.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg_4_5=document.createElement('div');
			this._svg_4_5__img=document.createElement('img');
			this._svg_4_5__img.className='ggskin ggskin_svg';
			this._svg_4_5__img.setAttribute('src',basePath + 'images/svg_4_5.svg');
			this._svg_4_5__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg_4_5__img['ondragstart']=function() { return false; };
			this._svg_4_5.appendChild(this._svg_4_5__img);
			this._svg_4_5.ggId="Svg 4_5";
			this._svg_4_5.ggLeft=-16;
			this._svg_4_5.ggTop=-5;
			this._svg_4_5.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg_4_5.ggVisible=true;
			this._svg_4_5.className='ggskin ggskin_svg ';
			this._svg_4_5.ggType='svg';
			hs ='';
			hs+='height : 23px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -5px;';
			hs+='visibility : inherit;';
			hs+='width : 31px;';
			hs+='pointer-events:auto;';
			this._svg_4_5.setAttribute('style',hs);
			this._svg_4_5.style[domTransform + 'Origin']='50% 50%';
			me._svg_4_5.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg_4_5.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg_4_5.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_2_5.appendChild(this._svg_4_5);
			this._ht_image_5.appendChild(this._rectangle_2_5);
			this.__div.appendChild(this._ht_image_5);
			this._hotspot_preview_2=document.createElement('div');
			this._hotspot_preview_2.ggId="hotspot_preview_2";
			this._hotspot_preview_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspot_preview_2.ggVisible=false;
			this._hotspot_preview_2.className='ggskin ggskin_container ';
			this._hotspot_preview_2.ggType='container';
			hs ='';
			hs+='height : 103px;';
			hs+='left : -81px;';
			hs+='position : absolute;';
			hs+='top : -139px;';
			hs+='visibility : hidden;';
			hs+='width : 153px;';
			hs+='pointer-events:none;';
			this._hotspot_preview_2.setAttribute('style',hs);
			this._hotspot_preview_2.style[domTransform + 'Origin']='50% 50%';
			me._hotspot_preview_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._hotspot_preview_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._hotspot_preview_2.ggCurrentLogicStateVisible = -1;
			this._hotspot_preview_2.ggUpdateConditionTimer=function () {
				var newLogicStateVisible;
				if (
					(me.elementMouseOver['_div'] == true)
				)
				{
					newLogicStateVisible = 0;
				}
				else {
					newLogicStateVisible = -1;
				}
				if (me._hotspot_preview_2.ggCurrentLogicStateVisible != newLogicStateVisible) {
					me._hotspot_preview_2.ggCurrentLogicStateVisible = newLogicStateVisible;
					me._hotspot_preview_2.style[domTransition]='';
					if (me._hotspot_preview_2.ggCurrentLogicStateVisible == 0) {
						me._hotspot_preview_2.style.visibility=(Number(me._hotspot_preview_2.style.opacity)>0||!me._hotspot_preview_2.style.opacity)?'inherit':'hidden';
						me._hotspot_preview_2.ggVisible=true;
					}
					else {
						me._hotspot_preview_2.style.visibility="hidden";
						me._hotspot_preview_2.ggVisible=false;
					}
				}
			}
			this._hotspot_preview_2.ggUpdatePosition=function (useTransition) {
			}
			this._preview_picture_frame_2=document.createElement('div');
			this._preview_picture_frame_2.ggId="preview_picture_frame _2";
			this._preview_picture_frame_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_picture_frame_2.ggVisible=true;
			this._preview_picture_frame_2.className='ggskin ggskin_rectangle ';
			this._preview_picture_frame_2.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 5px;';
			hs+='border-radius : 5px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : default;';
			hs+='height : 100px;';
			hs+='left : 2px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 150px;';
			hs+='pointer-events:auto;';
			this._preview_picture_frame_2.setAttribute('style',hs);
			this._preview_picture_frame_2.style[domTransform + 'Origin']='50% 50%';
			me._preview_picture_frame_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._preview_picture_frame_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._preview_picture_frame_2.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview_2.appendChild(this._preview_picture_frame_2);
			this._preview_nodeimage_2=document.createElement('div');
			this._preview_nodeimage_2__img=document.createElement('img');
			this._preview_nodeimage_2__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage_2__img.setAttribute('src',basePath + "images/preview_nodeimage_1_" + nodeId + ".png");
			this._preview_nodeimage_2.ggNodeId=nodeId;
			this._preview_nodeimage_2__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._preview_nodeimage_2__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage_2__img['ondragstart']=function() { return false; };
			this._preview_nodeimage_2.appendChild(this._preview_nodeimage_2__img);
			this._preview_nodeimage_2.ggId="Preview NodeImage_2";
			this._preview_nodeimage_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_nodeimage_2.ggVisible=true;
			this._preview_nodeimage_2.className='ggskin ggskin_nodeimage ';
			this._preview_nodeimage_2.ggType='nodeimage';
			hs ='';
			hs+='height : 90px;';
			hs+='left : 7px;';
			hs+='position : absolute;';
			hs+='top : 5px;';
			hs+='visibility : inherit;';
			hs+='width : 140px;';
			hs+='pointer-events:auto;';
			this._preview_nodeimage_2.setAttribute('style',hs);
			this._preview_nodeimage_2.style[domTransform + 'Origin']='50% 50%';
			me._preview_nodeimage_2.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me._preview_nodeimage_2.ggElementNodeId=function() {
				return this.ggNodeId;
			}
			this._preview_nodeimage_2.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview_2.appendChild(this._preview_nodeimage_2);
			this._tooltip_2=document.createElement('div');
			this._tooltip_2__text=document.createElement('div');
			this._tooltip_2.className='ggskin ggskin_textdiv';
			this._tooltip_2.ggTextDiv=this._tooltip_2__text;
			this._tooltip_2.ggId="tooltip_2";
			this._tooltip_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._tooltip_2.ggVisible=true;
			this._tooltip_2.className='ggskin ggskin_text ';
			this._tooltip_2.ggType='text';
			hs ='';
			hs+='height : 16px;';
			hs+='left : 45px;';
			hs+='position : absolute;';
			hs+='top : 90px;';
			hs+='visibility : inherit;';
			hs+='width : 50px;';
			hs+='pointer-events:auto;';
			this._tooltip_2.setAttribute('style',hs);
			this._tooltip_2.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #42c8c8;';
			hs+='border: 5px solid #42c8c8;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(255,255,255,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 1px 2px 1px 2px;';
			hs+='overflow: hidden;';
			this._tooltip_2__text.setAttribute('style',hs);
			this._tooltip_2__text.innerHTML=me.hotspot.title;
			this._tooltip_2.appendChild(this._tooltip_2__text);
			me._tooltip_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._tooltip_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._tooltip_2.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((62-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this._hotspot_preview_2.appendChild(this._tooltip_2);
			this.__div.appendChild(this._hotspot_preview_2);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				if (me.elementMouseOver['_div']) {
				}
				me._rectangle_1_5.ggUpdateConditionTimer();
				me._rectangle_2_5.ggUpdateConditionTimer();
				me._hotspot_preview_2.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_node-r') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node-r";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.elementMouseOver['_div']=true;
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.elementMouseOver['_div']=false;
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ontouchend=function (e) {
				me.elementMouseOver['_div']=false;
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image_4=document.createElement('div');
			this._ht_image_4.ggId="ht_image_4";
			this._ht_image_4.ggParameter={ rx:0,ry:0,a:90,sx:1.2,sy:1.2 };
			this._ht_image_4.ggVisible=true;
			this._ht_image_4.className='ggskin ggskin_container ';
			this._ht_image_4.ggType='container';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -15px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:none;';
			this._ht_image_4.setAttribute('style',hs);
			this._ht_image_4.style[domTransform + 'Origin']='50% 50%';
			this._ht_image_4.style[domTransform]=parameterToTransform(this._ht_image_4.ggParameter);
			me._ht_image_4.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image_4.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image_4.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_1_4=document.createElement('div');
			this._rectangle_1_4.ggId="Rectangle 1_4";
			this._rectangle_1_4.ggLeft=-16;
			this._rectangle_1_4.ggTop=-16;
			this._rectangle_1_4.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_1_4.ggVisible=true;
			this._rectangle_1_4.className='ggskin ggskin_rectangle ';
			this._rectangle_1_4.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_1_4.setAttribute('style',hs);
			this._rectangle_1_4.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_1_4.style[domTransform]=parameterToTransform(this._rectangle_1_4.ggParameter);
			me._rectangle_1_4.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_1_4.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_1_4.ggCurrentLogicStateScaling = -1;
			me._rectangle_1_4.ggCurrentLogicStateAlpha = -1;
			this._rectangle_1_4.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_1_4.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_1_4.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_1_4.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_4.ggCurrentLogicStateScaling == 0) {
						me._rectangle_1_4.ggParameter.sx = 1;
						me._rectangle_1_4.ggParameter.sy = 1;
						me._rectangle_1_4.style[domTransform]=parameterToTransform(me._rectangle_1_4.ggParameter);
					}
					else {
						me._rectangle_1_4.ggParameter.sx = 0.8;
						me._rectangle_1_4.ggParameter.sy = 0.8;
						me._rectangle_1_4.style[domTransform]=parameterToTransform(me._rectangle_1_4.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_1_4.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_1_4.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_1_4.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_4.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_1_4.style.visibility="hidden";
						me._rectangle_1_4.style.opacity=0;
					}
					else {
						me._rectangle_1_4.style.visibility=me._rectangle_1_4.ggVisible?'inherit':'hidden';
						me._rectangle_1_4.style.opacity=1;
					}
				}
			}
			this._rectangle_1_4.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._cyrcle=document.createElement('div');
			this._cyrcle.ggId="cyrcle";
			this._cyrcle.ggLeft=-24;
			this._cyrcle.ggTop=-13;
			this._cyrcle.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._cyrcle.ggVisible=true;
			this._cyrcle.className='ggskin ggskin_rectangle ';
			this._cyrcle.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 42px;';
			hs+='border-radius : 42px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : pointer;';
			hs+='height : 46px;';
			hs+='left : -24px;';
			hs+='position : absolute;';
			hs+='top : -13px;';
			hs+='visibility : inherit;';
			hs+='width : 46px;';
			hs+='pointer-events:auto;';
			this._cyrcle.setAttribute('style',hs);
			this._cyrcle.style[domTransform + 'Origin']='50% 50%';
			me._cyrcle.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._cyrcle.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._cyrcle.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_1_4.appendChild(this._cyrcle);
			this._ht_image_4.appendChild(this._rectangle_1_4);
			this._rectangle_2_4=document.createElement('div');
			this._rectangle_2_4.ggId="Rectangle 2_4";
			this._rectangle_2_4.ggLeft=-17;
			this._rectangle_2_4.ggTop=-16;
			this._rectangle_2_4.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_2_4.ggVisible=true;
			this._rectangle_2_4.className='ggskin ggskin_rectangle ';
			this._rectangle_2_4.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -17px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_2_4.setAttribute('style',hs);
			this._rectangle_2_4.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_2_4.style[domTransform]=parameterToTransform(this._rectangle_2_4.ggParameter);
			me._rectangle_2_4.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_2_4.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_2_4.ggCurrentLogicStateScaling = -1;
			me._rectangle_2_4.ggCurrentLogicStateAlpha = -1;
			this._rectangle_2_4.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_2_4.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_2_4.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_2_4.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_4.ggCurrentLogicStateScaling == 0) {
						me._rectangle_2_4.ggParameter.sx = 0.7;
						me._rectangle_2_4.ggParameter.sy = 0.7;
						me._rectangle_2_4.style[domTransform]=parameterToTransform(me._rectangle_2_4.ggParameter);
					}
					else {
						me._rectangle_2_4.ggParameter.sx = 0.8;
						me._rectangle_2_4.ggParameter.sy = 0.8;
						me._rectangle_2_4.style[domTransform]=parameterToTransform(me._rectangle_2_4.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_2_4.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_2_4.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_2_4.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_4.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_2_4.style.visibility=me._rectangle_2_4.ggVisible?'inherit':'hidden';
						me._rectangle_2_4.style.opacity=1;
					}
					else {
						me._rectangle_2_4.style.visibility=me._rectangle_2_4.ggVisible?'inherit':'hidden';
						me._rectangle_2_4.style.opacity=1;
					}
				}
			}
			this._rectangle_2_4.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg_4_4=document.createElement('div');
			this._svg_4_4__img=document.createElement('img');
			this._svg_4_4__img.className='ggskin ggskin_svg';
			this._svg_4_4__img.setAttribute('src',basePath + 'images/svg_4_4.svg');
			this._svg_4_4__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg_4_4__img['ondragstart']=function() { return false; };
			this._svg_4_4.appendChild(this._svg_4_4__img);
			this._svg_4_4.ggId="Svg 4_4";
			this._svg_4_4.ggLeft=-16;
			this._svg_4_4.ggTop=-5;
			this._svg_4_4.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg_4_4.ggVisible=true;
			this._svg_4_4.className='ggskin ggskin_svg ';
			this._svg_4_4.ggType='svg';
			hs ='';
			hs+='height : 23px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -5px;';
			hs+='visibility : inherit;';
			hs+='width : 31px;';
			hs+='pointer-events:auto;';
			this._svg_4_4.setAttribute('style',hs);
			this._svg_4_4.style[domTransform + 'Origin']='50% 50%';
			me._svg_4_4.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg_4_4.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg_4_4.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_2_4.appendChild(this._svg_4_4);
			this._ht_image_4.appendChild(this._rectangle_2_4);
			this.__div.appendChild(this._ht_image_4);
			this._hotspot_preview_1=document.createElement('div');
			this._hotspot_preview_1.ggId="hotspot_preview_1";
			this._hotspot_preview_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._hotspot_preview_1.ggVisible=false;
			this._hotspot_preview_1.className='ggskin ggskin_container ';
			this._hotspot_preview_1.ggType='container';
			hs ='';
			hs+='height : 103px;';
			hs+='left : -81px;';
			hs+='position : absolute;';
			hs+='top : -139px;';
			hs+='visibility : hidden;';
			hs+='width : 153px;';
			hs+='pointer-events:none;';
			this._hotspot_preview_1.setAttribute('style',hs);
			this._hotspot_preview_1.style[domTransform + 'Origin']='50% 50%';
			me._hotspot_preview_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._hotspot_preview_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._hotspot_preview_1.ggCurrentLogicStateVisible = -1;
			this._hotspot_preview_1.ggUpdateConditionTimer=function () {
				var newLogicStateVisible;
				if (
					(me.elementMouseOver['_div'] == true)
				)
				{
					newLogicStateVisible = 0;
				}
				else {
					newLogicStateVisible = -1;
				}
				if (me._hotspot_preview_1.ggCurrentLogicStateVisible != newLogicStateVisible) {
					me._hotspot_preview_1.ggCurrentLogicStateVisible = newLogicStateVisible;
					me._hotspot_preview_1.style[domTransition]='';
					if (me._hotspot_preview_1.ggCurrentLogicStateVisible == 0) {
						me._hotspot_preview_1.style.visibility=(Number(me._hotspot_preview_1.style.opacity)>0||!me._hotspot_preview_1.style.opacity)?'inherit':'hidden';
						me._hotspot_preview_1.ggVisible=true;
					}
					else {
						me._hotspot_preview_1.style.visibility="hidden";
						me._hotspot_preview_1.ggVisible=false;
					}
				}
			}
			this._hotspot_preview_1.ggUpdatePosition=function (useTransition) {
			}
			this._preview_picture_frame_1=document.createElement('div');
			this._preview_picture_frame_1.ggId="preview_picture_frame _1";
			this._preview_picture_frame_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_picture_frame_1.ggVisible=true;
			this._preview_picture_frame_1.className='ggskin ggskin_rectangle ';
			this._preview_picture_frame_1.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 5px;';
			hs+='border-radius : 5px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : default;';
			hs+='height : 100px;';
			hs+='left : 2px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 150px;';
			hs+='pointer-events:auto;';
			this._preview_picture_frame_1.setAttribute('style',hs);
			this._preview_picture_frame_1.style[domTransform + 'Origin']='50% 50%';
			me._preview_picture_frame_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._preview_picture_frame_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._preview_picture_frame_1.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview_1.appendChild(this._preview_picture_frame_1);
			this._preview_nodeimage_1=document.createElement('div');
			this._preview_nodeimage_1__img=document.createElement('img');
			this._preview_nodeimage_1__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage_1__img.setAttribute('src',basePath + "images/preview_nodeimage_1_" + nodeId + ".png");
			this._preview_nodeimage_1.ggNodeId=nodeId;
			this._preview_nodeimage_1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._preview_nodeimage_1__img.className='ggskin ggskin_nodeimage';
			this._preview_nodeimage_1__img['ondragstart']=function() { return false; };
			this._preview_nodeimage_1.appendChild(this._preview_nodeimage_1__img);
			this._preview_nodeimage_1.ggId="Preview NodeImage_1";
			this._preview_nodeimage_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._preview_nodeimage_1.ggVisible=true;
			this._preview_nodeimage_1.className='ggskin ggskin_nodeimage ';
			this._preview_nodeimage_1.ggType='nodeimage';
			hs ='';
			hs+='height : 90px;';
			hs+='left : 7px;';
			hs+='position : absolute;';
			hs+='top : 5px;';
			hs+='visibility : inherit;';
			hs+='width : 140px;';
			hs+='pointer-events:auto;';
			this._preview_nodeimage_1.setAttribute('style',hs);
			this._preview_nodeimage_1.style[domTransform + 'Origin']='50% 50%';
			me._preview_nodeimage_1.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me._preview_nodeimage_1.ggElementNodeId=function() {
				return this.ggNodeId;
			}
			this._preview_nodeimage_1.ggUpdatePosition=function (useTransition) {
			}
			this._hotspot_preview_1.appendChild(this._preview_nodeimage_1);
			this._tooltip_1=document.createElement('div');
			this._tooltip_1__text=document.createElement('div');
			this._tooltip_1.className='ggskin ggskin_textdiv';
			this._tooltip_1.ggTextDiv=this._tooltip_1__text;
			this._tooltip_1.ggId="tooltip_1";
			this._tooltip_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._tooltip_1.ggVisible=true;
			this._tooltip_1.className='ggskin ggskin_text ';
			this._tooltip_1.ggType='text';
			hs ='';
			hs+='height : 16px;';
			hs+='left : 45px;';
			hs+='position : absolute;';
			hs+='top : 90px;';
			hs+='visibility : inherit;';
			hs+='width : 50px;';
			hs+='pointer-events:auto;';
			this._tooltip_1.setAttribute('style',hs);
			this._tooltip_1.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #42c8c8;';
			hs+='border: 5px solid #42c8c8;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(255,255,255,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 1px 2px 1px 2px;';
			hs+='overflow: hidden;';
			this._tooltip_1__text.setAttribute('style',hs);
			this._tooltip_1__text.innerHTML=me.hotspot.title;
			this._tooltip_1.appendChild(this._tooltip_1__text);
			me._tooltip_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._tooltip_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._tooltip_1.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((62-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this._hotspot_preview_1.appendChild(this._tooltip_1);
			this.__div.appendChild(this._hotspot_preview_1);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				if (me.elementMouseOver['_div']) {
				}
				me._rectangle_1_4.ggUpdateConditionTimer();
				me._rectangle_2_4.ggUpdateConditionTimer();
				me._hotspot_preview_1.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_node-lс') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node-l\u0441";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image_3=document.createElement('div');
			this._ht_image_3.ggId="ht_image_3";
			this._ht_image_3.ggParameter={ rx:0,ry:0,a:-90,sx:1.2,sy:1.2 };
			this._ht_image_3.ggVisible=true;
			this._ht_image_3.className='ggskin ggskin_container ';
			this._ht_image_3.ggType='container';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -15px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:none;';
			this._ht_image_3.setAttribute('style',hs);
			this._ht_image_3.style[domTransform + 'Origin']='50% 50%';
			this._ht_image_3.style[domTransform]=parameterToTransform(this._ht_image_3.ggParameter);
			me._ht_image_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image_3.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_1_3=document.createElement('div');
			this._rectangle_1_3.ggId="Rectangle 1_3";
			this._rectangle_1_3.ggLeft=-16;
			this._rectangle_1_3.ggTop=-16;
			this._rectangle_1_3.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_1_3.ggVisible=true;
			this._rectangle_1_3.className='ggskin ggskin_rectangle ';
			this._rectangle_1_3.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_1_3.setAttribute('style',hs);
			this._rectangle_1_3.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_1_3.style[domTransform]=parameterToTransform(this._rectangle_1_3.ggParameter);
			me._rectangle_1_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_1_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_1_3.ggCurrentLogicStateScaling = -1;
			me._rectangle_1_3.ggCurrentLogicStateAlpha = -1;
			this._rectangle_1_3.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_1_3.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_1_3.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_1_3.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_3.ggCurrentLogicStateScaling == 0) {
						me._rectangle_1_3.ggParameter.sx = 1;
						me._rectangle_1_3.ggParameter.sy = 1;
						me._rectangle_1_3.style[domTransform]=parameterToTransform(me._rectangle_1_3.ggParameter);
					}
					else {
						me._rectangle_1_3.ggParameter.sx = 0.8;
						me._rectangle_1_3.ggParameter.sy = 0.8;
						me._rectangle_1_3.style[domTransform]=parameterToTransform(me._rectangle_1_3.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_1_3.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_1_3.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_1_3.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_3.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_1_3.style.visibility="hidden";
						me._rectangle_1_3.style.opacity=0;
					}
					else {
						me._rectangle_1_3.style.visibility=me._rectangle_1_3.ggVisible?'inherit':'hidden';
						me._rectangle_1_3.style.opacity=1;
					}
				}
			}
			this._rectangle_1_3.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_3_2=document.createElement('div');
			this._rectangle_3_2.ggId="Rectangle 3_2";
			this._rectangle_3_2.ggLeft=-24;
			this._rectangle_3_2.ggTop=-14;
			this._rectangle_3_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._rectangle_3_2.ggVisible=true;
			this._rectangle_3_2.className='ggskin ggskin_rectangle ';
			this._rectangle_3_2.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 42px;';
			hs+='border-radius : 42px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : pointer;';
			hs+='height : 46px;';
			hs+='left : -24px;';
			hs+='position : absolute;';
			hs+='top : -14px;';
			hs+='visibility : inherit;';
			hs+='width : 46px;';
			hs+='pointer-events:auto;';
			this._rectangle_3_2.setAttribute('style',hs);
			this._rectangle_3_2.style[domTransform + 'Origin']='50% 50%';
			me._rectangle_3_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_3_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._rectangle_3_2.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_1_3.appendChild(this._rectangle_3_2);
			this._ht_image_3.appendChild(this._rectangle_1_3);
			this._rectangle_2_3=document.createElement('div');
			this._rectangle_2_3.ggId="Rectangle 2_3";
			this._rectangle_2_3.ggLeft=-17;
			this._rectangle_2_3.ggTop=-16;
			this._rectangle_2_3.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_2_3.ggVisible=true;
			this._rectangle_2_3.className='ggskin ggskin_rectangle ';
			this._rectangle_2_3.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -17px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_2_3.setAttribute('style',hs);
			this._rectangle_2_3.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_2_3.style[domTransform]=parameterToTransform(this._rectangle_2_3.ggParameter);
			me._rectangle_2_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_2_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_2_3.ggCurrentLogicStateScaling = -1;
			me._rectangle_2_3.ggCurrentLogicStateAlpha = -1;
			this._rectangle_2_3.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_2_3.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_2_3.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_2_3.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_3.ggCurrentLogicStateScaling == 0) {
						me._rectangle_2_3.ggParameter.sx = 0.7;
						me._rectangle_2_3.ggParameter.sy = 0.7;
						me._rectangle_2_3.style[domTransform]=parameterToTransform(me._rectangle_2_3.ggParameter);
					}
					else {
						me._rectangle_2_3.ggParameter.sx = 0.8;
						me._rectangle_2_3.ggParameter.sy = 0.8;
						me._rectangle_2_3.style[domTransform]=parameterToTransform(me._rectangle_2_3.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_2_3.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_2_3.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_2_3.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_3.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_2_3.style.visibility=me._rectangle_2_3.ggVisible?'inherit':'hidden';
						me._rectangle_2_3.style.opacity=1;
					}
					else {
						me._rectangle_2_3.style.visibility=me._rectangle_2_3.ggVisible?'inherit':'hidden';
						me._rectangle_2_3.style.opacity=1;
					}
				}
			}
			this._rectangle_2_3.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg_4_3=document.createElement('div');
			this._svg_4_3__img=document.createElement('img');
			this._svg_4_3__img.className='ggskin ggskin_svg';
			this._svg_4_3__img.setAttribute('src',basePath + 'images/svg_4_3.svg');
			this._svg_4_3__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg_4_3__img['ondragstart']=function() { return false; };
			this._svg_4_3.appendChild(this._svg_4_3__img);
			this._svg_4_3.ggId="Svg 4_3";
			this._svg_4_3.ggLeft=-16;
			this._svg_4_3.ggTop=-5;
			this._svg_4_3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg_4_3.ggVisible=true;
			this._svg_4_3.className='ggskin ggskin_svg ';
			this._svg_4_3.ggType='svg';
			hs ='';
			hs+='height : 23px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -5px;';
			hs+='visibility : inherit;';
			hs+='width : 31px;';
			hs+='pointer-events:auto;';
			this._svg_4_3.setAttribute('style',hs);
			this._svg_4_3.style[domTransform + 'Origin']='50% 50%';
			me._svg_4_3.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg_4_3.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg_4_3.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_2_3.appendChild(this._svg_4_3);
			this._ht_image_3.appendChild(this._rectangle_2_3);
			this.__div.appendChild(this._ht_image_3);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._rectangle_1_3.ggUpdateConditionTimer();
				me._rectangle_2_3.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_node-rс') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node-r\u0441";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image_2=document.createElement('div');
			this._ht_image_2.ggId="ht_image_2";
			this._ht_image_2.ggParameter={ rx:0,ry:0,a:90,sx:1.2,sy:1.2 };
			this._ht_image_2.ggVisible=true;
			this._ht_image_2.className='ggskin ggskin_container ';
			this._ht_image_2.ggType='container';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -15px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:none;';
			this._ht_image_2.setAttribute('style',hs);
			this._ht_image_2.style[domTransform + 'Origin']='50% 50%';
			this._ht_image_2.style[domTransform]=parameterToTransform(this._ht_image_2.ggParameter);
			me._ht_image_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image_2.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_1_2=document.createElement('div');
			this._rectangle_1_2.ggId="Rectangle 1_2";
			this._rectangle_1_2.ggLeft=-16;
			this._rectangle_1_2.ggTop=-16;
			this._rectangle_1_2.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_1_2.ggVisible=true;
			this._rectangle_1_2.className='ggskin ggskin_rectangle ';
			this._rectangle_1_2.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_1_2.setAttribute('style',hs);
			this._rectangle_1_2.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_1_2.style[domTransform]=parameterToTransform(this._rectangle_1_2.ggParameter);
			me._rectangle_1_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_1_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_1_2.ggCurrentLogicStateScaling = -1;
			me._rectangle_1_2.ggCurrentLogicStateAlpha = -1;
			this._rectangle_1_2.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_1_2.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_1_2.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_1_2.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_2.ggCurrentLogicStateScaling == 0) {
						me._rectangle_1_2.ggParameter.sx = 1;
						me._rectangle_1_2.ggParameter.sy = 1;
						me._rectangle_1_2.style[domTransform]=parameterToTransform(me._rectangle_1_2.ggParameter);
					}
					else {
						me._rectangle_1_2.ggParameter.sx = 0.8;
						me._rectangle_1_2.ggParameter.sy = 0.8;
						me._rectangle_1_2.style[domTransform]=parameterToTransform(me._rectangle_1_2.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_1_2.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_1_2.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_1_2.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_2.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_1_2.style.visibility="hidden";
						me._rectangle_1_2.style.opacity=0;
					}
					else {
						me._rectangle_1_2.style.visibility=me._rectangle_1_2.ggVisible?'inherit':'hidden';
						me._rectangle_1_2.style.opacity=1;
					}
				}
			}
			this._rectangle_1_2.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._cyrcle_1=document.createElement('div');
			this._cyrcle_1.ggId="cyrcle_1";
			this._cyrcle_1.ggLeft=-24;
			this._cyrcle_1.ggTop=-13;
			this._cyrcle_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._cyrcle_1.ggVisible=true;
			this._cyrcle_1.className='ggskin ggskin_rectangle ';
			this._cyrcle_1.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 42px;';
			hs+='border-radius : 42px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : pointer;';
			hs+='height : 46px;';
			hs+='left : -24px;';
			hs+='position : absolute;';
			hs+='top : -13px;';
			hs+='visibility : inherit;';
			hs+='width : 46px;';
			hs+='pointer-events:auto;';
			this._cyrcle_1.setAttribute('style',hs);
			this._cyrcle_1.style[domTransform + 'Origin']='50% 50%';
			me._cyrcle_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._cyrcle_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._cyrcle_1.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_1_2.appendChild(this._cyrcle_1);
			this._ht_image_2.appendChild(this._rectangle_1_2);
			this._rectangle_2_2=document.createElement('div');
			this._rectangle_2_2.ggId="Rectangle 2_2";
			this._rectangle_2_2.ggLeft=-17;
			this._rectangle_2_2.ggTop=-16;
			this._rectangle_2_2.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_2_2.ggVisible=true;
			this._rectangle_2_2.className='ggskin ggskin_rectangle ';
			this._rectangle_2_2.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -17px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_2_2.setAttribute('style',hs);
			this._rectangle_2_2.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_2_2.style[domTransform]=parameterToTransform(this._rectangle_2_2.ggParameter);
			me._rectangle_2_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_2_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_2_2.ggCurrentLogicStateScaling = -1;
			me._rectangle_2_2.ggCurrentLogicStateAlpha = -1;
			this._rectangle_2_2.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_2_2.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_2_2.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_2_2.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_2.ggCurrentLogicStateScaling == 0) {
						me._rectangle_2_2.ggParameter.sx = 0.7;
						me._rectangle_2_2.ggParameter.sy = 0.7;
						me._rectangle_2_2.style[domTransform]=parameterToTransform(me._rectangle_2_2.ggParameter);
					}
					else {
						me._rectangle_2_2.ggParameter.sx = 0.8;
						me._rectangle_2_2.ggParameter.sy = 0.8;
						me._rectangle_2_2.style[domTransform]=parameterToTransform(me._rectangle_2_2.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_2_2.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_2_2.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_2_2.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_2.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_2_2.style.visibility=me._rectangle_2_2.ggVisible?'inherit':'hidden';
						me._rectangle_2_2.style.opacity=1;
					}
					else {
						me._rectangle_2_2.style.visibility=me._rectangle_2_2.ggVisible?'inherit':'hidden';
						me._rectangle_2_2.style.opacity=1;
					}
				}
			}
			this._rectangle_2_2.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg_4_2=document.createElement('div');
			this._svg_4_2__img=document.createElement('img');
			this._svg_4_2__img.className='ggskin ggskin_svg';
			this._svg_4_2__img.setAttribute('src',basePath + 'images/svg_4_2.svg');
			this._svg_4_2__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg_4_2__img['ondragstart']=function() { return false; };
			this._svg_4_2.appendChild(this._svg_4_2__img);
			this._svg_4_2.ggId="Svg 4_2";
			this._svg_4_2.ggLeft=-16;
			this._svg_4_2.ggTop=-5;
			this._svg_4_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg_4_2.ggVisible=true;
			this._svg_4_2.className='ggskin ggskin_svg ';
			this._svg_4_2.ggType='svg';
			hs ='';
			hs+='height : 23px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -5px;';
			hs+='visibility : inherit;';
			hs+='width : 31px;';
			hs+='pointer-events:auto;';
			this._svg_4_2.setAttribute('style',hs);
			this._svg_4_2.style[domTransform + 'Origin']='50% 50%';
			me._svg_4_2.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg_4_2.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg_4_2.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_2_2.appendChild(this._svg_4_2);
			this._ht_image_2.appendChild(this._rectangle_2_2);
			this.__div.appendChild(this._ht_image_2);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._rectangle_1_2.ggUpdateConditionTimer();
				me._rectangle_2_2.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		{
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node-c";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 0px;';
			hs+='position : absolute;';
			hs+='top : 0px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_image_1=document.createElement('div');
			this._ht_image_1.ggId="ht_image_1";
			this._ht_image_1.ggParameter={ rx:0,ry:0,a:0,sx:1.2,sy:1.2 };
			this._ht_image_1.ggVisible=true;
			this._ht_image_1.className='ggskin ggskin_container ';
			this._ht_image_1.ggType='container';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -15px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:none;';
			this._ht_image_1.setAttribute('style',hs);
			this._ht_image_1.style[domTransform + 'Origin']='50% 50%';
			this._ht_image_1.style[domTransform]=parameterToTransform(this._ht_image_1.ggParameter);
			me._ht_image_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_image_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_image_1.ggUpdatePosition=function (useTransition) {
			}
			this._rectangle_1_1=document.createElement('div');
			this._rectangle_1_1.ggId="Rectangle 1_1";
			this._rectangle_1_1.ggLeft=-16;
			this._rectangle_1_1.ggTop=-16;
			this._rectangle_1_1.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_1_1.ggVisible=true;
			this._rectangle_1_1.className='ggskin ggskin_rectangle ';
			this._rectangle_1_1.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 33px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_1_1.setAttribute('style',hs);
			this._rectangle_1_1.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_1_1.style[domTransform]=parameterToTransform(this._rectangle_1_1.ggParameter);
			me._rectangle_1_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_1_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_1_1.ggCurrentLogicStateScaling = -1;
			me._rectangle_1_1.ggCurrentLogicStateAlpha = -1;
			this._rectangle_1_1.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_1_1.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_1_1.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_1_1.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_1.ggCurrentLogicStateScaling == 0) {
						me._rectangle_1_1.ggParameter.sx = 1;
						me._rectangle_1_1.ggParameter.sy = 1;
						me._rectangle_1_1.style[domTransform]=parameterToTransform(me._rectangle_1_1.ggParameter);
					}
					else {
						me._rectangle_1_1.ggParameter.sx = 0.8;
						me._rectangle_1_1.ggParameter.sy = 0.8;
						me._rectangle_1_1.style[domTransform]=parameterToTransform(me._rectangle_1_1.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_1_1.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_1_1.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_1_1.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_1_1.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_1_1.style.visibility="hidden";
						me._rectangle_1_1.style.opacity=0;
					}
					else {
						me._rectangle_1_1.style.visibility=me._rectangle_1_1.ggVisible?'inherit':'hidden';
						me._rectangle_1_1.style.opacity=1;
					}
				}
			}
			this._rectangle_1_1.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_3_1=document.createElement('div');
			this._rectangle_3_1.ggId="Rectangle 3_1";
			this._rectangle_3_1.ggLeft=-24;
			this._rectangle_3_1.ggTop=-14;
			this._rectangle_3_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._rectangle_3_1.ggVisible=true;
			this._rectangle_3_1.className='ggskin ggskin_rectangle ';
			this._rectangle_3_1.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'border-radius : 42px;';
			hs+='border-radius : 42px;';
			hs+='background : #42c8c8;';
			hs+='border : 0px solid #000000;';
			hs+='cursor : pointer;';
			hs+='height : 46px;';
			hs+='left : -24px;';
			hs+='position : absolute;';
			hs+='top : -14px;';
			hs+='visibility : inherit;';
			hs+='width : 46px;';
			hs+='pointer-events:auto;';
			this._rectangle_3_1.setAttribute('style',hs);
			this._rectangle_3_1.style[domTransform + 'Origin']='50% 50%';
			me._rectangle_3_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_3_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._rectangle_3_1.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_1_1.appendChild(this._rectangle_3_1);
			this._ht_image_1.appendChild(this._rectangle_1_1);
			this._rectangle_2_1=document.createElement('div');
			this._rectangle_2_1.ggId="Rectangle 2_1";
			this._rectangle_2_1.ggLeft=-16;
			this._rectangle_2_1.ggTop=-16;
			this._rectangle_2_1.ggParameter={ rx:0,ry:0,a:0,sx:0.8,sy:0.8 };
			this._rectangle_2_1.ggVisible=true;
			this._rectangle_2_1.className='ggskin ggskin_rectangle ';
			this._rectangle_2_1.ggType='rectangle';
			hs ='';
			hs+=cssPrefix + 'background-clip : padding-box;';
			hs+='background-clip : padding-box;';
			hs+=cssPrefix + 'border-radius : 999px;';
			hs+='border-radius : 999px;';
			hs+='border : 0px solid rgba(255,255,255,0);';
			hs+='cursor : pointer;';
			hs+='height : 32px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 32px;';
			hs+='pointer-events:auto;';
			this._rectangle_2_1.setAttribute('style',hs);
			this._rectangle_2_1.style[domTransform + 'Origin']='50% 50%';
			this._rectangle_2_1.style[domTransform]=parameterToTransform(this._rectangle_2_1.ggParameter);
			me._rectangle_2_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._rectangle_2_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._rectangle_2_1.ggCurrentLogicStateScaling = -1;
			me._rectangle_2_1.ggCurrentLogicStateAlpha = -1;
			this._rectangle_2_1.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._rectangle_2_1.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._rectangle_2_1.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._rectangle_2_1.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_1.ggCurrentLogicStateScaling == 0) {
						me._rectangle_2_1.ggParameter.sx = 0.7;
						me._rectangle_2_1.ggParameter.sy = 0.7;
						me._rectangle_2_1.style[domTransform]=parameterToTransform(me._rectangle_2_1.ggParameter);
					}
					else {
						me._rectangle_2_1.ggParameter.sx = 0.8;
						me._rectangle_2_1.ggParameter.sy = 0.8;
						me._rectangle_2_1.style[domTransform]=parameterToTransform(me._rectangle_2_1.ggParameter);
					}
				}
				var newLogicStateAlpha;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateAlpha = 0;
				}
				else {
					newLogicStateAlpha = -1;
				}
				if (me._rectangle_2_1.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
					me._rectangle_2_1.ggCurrentLogicStateAlpha = newLogicStateAlpha;
					me._rectangle_2_1.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms, opacity 500ms ease 0ms, visibility 500ms ease 0ms';
					if (me._rectangle_2_1.ggCurrentLogicStateAlpha == 0) {
						me._rectangle_2_1.style.visibility=me._rectangle_2_1.ggVisible?'inherit':'hidden';
						me._rectangle_2_1.style.opacity=1;
					}
					else {
						me._rectangle_2_1.style.visibility=me._rectangle_2_1.ggVisible?'inherit':'hidden';
						me._rectangle_2_1.style.opacity=1;
					}
				}
			}
			this._rectangle_2_1.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._svg_4_1=document.createElement('div');
			this._svg_4_1__img=document.createElement('img');
			this._svg_4_1__img.className='ggskin ggskin_svg';
			this._svg_4_1__img.setAttribute('src',basePath + 'images/svg_4_1.svg');
			this._svg_4_1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._svg_4_1__img['ondragstart']=function() { return false; };
			this._svg_4_1.appendChild(this._svg_4_1__img);
			this._svg_4_1.ggId="Svg 4_1";
			this._svg_4_1.ggLeft=-17;
			this._svg_4_1.ggTop=-5;
			this._svg_4_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._svg_4_1.ggVisible=true;
			this._svg_4_1.className='ggskin ggskin_svg ';
			this._svg_4_1.ggType='svg';
			hs ='';
			hs+='height : 23px;';
			hs+='left : -17px;';
			hs+='position : absolute;';
			hs+='top : -5px;';
			hs+='visibility : inherit;';
			hs+='width : 31px;';
			hs+='pointer-events:auto;';
			this._svg_4_1.setAttribute('style',hs);
			this._svg_4_1.style[domTransform + 'Origin']='50% 50%';
			me._svg_4_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._svg_4_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._svg_4_1.ggUpdatePosition=function (useTransition) {
				if (useTransition==='undefined') {
					useTransition = false;
				}
				if (!useTransition) {
					this.style[domTransition]='none';
				}
				if (this.parentNode) {
					var w=this.parentNode.offsetWidth;
						this.style.left=(this.ggLeft - 0 + w/2) + 'px';
					var h=this.parentNode.offsetHeight;
						this.style.top=(this.ggTop - 0 + h/2) + 'px';
				}
			}
			this._rectangle_2_1.appendChild(this._svg_4_1);
			this._ht_image_1.appendChild(this._rectangle_2_1);
			this.__div.appendChild(this._ht_image_1);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._rectangle_1_1.ggUpdateConditionTimer();
				me._rectangle_2_1.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		}
	};
	this.addSkinHotspot=function(hotspot) {
		return new SkinHotspotClass(me,hotspot);
	}
	function SkinCloner_dropdown_cloner_Class(nodeId, parent) {
		var me=this;
		this.skin=parent;
		this.player=this.skin.player;
		this.findElements=this.skin.findElements;
		this.ggNodeId=nodeId;
		this.ggUserdata=this.skin.player.getNodeUserdata(nodeId);
		this.elementMouseDown=[];
		this.elementMouseOver=[];
		this.__div=document.createElement('div');
		this.__div.setAttribute('style','position: absolute; left: 0px; top: 0px; width: 184px; height: 58px; visibility: inherit;');
		this.__div.ggIsActive = function() {
			return me.player.getCurrentNode()==me.ggNodeId;
		}
		this.__div.ggElementNodeId=function() {
			return me.ggNodeId;
		}
		this._dropdown_menu_text=document.createElement('div');
		this._dropdown_menu_text__text=document.createElement('div');
		this._dropdown_menu_text.className='ggskin ggskin_textdiv';
		this._dropdown_menu_text.ggTextDiv=this._dropdown_menu_text__text;
		this._dropdown_menu_text.ggId="Dropdown Menu Text";
		this._dropdown_menu_text.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._dropdown_menu_text.ggVisible=true;
		this._dropdown_menu_text.className='ggskin ggskin_text ';
		this._dropdown_menu_text.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 56px;';
		hs+='left : 3px;';
		hs+='position : absolute;';
		hs+='top : 1px;';
		hs+='visibility : inherit;';
		hs+='width : 175px;';
		hs+='pointer-events:auto;';
		this._dropdown_menu_text.setAttribute('style',hs);
		this._dropdown_menu_text.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 175px;';
		hs+='height: 56px;';
		hs+='background: #c2c2c2;';
		hs+='background: rgba(194,194,194,0.588235);';
		hs+='border: 0px solid #848484;';
		hs+='border-radius: 5px;';
		hs+=cssPrefix + 'border-radius: 5px;';
		hs+='color: rgba(0,0,0,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 1px 2px 1px 2px;';
		hs+='overflow: hidden;';
		this._dropdown_menu_text__text.setAttribute('style',hs);
		this._dropdown_menu_text__text.innerHTML="<br\/><span style=\"font-family: 'Open Sans', sans-serif; font-size: 14px\">"+me.ggUserdata.title+"<\/span><br \/><span style=\"font-family: 'Open Sans', sans-serif; font-size: 12px\">"+me.ggUserdata.description+"<\/span>";
		this._dropdown_menu_text.appendChild(this._dropdown_menu_text__text);
		me._dropdown_menu_text.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me._dropdown_menu_text.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this._dropdown_menu_text.onclick=function (e) {
			me.player.openNext("{"+me.ggNodeId+"}",me.player.hotspot.target);
		}
		this._dropdown_menu_text.onmouseover=function (e) {
			me.elementMouseOver['dropdown_menu_text']=true;
		}
		this._dropdown_menu_text.onmouseout=function (e) {
			me.elementMouseOver['dropdown_menu_text']=false;
		}
		this._dropdown_menu_text.ontouchend=function (e) {
			me.elementMouseOver['dropdown_menu_text']=false;
		}
		me._dropdown_menu_text.ggCurrentLogicStateBackgroundColor = -1;
		me._dropdown_menu_text.ggCurrentLogicStateTextColor = -1;
		this._dropdown_menu_text.ggUpdateConditionTimer=function () {
			var newLogicStateBackgroundColor;
			if (
				(me.elementMouseOver['dropdown_menu_text'] == true)
			)
			{
				newLogicStateBackgroundColor = 0;
			}
			else if (
				(me._dropdown_menu_text.ggIsActive() == true)
			)
			{
				newLogicStateBackgroundColor = 1;
			}
			else {
				newLogicStateBackgroundColor = -1;
			}
			if (me._dropdown_menu_text.ggCurrentLogicStateBackgroundColor != newLogicStateBackgroundColor) {
				me._dropdown_menu_text.ggCurrentLogicStateBackgroundColor = newLogicStateBackgroundColor;
				me._dropdown_menu_text__text.style[domTransition]='background-color none, color none';
				if (me._dropdown_menu_text.ggCurrentLogicStateBackgroundColor == 0) {
					me._dropdown_menu_text__text.style.backgroundColor="rgba(66,200,200,0.588235)";
				}
				else if (me._dropdown_menu_text.ggCurrentLogicStateBackgroundColor == 1) {
					me._dropdown_menu_text__text.style.backgroundColor="rgba(66,200,200,0.588235)";
				}
				else {
					me._dropdown_menu_text__text.style.backgroundColor="rgba(194,194,194,0.588235)";
				}
			}
			var newLogicStateTextColor;
			if (
				(me._dropdown_menu_text.ggIsActive() == true)
			)
			{
				newLogicStateTextColor = 0;
			}
			else if (
				(me.elementMouseOver['dropdown_menu_text'] == true)
			)
			{
				newLogicStateTextColor = 1;
			}
			else {
				newLogicStateTextColor = -1;
			}
			if (me._dropdown_menu_text.ggCurrentLogicStateTextColor != newLogicStateTextColor) {
				me._dropdown_menu_text.ggCurrentLogicStateTextColor = newLogicStateTextColor;
				me._dropdown_menu_text__text.style[domTransition]='background-color none, color none';
				if (me._dropdown_menu_text.ggCurrentLogicStateTextColor == 0) {
					me._dropdown_menu_text__text.style.color="rgba(255,255,255,1)";
				}
				else if (me._dropdown_menu_text.ggCurrentLogicStateTextColor == 1) {
					me._dropdown_menu_text__text.style.color="rgba(0,0,0,1)";
				}
				else {
					me._dropdown_menu_text__text.style.color="rgba(0,0,0,1)";
				}
			}
		}
		this._dropdown_menu_text.ggUpdatePosition=function (useTransition) {
		}
		this.__div.appendChild(this._dropdown_menu_text);
	};
	this.addSkin();
};