<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Виртуальный тур");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Виртуальный тур</h1>
            </div>
        </div>
        <script type="text/javascript" src="pano2vr_player.js"></script>
        <script type="text/javascript" src="skin.js"></script>
        <div class="vtour">
            <div id="container" style="width:100%;height:100%;overflow:hidden;">
                <br>Loading...<br><br>
                This content requires HTML5 with CSS3 3D Transforms or WebGL.
            </div>
        </div>
        <script type="text/javascript">
            // create the panorama player with the container
            pano=new pano2vrPlayer("container");
            // add the skin object
            skin=new pano2vrSkin(pano);
            // load the configuration
            window.addEventListener("load", function() {
            pano.readConfigUrlAsync("pano.xml");
            });
        </script>
        <noscript>
            <p><b>Please enable Javascript!</b></p>
        </noscript>
        <div style="width:1px;height:1px;"></div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
