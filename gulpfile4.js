const gulp = require('gulp'),
    babel       = require('gulp-babel'),
    prefixer    = require('gulp-autoprefixer'),
	terser      = require('gulp-terser'),
    sass        = require('gulp-sass'),
    include     = require('gulp-include'),
    cleanCSS    = require('gulp-clean-css'),
    rename      = require('gulp-rename'),
    browserSync = require('browser-sync'),
    through     = require('through2'),
    fs          = require('fs'),
    path        = require('path'),
    glob        = require('glob'),
    hash        = require('gulp-hash'),
    svgSprite   = require('gulp-svg-sprites'),
    svgmin      = require('gulp-svgmin'),
    cheerio     = require('gulp-cheerio'),
    replace     = require('gulp-replace');

const PATH = {
    DEV: __dirname,
    SRC: {
        PHP: '**/*.php',
        JS: ['**/main.js', '!bitrix/**', '!lib', '!partials', '!node_modules/**'],
        STYLE: ['**/main.scss', '!bitrix/**', '!lib', '!partials', '!node_modules/**'],
    },
    WATCH: {
        JS: ['**/main.js', '!bitrix/**', '!node_modules/**'],
        STYLE: ['**/main.scss', '!bitrix/**', '!node_modules/**'],
        SVG: ['/images/icon/*.svg'],
        VENDORS: {
            JS: ['js/lib/**', 'js/partials/**'],
            STYLE: ['css/lib/**', 'css/partials/**'],
        }
    },
};

const EXTERNAL_LINK_PARTS = ['.pdf', '.doc', '.docx', 'http://', 'https://'];

const JS_FILE_REGEXP_TEMPLATE = /main.min.js/g;
const CSS_FILE_REGEXP_TEMPLATE = /main.min.css/g;

const FILE_HASH_TEMPLATE = '<%= name %>.min<%= ext %>';

const PHP_FILES = ['index.php', 'detail.php'];
const MAIN_JS_FILE = 'main.js';
const MAIN_STYLE_FILE = 'main.scss';

const FILE = {
    PHP: ['index.php', 'detail.php'],
    JS: 'main.js',
    STYLE: 'main.scss',
    SVG: '/images/icon/*.svg',
    PATH: {
        HEADER: '../../template/header-includes.php',
        FOOTER: '../../template/footer-includes.php',
    }
};

const CHANGE_EVENT = 'change';

function getPhpFiles(chunkPath, mainFolder, mainTemplate) {
    return chunkPath.includes(mainFolder)
        ? [path.resolve(chunkPath, mainTemplate)]
        : FILE.PHP.map(phpFile => path.resolve(chunkPath, '..', phpFile));
}

function changeTemplateFiles(phpFiles, fileName, template, withMessage) {
    phpFiles.forEach(phpFile => {
        if (fs.existsSync(phpFile)) {
            changeTemplateFile(phpFile, fileName, template);

            if (withMessage) console.log(`*** Изменен PHP файл: ${phpFile} ***`);
        }
    });
}

function changeTemplateFile(sourceFile, destFileName, template) {
    fs.readFile(sourceFile, 'utf8', (err, data) => {
        if (err) return console.log(err);

        const result = data.replace(template, destFileName);
        fs.writeFile(sourceFile, result, 'utf8', err => {
            if (err) return console.log(err);
        });
    });
}

function removeBuilds(folder, template) {
    fs.readdir(folder, (error, files) => {
        if (error) throw console.log(err);

        files
            .filter(name => template.test(name))
            .forEach(name => fs.unlinkSync(path.resolve(folder, name)));
    });
}

function buildJs(src, dest, withMessage) {
    return gulp.src(src)
        .pipe(include())
            .on('error', console.log)       
        .pipe(hash({ template: FILE_HASH_TEMPLATE }))
    /*    .pipe(through.obj((chunk, enc, cb) => {
        	chunkPath = chunk.path.replace(/\\/g,"/");
            var jsFolder = path.resolve(chunkPath, '..');
            var jsFileName = path.basename(chunkPath);

            var phpFile = chunkPath.includes('/js/')
                ? path.resolve(chunkPath, '../../template/footer-includes.php')
                : path.resolve(chunkPath, '../index.php');

            removeBuilds(jsFolder, JS_FILE_REGEXP_TEMPLATE);
         //   changeTemplateFile(phpFile, jsFileName, JS_FILE_REGEXP_TEMPLATE, withMessage);
            cb(null, chunk);
		})) */
	//	.pipe(babel())
		.pipe(terser())
        .pipe(gulp.dest(dest))
        .pipe(browserSync.stream());
}

function buildCss(src, dest, withMessage) {   
    return gulp.src(src)
        .pipe(sass({ errLogToConsole: true }))
        .pipe(prefixer())
        .pipe(cleanCSS())
        .pipe(hash({ template: FILE_HASH_TEMPLATE }))
    /*    .pipe(through.obj((chunk, enc, cb) => {
        	chunkPath = chunk.path.replace(/\\/g,"/");
            var cssFolder = path.resolve(chunkPath, '..');
            var cssFileName = path.basename(chunkPath);

            var phpFile = chunkPath.includes('/css/')
                ? path.resolve(chunkPath, '../../template/header-includes.php')
                : path.resolve(chunkPath, '../index.php');

            removeBuilds(cssFolder, CSS_FILE_REGEXP_TEMPLATE);
        //    changeTemplateFile(phpFile, cssFileName, CSS_FILE_REGEXP_TEMPLATE, withMessage); 
            cb(null, chunk); 
        })) */
        .pipe(gulp.dest(dest))
        .pipe(browserSync.stream());
}

function checkIfLinkIsExternal(link) {
    let result;

    EXTERNAL_LINK_PARTS.forEach(part => {
        if (link.includes(part)) result = true;
    });

    return result;
}

function checkLinkCorrectness(link) {
    const HREF_REG_EXP = /href="(.*?)"/g;
    const TARGET_REG_EXP = /target="(.*?)"/g;

    let match = HREF_REG_EXP.exec(link);
    if (!match) return false;

    const href = match[1];

    if (!checkIfLinkIsExternal(href)) return true;

    match = TARGET_REG_EXP.exec(link);
    if (!match) return false;

    const target = match[1];

    return target === '_blank';
}

function parseLinks(content) {
    const LINK_REG_EXP = /<a\b[^>]*>([\s\S]*?)<\/a>/gm;

    let match;
    let links = [];
    while (match = LINK_REG_EXP.exec(content)) {
        links.push(match[0]);
    }

    return links;
}

gulp.task('links:check', async function() {
    await glob(PATH.SRC.PHP, {}, function (err, files) {
        if (err) throw err;

        files.forEach(file => {
            const content = fs.readFileSync(__dirname + '/' + file, 'utf8');

            const links = parseLinks(content);

            links.forEach(link => {
                if (!checkLinkCorrectness(link)) {
                    console.log('*** ПОДОЗРИТЕЛЬНАЯ ССЫЛКА ***');
                    console.log('Файл: ' + file);
                    console.log('Тэг: ' + link);
                    console.log('--------------------');
                }
            });
        })
    });
});

gulp.task('svgSpriteBuild', function () {
    return gulp.src('images/icon/*.svg')
        // minify svg
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill and style declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        // cheerio plugin create unnecessary string '>', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
                mode: "symbols",
                preview: false,
                selector: "icon-%f",
                svg: {
                    symbols: 'svg_sprite.php'
                }
            }
        ))
        .pipe(gulp.dest('images/'));
});

gulp.task('js:build', () => buildJs(PATH.SRC.JS, PATH.DEV));

gulp.task('js:watch', () => gulp.watch(PATH.WATCH.JS).on(CHANGE_EVENT, (file, stats) => {
    console.log(`*** Изменен JS файл: ${file} ***`);
    const src = path.resolve(file);
    buildJs(src, path.resolve(file, '..'), true);
}));

gulp.task('css:build', () => buildCss(PATH.SRC.STYLE, PATH.DEV));

gulp.task('css:watch', () => gulp.watch(PATH.WATCH.STYLE).on(CHANGE_EVENT, (file, stats) => {
    console.log(`*** Изменен CSS файл: ${file} ***`);
    const src = path.resolve(file);
    buildCss(src, path.resolve(file, '..'), true); 
}));

gulp.task('svg:watch', function(){
    gulp.watch('images/icon/*.svg', gulp.series('svgSpriteBuild'));
});

gulp.task('js:vendor:watch', () => gulp.watch(PATH.WATCH.VENDORS.JS).on(CHANGE_EVENT, (file, stats) => {
    console.log(`*** Изменен JS файл: ${file} ***`);

	const filePath = path.resolve(file, '../..', FILE.JS);

    buildJs(filePath, path.resolve(filePath, '..'), true);
}));

gulp.task('css:vendor:watch', () => gulp.watch(PATH.WATCH.VENDORS.STYLE).on(CHANGE_EVENT, (file, stats) => {
    console.log(`*** Изменен CSS файл: ${file} ***`);

    const filePath = path.resolve(file, '../..', FILE.STYLE);
    buildCss(filePath, path.resolve(filePath, '..'), true);
}));

/* gulp.task('project:build', ['js:build', 'css:build']);

gulp.task('project:watch', ['js:watch', 'css:watch', 'svg:watch', 'css:vendor:watch', 'js:vendor:watch']);

gulp.task('project:run', () => runSequence('project:build', 'links:check', 'svgSpriteBuild','project:watch')); */

gulp.task('project:build', gulp.parallel('js:build', 'css:build'));
gulp.task('project:watch', gulp.parallel('js:watch', 'css:watch', 'svg:watch', 'css:vendor:watch', 'js:vendor:watch'));
gulp.task('project:run', gulp.series('project:build', 'links:check', 'svgSpriteBuild', 'project:watch')); 

/* чтоб не падал ватчер на убунте, надо ввести команду 

$ sudo echo fs.inotify.max_user_watches=500000 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

*/
