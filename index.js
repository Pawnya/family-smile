$(function() {
    new Vue({
        el: '#app',
        name: 'app',
        data() {
            return {
                options: {
                    menu:      '#menu',
                    scrollBar: true,
                    anchors:   ['page1', 'page2', 'page3', 'page4', 'page5', 'page6'],
                    lockAnchors: true,
                    recordHistory: false,
                    verticalCentered: false,
                    normalScrollElements: '.footer',
                    responsiveWidth: 1190
                },
                advantages: [
                    {
                        icon: 'monitor',
                        title: 'Всестороннее обследование',
                        text: 'Лечение начинается с осмотра, компьютерной, мануальной и функциональной диагностики'
                    },
                    {
                        icon: 'team',
                        title: 'Командный подход',
                        text: 'Каждый участник процесса решает вашу проблему с учетом опыта врачей смежных специальностей'
                    },
                    {
                        icon: 'cloud',
                        title: 'Внимательное отношение',
                        text: 'Мы постарались создать в клинике максимально комфортную атмосферу как для взрослых, так и для детей'
                    },
                    {
                        icon: 'doctor',
                        title: 'Разные врачи',
                        text: 'У нас работают специалисты всех направлений стоматологии, которые решат любую задачу'
                    },
                    {
                        icon: 'quality',
                        title: 'Современные материалы',
                        text: 'Мы применяем исключительно проверенные материалы от европейских производителей'
                    }
                ],
                infographics: [
                    {
                        icon: 'implant',
                        title: 'Планирование имплантации',
                        text: 'Только томография позволяет оценить объем и состояние костной ткани для надежного крепления имплантата'
                    },
                    {
                        icon: 'hiryrgiya',
                        title: 'Удаление проблемных зубов',
                        text: 'Доктор видит положение многокорневых зубов, в том числе лежачие «восьмерки», и точно планирует операцию'
                    },
                    {
                        icon: 'terapiya',
                        title: 'Эффективное терапевтическое лечение',
                        text: 'На томограмме отлично видны любые скрытые кариозные полости и анатомически сложные каналы зубов'
                    },
                    {
                        icon: 'ortopediya',
                        title: 'Составление плана лечения у ортодонта',
                        text: 'Для правильного расчета силы давления конструкций нужны точные данные о состоянии костной ткани, положении зубов и корней'
                    },
                    {
                        icon: 'kista',
                        title: 'Выявление кистозных новообразований',
                        text: 'В начале заболевания киста видна только на снимке — исследование помогает распознать и точно локализировать проблему'
                    },
                    {
                        icon: 'gnatologiya',
                        title: 'Оценка состояния костей при травмах',
                        text: 'На томограмме видна костная ткань челюсти, височно-нижнечелюстной сустав, а также кости всего лица'
                    }
                ],
                menus: [],
                title: 'Все специалисты в одном месте',
                text: 'У нас есть врачи всех стоматологических направлений. Это очень удобно. В организме все взаимосвязано, поэтому зачастую для полного восстановления вашего здоровья требуется единовременная консультация нескольких специалистов.',
                href: ''
            }
        },
        methods: {
            classActive: function(el) {
                this.title = el.target.text
                this.href = el.target.href
                var submenu = []
                this.text = el.target.nextElementSibling.textContent
                if (el.srcElement.nextElementSibling !== null) {
                    var link = el.srcElement.nextElementSibling.querySelectorAll('.nav-submenu__title')
                    link.forEach(function(elem,index) {
                        this.menus = Object.assign({}, this.menus, {
                            href: elem.href,
                            text: elem.text
                        })
                        submenu.push(this.menus)
                    })
                    this.$set(this.menus, 0, submenu)
                } else {
                    this.$delete(this.menus, 0)
                }
            }
        }
    });

    if (document.body.clientWidth < 1050) {
        fullpage_api.destroy();
    }
});
