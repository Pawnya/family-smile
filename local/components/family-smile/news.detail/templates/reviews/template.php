<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="reviews-detail">
    <div class="reviews-detail-photo">
        <?if (!empty($arResult['PROPERTIES']['VIDEO']['VALUE'])):?>
            <video src="<?=$CFile::GetPath($arResult['PROPERTIES']['VIDEO']['VALUE'])?>" poster="posterimage.jpg" class="reviews__img reviews__img_full">
            </video>
        <?elseif (!empty($arResult['PREVIEW_PICTURE'])):?>
            <img src="<?=$arResult['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arResult['NAME']?>" class="reviews__img reviews__img_full">
        <?else:?>
            <img src="/images/reviews.png" alt="<?=$arResult['NAME']?>" class="reviews__img">
        <?endif;?>
    </div>
    <div class="reviews-slide">
        <p class="reviews__text mb-25"><?=$arResult['DETAIL_TEXT']?></p>
        <p class="reviews__ps mb-25"><?=$arResult['ACTIVE_FROM']?> /  <?=$arResult['NAME']?></p>
    </div>
</div>
