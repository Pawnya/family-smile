<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="works-detail">
    <div class="works-detail-photo swiper-container">
        <div class="works-detail-wrapper swiper-wrapper">
            <?foreach ($arResult['PROPERTIES']['STAGES']['VALUE'] as $arStage): ?>
                <div class="works-detail-slide swiper-slide">
                    <img src="<?=CFile::GetPath($arStage)?>" alt="">
                </div>
            <?endforeach; ?>
        </div>
    </div>
    <div class="works-detail-thumb swiper-container">
        <div class="works-detail-wrapper swiper-wrapper">
            <?foreach ($arResult['PROPERTIES']['STAGES']['VALUE'] as $arStage): ?>
                <div class="works-detail-thumb-slide swiper-slide">
                    <img src="<?=CFile::GetPath($arStage)?>" alt="">
                </div>
            <?endforeach; ?>
        </div>
    </div>
    <div class="works-item-desc">
        <p class="works__subtitle subtitle"><?=$arResult['PROPERTIES']['SECTION']['VALUE']?></p>
        <h3 class="works__title mb-30"><?=$arResult['NAME']?></h3>
    </div>
</div>
