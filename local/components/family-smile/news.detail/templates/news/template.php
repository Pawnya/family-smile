<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="doctors-detail">
    <div class="section-title mb-50">
        <h2 class="h2 mb-15"><?=$arResult['NAME']?></h2>
    </div>
    <div class="block-img block-img_full doctors-detail-img">
    <? if(!empty($arResult['DETAIL_PICTURE']['SRC'])){?>
        <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['NAME']?>" class="block__img_full">
    <? } ?>
    </div>
    <div class="mb-50 news-detail__content">                
        <?=$arResult['DETAIL_TEXT'];?>
    </div>
</div>
