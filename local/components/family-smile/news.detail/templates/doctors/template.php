<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="doctors-detail">
    <div class="block-img block-img_full doctors-detail-img">
        <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['NAME']?>" class="block__img_full">
    </div>
    <div class="doctors-detail-header">
        <h2 class="h2 mb-15"><?=$arResult['NAME']?></h2>
        <p class="subtitle"><?=$arResult['PROPERTIES']['SPECIALIZATION']['VALUE']?></p>
    </div>
    <div class="doctors-education block">
        <div class="block-wrapper pr-0-xs pr-90-xl">
            <?if($arResult['PROPERTIES']['EDUCATION']['~VALUE']){ ?>
                <div class="doctors-block mb-80">
                    <h3 class="h3 mb-40">Образование:</h3>
                    <?=$arResult['PROPERTIES']['EDUCATION']['~VALUE']['TEXT']?>
                </div>
            <?}?>
            <?if($arResult['PROPERTIES']['EXPERIENCE']['~VALUE']){ ?>
                <div class="doctors-block">
                    <h3 class="h3 mb-40">Опыт работы:</h3>
                    <?=$arResult['PROPERTIES']['EXPERIENCE']['~VALUE']['TEXT']?>
                </div>
            <?}?>
        </div>
        <?if($arResult['PROPERTIES']['EDUCATION_ADDITIONAL']['~VALUE']){ ?>
            <div class="block-wrapper mt-80-xs mt-0-xl">
                <div class="doctors-block">
                    <h3 class="h3 mb-40">Дополнительное образование:</h3>
                    <?=$arResult['PROPERTIES']['EDUCATION_ADDITIONAL']['~VALUE']['TEXT']?>
                </div>
            </div>
        <?}?>
    </div>
    <?if ($arResult['PROPERTIES']['DIPLOMA']['VALUE']){?>
        <div class="section doctors-sertificate">
            <h2 class="h2 mb-70">Дипломы</h2>
            <div class="license">
                <div class="license-slider swiper-container">
                    <div class="license-wrapper swiper-wrapper">
                        <?foreach ($arResult['PROPERTIES']['DIPLOMA']['VALUE'] as $arDiploma): ?>
                            <div class="license-slide swiper-slide">
                                <div class="license-item">
                                    <a href="<?=CFile::GetPath($arDiploma)?>" data-fancybox="diploma">
                                        <img src="<?=CFile::GetPath($arDiploma)?>" alt="">
                                    </a>
                                </div>
                            </div>
                        <?endforeach; ?>
                    </div>
                </div>
                <div class="arrow-container arrow-container_license">
                    <div class="arrow arrow_prev arrow-license_prev"></div>
                    <div class="arrow arrow_next arrow-license_next"></div>
                </div>
            </div>
        </div>
    <?}?>
    <?if ($arResult['PROPERTIES']['PATENT']['VALUE']){?>
        <div class="section doctors-sertificate">
            <h2 class="h2 mb-70">Патенты</h2>
            <div class="license">
                <div class="license-slider swiper-container">
                    <div class="license-wrapper swiper-wrapper">
                        <?foreach ($arResult['PROPERTIES']['PATENT']['VALUE'] as $arPatent): ?>
                            <div class="license-slide swiper-slide">
                                <div class="license-item">
                                    <a href="<?=CFile::GetPath($arPatent)?>" data-fancybox="patent">
                                        <img src="<?=CFile::GetPath($arPatent)?>" alt="">
                                    </a>
                                </div>
                            </div>
                        <?endforeach; ?>
                    </div>
                </div>
                <div class="arrow-container arrow-container_license">
                    <div class="arrow arrow_prev arrow-license_prev"></div>
                    <div class="arrow arrow_next arrow-license_next"></div>
                </div>
            </div>
        </div>
    <?}?>
    <?if ($arResult['PROPERTIES']['SERTIFICATE']['VALUE']){?>
        <div class="section doctors-sertificate">
            <h2 class="h2 mb-70">Сертификаты</h2>
            <div class="license">
                <div class="license-slider swiper-container">
                    <div class="license-wrapper swiper-wrapper">
                        <?foreach ($arResult['PROPERTIES']['SERTIFICATE']['VALUE'] as $arSert): ?>
                            <div class="license-slide swiper-slide">
                                <div class="license-item">
                                    <a href="<?=CFile::GetPath($arSert)?>" data-fancybox="sertificate">
                                        <img src="<?=CFile::GetPath($arSert)?>" alt="">
                                    </a>
                                </div>
                            </div>
                        <?endforeach; ?>
                    </div>
                </div>
                <div class="arrow-container arrow-container_license">
                    <div class="arrow arrow_prev arrow-license_prev"></div>
                    <div class="arrow arrow_next arrow-license_next"></div>
                </div>
            </div>
        </div>
    <?}?>
    <?/*
        <div class="section doctors-works">
            <h2 class="h2 mb-70">Работы врача</h2>
            <div class="works">
                <div class="works-slider swiper-container">
                    <div class="works-wrapper swiper-wrapper">
                        <?
                            $arSelect = Array();
                            $arFilter = Array("IBLOCK_ID"=>3, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arResult['ID']);
                            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
                            while($ob = $res->GetNextElement())
                            {
                                $arFields = $ob->GetFields();
                                $arProps = $ob->GetProperties();?>
                                <div class="works-slide swiper-slide">
                                    <div class="works-item">
                                        <div class="works-item-photo">
                                            <div class="works-item-photo-item works-item-photo_before">
                                                <img src="<?=CFile::GetPath($arProps['PHOTO_BEFORE']['VALUE'])?>" alt="<?=$arProps['PHOTO_BEFORE']['NAME']?>" class="works-item-photo__img">
                                            </div>
                                            <div class="works-item-photo-item works-item-photo_after">
                                                <img src="<?=CFile::GetPath($arProps['PHOTO_AFTER']['VALUE'])?>" alt="<?=$arProps['PHOTO_AFTER']['NAME']?>" class="works-item-photo__img">
                                            </div>
                                        </div>
                                        <div class="works-item-desc">
                                            <p class="subtitle"><?=$arProps['SECTION']['VALUE']?></p>
                                            <h4 class="h4 mb-30"><?=$arFields['NAME']?></h4>
                                            <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="box__link"></a>
                                        </div>
                                    </div>
                                </div><?
                            }
                        ?>
                    </div>
                </div>
                <div class="arrow-container arrow-container_works">
                    <div class="arrow arrow_prev"></div>
                    <div class="arrow arrow_next"></div>
                </div>
            </div>
        </div>
    */?>
</div>
