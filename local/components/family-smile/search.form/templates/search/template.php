<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="footer-search">
    <form class="footer-search-form" action="<?=$arResult["FORM_ACTION"]?>">
        <input type="text" name="text" value="" placeholder="Поиск" class="footer-search__input">
        <input type="submit" name="s" class="footer-search__btn search__btn" value="">
    </form>
</div>
