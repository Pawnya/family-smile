<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="nav-mobile">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<div class="nav-item">
                <div class="nav-link">
                    <a href="<?=$arItem["LINK"]?>" class="nav__link"><?=$arItem["TEXT"]?></a>
                    <button class="nav-mobile__arrow" type="button" name="button" aria-label="Arrow"></button>
                </div>
				<div class="nav-submenu">
                    <button class="nav-mobile__close" type="button" name="button" aria-label="Close"></button>
		<?else:?>
			<div class="nav-submenu-item">
                <div class="nav-submenu-title">
                    <a href="<?=$arItem["LINK"]?>" class="nav-submenu__title"><?=$arItem["TEXT"]?></a>
                    <button class="nav-mobile__arrow" type="button" name="button" aria-label="Arrow"></button>
                </div>
				<div class="nav-submenu-list">
                    <button class="nav-mobile__close" type="button" name="button" aria-label="Close"></button>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<div class="nav-item">
                    <a href="<?=$arItem["LINK"]?>" class="nav__link"><?=$arItem["TEXT"]?></a>
                </div>
			<?elseif ($arItem["DEPTH_LEVEL"] > 2):?>
                    <a href="<?=$arItem["LINK"]?>" class="nav-submenu__link"><?=$arItem["TEXT"]?></a>
                <?else:?>
                    <div class="nav-submenu-item">
                        <a href="<?=$arItem["LINK"]?>" class="nav-submenu__title mb-0-xs mb-20-md"><?=$arItem["TEXT"]?></a>
                    </div>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</div></div>", ($previousLevel-1) );?>
<?endif?>

</div>
<?endif?>
