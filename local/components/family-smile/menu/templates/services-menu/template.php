<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="services-menu">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<div class="services-item hide">
                <a href="<?=$arItem["LINK"]?>" @click="classActive" class="subtitle services__link"><?=$arItem["TEXT"]?></a>
				<div class="nav-submenu">
		<?else:?>
			<div class="nav-submenu-item">
                <a href="<?=$arItem["LINK"]?>" class="nav-submenu__title"><?=$arItem["TEXT"]?></a>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<div class="services-item hide">
                    <a href="<?=$arItem["LINK"]?>" @click="classActive" class="subtitle services__link"><?=$arItem["TEXT"]?></a>
                    <p class="services__info hide"><?=$arItem['PARAMS']['INFO']?></p>
                </div>
            <?else:?>
                <div class="nav-submenu-item">
                    <a href="<?=$arItem["LINK"]?>" class="nav-submenu__title mb-20"><?=$arItem["TEXT"]?></a>
                </div>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</div></div>", ($previousLevel-1) );?>
<?endif?>

</div>
<?endif?>
