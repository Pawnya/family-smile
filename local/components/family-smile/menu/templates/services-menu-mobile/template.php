<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="services-m-wrap swiper-wrapper">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<div class="services-m-item swiper-slide">
                <a href="<?=$arItem["LINK"]?>" class="btn services__btn">подробнее</a>
                <div class="services-m-link">
                    <a href="<?=$arItem["LINK"]?>" class="subtitle services-m__link"><?=$arItem["TEXT"]?></a>
                </div>
				<div class="services-m-sub">
		<?else:?>
			<div class="services-m-sub-item">
                <a href="<?=$arItem["LINK"]?>" class="services-m__title"><?=$arItem["TEXT"]?></a>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<div class="services-m-item swiper-slide">
                    <a href="<?=$arItem["LINK"]?>" class="btn services__btn">подробнее</a>
                    <div class="services-m-link">
                        <a href="<?=$arItem["LINK"]?>" class="subtitle services-m__link"><?=$arItem["TEXT"]?></a>
                    </div>
                    <p class="services-m__info"><?=$arItem['PARAMS']['INFO']?></p>
                </div>
            <?else:?>
                <div class="services-m-sub-item">
                    <a href="<?=$arItem["LINK"]?>" class="services-m__title"><?=$arItem["TEXT"]?></a>
                </div>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</div></div>", ($previousLevel-1) );?>
<?endif?>

</div>
<div class="pagination-container pagination-container_services">
    <div class="pagination pagination_services swiper-pagination"></div>
</div>
<?endif?>
