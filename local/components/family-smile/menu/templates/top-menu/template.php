<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="nav-list">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<div class="nav-item">
                <a href="<?=$arItem["LINK"]?>" class="nav__link"><?=$arItem["TEXT"]?></a>
				<div class="nav-submenu">
		<?else:?>
			<div class="nav-submenu-item">
                <a href="<?=$arItem["LINK"]?>" class="nav-submenu__title"><?=$arItem["TEXT"]?></a>
				<div class="nav-submenu-list">
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<div class="nav-item">
                    <a href="<?=$arItem["LINK"]?>" class="nav__link"><?=$arItem["TEXT"]?></a>
                </div>
			<?elseif ($arItem["DEPTH_LEVEL"] > 2):?>
                    <a href="<?=$arItem["LINK"]?>" class="nav-submenu__link"><?=$arItem["TEXT"]?></a>
                <?else:?>
                    <div class="nav-submenu-item">
                        <a href="<?=$arItem["LINK"]?>" class="nav-submenu__title mb-20"><?=$arItem["TEXT"]?></a>
                    </div>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</div></div>", ($previousLevel-1) );?>
<?endif?>

</div>
<?endif?>
