<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<? $val = 1; ?>


<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
		continue;
?>
	<?if($arItem["SELECTED"]):?>
        <div class="box-slide swiper-slide">
            <div class="box-item">
                <div class="box-img box-img_menu">
                    <img src="images/1-<?=$val?>.png" alt="box-menu<?=$val?>" class="box__img">
                </div>
                <div class="box-desc">
                    <p class="box__title"><?=$arItem['TEXT']?></p>
                    <p class="text box__text"><?=$arItem['PARAMS']['INFO']?></p>
                    <a href="<?=$arItem['LINK']?>" class="box__link"></a>
                </div>
            </div>
        </div>
	<?else:?>
        <div class="box-slide swiper-slide">
            <div class="box-item">
                <div class="box-img box-img_menu">
                    <img src="images/1-<?=$val?>.png" alt="box-menu<?=$val?>" class="box__img">
                </div>
                <div class="box-desc">
                    <p class="box__title"><?=$arItem['TEXT']?></p>
                    <p class="text box__text"><?=$arItem['PARAMS']['INFO']?></p>
                    <a href="<?=$arItem['LINK']?>" class="box__link"></a>
                </div>
            </div>
        </div>
	<?endif?>
    <?$val++;?>

<?endforeach?>
<?endif?>
