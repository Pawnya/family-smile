<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<?
$val = 1;
$previousLevel = 0;
foreach($arResult as $arItem):?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div></div></div></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<div class="box-slide swiper-slide">
                <div class="box-item">
                    <div class="box-img box-img_menu">
                        <img src="images/1-<?=$val;?>.png" alt="box-menu<?=$val;?>" class="box__img">
                    </div>
                    <div class="box-desc">
                        <p class="box__title"><?=$arItem['TEXT']?></p>
                        <a href="<?=$arItem['LINK']?>" class="box__link"></a>
                        <div class="box-submenu">
		<?else:?>
			<div class="box-submenu-item">
                <a href="<?=$arItem["LINK"]?>" class="box-submenu__title"><?=$arItem["TEXT"]?></a>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<div class="box-slide swiper-slide">
                    <div class="box-item">
                        <div class="box-img box-img_menu">
                            <img src="images/1-<?=$val;?>.png" alt="box-menu<?=$val;?>" class="box__img">
                        </div>
                        <div class="box-desc">
                            <p class="box__title"><?=$arItem['TEXT']?></p>
                            <p class="text box__text"><?=$arItem['PARAMS']['INFO']?></p>
                            <a href="<?=$arItem['LINK']?>" class="box__link"></a>
                        </div>
                    </div>
                </div>
            <?else:?>
                <div class="box-submenu-item">
                    <a href="<?=$arItem["LINK"]?>" class="box-submenu__title"><?=$arItem["TEXT"]?></a>
                </div>
			<?endif?>

		<?endif?>

	<?endif?>

	<?
        $previousLevel = $arItem["DEPTH_LEVEL"];
        if ($arItem['DEPTH_LEVEL'] == 1) {
            $val++;
        }
    ?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</div></div></div></div>", ($previousLevel-1) );?>
<?endif?>
<?endif?>
