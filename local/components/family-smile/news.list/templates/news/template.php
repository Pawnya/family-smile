<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="doctors swiper-container news">
    <div class="doctors-wrapper swiper-wrapper news-wrapper">
        <?foreach ($arResult['ITEMS'] as $arItem): ?>
            <div class="doctors-slide swiper-slide active">
                <div class="doctors-item">
                    <div class="doctors-img doctors-img_doctors">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['CODE']?>" class="doctors__img">
                    </div>
                    <div class="doctors-desc">
                        <p class="doctors__title"><?=$arItem['NAME']?></p>
                        <p class="text news__text"><?=$arItem['~PREVIEW_TEXT']?></p>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="doctors__link"></a>
                    </div>
                </div>
            </div>
        <?endforeach; ?>
    </div>
    <div class="pagination-container pagination-container_doctors">
        <div class="pagination pagination_doctors swiper-pagination"></div>
    </div>
</div>
