<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="license-wrapper swiper-wrapper">
    <?foreach ($arResult['ITEMS'] as $arItem): ?>
        <div class="license-slide swiper-slide">
            <div class="license-item">
                <a href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" data-fancybox="gallery">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                </a>
            </div>
        </div>
    <?endforeach; ?>
</div>
