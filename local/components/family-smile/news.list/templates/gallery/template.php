<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="gallery-slider swiper-container">
    <div class="gallery-slider-wrapper swiper-wrapper">
        <?$i = 1;?>
        <?foreach ($arResult['ITEMS'] as $arItem): ?>
            <div class="gallery-slider-item swiper-slide">
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>-<?=$i;?>" class="gallery-slider__img">
            </div>
        <?endforeach; ?>
    </div>
    <div class="gallery-arrows">
        <div class="gallery-arrow gallery-arrow_prev"></div>
        <div class="gallery-arrow gallery-arrow_next"></div>
    </div>
    <div class="swiper-pagination pagination_gallery"></div>
</div>
<div class="gallery-thumbs swiper-container">
    <div class="swiper-wrapper">
        <?$val = 1;?>
        <?foreach ($arResult['ITEMS'] as $arItem): ?>
            <div class="gallery-thumbs-item swiper-slide">
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>-<?=$val;?>" class="gallery-thumbs__img">
            </div>
            <?$val++;?>
        <?endforeach; ?>
    </div>
</div>
