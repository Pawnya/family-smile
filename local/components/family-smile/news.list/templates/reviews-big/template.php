<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach ($arResult['ITEMS'] as $arItem): ?>
    <div class="reviews-slide swiper-slide">
        <div class="reviews-img">
            <?if (!empty($arItem['PROPERTIES']['VIDEO']['VALUE'])):?>
                <video src="<?=$CFile::GetPath($arItem['PROPERTIES']['VIDEO']['VALUE'])?>" poster="posterimage.jpg" class="reviews__img">
                </video>
            <?elseif (!empty($arItem['PREVIEW_PICTURE'])):?>
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" class="reviews__img">
            <?else:?>
                <img src="/images/reviews.png" alt="<?=$arItem['NAME']?>" class="reviews__img">
            <?endif;?>
        </div>
        <p class="reviews__text mb-25"><?=$arItem['PREVIEW_TEXT']?></p>
        <p class="reviews__ps mb-25"><?=$arItem['ACTIVE_FROM']?> /  <?=$arItem['NAME']?></p>
        <a href="/reviews/" class="reviews__link link_section">Все отзывы</a>
    </div>
<?endforeach; ?>
