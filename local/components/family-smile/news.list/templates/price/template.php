<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="price">
    <?
        $tree = CIBlockSection::GetTreeList(
        $arFilter=Array('IBLOCK_ID' => 7),
        $arSelect=Array()
        );
        while($section = $tree->GetNext())
        { ?>
            <div class="price-item">
                <div class="price-link">
                    <h3 class="h3"><?=$section['NAME']?></h3>
                    <button class="box__link price__btn" aria-label="Button"></button>
                </div>
                <div class="price-list">
                <?
                    $arSelect = Array();
                    $arFilter = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","SECTION_ID"=>$section['ID'] );
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $arProps  = $ob->GetProperties();
                        ?>
                            <div class="price-list-item">
                                <div class="price-name">
                                    <p class="price-name__title"><?=$arFields['NAME']?></p>
                                </div>
                                <div class="price-cost">
                                    <span class="price__cost"><?=number_format($arProps['PRICE']['VALUE'], 0, ',', ' ');?> ₽</span>
                                </div>
                            </div>
                        <?
                    }
                ?>
                </div>
            </div>
            <?
        }
    ?>
</div>
