<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="works-slider">
    <div class="works-wrapper">
        <?foreach ($arResult['ITEMS'] as $arItem): ?>
            <div class="works-slide active" data-category="<?=$arItem['PROPERTIES']['SECTION']['VALUE_XML_ID']?>">
                <div class="works-item">
                    <?if (empty($arItem['PROPERTIES']['HORIZONTAL']['VALUE'])):?>
                        <div class="works-item-photo">
                            <div class="works-item-photo-item works-item-photo_before">
                                <img src="<?=CFile::GetPath($arItem['PROPERTIES']['PHOTO_BEFORE']['VALUE'])?>" alt="<?=$arItem['PROPERTIES']['PHOTO_BEFORE']['VALUE']?>" class="works-item-photo__img">
                            </div>
                            <div class="works-item-photo-item works-item-photo_after">
                                <img src="<?=CFile::GetPath($arItem['PROPERTIES']['PHOTO_AFTER']['VALUE'])?>" alt="<?=$arItem['PROPERTIES']['PHOTO_AFTER']['VALUE']?>" class="works-item-photo__img">
                            </div>
                        </div>
                    <?else:?>
                        <div class="works-item-photo works-item-photo_horizontal">
                            <div class="works-item-photo-item works-item-photo_before">
                                <img src="<?=CFile::GetPath($arItem['PROPERTIES']['PHOTO_BEFORE']['VALUE'])?>" alt="<?=$arItem['PROPERTIES']['PHOTO_BEFORE']['VALUE']?>" class="works-item-photo__img">
                            </div>
                            <div class="works-item-photo-item works-item-photo_after">
                                <img src="<?=CFile::GetPath($arItem['PROPERTIES']['PHOTO_AFTER']['VALUE'])?>" alt="<?=$arItem['PROPERTIES']['PHOTO_AFTER']['VALUE']?>" class="works-item-photo__img">
                            </div>
                        </div>
                    <?endif;?>
                    <div class="works-item-desc">
                        <p class="subtitle"><?=$arItem['PROPERTIES']['SECTION']['VALUE']?></p>
                        <h4 class="h4 mb-30"><?=$arItem['NAME']?></h4>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="box__link"></a>
                    </div>
                </div>
            </div>
        <?endforeach; ?>
    </div>
</div>
