<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<!doctype html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no"/>
	    <title><?$APPLICATION->ShowTitle()?></title>
	    <?$APPLICATION->ShowHead();?>
	    <?$GLOBALS["formCounter"] = 0;?>
	    <?require($_SERVER["DOCUMENT_ROOT"]."/template/header-includes.php");?>
        <?require($_SERVER["DOCUMENT_ROOT"]."/images/svg_sprite.php");?>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#2d89ef">
        <meta name="theme-color" content="#ffffff">
	</head>
	<body>
		<?$APPLICATION->ShowPanel();?>
        <header class="header">
            <div class="container header-container">
                <a href="/" class="header-logo">
                    <img src="/images/logo.svg" alt="Family-Smile" class="header-logo__img">
                </a>
                <nav class="nav">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "top-menu",
                        Array(
                            "ROOT_MENU_TYPE"        => "menu",
                            "MAX_LEVEL"             => "3",
                            "CHILD_MENU_TYPE"       => "submenu",
                            "USE_EXT"               => "Y",
                            "DELAY"                 => "N",
                            "ALLOW_MULTI_SELECT"    => "Y",
                            "MENU_CACHE_TYPE"       => "N",
                            "MENU_CACHE_TIME"       => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS"   => ""
                        )
                    );?>
                    <a href="/vtour/" class="nav__link nav__link_tour">3D тур</a>
                </nav>
                <div class="header-contacts">
                    <div class="header-contacts-phone">
                        <p class="header-contacts__phone"><span class="callibri_little_phone1">8 (4812)</span>
                            <a href="tel:84812305444" class="header-contacts__link callibri_little_phone3">305-444</a>
                            <a href="tel:84812305445" class="header-contacts__link callibri_little_phone2">305-445</a>
                        </p>
                    </div>
                    <div class="header-contacts-address">
                        <p class="header-contacts__address">г.Смоленск, ул.Воробьева, д.11/9</p>
                        <button class="header-contacts__pin" type="button" name="button">
                            <img src="/images/icon/address.svg" alt="ping">
                        </button>
                        <div class="header-contacts-modal">
                            <button class="nav-mobile__close" type="button" name="button"></button>
                            <p>г.Смоленск, ул.Воробьева, д.11/9</p>
                        </div>
                    </div>
                </div>
                <div class="header-star">
                    <a href="http://www.e-stomatology.ru/" target="_blank"><img src="/images/center-star.png" alt="ii-center-star" class="header-star__img"></a>
                </div>
                <button class="mobile-menu-btn" aria-label="Burger menu"></button>
                <div class="mobile-menu-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "top-menu-mobile",
                        Array(
                            "ROOT_MENU_TYPE"        => "menu",
                            "MAX_LEVEL"             => "3",
                            "CHILD_MENU_TYPE"       => "submenu",
                            "USE_EXT"               => "Y",
                            "DELAY"                 => "N",
                            "ALLOW_MULTI_SELECT"    => "Y",
                            "MENU_CACHE_TYPE"       => "N",
                            "MENU_CACHE_TIME"       => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS"   => ""
                        )
                    );?>
                </div>
            </div>
        </header>
