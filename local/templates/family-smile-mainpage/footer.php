<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
		<script type="text/javascript" src="/js/lib/jquery.min.3.3.1.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue"></script>
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script defer src="/index.js"></script>
		<script defer src="/js/lib/vue-fullpage.min.js"></script>
		<?require($_SERVER["DOCUMENT_ROOT"]."/template/footer-includes.php");?>
		<div class="disclamer">
			<div class="disclamer-text">Сайт использует cookie и аналогичные технологии для удобного и корректного отображения
				информации. Пользуясь нашим сервисом, вы соглашаетесь с их использованием.</div>
			<div class="disclamer-link">
				<a href="/yuridicheskaya-informatsiya/doc/10.pdf">Узнать подробнее</a>
				<div class="disclamer-link__btn">Согласен</div>
			</div>
		</div>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function () {
					try {
						w.yaCounter44327014 = new Ya.Metrika({
							id: 44327014,
							clickmap: true,
							trackLinks: true,
							accurateTrackBounce: true,
							webvisor: true
						});
					} catch (e) {
					}
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () {
						n.parentNode.insertBefore(s, n);
					};
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else {
					f();
				}
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript>
			<div><img src="https://mc.yandex.ru/watch/44327014" style="position:absolute; left:-9999px;" alt=""/></div>
		</noscript>
		<!-- /Yandex.Metrika counter -->

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135196807-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-135196807-1');
		</script>
		<!-- callibry -->
		<script src="//cdn.callibri.ru/callibri.js" type="text/javascript" charset="utf-8"></script>
	</body>
</html>
