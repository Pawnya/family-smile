<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
		<footer class="footer">
			<div class="container">
				<div class="footer-list">
					<div class="footer-menu">
						<a href="/sitemap/" class="footer__link">Карта сайта</a>
						<a href="#" class="footer__link bvi-panel-open-menu">Для слабовидящих</a>
						<a href="/yuridicheskaya-informatsiya/" class="footer__link">Юридическая информация</a>
					</div>
					<?$APPLICATION->IncludeComponent(
						"family-smile:search.form",
						"search",
						Array(
							"USE_SUGGEST" => "N",
							"PAGE"        => "#SITE_DIR#search/index.php"
							)
					);?>
				</div>
				<div class="footer-bottom">
					<p class="footer__copyright">© ООО «Family Smile», 2019</p>
					<div class="footer-social">
						<a href="https://vk.com/club156410488" class="footer-social__link footer-social__link_vk" target="_blank">
							<svg class="icon icon-vk">
								<use xlink:href="#icon-vk"></use>
							</svg>
						</a>
						<a href="https://www.facebook.com/familysmile67/" class="footer-social__link footer-social__link_fb" target="_blank">
							<svg class="icon icon-fb">
								<use xlink:href="#icon-fb"></use>
							</svg>
						</a>
						<a href="https://www.instagram.com/family_smile_smolensk/" class="footer-social__link footer-social__link_inst" target="_blank">
							<svg class="icon icon-inst">
								<use xlink:href="#icon-inst"></use>
							</svg>
						</a>
					</div>
					<a href="https://pro100dental.ru" class="footer-agency" target="_blank">
						<img src="/images/icon/pro100.svg" alt="pro100">
					</a>
				</div>
			</div>
		</footer>
		<div class="scroll-top"></div>
		<script type="text/javascript" src="/js/lib/jquery.min.3.3.1.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue"></script>
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<?require($_SERVER["DOCUMENT_ROOT"]."/template/footer-includes.php");?>
		<div class="disclamer">
			<div class="disclamer-text">Сайт использует cookie и аналогичные технологии для удобного и корректного отображения
				информации. Пользуясь нашим сервисом, вы соглашаетесь с их использованием.</div>
			<div class="disclamer-link">
				<a href="/yuridicheskaya-informatsiya/doc/10.pdf">Узнать подробнее</a>
				<div class="disclamer-link__btn">Согласен</div>
			</div>
		</div>
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function () {
					try {
						w.yaCounter44327014 = new Ya.Metrika({
							id: 44327014,
							clickmap: true,
							trackLinks: true,
							accurateTrackBounce: true,
							webvisor: true
						});
					} catch (e) {
					}
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () {
						n.parentNode.insertBefore(s, n);
					};
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else {
					f();
				}
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript>
			<div><img src="https://mc.yandex.ru/watch/44327014" style="position:absolute; left:-9999px;" alt=""/></div>
		</noscript>
		<!-- /Yandex.Metrika counter -->

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135196807-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-135196807-1');
		</script>
		<!-- callibry -->
		<script src="//cdn.callibri.ru/callibri.js" type="text/javascript" charset="utf-8"></script>
	</body>
</html>
