<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Врачи");
?>
<link rel="stylesheet" href="main.min.css">
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Наши специалисты</h1>
            </div>
        </div>
        <div class="filter-list">
            <a href="#" class="filter__link filter__link_all filter__link_active" data-specialization="all">Все</a>
            <?
            CModule::IncludeModule('iblock');

            $property_enums = CIBlockPropertyEnum::GetList(
                Array(
                    "DEF"  => "DESC",
                    "SORT" => "ASC"
                ),
                Array(
                    "IBLOCK_ID" => 4,
                    "CODE"      => "CATEGORY"
                    )
                );
                while($enum_fields = $property_enums->GetNext()) {
                    $dataID = $enum_fields['EXTERNAL_ID'];
                    $dataName = $enum_fields['VALUE'];
                    echo '<a href="#" class="filter__link" data-specialization="'.$dataID.'">'.$dataName.'</a>';
                }
                ?>
        </div>
        <div class="doctors-container"></div>
    </div>
</section>
<script defer src="main.min.js"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
