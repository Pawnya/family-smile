<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Врачи");
?>
<link rel="stylesheet" href="/doctors/main.min.css">
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Наши специалисты</h1>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent(
            "family-smile:news.detail",
            "doctors",
            Array(
                "DISPLAY_DATE"              => "Y",
                "DISPLAY_NAME"              => "Y",
                "DISPLAY_PICTURE"           => "Y",
                "DISPLAY_PREVIEW_TEXT"      => "Y",
                "USE_SHARE"                 => "Y",
                "SHARE_HIDE"                => "N",
                "SHARE_TEMPLATE"            => "",
                "SHARE_HANDLERS"            => array("delicious"),
                "SHARE_SHORTEN_URL_LOGIN"   => "",
                "SHARE_SHORTEN_URL_KEY"     => "",
                "AJAX_MODE"                 => "N",
                "IBLOCK_TYPE"               => "content",
                "IBLOCK_ID"                 => "doctors",
                "ELEMENT_ID"                => $_REQUEST["ELEMENT_ID"],
                "ELEMENT_CODE"              => $_REQUEST["CODE"],
                "CHECK_DATES"               => "Y",
                "FIELD_CODE"                => Array("ID"),
                "PROPERTY_CODE"             => Array("SECTION","DOCTORS"),
                "IBLOCK_URL"                => "",
                "DETAIL_URL"                => "",
                "SET_TITLE"                 => "Y",
                "SET_CANONICAL_URL"         => "Y",
                "SET_BROWSER_TITLE"         => "Y",
                "BROWSER_TITLE"             => "-",
                "SET_META_KEYWORDS"         => "Y",
                "META_KEYWORDS"             => "-",
                "SET_META_DESCRIPTION"      => "Y",
                "META_DESCRIPTION"          => "-",
                "SET_STATUS_404"            => "Y",
                "SET_LAST_MODIFIED"         => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN"        => "N",
                "ADD_ELEMENT_CHAIN"         => "Y",
                "ACTIVE_DATE_FORMAT"        => "d.m.Y",
                "USE_PERMISSIONS"           => "N",
                "GROUP_PERMISSIONS"         => Array("1"),
                "CACHE_TYPE"                => "A",
                "CACHE_TIME"                => "3600",
                "CACHE_GROUPS"              => "Y",
                "DISPLAY_TOP_PAGER"         => "Y",
                "DISPLAY_BOTTOM_PAGER"      => "Y",
                "PAGER_TITLE"               => "Врачи",
                "PAGER_TEMPLATE"            => "",
                "PAGER_SHOW_ALL"            => "Y",
                "PAGER_BASE_LINK_ENABLE"    => "Y",
                "SHOW_404"                  => "Y",
                "MESSAGE_404"               => "",
                "STRICT_SECTION_CHECK"      => "Y",
                "PAGER_BASE_LINK"           => "",
                "PAGER_PARAMS_NAME"         => "arrPager",
                "AJAX_OPTION_JUMP"          => "N",
                "AJAX_OPTION_STYLE"         => "Y",
                "AJAX_OPTION_HISTORY"       => "N"
            )
        );?>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Если у вас остались какие-то вопросы, вы можете задать их нашему специалисту. Или запишитесь на прием, и врач ответит на все вопросы во время консультации. Приходите в стоматологию FamilySmile, и ваша улыбка будет всегда здоровой и красивой!</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<script defer src="/doctors/main.min.js"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
