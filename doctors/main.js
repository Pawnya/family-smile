$(function() {
    loadData();
    $('.filter__link').click(function(e) {
        e.preventDefault();
        var $dataText = $(this).text(),
            $dataSpec = $(this).data('specialization');
        $('.filter__link').removeClass('filter__link_active');
        $(this).addClass('filter__link_active');
        loadData($dataText);

    });

    var license = new Swiper('.license-slider', {
        slidesPerView: 3,
        loop: true,
        speed: 1000,
        spaceBetween: 22,
        breakpoints: {
            768: {
                slidesPerView:   1,
                centeredSlides: true
            },
            1200: {
                slidesPerView:   2
            }
        }
    });

    var btnPrev = document.querySelectorAll('.arrow-license_prev'),
        btnNext = document.querySelectorAll('.arrow-license_next');

    btnPrev.forEach(function(item, index) {
        item.onclick = function() {
            if (license.length > 1) {
                license[index].slidePrev(2000);
            } else {
                license.slidePrev(2000);
            }
        }
    });

    btnNext.forEach(function(item, index) {
        item.onclick = function() {
            if (license.length > 1) {
                license[index].slideNext(2000);
            } else {
                license.slideNext(2000);
            }
        }
    });
});

function loadData(dataSpec) {
    $.ajax({
        type: 'POST',
        url: '/doctors/load.php',
        data: { category: dataSpec, mode: 'ajax' },
        success:function(data) {
            $('.doctors-container').html(data);
        }
    });
}
