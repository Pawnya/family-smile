<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $section = $_POST['category'];
    if ($_POST['category'] == "Все") {
        $section = "";
    }
}
$GLOBALS['arrFilter'] = array("PROPERTY_CATEGORY_VALUE" => $section);
if($_REQUEST['mode']=='ajax')
   $APPLICATION->RestartBuffer();
$APPLICATION->IncludeComponent(
    "family-smile:news.list",
    "doctors",
    Array(
        "DISPLAY_DATE"                    => "Y",
        "DISPLAY_NAME"                    => "Y",
        "DISPLAY_PICTURE"                 => "Y",
        "DISPLAY_PREVIEW_TEXT"            => "Y",
        "AJAX_MODE"                       => "N",
        "IBLOCK_TYPE"                     => "content",
        "IBLOCK_ID"                       => "doctors",
        "NEWS_COUNT"                      => "50",
        "SORT_BY1"                        => "ACTIVE_FROM",
        "SORT_ORDER1"                     => "DESC",
        "SORT_BY2"                        => "SORT",
        "SORT_ORDER2"                     => "ASC",
        "FILTER_NAME"                     => "arrFilter",
        "FIELD_CODE"                      => Array("ID"),
        "PROPERTY_CODE"                   => Array("DESCRIPTION"),
        "CHECK_DATES"                     => "Y",
        "DETAIL_URL"                      => "",
        "PREVIEW_TRUNCATE_LEN"            => "",
        "ACTIVE_DATE_FORMAT"              => "d.m.Y",
        "SET_TITLE"                       => "N",
        "SET_BROWSER_TITLE"               => "N",
        "SET_META_KEYWORDS"               => "Y",
        "SET_META_DESCRIPTION"            => "Y",
        "SET_LAST_MODIFIED"               => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
        "ADD_SECTIONS_CHAIN"              => "Y",
        "HIDE_LINK_WHEN_NO_DETAIL"        => "Y",
        "PARENT_SECTION"                  => "",
        "PARENT_SECTION_CODE"             => "",
        "INCLUDE_SUBSECTIONS"             => "Y",
        "CACHE_TYPE"                      => "N",
        "CACHE_TIME"                      => "3600",
        "CACHE_FILTER"                    => "Y",
        "CACHE_GROUPS"                    => "Y",
        "DISPLAY_TOP_PAGER"               => "Y",
        "DISPLAY_BOTTOM_PAGER"            => "Y",
        "PAGER_TITLE"                     => "Новости",
        "PAGER_SHOW_ALWAYS"               => "Y",
        "PAGER_TEMPLATE"                  => "",
        "PAGER_DESC_NUMBERING"            => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL"                  => "N",
        "PAGER_BASE_LINK_ENABLE"          => "N",
        "SET_STATUS_404"                  => "Y",
        "SHOW_404"                        => "Y",
        "MESSAGE_404"                     => "",
        "PAGER_BASE_LINK"                 => "",
        "PAGER_PARAMS_NAME"               => "arrPager",
        "AJAX_OPTION_JUMP"                => "N",
        "AJAX_OPTION_STYLE"               => "Y",
        "AJAX_OPTION_HISTORY"             => "N",
        "AJAX_OPTION_ADDITIONAL"          => ""
    )
);
?>
<script type="text/javascript">
    var doctors = new Swiper('.doctors', {
        init:            false,
        speed:           1000,
        slidesPerView:   1,
        slidesPerColumn: 1,
        touchRatio:      1,
        simulateTouch:   true,
        loop:            true,
        pagination: {
            el:                '.pagination_doctors',
            type:              'bullets',
            bulletClass:       'pagination-dot',
            bulletActiveClass: 'active',
            clickable:         true
        }
    });
    if (document.body.clientWidth < 1050) {
        doctors.init();
    }
</script>
