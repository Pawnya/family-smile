<?
$arUrlRewrite = array(
    array(
        "CONDITION" => "#^/doctors/(.*)/\$#",
        "RULE" => "CODE=\$1",
        "ID" => "",
        "PATH" => "/doctors/detail.php",
    ),
    array(
        "CONDITION" => "#^/about/our-works/(.*)/\$#",
        "RULE" => "CODE=\$1",
        "ID" => "",
        "PATH" => "/about/our-works/detail.php",
    ),
    array(
        "CONDITION" => "#^/reviews/(.*)/\$#",
        "RULE" => "CODE=\$1",
        "ID" => "",
        "PATH" => "/reviews/detail.php",
    ),
    array(
        "CONDITION" => "#^/about/news/(.*)/\$#",
        "RULE" => "CODE=\$1",
        "ID" => "",
        "PATH" => "/about/news/detail.php",
    ),
);

?>
