<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Юридическая информация");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Юридическая информация</h1>
            </div>
        </div>
        <div class="legal">
            <div class="legal-item">
                <a href="doc/1.1.pdf" class="legal__link" target="_blank">Юридическое название организации</a>
            </div>
            <div class="legal-item">
                <a href="doc/1.pdf" class="legal__link" target="_blank">Учредитель, его дата рождения</a>
            </div>
            <div class="legal-item">
                <a href="doc/1.3.pdf" class="legal__link" target="_blank">Режим и график работы</a>
            </div>
            <div class="legal-item">
                <a href="doc/2.pdf" class="legal__link" target="_blank">Структура компании</a>
            </div>
            <div class="legal-item">
                <a href="doc/3.pdf" class="legal__link" target="_blank">Правила внутреннего распорядка для пациентов</a>
            </div>
            <div class="legal-item">
                <a href="doc/4.pdf" class="legal__link" target="_blank">Положение о правилах оказания платных медицинских услуг</a>
            </div>
            <div class="legal-item">
                <a href="doc/5.pdf" class="legal__link" target="_blank">Правила подготовки к диагностическим исследованиям, рентгенологическому исследованию</a>
            </div>
            <div class="legal-item">
                <a href="doc/6.pdf" class="legal__link" target="_blank">Контакты контролирующих органов</a>
            </div>
            <div class="legal-item">
                <a href="doc/7.pdf" class="legal__link" target="_blank">Прайс-лист клиники</a>
            </div>
            <div class="legal-item">
                <a href="doc/8.pdf" class="legal__link" target="_blank">Лицензия на право ведения медицинской деятельности</a>
            </div>
            <div class="legal-item">
                <a href="doc/9.pdf" class="legal__link" target="_blank">Свидетельство о постановке на учет в налоговом органе</a>
            </div>
            <div class="legal-item">
                <a href="doc/10.pdf" class="legal__link" target="_blank">Политика в отношении обработки и защиты персональных данных</a>
            </div>
            <div class="legal-item">
                <a href="doc/11.pdf" class="legal__link" target="_blank">Перечень жизненно необходимых и важнейших лекарственных препаратов</a>
            </div>
            <div class="legal-item">
                <a href="doc/12.pdf" class="legal__link" target="_blank">О Территориальной программе государственных гарантий бесплатного оказания гражданам медицинской помощи в Смоленской области на 2018 год и на плановый период 2019 и 2020 годов</a>
            </div>
            <div class="legal-item">
                <a href="doc/13.pdf" class="legal__link" target="_blank">О сроке, порядке, результатах диспансеризации населения</a>
            </div>
            <div class="legal-item">
                <a href="doc/14.pdf" class="legal__link" target="_blank">Правила записи на первичный прием</a>
            </div>
            <div class="legal-item">
                <a href="doc/15.pdf" class="legal__link" target="_blank">Сведения о страховых медицинских организациях</a>
            </div>
            <div class="legal-item">
                <a href="doc/16.pdf" class="legal__link" target="_blank">Права и обязанности граждан РФ в сфере здравоохранения</a>
            </div>
            <div class="legal-item">
                <a href="doc/17.pdf" class="legal__link" target="_blank">О программе государственных гарантий бесплатного оказания гражданам медицинской помощи на 2018 год (постановление правительства рф от 19.12.2015 № 1382)</a>
            </div>
            <div class="legal-item">
                <a href="doc/18.pdf" class="legal__link" target="_blank">Вакансии</a>
            </div>
            <div class="legal-item">
                <a href="doc/19.pdf" class="legal__link" target="_blank">О правилах и сроках госпитализации</a>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
