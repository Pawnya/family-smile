<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");

?>
<section class="section section-padding_inner section-margin_bottom">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner found">
            <div class="found-img">
                <img src="/images/404.png" alt="404 not found" class="found__img mb-80">
            </div>
            <h1 class="h1 mb-30">Страница не найдена</h1>
            <p class="text mb-50">Если вы ввели адрес вручную в адресной строке браузера, проверьте, всё ли вы написали правильно.<br> Если вы пришли по ссылке с другого ресурса, попробуйте перейти на <a href="/">главную страницу</a>, вполне вероятно, что там вы найдёте нужный вам материал.</p>
            <a href="/" class="btn">На главную страницу</a>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
