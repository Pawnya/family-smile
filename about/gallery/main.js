$(function() {
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween:          60,
        slidesPerView:         4,
        freeMode:              true,
        loop:                  true,
        watchSlidesVisibility: true,
        watchSlidesProgress:   true,
        loopedSlides:          5,
        breakpoints: {
            320: {
                slidesPerView: 1,
                simulateTouch: false
            },
            768: {
                slidesPerView: 2
            },
            1200: {
                slidesPerView: 3
            }
        }
    });
    var galleryTop = new Swiper('.gallery-slider', {
        spaceBetween: 10,
        loop:         true,
        thumbs: {
            swiper: galleryThumbs
        },
        pagination: {
            el:                 '.swiper-pagination',
            type:               'bullets',
            clickable:          true,
            dynamicBullets:     true,
            dynamicMainBullets: 5
        },
        loopedSlides: 5
    });
    $('.gallery-arrow_prev').click(function() {
        galleryTop.slidePrev(2000);
    });
    $('.gallery-arrow_next').click(function() {
        galleryTop.slideNext(2000);
    });
});
