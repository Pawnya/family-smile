$(function() {
    loadData();
    $('.filter__link').click(function(e) {
        e.preventDefault();
        var $dataText = $(this).text(),
            $dataSpec = $(this).data('specialization');
        $('.filter__link').removeClass('filter__link_active');
        $(this).addClass('filter__link_active');
        loadData($dataText);

    });

    var galleryThumbs = new Swiper('.works-detail-thumb', {
        spaceBetween: 20,
        slidesPerView: 5,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            320: {
                slidesPerView: 1
            },
            768: {
                slidesPerView: 2
            },
            1200: {
                slidesPerView: 3
            }
        }
    });
    var galleryTop = new Swiper('.works-detail-photo', {
        spaceBetween: 10,
        thumbs: {
            swiper: galleryThumbs
        }
    });
});

function loadData(dataSpec) {
    $.ajax({
        type: 'POST',
        url: '/about/our-works/load.php',
        data: { category: dataSpec, mode: 'ajax' },
        success:function(data) {
            $('.works-container').html(data);
        }
    });
}
