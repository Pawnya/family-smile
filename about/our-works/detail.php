<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши работы");
?>
<link rel="stylesheet" href="/about/our-works/main.min.css">
<script defer src="/about/our-works/main.min.js"></script>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Наши работы</h1>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent(
            "family-smile:news.detail",
            "our-works",
            Array(
                "DISPLAY_DATE"              => "Y",
                "DISPLAY_NAME"              => "Y",
                "DISPLAY_PICTURE"           => "Y",
                "DISPLAY_PREVIEW_TEXT"      => "Y",
                "USE_SHARE"                 => "Y",
                "SHARE_HIDE"                => "N",
                "SHARE_TEMPLATE"            => "",
                "SHARE_HANDLERS"            => array("delicious"),
                "SHARE_SHORTEN_URL_LOGIN"   => "",
                "SHARE_SHORTEN_URL_KEY"     => "",
                "AJAX_MODE"                 => "N",
                "IBLOCK_TYPE"               => "content",
                "IBLOCK_ID"                 => "works",
                "ELEMENT_ID"                => $_REQUEST["ELEMENT_ID"],
                "ELEMENT_CODE"              => $_REQUEST["CODE"],
                "CHECK_DATES"               => "Y",
                "FIELD_CODE"                => Array("ID"),
                "PROPERTY_CODE"             => Array("SECTION","DOCTORS"),
                "IBLOCK_URL"                => "",
                "DETAIL_URL"                => "",
                "SET_TITLE"                 => "Y",
                "SET_CANONICAL_URL"         => "Y",
                "SET_BROWSER_TITLE"         => "Y",
                "BROWSER_TITLE"             => "-",
                "SET_META_KEYWORDS"         => "Y",
                "META_KEYWORDS"             => "-",
                "SET_META_DESCRIPTION"      => "Y",
                "META_DESCRIPTION"          => "-",
                "SET_STATUS_404"            => "Y",
                "SET_LAST_MODIFIED"         => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN"        => "N",
                "ADD_ELEMENT_CHAIN"         => "Y",
                "ACTIVE_DATE_FORMAT"        => "d.m.Y",
                "USE_PERMISSIONS"           => "N",
                "GROUP_PERMISSIONS"         => Array("1"),
                "CACHE_TYPE"                => "A",
                "CACHE_TIME"                => "3600",
                "CACHE_GROUPS"              => "Y",
                "DISPLAY_TOP_PAGER"         => "Y",
                "DISPLAY_BOTTOM_PAGER"      => "Y",
                "PAGER_TITLE"               => "Работы",
                "PAGER_TEMPLATE"            => "",
                "PAGER_SHOW_ALL"            => "Y",
                "PAGER_BASE_LINK_ENABLE"    => "Y",
                "SHOW_404"                  => "Y",
                "MESSAGE_404"               => "",
                "STRICT_SECTION_CHECK"      => "Y",
                "PAGER_BASE_LINK"           => "",
                "PAGER_PARAMS_NAME"         => "arrPager",
                "AJAX_OPTION_JUMP"          => "N",
                "AJAX_OPTION_STYLE"         => "Y",
                "AJAX_OPTION_HISTORY"       => "N"
            )
        );?>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
