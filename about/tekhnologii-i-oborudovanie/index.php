<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Технологии и оборудование");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Применяем передовое оборудование для эффективного лечения</h1>
                </div>
                <p class="text mb-30">Для качественного лечения необходимо не только мастерство врачей, но и современное оснащение клиники. Стоматология FamilySmile соответствует этим критериям. Наши специалисты повышают свою квалификацию и осваивают новейшие методики. Также у нас установлено передовое оборудование, которое позволяет добиться надежного результата на долгие годы. Благодаря инновационным технологиям мы восстановим ваше здоровье и вернем прежний комфорт!</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="main-inner">
                <div class="section-title">
                    <h2 class="h2 mb-40">Тщательное обследование</h2>
                </div>
                <p class="text">Наши специалисты — профессионалы в своем деле. Они регулярно проходят курсы в учебном центре клиники и участвуют в зарубежных конференциях, семинарах. Врачи следят за нововведениями в области стоматологии, а также мастерски умеют работать на современном оборудовании для диагностики и лечения.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-90-xl order-2">
                <h2 class="h2 mb-40">Компьютерный томограф KaVo Pan Exam Plus 3D (Германия)</h2>
                <p class="text">Это самый современный диагностический аппарат. 3D-обследование достаточно информативное – врач изучает зубочелюстную систему, верхнечелюстные пазухи и носовую перегородку. Полученные снимки актуальны для терапевта, пародонтолога, хирурга, ортопеда и отоларинголога. </p>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества обследования на компьютерном томографе</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-90-xl order-2">
                <h2 class="h2 mb-40">Визиограф Gendex Expert DC</h2>
                <p class="text">Инновационный датчик, который позволяет провести точное рентгенологическое обследование одного зуба. Прицельный снимок показывает скрытый кариес и воспаления, строение каналов. Еще один плюс такой диагностики в том, что она проходит, пока вы сидите в кресле, — вам не нужно никуда идти. Аппарат установлен в кабинете врача.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/4.png" alt="block4" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2 mb-40">Фотодиагностика</h2>
                <p class="text">Доктор фотографирует вас на первичном приеме в различных ракурсах. Затем, используя специальное ПО, выявляет все особенности симметрии, непропорциональность лица, а также зубов, прикуса и осанки. Результат выводится на экран телевизора или монитора, врач обсуждает с вами план восстановления здоровья. Такое обследование применяется и для контроля лечения: специалист наглядно показывает вам, какие манипуляции он провел.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container">
            <div class="section-title mb-80">
                <h2 class="h2">Аккуратное и качественное лечение зубов</h2>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Бинокуляры</h2>
                <p class="text">Это специальный прибор, который увеличивает рабочую зону до 5–6 раз. Врач видит зуб намного отчетливее, во всех подробностях. Это помогает специалисту провести аккуратное, качественное лечение и идеальную реставрацию зуба. Кроме того, наши доктора используют бинокуляры при осмотре, проведении профессиональной гигиены полости рта, удалении и восстановлении зубов. С такой оптикой мы гарантируем долговечный и эстетичный результат.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/5.png" alt="block5" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/6.png" alt="block6" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2 mb-40">Микроскоп</h2>
                <p class="text">При лечении кариеса и обработке зубов под ортопедические конструкции наши врачи применяют мощную оптику — стоматологический микроскоп Seiler Evolution XR6 (США). Он увеличивает рабочую область до 30 раз. Зуб и его тонкие каналы можно рассмотреть во всех деталях, а значит, доктор работает не на ощупь, а предельно точно. Увеличительное оборудование просто необходимо для тщательного удаления кариеса, качественной обработки каналов и герметичного пломбирования. А если в вашем случае понадобится препарировать зубы, чтобы зафиксировать виниры или коронки, то специалисты проведут процедуру очень аккуратно и точно — с оптикой.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества микроскопа</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <blockquote class="blockquote mb-50">Стоматологический микроскоп позволяет проводить лечение с ювелирной точностью и помогает спасти зубы даже в самых сложных случаях.</blockquote>
                            <div class="btn-wrapper">
                                <?$APPLICATION->IncludeComponent(
                                    "family-smile:form.result.new",
                                    "inline",
                                    Array(
                                        "WEB_FORM_ID"            => "1",
                                        "AJAX_OPTION_STYLE"      => "Y",
                                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                                        "USE_EXTENDED_ERRORS"    => "Y",
                                        "SEF_MODE"               => "N",
                                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                        "CACHE_TYPE"             => "N",
                                        "CACHE_TIME"             => "3600",
                                        "LIST_URL"               => "",
                                        "EDIT_URL"               => "",
                                        "SUCCESS_URL"            => "",
                                        "CHAIN_ITEM_TEXT"        => "",
                                        "CHAIN_ITEM_LINK"        => "",
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-50-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Стоматологический микроскоп позволяет проводить лечение с ювелирной точностью и помогает спасти зубы даже в самых сложных случаях.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Эндомотор X-Smart Dentsply Maillefer (Швейцария)</h2>
                <p class="text">Это специальный прибор с удобной панелью управления и гибким наконечником. Врач выбирает нужные настройки — аппарат работает аккуратно, контролируя скорость вращения. Эндомотор помогает тщательно очищать даже изогнутые каналы.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/7.png" alt="block7" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-img block-img_tleft">
                <img src="images/8.png" alt="block8" class="block__img">
            </div>
            <div class="block-wrapper">
                <h2 class="h2 mb-40">Компьютерная анестезия QuickSleeper 5 (Франция)</h2>
                <p class="text">Аппарат автоматически рассчитывает дозу анестетика. При этом прибор контролирует скорость подачи препарата — вы ничего не почувствуете. Эффект обезболивания наступает сразу после введения анестетика. Такая надежная система помогает ощущать себя комфортно не только взрослым, но и детям. Процедура с компьютерной анестезией пройдет для вашего ребенка совершенно безболезненно, при этом врач сможет вылечить сразу несколько зубов. К тому же аппарат не похож на шприц, а значит, малыш не будет бояться.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Водно-абразивная система Aquacut Quattro Velopex (Великобритания)</h2>
                <p class="text">Это специальный прибор, действие которого основано на энергии абразивных частиц и потока воды. С помощью аппарата можно бережно препарировать зуб, удалять зубной камень, налет, а также реставрировать пломбу. Система бесшумная и не соприкасается с зубом — нет неприятных ощущений.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/9.png" alt="block9" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/10.png" alt="block10" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Лампа VALO Ultradent (США)</h2>
                <p class="text">Фотополимеризационная лампа применятся для реставрации зубов с помощью композитных материалов. Лампа очень мощная, может работать в разных режимах. Врач сам выбирает нужную интенсивность света, направляет прибор на пломбу, и она быстро затвердевает. Аппарат легкий, удобный в использовании. Благодаря лампе и материалу специалист идеально воссоздает анатомию зуба всего за один прием. </p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container">
            <div class="section-title mb-80">
                <h2 class="h2">Эффективное лечение десен</h2>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Vector Paro Durr Dental (Германия)</h2>
                <p class="text">Современный ультразвуковой аппарат, который устраняет воспаление десен и укрепляет мягкие ткани. У прибора есть специальная насадка, которая безболезненно очищает зубодесневые карманы. Она совершает возвратно-поступательные движения и подает поток жидкости с очищающими частицами. Энергия ультразвука снимает микробную пленку и аккуратно полирует поверхность корня.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/11.png" alt="block11" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Плюсы Vector-терапии</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="plus in pluss">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + plus.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ plus.title }}</p>
                            <p class="text">{{ plus.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner">
                <p class="text mb-50">После лечения с помощью Vector десны становятся более плотными, обретают здоровый розовый оттенок, также проходят кровоточивость и отечность.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/12.png" alt="block12" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">FotoSan 630 (Дания)</h2>
                <p class="text">Это светодиодная лампа для фотодинамической терапии. Процедура светоактивируемой дезинфекции мягких тканей проводится после лечения аппаратом Vector. Она закрепляет эффект и не допускает повторного развития воспаления. Как проходит процесс? В зубодесневые карманы закладывается специальный гель, который воздействует только на проблемные участки. Дезинфицирующее вещество активируется от света — освобождается кислород, который устраняет бактерии и разрушает биопленку.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 mb-40">Плазмолифтинг</h2>
                <p class="text">Современная методика для восстановления клеток мягких тканей. Сначала врач забирает небольшое количество вашей крови, она обрабатывается (отделяется плазма), и после специалист делает инъекции в десны. После такой процедуры проходит кровоточивость и неприятный запах изо рта, уменьшается подвижность зубов, восстанавливается естественный цвет и форма десен. Также плазмолифтинг применяют для скорейшего заживления после челюстно-лицевых операций.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Центрифуга универсальная EВА-200 Hettich (Германия)</h2>
                <p class="text">Устройство оснащено панелью управления, 8-местным угловым ротором для размещения пробирок объемом до 15 мл, а также датчиком вращения с широким диапазоном. Такое оборудование необходимо при плазмолифтинге. Именно в центрифуге плазма отделяется от крови.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/13.png" alt="block13" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/14.png" alt="block14" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Термостат PT-Plasmolifting Gel</h2>
                <p class="text">Это компактный прибор для приготовления естественного геля из вашей плазмы. Как проходит процесс? Сначала плазма обрабатывается в центрифуге, потом на 12 минут помещается в специальный шприц, который устанавливается в термостат. Оборудование простое в использовании: есть дисплей, таймер и регулятор температуры.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container">
            <div class="section-title mb-80">
                <h2 class="h2">Эффективная профилактика стоматологических заболеваний </h2>
            </div>
        </div>
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Многофункциональный аппарат Air-Flow Master Piezon (Швейцария)</h2>
                <p class="text">У прибора есть несколько наконечников, подсветка и два режима работы. Под давлением из тонкой насадки выходит поток воды, смешанный с абразивным порошком. Процедуру проводят для удаления мягкого налета после ультразвуковой чистки. Всего за один визит состояние десен значительно улучшится, эмаль станет намного белее, а зубы будут гладкими, что надежно защищает от кариеса.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/15.png" alt="block15" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container">
            <div class="section-title mb-80">
                <h2 class="h2">Белоснежная улыбка всего за один визит</h2>
            </div>
        </div>
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/16.png" alt="block16" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Philips Zoom WhiteSpeed (Zoom 4)</h2>
                <p class="text">Новейшая система осветления эмали. На зубы наносится безопасный отбеливающий гель, затем направляется светодиодная лампа. Под действием света препарат активируется — выделяются атомы кислорода. Они устраняют пигментацию и эффективно изменяют цвет эмали. Улыбка кардинально преобразится, так как зубы станут светлее на 11 тонов. При правильной гигиене такой эффект сохраняется на протяжении 2 лет.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container">
            <div class="section-title mb-80">
                <h2 class="h2">Бережная хирургия</h2>
            </div>
        </div>
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/17.png" alt="block17" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md">
                <h2 class="h2 mb-40">Хирургический аппарат Piezon Master Surgery EMS (Швейцария)</h2>
                <p class="text">Он работает на основе ультразвуковых колебаний. Прибор позволяет проводить удаление зуба, синус-лифтинг и костную пластику очень бережно и аккуратно. Оборудование воздействует только на твердые ткани, мягкие при этом остаются нетронутыми. Поэтому такие операции малотравматичны, а значит, и реабилитационный период сокращается.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Физиодиспенсер Implantmed W&H (Австрия)</h2>
                <p class="text">Легкий и удобный аппарат с мощным мотором, который необходим при восстановлении зубов. С физиодиспенсером врач подготавливает место для имплантата и предельно точно его устанавливает. Прибор контролирует движение титанового корня во время фиксации.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/18.png" alt="block18" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container">
            <div class="section-title mb-80">
                <h2 class="h2">Безупречное восстановление зубов</h2>
            </div>
        </div>
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/19.png" alt="block19" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md">
                <h2 class="h2 mb-40">Артикулятор Reference SL Gamma Dental</h2>
                <p class="text mb-40">Прибор применяется вместе с лицевой дугой и работает как единый комплекс. Сначала специалист делает слепки, затем устанавливает дугу. Она надевается на голову и фиксирует индивидуальные особенности функционирования нижней челюсти, а также височно-нижнечелюстных суставов.</p>
                <p class="text mb-40">Полученные данные позволяют настроить артикулятор, который точно воспроизведет все эти движения. Благодаря такому подходу учитываются все детали, и в результате получаются удобные коронки, вкладки, виниры, мостовидные протезы и съемные конструкции. Они полностью соответствуют вашим параметрам, прикусу и идеально подходят для вашего зубного ряда. Готовые изделия комфортно ощущаются, эстетично выглядят.</p>
                <p class="text">Также с помощью такого обследования можно создать индивидуальные миопатические и артропатические капы (сплинты). Они эффективно справляются с нарушениями височно-нижнечелюстного сустава и просто необходимы для подготовки к установке брекетов.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Цифровая технология CAD/CAM</h2>
                <p class="text">При протезировании, особенно в зоне улыбки, очень важно, чтобы коронка не отличилась от настоящих зубов. Поэтому мы создадим для вас естественную и удобную ортопедическую конструкцию по вашим цифровым слепкам. Изделие в точности повторяет анатомию родных зубов, следовательно, после его установки эстетика и функциональность зубного ряда будут восстановлены.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/20.png" alt="block20" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/21.png" alt="block21" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md">
                <h2 class="h2 mb-40">Аксиограф Cadiax 4 Gamma Dental (Австрия)</h2>
                <p class="text mb-40">Многофункциональный аппарат для исследования движений и нарушений височно-нижнечелюстного сустава. Благодаря такому методу можно оценить объем и симметричность движений суставных головок, выявить преждевременные окклюзионные контакты, установить время и место возникновения щелчка. Эти данные необходимы для изготовления индивидуальной миопатической и артропатической капы (сплинта), которая устраняет дисфункцию и вывихи сустава.</p>
                <p class="text mb-40">Также подробная диагностика движений челюсти во всех направлениях помогает создавать удобные ортопедические и терапевтические конструкции. Точные коронки, соответствующие вашим особенностям, не потрескаются со временем, а наоборот, прослужат долгие годы.</p>
                <p class="text">Аксиограф применяется и при выравнивании зубов. Он помогает выявлять проблемы височно-нижнечелюстного сустава. Ведь перед фиксацией брекетов необходимо нормализовать работу всей зубочелюстной системы. Аппарат нужен для контроля ортодонтического лечения: он показывает, есть ли какие-либо нарушения в суставе при изменении прикуса.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Электромиограф «Синапсис» («Нейротех»)</h2>
                <p class="text">Специальный комплекс для исследования активности мышц и нервных структур. Аппарат состоит из электродов, сенсоров и программного обеспечения. Электрические сигналы, пропущенные через мышцы, преобразуются в графические и цифровые отчеты. Благодаря им врач может зафиксировать мышечные нарушения. Применяется электромиография при неправильном прикусе, бруксизме, отсутствии зубов, заболеваниях височно-нижнечелюстного сустава.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/22.png" alt="block22" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/23.png" alt="block23" class="block__img">
            </div>
            <div class="block-wrapper pr-0-xs pr-95-md">
                <h2 class="h2 mb-40">Насадки Valo Translume Ultradent</h2>
                <p class="text">Эти цветные линзы-светофильтры предназначены для исследования состояния тканей зуба и ортопедических конструкций из любых материалов. С помощью зеленой и оранжевой насадки можно определить, насколько плотно прилегает изделие к зубу, а также увидеть трещины на нем и на эмали. </p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-30">Для нас важно ваше здоровье, поэтому в клинике FamilySmile созданы все условия для качественного лечения. Наши опытные специалисты применяют только современное оборудование, новейшие методики и технологии. Запишитесь на прием, мы позаботимся о вас и поможем решить все стоматологические проблемы.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
