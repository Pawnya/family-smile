<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О клинике");
?>
<style>
    @media screen and (max-width: 1024px) {
        .main-container {
            padding-top: 0;
            padding-bottom: 0;
        }
        .inf {
            margin-bottom: 0;
        }
    }
</style>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Инновационные <br>и классические методики <br>для вашего здоровья </h1>
                </div>
                <p class="text mb-30">FamilySmile — современная стоматологическая клиника для всей семьи. У нас представлены все виды услуг для здоровья и красоты улыбки взрослых и детей. Вы можете обратиться за помощью даже в самой сложной ситуации. Мы надежно восстановим здоровье зубочелюстной системы, опираясь на научные знания, эффективные методики и технологии.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-80-xl">
                <p class="text">Клиника FamilySmile была создана в марте 2017 года. Изначально был взят курс на внедрение новейших достижений стоматологии, изучение передового мирового опыта, создание своих разработок. Стоматология оснащена специальным оборудованием для сложного лечения. Наши специалисты проводят уникальные процедуры, которым обучают своих коллег из других клиник в собственном учебном центре.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl pr-0-xs pr-80-xl mt-30-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Клинике FamilySmile присвоен статус <a href="http://www.e-stomatology.ru/" target="_blank">инновационного центра Стоматологической ассоциации России</a>. Это престижное звание федерального уровня подтверждает высокое качество услуг и их соответствие современным стандартам стоматологии.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Узнайте больше о стоматологической клинике FamilySmile</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <h2 class="h2">Лицензии и сертификаты</h2>
            <div class="license">
                <div class="license-slider swiper-container">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:news.list",
                        "license",
                        Array(
                            "DISPLAY_DATE"                    => "Y",
                            "DISPLAY_NAME"                    => "Y",
                            "DISPLAY_PICTURE"                 => "Y",
                            "DISPLAY_PREVIEW_TEXT"            => "Y",
                            "AJAX_MODE"                       => "N",
                            "IBLOCK_TYPE"                     => "content",
                            "IBLOCK_ID"                       => "license",
                            "NEWS_COUNT"                      => "20",
                            "SORT_BY1"                        => "ACTIVE_FROM",
                            "SORT_ORDER1"                     => "DESC",
                            "SORT_BY2"                        => "SORT",
                            "SORT_ORDER2"                     => "ASC",
                            "FILTER_NAME"                     => "",
                            "FIELD_CODE"                      => Array("ID"),
                            "PROPERTY_CODE"                   => Array("DESCRIPTION"),
                            "CHECK_DATES"                     => "Y",
                            "DETAIL_URL"                      => "",
                            "PREVIEW_TRUNCATE_LEN"            => "",
                            "ACTIVE_DATE_FORMAT"              => "d.m.Y",
                            "SET_TITLE"                       => "N",
                            "SET_BROWSER_TITLE"               => "N",
                            "SET_META_KEYWORDS"               => "Y",
                            "SET_META_DESCRIPTION"            => "Y",
                            "SET_LAST_MODIFIED"               => "Y",
                            "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
                            "ADD_SECTIONS_CHAIN"              => "Y",
                            "HIDE_LINK_WHEN_NO_DETAIL"        => "Y",
                            "PARENT_SECTION"                  => "",
                            "PARENT_SECTION_CODE"             => "",
                            "INCLUDE_SUBSECTIONS"             => "Y",
                            "CACHE_TYPE"                      => "A",
                            "CACHE_TIME"                      => "3600",
                            "CACHE_FILTER"                    => "Y",
                            "CACHE_GROUPS"                    => "Y",
                            "DISPLAY_TOP_PAGER"               => "Y",
                            "DISPLAY_BOTTOM_PAGER"            => "Y",
                            "PAGER_TITLE"                     => "Новости",
                            "PAGER_SHOW_ALWAYS"               => "Y",
                            "PAGER_TEMPLATE"                  => "",
                            "PAGER_DESC_NUMBERING"            => "Y",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL"                  => "Y",
                            "PAGER_BASE_LINK_ENABLE"          => "Y",
                            "SET_STATUS_404"                  => "Y",
                            "SHOW_404"                        => "Y",
                            "MESSAGE_404"                     => "",
                            "PAGER_BASE_LINK"                 => "",
                            "PAGER_PARAMS_NAME"               => "arrPager",
                            "AJAX_OPTION_JUMP"                => "N",
                            "AJAX_OPTION_STYLE"               => "Y",
                            "AJAX_OPTION_HISTORY"             => "N",
                            "AJAX_OPTION_ADDITIONAL"          => ""
                        )
                    );?>
                </div>
                <div class="arrow-container arrow-container_license">
                    <div class="arrow arrow_prev"></div>
                    <div class="arrow arrow_next"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Какие услуги мы оказываем</h2>
            </div>
        </div>
        <div class="container block mt-40">
            <div class="block-wrapper">
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">Лечение зубов у детей без боли и страха</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Всесторонняя диагностика полости рта с помощью компьютерной томографии</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Профессиональная гигиена для профилактики заболеваний зубов и десен</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Эстетическое преображение улыбки с помощью отбеливания и виниров</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Лечение кариеса и его осложнений под микроскопом, спасение безнадежных зубов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Эффективное восстановление утраченных зубов с помощью имплантации</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper">
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">Протезирование зубов с использованием цифровых технологий</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Исправление проблем прикуса и выравнивание зубов эстетичными брекетами</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Восстановление правильного взаимодействия элементов зубочелюстной системы</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Лечение бруксизма (зубного скрежета) и храпа</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Изготовление нейромышечных кап</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Останавливаем воспалительные процессы десен  </p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Бережные хирургические операции с помощью ультразвукового оборудования</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-inner mt-50">
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Наша гордость — <br>команда квалифицированных<br>докторов</h2>
                <p class="text mb-40">Создавая FamilySmile, учредитель и руководитель клиники доктор медицинских наук Петр Николаевич Гелетин ставил целью организовать дружную команду современно мыслящих докторов, полных энтузиазма и преданных своему делу. На сегодняшний день у нас работают настоящие профессионалы. Они регулярно обучаются, в том числе за рубежом, следят за трендами и новинками отрасли, мастерски работают на новом оборудовании, установленном в кабинетах.</p>
                <a href="/doctors/" class="link link_section">Подробнее</a>
                <blockquote class="blockquote mt-80 mb-50">FamilySmile — единственная на сегодняшний день стоматология в Смоленске, в которой <a href="/services/gnatologiya/" class="link">лечат височно-нижнечелюстной сустав и восстанавливают правильное смыкание челюстей</a>, используя научный подход и специализированное оборудование. Также в числе уникальных услуг, на которых специализируется клиника, — <a href="/services/ispravlenie-prikusa/ispravlenie-prikusa-po-metodike-sadao-sato/" class="link">многопетлевая техника исправления прикуса</a> и <a href="/services/gnatologiya/" class="link">окклюзионные композитные накладки.</a>.</blockquote>
                <div class="btn-wrapper pb-10">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <?/*<div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>*/?>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center pt-0-md pb-0-md">
            <div class="section-title">
                <h2 class="h2 tac">Традиции стоматологической<br>клиники FamilySmile</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Профессиональная <br>помощь для всей семьи</h3>
                <p class="text mb-70">В клинике FamilySmile есть кабинеты терапии, пародонтологии и профессиональной гигиены, ортопедии, хирургии и имплантации. Кроме того, у нас есть специально оснащенный детский кабинет, собственная зуботехническая лаборатория, полностью оборудованный диагностический кабинет и отдельный стерилизационный блок.</p>
                <blockquote class="blockquote mb-40">Что будет, если чемпион по скоростным гонкам поедет на обычном автомобиле? Конечно, он «выжмет» из машины максимум, но вряд ли достигнет выдающихся результатов. В стоматологии точно так же: мало быть профессионалом — нужно располагать техническими возможностями. В клинике FamilySmile это учтено. Мы помогаем даже в безнадежных случаях благодаря стоматологическому оборудованию, инструментам и материалам от всемирно известных производителей.</blockquote>
                <a href="/about/tekhnologii-i-oborudovanie/" class="link link_section">Подробнее</a>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-30">Вы в поисках клиники, которой можно довериться? Выбирайте FamilySmile! Мы с пониманием отнесемся к вашим пожеланиям и поможем сохранить здоровье зубов с максимальным комфортом. Приходите, чтобы оценить, насколько может быть эффективным и приятным лечение зубов в современной высокотехнологичной клинике.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<script defer src="main.js"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
