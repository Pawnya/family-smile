$(function() {
    new Vue({
        el: '#app',
        name: 'app',
        data: {
            infographics: [
                {
                    icon: 'known',
                    title: 'Постоянное освоение новых тенденций',
                    text: 'Мы стремимся к новым, более эффективным и комфортным для пациентов решениям.'
                },
                {
                    icon: 'complex',
                    title: 'Комплексный подход',
                    text: 'К каждой улыбке мы подходим индивидуально, стремясь добиться полного оздоровления полости рта, а не решить единичную проблему.'
                },
                {
                    icon: 'anestesia',
                    title: 'Лечение без боли и стресса',
                    text: 'Считаем, что даже сложные манипуляции должны быть комфортными для вас, и в этом нам помогает компьютерная анестезия быстрого действия.'
                },
                {
                    icon: 'security',
                    title: 'Безопасность на всех этапах',
                    text: 'Используем индивидуальные наборы инструментов, которые проходят многоступенчатую обработку в стерилизационном блоке.'
                },
                {
                    icon: 'watch',
                    title: 'Забота о вашем здоровье',
                    text: 'Мы всегда отслеживаем результаты лечения, приглашаем на профилактические осмотры для контроля состояния здоровья вашей полости рта.'
                }
            ]
        }
    });

    var license = new Swiper('.license-slider', {
        slidesPerView:   3,
        loop: true,
        speed: 1000,
        spaceBetween: 22,
        breakpoints: {
            1200: {
                slidesPerView:   2
            },
            768: {
                slidesPerView:   1
            }
        }
    });
    $('.arrow-container_license .arrow_prev').click(function() {
        license.slidePrev(2000);
    });
    $('.arrow-container_license .arrow_next').click(function() {
        license.slideNext(2000);
    });
})
