<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стоматологическая клиника «Family Smile» - профессиональная стоматология в Смоленске");
?>
<link rel="stylesheet" href="/css/lib/fullpage.css">
<div id="app">
    <ul id="menu">
        <li data-menuanchor="page1" class="active"><a href="#page1"></a></li>
        <li data-menuanchor="page2"><a href="#page2"></a></li>
        <li data-menuanchor="page3"><a href="#page3"></a></li>
        <li data-menuanchor="page4"><a href="#page4"></a></li>
        <li data-menuanchor="page5"><a href="#page5"></a></li>
        <li data-menuanchor="page6"><a href="#page6"></a></li>
    </ul>
    <full-page ref="fullpage" :options="options" id="fullpage">
        <section class="section section-banner fp-auto-height section-overflow">
            <div class="section-layer section-layer_grey">
                <div class="section-layer__parallax" style="background-image: url('/images/section-layer1-1.png')"></div>
            </div>
            <div class="container main-container main-banner">
                <h1 class="mb-30">Традиционный подход<br>и современные<br>технологии </h1>
                <p class="text main__text">Стоматологическая клиника FamilySmile — это команда опытных врачей, современное оснащение, внимательное отношение к каждому пациенту. У нас есть специалисты всех стоматологических направлений, поскольку мы нацелены на комплексную реабилитацию вашего здоровья.</p>
                <div class="btn-wrapper btn-wrapper_posa">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="/about/" class="link link_section ml-0-xs ml-70-md mt-10-xs mt-15-md">Подробнее</a>
                </div>
            </div>
        </section>
        <section class="section section-banner advantages fp-auto-height">
            <div class="section-layer">
                <div class="section-layer__parallax" style="background-image: url('/images/section-layer2.png')"></div>
            </div>
            <div class="container main-container main-container_center">
                <p class="subtitle advantages__subtitle mb-15-xs mb-20-md">лучшие традиции семейной стоматологии</p>
                <div class="section-title">
                    <h2 class="h2 advantages__title mb-25-xs mb-40-md">Преимущества лечения в клинике FamilySmile</h2>
                </div>
                <p class="text advantages__text">В клинике FamilySmile действует скидка 10% в рамках специально выделенного времени — с 9:00 до 12:00 — на профессиональную гигиену полости рта и терапевтическое лечение. Для того чтобы получить скидку, вам необходимо записаться на прием в любой день и оплатить оказанную услугу до 12:00.</p>
                <div class="advantages-sliders">
                    <div class="advantages-slider advantages-slider_prev">
                        <div v-for="advan in advantages">
                            <div class="advantages-slider-item">
                                <div class="advantages-slider-logo">
                                    <img :src="'/images/icon/' + advan.icon + '.svg'" :alt="advan.icon">
                                </div>
                                <p class="advantages-slider__title">{{ advan.title }}</p>
                                <p class="advantages-slider__text">{{ advan.text }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="advantages-slider advantages-slider_main advantages-slider_active">
                        <div v-for="advan in advantages">
                            <div class="advantages-slider-item">
                                <div class="advantages-slider-logo">
                                    <img :src="'/images/icon/' + advan.icon + '.svg'" alt="advan">
                                </div>
                                <p class="advantages-slider__title">{{ advan.title }}</p>
                                <p class="advantages-slider__text">{{ advan.text }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="advantages-slider advantages-slider_next">
                        <div v-for="advan in advantages">
                            <div class="advantages-slider-item">
                                <div class="advantages-slider-logo">
                                    <img :src="'/images/icon/' + advan.icon + '.svg'" alt="advan">
                                </div>
                                <p class="advantages-slider__title">{{ advan.title }}</p>
                                <p class="advantages-slider__text">{{ advan.text }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_advantages">
                    <div class="pagination pagination_advantages"></div>
                </div>
            </div>
        </section>
        <section class="section section-banner section-overflow fp-auto-height">
            <div class="section-layer">
                <div class="section-layer__parallax section-layer__parallax_ring" style="background-image: url('/images/section-ring.png')"></div>
            </div>
            <div class="container main-container main-container_center services">
                <div class="services-wrapper">
                    <div class="services-ring hide" data-anim="base ring">
                        <div class="services-ring__part" data-anim="base left"></div>
                        <div class="services-ring__part" data-anim="base right"></div>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services-menu",
                        Array(
                            "ROOT_MENU_TYPE"        => "services",
                            "MAX_LEVEL"             => "2",
                            "CHILD_MENU_TYPE"       => "submenu",
                            "USE_EXT"               => "Y",
                            "DELAY"                 => "N",
                            "ALLOW_MULTI_SELECT"    => "Y",
                            "MENU_CACHE_TYPE"       => "N",
                            "MENU_CACHE_TIME"       => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS"   => ""
                        )
                    );?>
                    <div class="services-content tac hide">
                        <p class="subtitle mb-15">услуги нашей клиники</p>
                        <transition name="fade">
                            <h3 class="h3 mb-25">{{ title }}</h3>
                        </transition>
                        <div v-if="menus.length == 0" class="services-desc">
                            <p class="text">{{ text }}</p>
                        </div>
                        <div v-else class="services-desc">
                            <div v-if="menus[0].length !== 0" class="services-submenu">
                                <div v-for="menu in menus[0]" class="services-submenu-item">
                                    <a :href="menu.href" class="services-submenu__link">{{ menu.text }}</a>
                                </div>
                            </div>
                            <p v-else class="text mb-30">{{ text }}</p>
                            <a :href="href" class="btn">подробнее</a>
                        </div>
                    </div>
                    <div class="services-m swiper-container">
                        <?$APPLICATION->IncludeComponent(
                            "family-smile:menu",
                            "services-menu-mobile",
                            Array(
                                "ROOT_MENU_TYPE"        => "services",
                                "MAX_LEVEL"             => "2",
                                "CHILD_MENU_TYPE"       => "submenu",
                                "USE_EXT"               => "Y",
                                "DELAY"                 => "N",
                                "ALLOW_MULTI_SELECT"    => "Y",
                                "MENU_CACHE_TYPE"       => "N",
                                "MENU_CACHE_TIME"       => "3600",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "MENU_CACHE_GET_VARS"   => ""
                            )
                        );?>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-banner section-overflow fp-auto-height">
            <div class="section-layer">
                <div class="section-layer__parallax" style="background-image: url('/images/section-layer2.png')"></div>
            </div>
            <div class="container block block_center block_full">
                <div class="block-wrapper order-2 pb-60-xs pb-0-xl">
                    <p class="subtitle mb-20">акция действует бессрочно</p>
                    <h2 class="h2 mb-40">Запишитесь на прием и получите скидку на лечение</h2>
                    <p class="text main__text mb-60">Заполните форму на нашем сайте, выберите удобное время приема и пройдите лечение со скидкой 5%. Подробности акции уточняйте у администратора.</p>
                    <div class="btn-wrapper btn-wrapper_posa">
                        <?$APPLICATION->IncludeComponent(
                            "family-smile:form.result.new",
                            "inline",
                            Array(
                                "WEB_FORM_ID"            => "1",
                                "AJAX_OPTION_STYLE"      => "Y",
                                "IGNORE_CUSTOM_TEMPLATE" => "N",
                                "USE_EXTENDED_ERRORS"    => "Y",
                                "SEF_MODE"               => "N",
                                "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                "CACHE_TYPE"             => "N",
                                "CACHE_TIME"             => "3600",
                                "LIST_URL"               => "",
                                "EDIT_URL"               => "",
                                "SUCCESS_URL"            => "",
                                "CHAIN_ITEM_TEXT"        => "",
                                "CHAIN_ITEM_LINK"        => "",
                                )
                            );
                        ?>
                        <a href="/about/shares-sales/ " class="link link_section ml-0-xs ml-70-md mt-10-xs mt-15-md">Подробнее</a>
                    </div>
                </div>
                <div class="block-img section-slider order-1">
                    <div class="shares-slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares1.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares2.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares1.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares2.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="shares-slider-copy swiper-container">
                        <div class="swiper-wrapper">
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares1.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares2.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares1.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                            <div class="shares-slide swiper-slide">
                                <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                    <img src="/images/shares2.png" alt="shares" class="shares__img">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="arrow-container arrow-container_shares">
                        <div class="arrow arrow_prev"></div>
                        <div class="arrow arrow_next"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-banner section-overflow fp-auto-height">
            <div class="section-layer">
                <div class="section-layer__parallax" style="background-image: url('/images/section-layer3.png')"></div>
            </div>
            <div class="container main-container main-container_end">
                <div class="block-img block-img_center pl-0-xs pl-20-md">
                    <p class="subtitle mb-20 pt-50-xs pt-0-md">передовые технологии и новейшие материалы</p>
                    <h2 class="h2 mb-50">Для вашего здоровья мы выбираем лучшее </h2>
                    <p class="text mb-20">Мы применяем материалы и инструменты от ведущих европейских производителей, что гарантирует безопасность лечения. А эффективный результат обеспечивают наши специалисты, которые посещают различные семинары, обучаются новым методикам, чтобы помочь вам даже в сложной ситуации.</p>
                    <p class="text mb-60">В клинике FamilySmile мы поддерживаем качество услуг на европейском уровне, предлагая квалифицированную помощь по приемлемой цене. Главное для нас — ваше крепкое здоровье, красивая улыбка и хорошее настроение. Приходите в FamilySmile и познакомьтесь с нашей командой.</p>
                    <div class="btn-wrapper btn-wrapper_posa">
                        <?$APPLICATION->IncludeComponent(
                            "family-smile:form.result.new",
                            "inline",
                            Array(
                                "WEB_FORM_ID"            => "1",
                                "AJAX_OPTION_STYLE"      => "Y",
                                "IGNORE_CUSTOM_TEMPLATE" => "N",
                                "USE_EXTENDED_ERRORS"    => "Y",
                                "SEF_MODE"               => "N",
                                "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                "CACHE_TYPE"             => "N",
                                "CACHE_TIME"             => "3600",
                                "LIST_URL"               => "",
                                "EDIT_URL"               => "",
                                "SUCCESS_URL"            => "",
                                "CHAIN_ITEM_TEXT"        => "",
                                "CHAIN_ITEM_LINK"        => "",
                                )
                            );
                        ?>
                        <a href="/about/" class="link link_section ml-0-xs ml-70-md mt-10-xs mt-15-md">Подробнее</a>
                    </div>
                </div>
            </div>
        </section>
        <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
        <section class="section footer">
            <div class="container">
                <div class="footer-list">
                    <div class="footer-menu">
                        <a href="/sitemap/" class="footer__link">Карта сайта</a>
                        <a href="#" class="footer__link bvi-panel-open-menu">Для слабовидящих</a>
                        <a href="/yuridicheskaya-informatsiya/" class="footer__link">Юридическая информация</a>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:search.form",
                        "search",
                        Array(
                            "USE_SUGGEST" => "N",
                            "PAGE"        => "#SITE_DIR#search/index.php"
                            )
                    );?>
                </div>
                <div class="footer-bottom">
                    <p class="footer__copyright">© ООО «Family Smile» 2019</p>
                    <div class="footer-social">
                        <a href="https://vk.com/club156410488" class="footer-social__link footer-social__link_vk" target="_blank">
                            <svg class="icon icon-vk">
                                <use xlink:href="#icon-vk"></use>
                            </svg>
                        </a>
                        <a href="https://www.facebook.com/familysmile67/" class="footer-social__link footer-social__link_fb" target="_blank">
                            <svg class="icon icon-fb">
                                <use xlink:href="#icon-fb"></use>
                            </svg>
                        </a>
                        <a href="https://www.instagram.com/family_smile_smolensk/" class="footer-social__link footer-social__link_inst" target="_blank">
                            <svg class="icon icon-inst">
                                <use xlink:href="#icon-inst"></use>
                            </svg>
                        </a>
                    </div>
                    <a href="https://pro100dental.ru" class="footer-agency" target="_blank">
                        <img src="/images/icon/pro100.svg" alt="pro100">
                    </a>
                </div>
            </div>
        </section>
    </full-page>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
