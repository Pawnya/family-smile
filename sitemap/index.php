<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<link rel="stylesheet" href="main.min.css">
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Карта сайта</h1>
            </div>
        </div>
        <div class="sitemap">
            <?$APPLICATION->IncludeComponent(
                "family-smile:menu",
                "sitemap",
                Array(
                    "ROOT_MENU_TYPE"        => "sitemap",
                    "MAX_LEVEL"             => "3",
                    "CHILD_MENU_TYPE"       => "submenu",
                    "USE_EXT"               => "Y",
                    "DELAY"                 => "N",
                    "ALLOW_MULTI_SELECT"    => "Y",
                    "MENU_CACHE_TYPE"       => "N",
                    "MENU_CACHE_TIME"       => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS"   => ""
                )
            );?>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
