<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница стилей");
?>
<div id="app">
    <section class="section section-banner">
        <div class="section-layer section-layer_grey">
            <img src="/images/section-layer1-1.png" alt="Баннер" class="section-layer__img">
        </div>
        <div class="container main-container main-banner">
            <p class="main__subtitle">стоматологическая клиника</p>
            <h1 class="mb-30">«Family Smile» — <br>современная и <br>высокотехнологичная <br>клиника</h1>
            <p class="text main__text">Наша клиника сочетает в себе лучшие традиции семейной стоматологии. Передовые технологии, современное оборудование, инструменты и материалы от ведущих европейских фирм - производителей обеспечивают нашим пациентам максимально безопасные условия и эффективные результаты лечения.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section ml-0-xs ml-70-md">Подробнее</a>
            </div>
        </div>
    </section>
    <section class="section section-banner advantages">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <p class="subtitle advantages__subtitle mb-15-xs mb-20-md">лучшие традиции семейной стоматологии</p>
            <div class="section-title">
                <h2 class="h2 advantages__title mb-25-xs mb-40-md">Преимущества лечения в клиники «Family Smile»</h2>
            </div>
            <p class="text advantages__text">В нашей клинике «Family Smile» действует система скидок 10% в рамках специально выделенного времени с 9.00 до 12.00 на профессиональную гигиену полости рта и терапевтическое лечение. Для того, чтобы получить скидку.</p>
            <div class="advantages-sliders">
                <div class="advantages-slider advantages-slider_prev">
                    <div v-for="advan in advantages">
                        <div class="advantages-slider-item">
                            <div class="advantages-slider-logo">
                                <img :src="'/images/icon/' + advan.icon + '.svg'" :alt="advan.icon">
                            </div>
                            <p class="advantages-slider__title">{{ advan.title }}</p>
                            <p class="advantages-slider__text">{{ advan.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="advantages-slider advantages-slider_main advantages-slider_active">
                    <div v-for="advan in advantages">
                        <div class="advantages-slider-item">
                            <div class="advantages-slider-logo">
                                <img :src="'/images/icon/' + advan.icon + '.svg'" alt="advan">
                            </div>
                            <p class="advantages-slider__title">{{ advan.title }}</p>
                            <p class="advantages-slider__text">{{ advan.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="advantages-slider advantages-slider_next">
                    <div v-for="advan in advantages">
                        <div class="advantages-slider-item">
                            <div class="advantages-slider-logo">
                                <img :src="'/images/icon/' + advan.icon + '.svg'" alt="advan">
                            </div>
                            <p class="advantages-slider__title">{{ advan.title }}</p>
                            <p class="advantages-slider__text">{{ advan.text }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination-container pagination-container_advantages">
                <div class="pagination pagination_advantages"></div>
            </div>
        </div>
    </section>
    <section class="section section-banner section-overflow">
        <div class="section-layer">
            <img src="/images/section-layer3.png" alt="" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center services">
            <div class="services-wrapper">
                <div class="services-ring hide" data-anim="base ring">
                    <div class="services-ring__part" data-anim="base left"></div>
                    <div class="services-ring__part" data-anim="base right"></div>
                </div>
                <?$APPLICATION->IncludeComponent(
                    "family-smile:menu",
                    "services-menu",
                    Array(
                        "ROOT_MENU_TYPE"        => "services",
                        "MAX_LEVEL"             => "2",
                        "CHILD_MENU_TYPE"       => "submenu",
                        "USE_EXT"               => "Y",
                        "DELAY"                 => "N",
                        "ALLOW_MULTI_SELECT"    => "Y",
                        "MENU_CACHE_TYPE"       => "N",
                        "MENU_CACHE_TIME"       => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS"   => ""
                    )
                );?>
                <div class="services-content tac hide">
                    <p class="subtitle mb-15">услуги нашей клиники</p>
                    <transition name="fade">
                        <h3 class="h3 mb-25">{{ title }}</h3>
                    </transition>
                    <div v-if="menus.length == 0" class="services-desc">
                        <p class="text">{{ text }}</p>
                    </div>
                    <div v-else class="services-desc">
                        <div v-if="menus[0].length !== 0" class="services-submenu">
                            <div v-for="menu in menus[0]" class="services-submenu-item">
                                <a :href="menu.href" class="services-submenu__link">{{ menu.text }}</a>
                            </div>
                        </div>
                        <p v-else class="text mb-30">{{ text }}</p>
                        <a :href="href" class="btn">подробнее</a>
                    </div>
                </div>
                <div class="services-m swiper-container">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services-menu-mobile",
                        Array(
                            "ROOT_MENU_TYPE"        => "services",
                            "MAX_LEVEL"             => "2",
                            "CHILD_MENU_TYPE"       => "submenu",
                            "USE_EXT"               => "Y",
                            "DELAY"                 => "N",
                            "ALLOW_MULTI_SELECT"    => "Y",
                            "MENU_CACHE_TYPE"       => "N",
                            "MENU_CACHE_TIME"       => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS"   => ""
                        )
                    );?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-banner section-overflow">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container block block_center block_full">
            <div class="block-wrapper order-2">
                <p class="subtitle mb-20">акция действует бессрочно</p>
                <h2 class="h2 mb-40">Заголовок акции <br>максимальное <br>количество строк 3</h2>
                <p class="text main__text mb-60">В нашей клинике «Family Smile» действует система скидок 10% в рамках специально выделенного времени с 9.00 до 12.00 на профессиональную гигиену полости рта и терапевтическое лечение. Для того, чтобы получить скидку.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section ml-0-xs ml-70-md">Подробнее</a>
                </div>
            </div>
            <div class="block-img section-slider order-1">
                <div class="shares-slider swiper-container">
                    <div class="swiper-wrapper">
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares1.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares2.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares1.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares2.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shares-slider-copy swiper-container">
                    <div class="swiper-wrapper">
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares1.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares2.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares1.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares1.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                        <div class="shares-slide swiper-slide">
                            <div class="parallax-bg" style="background-image:url('/images/shares2.png')" data-swiper-parallax="" data-swiper-parallax-duration="">
                                <img src="/images/shares2.png" alt="shares" class="shares__img">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="arrow-container arrow-container_shares">
                    <div class="arrow arrow_prev"></div>
                    <div class="arrow arrow_next"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-banner section-overflow">
        <div class="section-layer">
            <img src="/images/section-layer3.png" alt="section-layer3" class="section-layer__img">
        </div>
        <div class="container main-container main-container_end">
            <div class="block-img block-img_center pl-0-xs pl-20-md">
                <p class="subtitle mb-20">передовые технологии и новейшие материалы</p>
                <h2 class="h2 mb-50">Сео блок, максимальное количество строк 3 сео блок </h2>
                <p class="text mb-20">FAMILY SMILE располагает широким спектром технических возможностей. Мы используем только самое современное стоматологическое оборудование, инструменты и материалы от лучших зарубежных фирм - производителей, что позволяет обеспечить высокую эффективность и безопасность лечения.</p>
                <p class="text mb-60">Стоматологи проводят наблюдения за результатами лечения, держат под контролем состояние здоровья всех пациентов, регулярно приглашают на профилактические осмотры. Мы всегда помним о своих пациентах и беспокоимся о их самочувствии! Врачи клиники FAMILY SMILE постоянно поддерживают внутренний диалог со смежными врачами - специалистами, чтобы лечение каждого пациента проходило комплексно и в полной мере.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section ml-0-xs ml-70-md">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
    <section class="section mt-70">
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <? ?>
                <div class="breadcrumbs mb-15">
                    <div class="breadcrumbs-item">
                        <a href="#" class="breadcrumbs__link">Главная</a>
                    </div>
                    <div class="breadcrumbs-item">
                        <a href="#" class="breadcrumbs__link">Главная</a>
                    </div>
                    <div class="breadcrumbs-item">
                        <a href="#" class="breadcrumbs__link">Главная</a>
                    </div>
                </div>
                <div class="section-title">
                    <h2 class="h1 mb-30">Эффективное восстановление здоровья десен </h2>
                </div>
                <p class="text mb-30">Десны требуют особо внимательного подхода. Они являются надежной опорой для зубов, делая улыбку красивой и здоровой. Приходите в стоматологию FamilySmile, если обнаружили кровоточивость или воспаление десен. Наши врачи составят для вас курс комплексной реабилитации пародонта с применением плазмолифтинга. Это передовая методика восстановления десен на клеточном уровне за счет собственных ресурсов организма.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="/images/full.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
                <h2 class="h2 mb-40">Как проходит лечение аппаратом FotoSan</h2>
                <div class="dots mb-60">
                    <div class="dots-item">
                        <p class="text dots__text">Специальный гель наносится тонким слоем на десну или закладывается в пародонтальный карман </p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Свет лазера активирует препарат — выделяется активный кислород, уничтожающий микроорганизмы изнутри</p>
                    </div>
                </div>
                <p class="text">Гель воздействует исключительно на микроорганизмы и не затрагивает здоровые ткани. Поэтому фотодинамическая терапия абсолютно безопасна и подходит всем.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Фотодинамическая терапия наиболее эффективна в комплексе с аппаратом Vector. Он бережно очищает пародонтальные карманы от зубных отложений. А FotoSan закрепляет эффект лечения. Сочетание двух методик устраняет воспаление и не допускает дальнейшего прогрессирования заболевания. Выполняйте все рекомендации врача FamilySmile, и вы забудете о проблемах с деснами.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Для чего нужна компьютерная томография челюсти</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner">
                <p class="text mb-50">Томография дает информации больше, чем десятки рентгеновских снимков. Всего за одно короткое обследование вы узнаете все о состоянии своих зубов и челюстей. Это очень выгодно, потому что отпадет необходимость дополнительных исследований и появится возможность выявить заболевания на ранней стадии, когда их проще всего вылечить.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Прицельный <br>снимок зуба</h2>
                <p class="text mb-80">Контроль лечения зубов на всех этапах мы осуществляем с помощью быстрого прицельного снимка на визиографе. Обследование охватывает 1–2 зуба. В стоматологической клинике FamilySmile есть собственный рентген-кабинет, поэтому вам не нужно будет никуда идти для диагностики. Мы сделаем снимок на высокоточном визиографе GENDEX EXPERT DC. Результат сразу же появится на экране. </p>
                <blockquote class="blockquote mb-50">Уникальность визиографа — в его максимальной безопасности. Датчик визиографа настолько чувствителен, что для его активации требуется минимальная доза рентгеновский лучей.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="/images/block1.png" alt="block1" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="/images/block2.png" alt="block2" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Что показывает <br>телерентгенография</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">Разновидность прикуса и его нарушения</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Положение челюстей относительно друг друга </p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Возможную асимметрию зубных рядов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Состояние дыхательных органов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Развитие шейного отдела позвоночника</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Для оздоровления десен мы применяем следующие методики: </h2>
            </div>
            <? $bq = 'Хирургическое вмешательство в нашей клинике проводится под многоуровневым контролем с заботой о вашем здоровье. Самой тщательной обработке подвергаются и рабочие инструменты, и помещение.';?>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="/images/services1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Фотодинамическая терапия</p>
                                <p class="text box__text">С помощью аппарата FotoSan мы проводим профилактику, а также лечение гингивита и пародонтита</p>
                                <a href="#" class="box__link"></a>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="/images/services1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Лечение десен с помощью Vector</p>
                                <p class="text box__text">Специалисты клиники эффективно очищают пародонтальные карманы, устраняя даже сильное воспаление</p>
                                <a href="#" class="box__link"></a>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="/images/services1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Плазмолифтинг</p>
                                <p class="text box__text">Процедура помогает оздоровить десны собственными ресурсами организма на финальном этапе их лечения</p>
                                <a href="#" class="box__link"></a>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="/images/services1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Хирургическая пародонтология</p>
                                <p class="text box__text">В сложных случаях, чтобы поддержать здоровье мягких тканей, необходима помощь хирурга</p>
                                <a href="#" class="box__link"></a>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item box-item_last">
                            <blockquote class="blockquote mb-50">
                                <?=$bq;?>
                            </blockquote>
                            <?$APPLICATION->IncludeComponent(
                                "family-smile:form.result.new",
                                "inline",
                                Array(
                                    "WEB_FORM_ID"            => "1",
                                    "AJAX_OPTION_STYLE"      => "Y",
                                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                                    "USE_EXTENDED_ERRORS"    => "Y",
                                    "SEF_MODE"               => "N",
                                    "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                    "CACHE_TYPE"             => "N",
                                    "CACHE_TIME"             => "3600",
                                    "LIST_URL"               => "",
                                    "EDIT_URL"               => "",
                                    "SUCCESS_URL"            => "",
                                    "CHAIN_ITEM_TEXT"        => "",
                                    "CHAIN_ITEM_LINK"        => "",
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq">
                <blockquote class="blockquote blockquote_tab mb-30">
                    <?=$bq;?>
                </blockquote>
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/doctors.php");?>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Преимущества плазмолифтинга для здоровья десен</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Эффективно</p>
                        <p class="text dots__text">Курс лечения восстанавливает деснам крепкую структуру и здоровый цвет </p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Надежно</p>
                        <p class="text dots__text">Плазмолифтинг возвращает десне правильную форму и способствует долговечности службы имплантатов, коронок</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Комфортно</p>
                        <p class="text dots__text">Инъекции практически неощутимы и в большинстве случаев не требуют анестезии</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Быстро</p>
                        <p class="text dots__text">Нужно не более 30 минут, чтобы провести забор крови, поместить ее в центрифугу, извлечь плазму и сделать инъекцию</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <p class="text mb-60">Плазмолифтинг не просто останавливает процесс воспаления в деснах, а способствует активному восстановлению формы, структуры и цвета десны. </p>
                <p class="text mb-50">Приходите в стоматологию FamilySmile на лечение десен к врачу-пародонтологу, кандидату медицинских наук. Он составит для вас курс лечебных процедур, которые эффективно помогут в вашем случае. С помощью плазмолифтинга наш специалист повысит иммунитет ваших десен к различным заболеваниям, а также ускорит восстановление тканей. Ждем вас на приеме с понедельника по субботу, в удобное вам время. </p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            advantages: [
                {
                    icon: 'knownlege',
                    title: 'повышение квалификации врачей',
                    text: 'Постоянное профильное обучение  докторов  на зарубежных семинарах'
                },
                {
                    icon: 'cad',
                    title: 'зуботехническая лаборатория',
                    text: 'Собственная зуботехническая лаборатория'
                },
                {
                    icon: 'token',
                    title: 'сильная команда',
                    text: ' Комллектив молодых квалифицированных специалистов'
                },
                {
                    icon: 'micro',
                    title: 'Современное оборудование',
                    text: 'Европейские материалы и новейшие методики лечения'
                },
                {
                    icon: 'shield',
                    title: 'качество услуг',
                    text: 'Мы оказываем качественную стоматологическую помощь по всем направления'
                },
                {
                    icon: 'chel',
                    title: 'Специализация на гнатологии',
                    text: 'Лечении бруксизма, решении проблем с ВНЧС'
                }
            ],
            infographics: [
                {
                    icon: 'implant',
                    title: 'Планирование имплантации',
                    text: 'Только томография позволяет оценить объем и состояние костной ткани для надежного крепления имплантата'
                },
                {
                    icon: 'hiryrgiya',
                    title: 'Удаление проблемных зубов',
                    text: 'Доктор видит положение многокорневых зубов, в том числе лежачие «восьмерки», и точно планирует операцию'
                },
                {
                    icon: 'terapiya',
                    title: 'Эффективное терапевтическое лечение',
                    text: 'На томограмме отлично видны любые скрытые кариозные полости и анатомически сложные каналы зубов'
                },
                {
                    icon: 'ortopediya',
                    title: 'Составление плана лечения у ортодонта',
                    text: 'Для правильного расчета силы давления конструкций нужны точные данные о состоянии костной ткани, положении зубов и корней'
                },
                {
                    icon: 'kista',
                    title: 'Выявление кистозных новообразований',
                    text: 'В начале заболевания киста видна только на снимке — исследование помогает распознать и точно локализировать проблему'
                },
                {
                    icon: 'gnatologiya',
                    title: 'Оценка состояния костей при травмах',
                    text: 'На томограмме видна костная ткань челюсти, височно-нижнечелюстной сустав, а также кости всего лица'
                }
            ],
            menus: [],
            title: 'Принцип нашей работы командный подход к лечению',
            text: 'У нас работают настоящие профессионалы высшего уровня, участвующие в международных стоматологических симпозиумах',
            href: ''
        },
        methods: {
            classActive: function(el) {
                this.title = el.target.text
                this.href = el.target.href
                var submenu = []
                this.text = el.target.nextElementSibling.textContent
                if (el.srcElement.nextElementSibling !== null) {
                    var link = el.srcElement.nextElementSibling.querySelectorAll('.nav-submenu__title')
                    link.forEach(function(elem,index) {
                        this.menus = Object.assign({}, this.menus, {
                            href: elem.href,
                            text: elem.text
                        })
                        submenu.push(this.menus)
                    })
                    this.$set(this.menus, 0, submenu)
                } else {
                    this.$delete(this.menus, 0)
                }
            }
        }
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
