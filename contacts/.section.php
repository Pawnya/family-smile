<?
$sSectionName = "Контакты";
$arDirProperties = array(
    "title" => "Контакты стоматологической клиники «Family Smile» в Смоленске",
    "description" => "Стоматология «Family Smile» находится по адресу: г.Смоленск, ул.Воробьева, д.11/9. Телефон для записи: 8 (4812) 305-445. E-mail: info@family-smile.ru",
    "keywords" => "телефон стоматологии, адрес стоматологии",
    "robots" => "index, follow",
);
?>
