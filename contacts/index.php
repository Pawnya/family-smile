<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<link rel="stylesheet" href="main.min.css">
<section class="section section-padding_inner">
	<div class="section-layer">
		<img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
	</div>
	<div class="container main-container main-container_center">
		<div class="main-inner">
			<?$APPLICATION->IncludeComponent(
				"family-smile:breadcrumb",
				"family-breadcrumbs",
				Array(
					"START_FROM" => "0",
					"PATH"       => "",
					"SITE_ID"    => "s1"
				)
			);?>
			<div class="section-title">
				<h1 class="h1 mb-30">Контакты</h1>
			</div>
		</div>
	</div>
</section>
<section class="section mt-60">
	<div class="container">
		<div class="block-img block-img_full">
			<img src="images/1.png" alt="Full image" class="block__img_full">
		</div>
	</div>
</section>
<section class="section section-margin_top">
	<div class="container">
		<div class="inf-contacts">
			<div class="inf-item">
				<div class="inf-img">
					<img src="/images/icon/pin.svg" alt="inf">
				</div>
				<p class="subtitle inf__title">Мы находимся:</p>
				<p class="text">г. Смоленск, <br>ул. Воробьева, д. 11/9</p>
			</div>
			<div class="inf-item">
				<div class="inf-img">
					<img src="/images/icon/phone.svg" alt="inf">
				</div>
				<p class="subtitle inf__title">Телефоны:</p>
				<p class="text"><a href="tel:+74812305444" class="callibri_phone">8 (4812) 305-444</a><br> <a href="tel:+784812305445" class="callibri_phone2">8 (4812) 305-445</a></p>
			</div>
			<div class="inf-item">
				<div class="inf-img">
					<img src="/images/icon/time.svg" alt="inf">
				</div>
				<p class="subtitle inf__title">Время работы:</p>
				<p class="text">Пн. – пт.: 9:00–20:00<br>Сб.: 9:00–18:00<br>Вс. — выходной</p>
			</div>
			<div class="inf-item">
				<div class="inf-img">
					<img src="/images/icon/mail.svg" alt="inf">
				</div>
				<p class="subtitle inf__title">E-mail:</p>
				<p class="text"><a href="mailto:info@family-smile.ru">info@family-smile.ru</a></p>
			</div>
		</div>
	</div>
</section>
<div id="map"></div>
<section class="section section-margin_top section-margin_bottom">
	<div class="container">
		<div class="main-inner">
			<p class="text mb-30">ИНН 6732127542, ОГРН 1166733062957, КПП 673201001, ОКПО 03097365</p>
			<p class="text">Лицензия на осуществление медицинской деятельности № ЛО-67-01-001202 от 31.01.2017, выданная Департаментом Смоленской обл. по здравоохранению.</p>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
