/*
    Custom
*/
//=include lib/slick.js
//=include lib/swiper.js
//=include lib/wow.min.js
//=include lib/aos.js
//=include lib/jquery.fancybox.js
//=include lib/jquery.maskedinput.min.js
//=include lib/parallax.min.js
//=include lib/responsivevoice.min.js
//=include lib/bvi-init-panel.min.js
//=include lib/bvi.min.js
//=include lib/js.cookie.min.js
//=include partials/map.js
//=include partials/script.js
