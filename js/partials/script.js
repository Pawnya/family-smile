$(function() {
    checkOS();
    sliders();
    formSubmit();
    saveCookie();
    if (document.body.clientWidth > 767) {
        ring();
    } else {
        if (document.querySelectorAll('.services-content').length > 0) {
            document.querySelector('.services-content').classList.remove('hide');
        }
    }
    mobileMenu();
    scrollInnerPage();
    scrollTop();
    $('.form__input_phone').mask('+7 999 999 99 99',{placeholder:''});
});

window.onscroll = function() {
    scrollTop();
}

function checkOS() {
    if (/Android/i.test(navigator.userAgent)) {
        $("html").addClass("android-device");
    }
}

function enableBodyScroll() {
    if (document.readyState === 'complete') {
        document.body.style.position = '';
        document.body.style.overflowY = '';
        if (document.body.style.marginTop) {
            var scrollTop = -parseInt(document.body.style.marginTop, 10);
            document.body.style.marginTop = '';
            window.scrollTo(window.pageXOffset, scrollTop);
        }
    } else {
        window.addEventListener('load', enableBodyScroll);
    }
}

function disableBodyScroll() {
    if (document.readyState === 'complete') {
        if (document.body.scrollHeight > window.innerHeight) {
            document.body.style.position = 'fixed';
            document.body.style.overflowY = 'scroll';
        }
    } else {
        window.addEventListener('load', disableBodyScroll);
    }
}

function sliders() {
    var $advan_prev = $('.advantages-slider_prev'),
        $advan      = $('.advantages-slider_main'),
        $advan_next = $('.advantages-slider_next');

    $advan_next.slick({
        dots:           false,
        arrows:         false,
        infinite:       true,
        speed:          300,
        initialSlide:   1,
        slidesToShow:   1,
        slidesToScroll: 1,
        draggable:      false,
        pauseOnHover:   false,
        pauseOnFocus:   false,
        responsive: [
            {
              breakpoint: 1025,
              settings: "unslick"
            }
        ]
    });

    $advan.slick({
        dots:           false,
        arrows:         false,
        infinite:       true,
        speed:          300,
        slidesToShow:   1,
        slidesToScroll: 1,
        draggable:      false,
        pauseOnHover:   false,
        pauseOnFocus:   false,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    appendDots: $('.pagination_advantages'),
                    dotsClass:  'pagination-dots',
                    dots:       true,
                    customPaging:   function(slider, i) {
                        return '<span class="pagination-dot"></span>'
                    }
              }
            }
        ],


    });

    $advan_prev.slick({
        dots:           false,
        arrows:         false,
        infinite:       true,
        speed:          300,
        initialSlide:   -1,
        slidesToShow:   1,
        slidesToScroll: 1,
        draggable:      false,
        pauseOnHover:   false,
        pauseOnFocus:   false,
        responsive: [
            {
              breakpoint: 1025,
              settings: "unslick"
            }
        ]
    });

    setInterval(function() {
        $('.advantages-slider').slick('slickNext');
    }, 3000);

    $('.advantages-slider').click(function() {
        $('.advantages-slider').removeClass('advantages-slider_active');
        $(this).addClass('advantages-slider_active');
    });

    $('.pagination_advantages .pagination-arrow_prev').click(function() {
        $advan.slick('slickPrev');
    });
    $('.pagination_advantages .pagination-arrow_next').click(function() {
        $advan.slick('slickNext');
    });

    slideWidth = $('.shares-slider .shares-slide').width();
    slideWidthCopy = $('.shares-slider-copy .shares-slide').width();

    if ($('.shares-slider').length > 0) {
        var Shares = new Swiper('.shares-slider', {
            slidesPerView: 1,
            parallax:      true,
            speed:         2000,
            loop:          true,
            loopedSlides:  1,
            simulateTouch: false,
            on: {
                init: function() {
                    $('.shares-slide .parallax-bg').attr('data-swiper-parallax', slideWidth);
                }
            }
        });
    }

    if ($('.shares-slider-copy').length > 0) {
        var SharesCopy = new Swiper('.shares-slider-copy', {
            slidesPerView:        1,
            parallax:             true,
            speed:                2000,
            loop:                 true,
            loopAdditionalSlides: 10,
            simulateTouch:        false,
            thumbs: {
                swiper: Shares
            },
            on: {
                init: function() {
                    $('.shares-slider-copy .shares-slide .parallax-bg').attr('data-swiper-parallax', slideWidthCopy);
                }
            }
        });
    }

    $('.arrow-container_shares .arrow_prev').click(function() {
        Shares.slidePrev(2000);
        SharesCopy.slidePrev(2000);
    });
    $('.arrow-container_shares .arrow_next').click(function() {
        Shares.slideNext(2000);
        SharesCopy.slideNext(2000);
    });

    if ($('.reviews-slider').length > 0) {
        var reviews = new Swiper('.reviews-slider', {
            speed:              2000,
            loop:               true,
            loopedSlides:       1,
            simulateTouch:      false,
            slidesPerView:      1,
            pagination: {
                el:                '.pagination_reviews',
                type:              'bullets',
                bulletClass:       'pagination-dot',
                bulletActiveClass: 'active',
                clickable:         true
            },
            breakpoints: {
                768: {
                    simulateTouch: true,
                    clickable:     true
                }
            }
        })
    }

    $('.arrow-container_reviews .arrow_prev').click(function() {
        reviews.slidePrev(2000);
    });
    $('.arrow-container_reviews .arrow_next').click(function() {
        reviews.slideNext(2000);
    });

    if ($('.inf').length > 0) {
        var infoGraphics;
        document.querySelectorAll('.inf').forEach(function(inf) {
            infoGraphics = new Swiper(inf, {
                speed:           0,
                slidesPerView:   'auto',
                touchRatio:      0,
                simulateTouch:   false,
                breakpoints: {
                    768: {
                        speed:           1000,
                        slidesPerView:   1,
                        slidesPerColumn: 1,
                        touchRatio:      1,
                        simulateTouch:   true,
                        loop:            true,
                        pagination: {
                            el:                '.pagination_inf',
                            type:              'bullets',
                            bulletClass:       'pagination-dot',
                            bulletActiveClass: 'active',
                            clickable: true
                        },
                    },
                    1024: {
                        speed:           1000,
                        slidesPerView:   2,
                        slidesPerColumn: 1,
                        touchRatio:      1,
                        simulateTouch:   true,
                        loop:            true,
                        pagination: {
                            el:                '.pagination_inf',
                            type:              'bullets',
                            bulletClass:       'pagination-dot',
                            bulletActiveClass: 'active',
                            clickable: true
                        },
                    }
                }
            });
        })
    }

    var boxSlides = document.querySelectorAll('.box-slide').length;

    if ($('.box_menu').length > 0) {
        var boxMenu = new Swiper('.box_menu', {
            init:            false,
            speed:           1000,
            slidesPerView:   1,
            slidesPerColumn: 1,
            touchRatio:      1,
            autoHeight:      true,
            simulateTouch:   true,
            loop:            true,
            pagination: {
                el:                '.pagination_box',
                type:              'bullets',
                bulletClass:       'pagination-dot',
                bulletActiveClass: 'active',
                clickable:         true
            }
        });
    }

    if ($('.doctors').length > 0) {
        var doctors = new Swiper('.doctors', {
            init:            false,
            speed:           1000,
            slidesPerView:   1,
            slidesPerColumn: 1,
            touchRatio:      1,
            simulateTouch:   true,
            loop:            true,
            pagination: {
                el:                '.pagination_doctors',
                type:              'bullets',
                bulletClass:       'pagination-dot',
                bulletActiveClass: 'active',
                clickable:         true
            }
        });
    }
    if (document.body.clientWidth < 1050) {

        if ($('.inf').length > 0) {
            document.querySelectorAll('.inf').forEach(function(elem) {
                var infSlides = elem.querySelectorAll('.inf-wrapper .swiper-slide:not(.swiper-slide-duplicate)').length - 1;
                if (elem.querySelectorAll('.inf-wrapper .swiper-slide:not(.swiper-slide-duplicate)')[infSlides].querySelector('.inf-item').classList.contains('inf-item_bq')) {
                    infoGraphics.removeSlide(infSlides);
                    infoGraphics.update();
                }
            });
        }
        if ($('.box_menu').length > 0) {
            boxMenu.on('init', function() {
                if (document.querySelectorAll('.box-slide')[boxSlides].querySelector('.box-item').classList.contains('box-item_last')) {
                    boxMenu.removeSlide(boxSlides - 1);
                }
            });

            boxMenu.init();
        }
        if ($('.doctors').length > 0) {
            doctors.init();
        }
    }

    if ($('.services-m').length > 0) {
        var services = new Swiper('.services-m', {
            init: false,
            speed: 2000,
            slidesPerView: 'auto',
            centeredSlides: true,
            loop: true,
            autoHeight: true,
            slideActiveClass: 'services-m-item_active',
            pagination: {
                el:                '.pagination_services',
                type:              'bullets',
                bulletClass:       'pagination-dot',
                bulletActiveClass: 'active',
                clickable:         true
            }
        });
    }

    if (document.body.clientWidth < 768) {
        if ($('.services-m').length > 0) {
            services.init();
        }
    }

}

function formSubmit() {
    $('.form').on("submit", function(){
        var el = $(this);
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize() + '&web_form_submit=' + el.find('button[type="submit"]').val(),
            success: function (data)
            {
                el.find('.form-input').addClass('hide');
                el.find('.btn-modal').addClass('hide');
                el.find('.form-success').addClass('show');
                setTimeout(function() {
                    el.find('.form-input').removeClass('hide');
                    el.find('.btn-modal').removeClass('hide');
                    el.find('.form-success').removeClass('show');
                },5000)
                el[0].reset();
            }
        });
        return false;
    });
}
function ring() {
    if (document.querySelectorAll('.services-ring').length > 0) {
        var services       = document.querySelectorAll('.services-item'),
            services_block = document.querySelector('.services');

        window.onscroll = function() {
            if ( services_block.getBoundingClientRect().top <= 0) {
                document.querySelector('.services-ring').classList.remove('hide');
                services.forEach(function(index,key) {
                    setTimeout(function() {
                        index.classList.remove('hide');
                    },key * 275);
                });
                setTimeout(function() {
                    document.querySelector('.services-content').classList.remove('hide');
                },3000)
            }
        }

        $('.services__link').click(function(e) {
            e.preventDefault();
            $('.services-item').removeClass('active');
            $(this).parent().addClass('active');
        });
    }
}

function mobileMenu() {
    var btnMenu = document.querySelector('.mobile-menu-btn');
    btnMenu.addEventListener('click', function() {
        document.querySelector('.mobile-menu-wrapper').classList.toggle('show');
        document.querySelectorAll('.nav-submenu').forEach(function(elem) {
            elem.classList.remove('show');
        })
    });

    var arrowMenu = document.querySelectorAll('.nav-mobile__arrow');
    arrowMenu.forEach(function(elem) {
        elem.addEventListener('click', function() {
            this.parentElement.nextElementSibling.classList.toggle('show');
        })
    });

    var closeMenu = document.querySelectorAll('.nav-mobile__close');
    closeMenu.forEach(function(elem) {
        elem.addEventListener('click', function() {
            this.parentElement.classList.remove('show');
        })
    });

    var pin = document.querySelector('.header-contacts__pin');
    pin.addEventListener('click', function() {
        document.querySelector('.header-contacts-modal').classList.add('show');
    });
}

function scrollInnerPage() {
    if (document.querySelectorAll('.section .main-inner .link_section').length > 0) {
        var btnLink       = document.querySelectorAll('.section .main-inner .link_section')[0],
            sectionScroll = document.querySelectorAll('.section .h2')[0].getBoundingClientRect().y;
        btnLink.addEventListener('click', function(event) {
            event.preventDefault();
            window.scrollTo({
                top: sectionScroll - 70,
                behavior: 'smooth'
            });
        });
    }
}

function saveCookie() {

    if (document.cookie.indexOf("family-smile=text") != -1) {
        console.log("Family-smile cookie present");
    } else {
        var cookieBtn = document.querySelector('.disclamer-link__btn');

        function addCookie() {
            var date = new Date;
            date.setDate(date.getDate() + 365);
            document.cookie = "family-smile=text; path=/; expires=" + date.toUTCString();
        }

        function cookieBoxDisplayFlex() {
            document.querySelector('.disclamer').style.display = "flex";
        }
        setTimeout(cookieBoxDisplayFlex, 2000);

        function cookieBoxDisplayDisable() {
            document.querySelector('.disclamer').style.display = "none";
            addCookie();
        }

        cookieBtn.addEventListener('click', cookieBoxDisplayDisable);
    }

}

function scrollTop() {
    if (document.getElementsByClassName('scroll-top').length > 0) {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop,
        btn = document.querySelector('.scroll-top');

        if (scrolled > 500) {
            btn.classList.add('show');
        } else {
            btn.classList.remove('show');
        }

        btn.onclick = function() {
            window.scrollTo({ top: 0, behavior: 'smooth' });
        }
    }
}
