$(function() {
    if ($("#map").length > 0) {
        var myMap;
        ymaps.ready(function () {
            myMap = new ymaps.Map('map', {
                center: [54.766839, 32.030318],
                zoom: 18,
                controls: ['zoomControl']
            });
            if (document.body.clientWidth < 1050) {
                myMap.setCenter([54.766761, 32.027432]);
            }
            if (location.pathname == "/contacts/") {
                myMap.setCenter([54.766761, 32.027432]);
            }
            myMap.behaviors.disable("scrollZoom");
            var myPlacemark1 = new ymaps.Placemark([54.766761, 32.027432], {}, {
                iconLayout: 'default#image',
                iconImageHref: '/images/icon/place.svg',
                iconImageSize: [68, 91],
                iconImageOffset: [-30,-100]
            });
            myMap.geoObjects.add(myPlacemark1);
        });
    }
});
