<section class="section section-margin_top">
    <div class="container">
        <div class="section-title">
            <h2 class="h2">Наши специалисты:</h2>
        </div>
        <div class="doctors swiper-container">
            <div class="doctors-wrapper swiper-wrapper">
                <div class="doctors-slide swiper-slide">
                    <div class="doctors-item">
                        <div class="doctors-img doctors-img_doctors">
                            <img src="/images/doctors1.png" alt="doctors1" class="doctors__img">
                        </div>
                        <div class="doctors-desc">
                            <p class="doctors__title">Наконечный Дмитрий Александрович </p>
                            <p class="text doctors__text">Кандидат медицинских наук, врач-стоматолог терапевт, пародонтолог</p>
                            <a href="#" class="doctors__link"></a>
                        </div>
                    </div>
                </div>
                <div class="doctors-slide swiper-slide">
                    <div class="doctors-item">
                        <div class="doctors-img doctors-img_doctors">
                            <img src="/images/doctors1.png" alt="doctors1" class="doctors__img">
                        </div>
                        <div class="doctors-desc">
                            <p class="doctors__title">Наконечный Дмитрий Александрович </p>
                            <p class="text doctors__text">Кандидат медицинских наук, врач-стоматолог терапевт, пародонтолог</p>
                            <a href="#" class="doctors__link"></a>
                        </div>
                    </div>
                </div>
                <div class="doctors-slide swiper-slide">
                    <div class="doctors-item">
                        <div class="doctors-img doctors-img_doctors">
                            <img src="/images/doctors1.png" alt="doctors1" class="doctors__img">
                        </div>
                        <div class="doctors-desc">
                            <p class="doctors__title">Наконечный Дмитрий Александрович </p>
                            <p class="text doctors__text">Кандидат медицинских наук, врач-стоматолог терапевт, пародонтолог</p>
                            <a href="#" class="doctors__link"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination-container pagination-container_doctors">
                <div class="pagination pagination_doctors swiper-pagination"></div>
            </div>
        </div>
    </div>
</section>
