<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отбеливание зубов");
?>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Сделаем зубы белоснежными, а улыбку — ослепительной</h1>
                </div>
                <p class="text mb-30">Яркая, запоминающаяся улыбка — лучший способ сделать ваш образ неотразимым. Обращайтесь в стоматологическую клинику FamilySmile — опытный врач подберет способ отбеливания, который лучше всего подойдет для ваших зубов. Всего за один прием мы сделаем эмаль светлее до 12 тонов с помощью современных безопасных методик. У нас есть несколько вариантов: кабинетные Zoom 4 и Opalescence Boost, а также домашнее отбеливание зубов.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-30">Не существует одинаковых улыбок, каждый случай индивидуален, поэтому подготовка к отбеливанию зубов должна проходить очень тщательно. В первую очередь необходимо устранить возможные противопоказания и очистить эмаль зубов. Зачем? Ваши зубы светлее на 1–2 тона, чем кажутся, просто они скрыты под пигментированным налетом. Кроме того, отбеливать зубной налет нет никакого смысла.</p>
                <a href="/services/esteticheskaya-stomatologiya/professionalnaya-gigiena/" class="link link_section">Подробнее</a>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Преимущества отбеливания зубов в стоматологии FamilySmile</h2>
                <div class="dots mb-50">
                    <div class="dots-item">
                        <p class="dots__title">Индивидуальный подход</p>
                        <p class="text dots__text">Мы учтем все особенности ваших зубов: наличие пломб, коронок, состояние эмали. Наши специалисты постараются выполнить ваши пожелания.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Комплексная подготовка</p>
                        <p class="text dots__text">Перед отбеливанием проведем полную диагностику полости рта, чтобы подготовить ваши зубы к процедуре. Мы вылечим кариес, устраним налет и укрепим эмаль.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Свобода выбора</p>
                        <p class="text dots__text">Вернем вашей эмали природный цвет с помощью профессиональной гигиены. После чего вы сможете сами выбрать желаемый оттенок зубов по специальной шкале.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Безопасность процедур</p>
                        <p class="text dots__text">Тщательно защищаем ваши глаза, губы и мягкие ткани полости рта. На десны наносим специальный гель, который твердеет и оберегает пародонт от внешнего воздействия.</p>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title mb-70">
                <h2 class="h2 mb-30">Отбеливание зубов Zoom 4</h2>
                <p class="text">Лампа холодного света Zoom 4 произвела настоящую революцию в отбеливании зубов и быстро стала популярной во всем мире. И это неудивительно: всего за один визит к стоматологу ваши зубы станут светлее до 12 тонов. Методика совершенно безопасна для эмали, а полученный результат при правильном уходе сохраняется около 2 лет.</p>
            </div>
            <div class="section-title">
                <h2 class="h2 tac">Как проходит отбеливание зубов <br>Zoom 4 в стоматологии FamilySmile</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Отбеливание зубов Opalescence Boost</h2>
                <p class="text mb-80">Основное отличие методики отбеливания зубов Opalescence Boost в том, что она проводится без использования светового воздействия. Этот способ является самым безопасным и простым: на зубы наносится гель из двух действующих компонентов, который через 20 минут удаляется.</p>
                <h3 class="h3 mb-40">Особенности отбеливания зубов Opalescence Boost</h3>
                <div class="dots mb-50">
                    <div class="dots-item">
                        <p class="dots__title">Равномерность</p>
                        <p class="text dots__text">У геля яркий цвет, поэтому врач легко регулирует плотность и качество его нанесения.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Эффективность</p>
                        <p class="text dots__text">За один прием можно провести несколько процедур и отбелить зубы до 8 оттенков.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Долговечность</p>
                        <p class="text dots__text"> Если соблюдать все рекомендации врача, эффект процедуры продлится более полутора лет.</p>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/4.png" alt="block4" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/5.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/6.png" alt="block6" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2 mb-40">Домашнее отбеливание зубов</h2>
                <p class="text mb-80">Возможно ли отбелить зубы профессиональными средствами в домашних условиях? Для вашего комфорта у нас есть проверенные системы Opalescence PF. В стандартный набор входит все необходимое, чтобы ваша улыбка засияла по-новому. Выбирая домашнее отбеливание зубов, необходимо учитывать, что оно занимает некоторое время.</p>
                <h3 class="h3 mb-40">Как проводить отбеливание зубов дома</h3>
                <div class="dots mb-65">
                    <div class="dots-item">
                        <p class="text dots__text">Заполните капу гелем и наденьте на зубы</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Капу можно оставить на всю ночь</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Утром нужно снять капу и убрать остатки геля</p>
                    </div>
                </div>
                <p class="text">Гель Opalescence PF насыщен фторидом и нитратом калия. Элементы не только укрепляют эмаль, препятствуя образованию кариеса, но и снижают чувствительность.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
                <blockquote class="blockquote">После отбеливания зубов, чтобы надолго сохранить эффект, потребуется тщательный уход за полостью рта. Необходимо будет соблюдать «белую» диету в течение 7–10 дней. То есть исключить из рациона продукты с красящими веществами: чай, кофе, соки, ягоды, вино и другие. А в дальнейшем регулярно проходить профессиональную гигиену.</blockquote>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <p class="text mb-50">Мы поможем вам обрести уверенность в себе благодаря красивой сверкающей улыбке. Специалисты FamilySmile учтут все нюансы и сделают ваши зубы белоснежными с помощью эффективных методик отбеливания. Ждем вас на консультации.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<script defer src="main.js"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
