<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Установка виниров");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h2 mb-30">Создадим для вас идеальную улыбку с многолетней гарантией</h2>
                </div>
                <p class="text mb-30">Ровные, красивые зубы и ослепительная улыбка, как у героев голливудских фильмов, — ваша мечта? Тогда приходите в стоматологическую клинику FamilySmile. Мы делаем улыбки не только здоровыми, но и красивыми. Для исправления недостатков передних зубов используем ультратонкие виниры из керамики или композита. Ваша улыбка станет роскошной, словно у кинозвезды!</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 tac mb-40">Эффективный способ получить <br>безупречную улыбку</h2>
                <p class="text mb-30">Виниры — тончайшие накладки, разработанные специально для звезд Голливуда в начале XX века. Они представляют собой керамические пластинки толщиной от 0,2 мм, которые крепятся на передние зубы. Виниры идеально прилегают к поверхности зубов, значительно преображая улыбку.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Вам стоит выбрать<br>керамические виниры,<br>если вы хотите:</h2>
                <div class="dots mb-50">
                    <div class="dots-item">
                        <p class="dots__title">Изменить форму зубов</p>
                        <p class="text dots__text">С помощью керамических накладок можно исправить слишком короткие или мелкие зубы.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Скрыть недостатки улыбки</p>
                        <p class="text dots__text">Виниры замаскируют слишком большие промежутки между резцами или скученность зубов.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Получить белоснежные зубы</p>
                        <p class="text dots__text">Если отбеливание невозможно или не дает нужного результата, помогут виниры.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Спрятать дефекты эмали</p>
                        <p class="text dots__text">Сколы, трещины, потертости, пятна и другие недостатки будут надежно скрыты.</p>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества керамических виниров</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img src="/images/icon/color.svg" alt="inf">
                            </div>
                            <p class="subtitle inf__title">Индивидуальное изготовление</p>
                            <p class="text">Подбирается идеальная форма и цвет зубов с учетом ваших пожеланий.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img src="/images/icon/veneer.svg" alt="inf">
                            </div>
                            <p class="subtitle inf__title">Безупречная эстетика</p>
                            <p class="text">Благодаря винирам ваши зубы будут выглядеть ровными и красивыми.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img src="/images/icon/durability.svg" alt="inf">
                            </div>
                            <p class="subtitle inf__title">Долгий срок службы</p>
                            <p class="text">При правильном уходе керамические накладки прослужат вам длительное время.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img src="/images/icon/resistance_veneer.svg" alt="inf">
                            </div>
                            <p class="subtitle inf__title">Износостойкость</p>
                            <p class="text">На гладкой поверхности не задерживается налет, цвет керамики не меняется с годами.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <blockquote class="blockquote">Виниры выглядят как настоящие зубы. Никто даже не догадается, что на самом деле ваша яркая улыбка — результат ювелирной работы стоматолога. И дело не только в миниатюрном размере накладок — современная керамика идентична настоящей эмали зубов по способности пропускать и отражать свет.</blockquote>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-50-xs mt-0-md">
                <blockquote class="blockquote blockquote_tab mb-30">
                    Виниры выглядят, как настоящие зубы. Никто даже не догадается, что на самом деле ваша яркая улыбка — результат ювелирной работы стоматолога. И дело не только в миниатюрном размере накладок: современная керамика идентична настоящей эмали зубов по способности пропускать и отражать свет.
                </blockquote>
            </div>
            <div class="main-inner">
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Цифровая точность изготовления керамических виниров</h2>
                <p class="text mb-80">Каждый винир — это стоматологический шедевр авторской работы, который изготовлен в зуботехнической лаборатории клиники FamilySmile. При создании керамической накладки используются цифровые технологии — система компьютерного моделирования CAD/CAM. А чтобы добиться совершенства, специалист дорабатывает изделие вручную.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/4.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Как создается винир</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Подготовка</p>
                        <p class="text dots__text">Доктор проводит консультацию, выясняет ваши пожелания относительно цвета и формы зубов. Он снимает слепки и делает фотопротокол улыбки.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Предварительное моделирование</p>
                        <p class="text dots__text">По слепкам изготавливается восковая модель виниров. Создается пластмассовый прототип изделий.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Согласование</p>
                        <p class="text dots__text">Врач показывает вам с помощью прототипа, как будет выглядеть ваша улыбка. На этом этапе вы можете оценить результат и внести изменения.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Моделирование</p>
                        <p class="text dots__text">Стоматолог обрабатывает зубы и снимает слепки, которые сканируются системой CAD/CAM. В программе создается виртуальная модель виниров.</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <div class="dots mb-80">
                    <div class="dots-item">
                        <p class="dots__title">Изготовление</p>
                        <p class="text dots__text">Цифровой макет поступает на специальный фрезерный станок. Изделие вытачивается из цельного блока керамики с точностью до микрона.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Доработка</p>
                        <p class="text dots__text">Зубной техник, словно ювелир, работает над виниром и придает ему индивидуальность, добиваясь идеального результата.</p>
                    </div>
                </div>
                <p class="text mb-50">Пока идет изготовление керамических накладок, на ваши зубы будут установлены временные виниры. Они надежно защитят обработанные поверхности от внешних воздействий и сохранят эстетику улыбки. После доктор снимет временные конструкции, и тончайшие керамические пластинки будут прочно зафиксированы на ваших зубах.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/5.png" alt="block5" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Маскировка мелких<br>изъянов передних зубов</h3>
                <p class="text mb-40-xs mb-80-xl">Если нужно поправить незначительные недостатки улыбки, виниры можно сделать всего за один визит в клинику FamilySmile. Стоматолог по долям миллиметра, слой за слоем нанесет на зубы композитный материал и сформирует тончайшую пластину, которая скроет скол, трещину или шероховатость. С помощью композитного винира также можно исправить и цвет зубов.</p>
                <blockquote class="blockquote">Эстетическая реставрация композитными винирами проводится с использованием специальных оптических приборов — бинокуляров и микроскопа. Благодаря многократному увеличению доктор учитывает мельчайшие переходы цвета и создает идеальный режущий край.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-50">Идеальную улыбку можно легко получить, посетив стоматологию FamilySmile. Для этого применяем современные технологии и материалы. Опытные специалисты создают настоящие произведения стоматологического искусства. Наши виниры делают зубы красивыми и служат много лет, преображая ваш имидж. </p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
