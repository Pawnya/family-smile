<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Профессиональная гигиена");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Регулярная забота о <br>зубах — залог их здоровья</h1>
            </div>
            <p class="text mb-30">Хотите как можно реже лечить зубы? Приходите в стоматологическую клинику FamilySmile на профессиональную чистку зубов. Мы эффективно устраним причину большинства стоматологических проблем — зубной налет — и укрепим эмаль. А еще у процедуры есть приятный бонус: зубы станут более гладкими и светлыми.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Чем опасен зубной налет</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="text dots__text">В нем находятся кислоты, разрушающие эмаль. Она со временем истончается, в результате чего образуется кариес.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Мягкий налет постепенно твердеет. Образуется зубной камень,   который травмирует нежные ткани десен, вызывая их воспаление.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">В отложениях скапливаются красящие вещества из пищи, напитков. Зубы кажутся темнее, так как их естественный оттенок скрыт под налетом.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h2 class="h2 mb-40">Профессиональная гигиена полости рта в стоматологии FamilySmile</h2>
            <p class="text mb-50">Мы не сомневаемся, что вы регулярно чистите зубы. Но, к сожалению, в домашних условиях   невозможно полностью удалить налет. Он остается в зубодесневых промежутках, между зубами и на дальних участках. Специалисты FamilySmile качественно очистят зубы от любых отложений с помощью специального оборудования, отполируют и защитят эмаль для сохранения долгосрочного эффекта.</p>
            <blockquote class="blockquote mb-50">Профессиональная гигиена полости рта — эффективная профилактика большинства стоматологических заболеваний. Она помогает сохранить здоровье зубов и десен, а значит, сэкономить на лечении у стоматолога. Достаточно проходить процедуру каждые 6 месяцев, чтобы обеспечить надежную защиту вашей улыбки.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper pr-0-xs pr-95-md order-2">
            <h2 class="h2 mb-40">Чистка зубов ультразвуком</h2>
            <p class="text mb-80">Именно твердые отложения придают зубам желтый или коричневый оттенок. Их невозможно убрать даже жесткой щеткой. Для этого у стоматологов есть специальное оборудование — ультразвуковой скалер. Процедура дает возможность деликатно устранить зубной камень, при этом она абсолютно безопасна для эмали.</p>
            <h3 class="h3 mb-50">Как проходит ультразвуковая чистка зубов</h3>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Быстро</p>
                    <p class="text dots__text">Процедура занимает примерно 15 минут.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Эффективно</p>
                    <p class="text dots__text">Колебания ультразвука разбивают твердые отложения.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Безопасно</p>
                    <p class="text dots__text">Частицы зубного камня вымываются раствором.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper block-wrapper_tleft">
            <blockquote class="blockquote mt-20 mb-50">Вся процедура занимает от 30 до 60 минут, в зависимости от сложности. Полностью очищаются даже труднодоступные для щетки места: межзубные промежутки, внутренняя поверхность зубов, дальние зубы. Ультразвуковая чистка является эффективной профилактикой воспалений пародонта: убирается зубной камень под деснами на глубине 1–3 мм, они становятся плотнее и лучше прилегают к зубам.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper pr-0-xs pr-95-md order-2">
            <h2 class="h2 mb-40">Очищение зубов Air-Flow</h2>
            <p class="text mb-80">Аппарат Air-Flow применяется для снятия мягкого зубного налета и пищевой пигментации. Такая чистка является первым этапом отбеливания зубов, потому что возвращает эмали природный цвет. После процедуры вы узнаете, что на самом деле ваши зубы гораздо светлее, чем обычно вы видите в зеркале.</p>
            <h3 class="h3 mb-50">Как проходит процедура</h3>
            <div class="dots">
                <div class="dots-item">
                    <p class="text dots__text">Доктор через насадку направляет на зубы смесь воды, воздуха и медицинской соды.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Под давлением мягко и безболезненно удаляются все отложения и бактерии.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Врач полирует эмаль укрепляющими пастами, выравнивая ее поверхность.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper block-wrapper_tleft">
            <blockquote class="blockquote mt-20 mb-50">Под воздействием аппарата Air-Flow зубы становятся не только белее, но и ровнее. Очищающий душ с абразивными частицами бережно полирует зубы, устраняя мелкие неровности эмали. Ваша улыбка станет привлекательнее.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/4.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Укрепление эмали с помощью фторирования</h2>
            <p class="text">После чистки зубов все профилактические процедуры максимально эффективны. Чтобы создать защитный барьер, восполнить недостаток минералов и снизить чувствительность зубов, эмаль покрывают лаком с содержанием фтора и кальция.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h3 class="h3 mb-50">Результаты профессиональной гигиены полости рта</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Чистота полости рта. Очищается вся поверхность зубов, в том числе области, недоступные для зубной щетки.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Здоровые зубы. Гладкая, укрепленная эмаль защищена от воздействия налета и образования кариеса.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Крепкие десны. Мягкие ткани меньше подвержены риску воспаления и развития заболеваний.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Мы с удовольствием станем вашим союзником в борьбе за здоровье улыбки. Приходите в стоматологию FamilySmile 2 раза в год на профессиональную гигиену полости рта. Наши специалисты надежно защитят ваши зубы и десны.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
