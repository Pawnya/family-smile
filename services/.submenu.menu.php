<?
$aMenuLinks = Array(
    Array(
        "Лечение зубов",
        "/services/lechenie-zubov/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Пародонтология",
        "/services/parodontologiya/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Гнатология",
        "/services/gnatologiya/",
        Array(),
        Array("INFO"=>"Составляем план лечения, учитывая все нюансы функционирования зубочелюстной системы"),
        ""
    ),
    Array(
        "Имплантация",
        "/services/implantatsiya/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Исправление прикуса",
        "/services/ispravlenie-prikusa/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Диагностика",
        "/services/diagnostika/",
        Array(),
        Array("INFO"=>"Проводим детальное обследование с помощью самого современного и точного оборудования"),
        ""
    ),
    Array(
        "Хирургия",
        "/services/khirurgiya/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Детская стоматология",
        "/services/detskaya-stomatologiya/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Протезирование",
        "/services/protezirovanie/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Эстетическая стоматология",
        "/services/esteticheskaya-stomatologiya/",
        Array(),
        Array(),
        ""
    )
);
?>
