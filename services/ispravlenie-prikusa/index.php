<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Исправление прикуса");
?>
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Сделать зубы ровными легче, чем вы думаете</h1>
                </div>
                <p class="text mb-30">Безупречная улыбка украшает каждого. Если у вас неровные зубы или неправильный прикус, важно обратиться к опытному ортодонту FamilySmile. Изменения не только положительно повлияют на ваш внешний вид, но и принесут пользу всему организму. С нашей помощью вы обретете счастливое будущее, наполненное здоровьем, комфортом и красотой. </p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Наши методики исправления прикуса</h2>
            </div>
            <? $bq = 'Персональный подход для нас не просто слова, а главный принцип работы. Для каждого случая врач-ортодонт FamilySmile находит свое решение. Важно, чтобы лечение проходило с пользой и комфортом для вас. Записывайтесь на прием, вместе мы подберем подходящий способ лечения.';?>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                    <div class="box-slide swiper-slide">
                        <div class="box-item box-item_last">
                            <blockquote class="blockquote mb-50">
                                <?=$bq;?>
                            </blockquote>
                            <?$APPLICATION->IncludeComponent(
                                "family-smile:form.result.new",
                                "inline",
                                Array(
                                    "WEB_FORM_ID"            => "1",
                                    "AJAX_OPTION_STYLE"      => "Y",
                                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                                    "USE_EXTENDED_ERRORS"    => "Y",
                                    "SEF_MODE"               => "N",
                                    "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                    "CACHE_TYPE"             => "N",
                                    "CACHE_TIME"             => "3600",
                                    "LIST_URL"               => "",
                                    "EDIT_URL"               => "",
                                    "SUCCESS_URL"            => "",
                                    "CHAIN_ITEM_TEXT"        => "",
                                    "CHAIN_ITEM_LINK"        => "",
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-50-xs mt-0-md">
                <blockquote class="blockquote blockquote_tab mb-30">
                    <?=$bq;?>
                </blockquote>
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/2.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Восстанавливаем красоту, не забываем о здоровье </h2>
                <p class="text mb-40">Эстетика улыбки — не первое, что должно волновать пациента с неровными зубами и неправильным прикусом. Важно провести ортодонтическое лечение для сохранения здоровья и целостности зубов, тщательного пережевывания пищи, правильного функционирования челюстного сустава и вашего комфортного самочувствия — без головных, шейных болей.</p>
                <p class="text">Не допускайте появления или развития серьезных последствий, записывайтесь на консультацию к ортодонту стоматологии FamilySmile. Мы владеем лучшими методиками лечения и основываемся на гнатологическом подходе, согласно которому выравнивание зубных рядов проходит по законам природы. </p>
            </div>
            <div class="block-img order-1">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block">
            <div class="block-wrapper block-wrapper_left">
                <h3 class="h3 mb-40">Показания для обращения к ортодонту</h3>
            </div>
        </div>
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
                <div class="dots mb-60">
                    <div class="dots-item">
                        <p class="text dots__text">Скученность зубов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Неполное смыкание челюстей</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Повышенная стираемость зубов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Сколы на коронках</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Хруст и боль в челюсти</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Начать ортодонтическое лечение и позаботиться о себе никогда не поздно. Клиника FamilySmile поддерживает семейные традиции. Устройте себе день здоровья. Записывайтесь на прием и приводите с собой маленьких пациентов. Наши врачи вовремя обнаруживают нарушения прикуса у детей и назначают своевременное лечение. А также возвращают здоровье и красоту улыбки взрослым.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества исправления прикуса в FamilySmile</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-50">Семья и здоровье — важные ценности каждого человека. Берегите себя и близких, а мы поможем вам в этом. Приходите к ортодонту в стоматологию FamilySmile. Специалист применяет лучшие системы исправления прикуса, чтобы ваша улыбка была здоровой и красивой.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
