<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Эстетические брекеты");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Выравнивание зубов изящной и почти незаметной конструкцией</h1>
                </div>
                <p class="text mb-30">Вы уже давно хотите исправить все недостатки прикуса и получить красивую улыбку, но переживаете из-за внешнего вида брекетов? Стоматологи клиники FamilySmile предлагают вам пройти ортодонтическое лечение с помощью легких керамических, сапфировых или циркониевых конструкций, которые практически незаметны на зубах. Вы будете чувствовать себя комфортно, поэтому лечение пройдет без стресса, а красивая улыбка будет радовать каждый день.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">В чем плюсы эстетических брекетов</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <p class="text mb-80">Керамические и сапфировые брекеты выдерживают большую нагрузку и не деформируются на протяжении всего периода лечения. В некоторых случаях, когда требуется особо прочная конструкция, врач рекомендует устанавливать циркониевые брекет-системы. Их замочки изготовлены из искусственного минерала, сочетающего в себе эстетику керамики и прочность металла.</p>
                <blockquote class="blockquote mb-80">В стоматологии FamilySmile план ортодонтического лечения составляется с учетом ваших индивидуальных особенностей и принципов гнатологии. После лечения у вас будет красивая улыбка и восстановится правильная работа зубочелюстной системы, так как зубы примут физиологически верное положение.</blockquote>
                <p class="text mb-50">Исправляя прикус и выравнивая зубной ряд, вы минимизируете риск стираемости зубов и рецессии десны, приводящих к повышенной чувствительности эмали и другим возможным негативным последствиям. Также прекращаются головные боли, которые были прямым следствием неправильной работы височно-нижнечелюстного сустава. Ждем вас на прием у наших специалистов для устранения причин и последствий неправильного прикуса!</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2">Особенности брекет-систем</h2>
            </div>
        </div>
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <p class="text mb-50">Конструкции из керамики, сапфира или циркония считаются наиболее эстетичными из-за свойств материалов. Благодаря гармоничному цвету замочки словно сливаются с эмалью и остаются на ней практически незаметными. Особо отметим искусственный сапфир: он полностью прозрачен и незаметен даже на свету.</p>
                <p class="text">Ортодонтическое лечение в нашей клинике начинается с подробной диагностики. Доктор проводит тщательное обследование – малый и большой функциональные анализы – и составляет наиболее эффективный план восстановления улыбки.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-50-xs mt-25-xl">
                <blockquote class="blockquote">Среди проводимых нами обследований — электромиография, аксиография, артикуляционный анализ. С помощью специализированного оборудования врач не только оценивает тонус и характер работы мышц и нервных волокон, но и выявляет причины болей, щелканья и хруста суставов челюсти. Наши подробные исследования — это гарантия качественного лечения.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества исправления прикуса в нашей клинике    </h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-50">Зубы можно сделать ровными в любом возрасте. Керамические, циркониевые и сапфировые брекеты помогут сохранить эстетику вашей улыбки во время лечения. Обратитесь в стоматологию FamilySmile — мы поможем вам избавиться от всех недостатков прикуса с помощью удобных изящных конструкций.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
