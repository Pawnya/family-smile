<?
$aMenuLinks = Array(
	Array(
		"Металлические брекеты",
		"/services/ispravlenie-prikusa/metallicheskie-brekety/",
		Array(),
		Array("INFO"=>"Классические системы, проверенные временем, для эффективного и надежного результата"),
		""
	),
	Array(
		"Эстетические брекеты",
		"/services/ispravlenie-prikusa/keramicheskie-i-sapfirovye-brekety/",
		Array(),
		Array("INFO"=>"Изящное решение для сохранения эстетики улыбки во время лечения"),
		""
	),
	Array(
		"Исправление прикуса по методике Садао Сато",
		"/services/ispravlenie-prikusa/ispravlenie-prikusa-po-metodike-sadao-sato/",
		Array(),
		Array("INFO"=>"Индивидуальная помощь в непростых случаях"),
		""
	),
	Array(
		"Исправление прикуса без брекетов",
		"/services/ispravlenie-prikusa/ispravlenie-prikusa-bez-breketov/",
		Array(),
		Array("INFO"=>"Современная методика для комфортного лечения съемными аппаратами"),
		""
	)
);
?>
