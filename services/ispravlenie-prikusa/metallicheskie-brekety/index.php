<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Металлические брекеты");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Надежный способ выравнивания зубов</h1>
            </div>
            <p class="text mb-30">Вы хотите сделать зубы ровными и почувствовать себя привлекательнее? Специалисты стоматологии FamilySmile предлагают вам эффективный способ исправления прикуса – металлические брекеты. Они помогут вам обрести красивую улыбку! Приходите на прием – ортодонтическое лечение можно проводить в любом возрасте.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Какие недостатки исправляют металлические брекеты</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="text dots__text">Искривление одного или нескольких зубов</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Изменения профиля</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Увеличенные промежутки между зубами</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Недостаток места на челюсти</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <p class="text mb-50">Важно понимать, что любая брекет-система является лишь инструментом в руках врача-ортодонта, который уже на первом приеме должен увидеть, как ваша улыбка будет выглядеть в результате лечения. Исправление прикуса брекетами — продолжительный процесс, поэтому у нас нет права на ошибку. Наши специалисты всегда с полной ответственностью подходят к решению данной проблемы. Детальный осмотр и качественная диагностика позволяют составить оптимальный план для достижения эффективного результата.</p>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <blockquote class="blockquote mb-50">Многие откладывают исправление прикуса из-за того, что брекеты кажутся тяжелыми, громоздкими. Однако современные системы имеют небольшие замочки обтекаемой формы. Они аккуратнее выглядят на зубах. Обратитесь к нашим специалистам, чтобы выбрать наиболее подходящую брекет-систему.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h3 class="h3 mb-40">Преимущества металлических брекетов</h3>
            <div class="dots">
                <div class="dots-item">
                    <p class="text dots__text">Исправление сложных недостатков прикуса. Металлические брекеты обладают достаточно высокой прочностью и жесткостью конструкции, они помогут исправить сложные патологии.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Сохранение цвета эмали. В процессе лечения металлические замочки не окрашивают зубы — после исправления прикуса и проведения профессиональной гигиены на эмали не останется темных пятен.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Высокая прочность конструкции. Брекеты изготавливаются из специальных металлических сплавов. Они не деформируются и сохраняют свою форму на протяжении всего периода лечения.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Металлические брекеты — это наиболее простой, надежный метод выравнивания зубов. Конечно, такое лечение требует терпения и регулярного посещения специалиста. Врачи клиники FamilySmile помогут вам получить красивую ровную улыбку и будут поддерживать вас на протяжении всего периода исправления прикуса.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
