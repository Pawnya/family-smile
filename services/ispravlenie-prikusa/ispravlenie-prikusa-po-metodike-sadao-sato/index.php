<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Исправление прикуса по методике Садао Сато");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Многопетлевая техника для устранения сложных патологий</h1>
                </div>
                <p class="text mb-30">Неправильный прикус — причина появления психологических комплексов и ухудшения состояния здоровья. Однако зубам можно придать правильное положение даже при серьезной патологии. Врачи стоматологии FamilySmile используют уникальную технику лечения брекетами, разработанную профессором Садао Сато. При исправлении прикуса применяется гнатологический подход, учитывается состояние височно-нижнечелюстного сустава и жевательных мышц. Мануальная и функциональная диагностики дают врачу полную информацию о состоянии зубочелюстной системы. Обращайтесь к нам для эффективного исправления патологий прикуса.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_top">
            <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
                <h2 class="h2 mb-40">Особенности многопетлевой техники </h2>
                <p class="text">Многопетлевая дуга изготавливается вручную для каждого пациента. При фиксации дуги используются металлические или керамические лигатурные брекеты. Вместе с врачом вы выберете наиболее подходящую систему, которая будет отвечать всем требованиям к эстетике и функциональности.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-80-xl">
                <blockquote class="blockquote mb-50">Изначально многопетлевая техника, или Multiloop Edgewise ArchWire (MEAW), была разработана для лечения открытого прикуса. Впоследствии профессор Садао Сато разработал несколько вариантов применения многопетлевой техники для устранения патологий прикуса любого типа с гнатологическим подходом.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества методики Садао Сато</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
        <div class="container block block_top mt-30-xs mt-0-md">
            <div class="block-wrapper pl-0-xs pl-95-xl">
                <p class="text mb-50">Главное преимущество многопетлевой техники состоит в том, что такой способ позволяет исправить недостатки прикуса без удаления премоляров. Кроме того, практически не требуется использование ортодонтических имплантатов и мини-пластин, без которых не обходится классическое лечение. В некоторых случаях методика Садао Сато успешно справляется со сложными проблемами, когда необходимо ортогнатическое хирургическое вмешательство.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-100-xl mt-60-xs mt-0-xl">
                <blockquote class="blockquote">При лечении по методике профессора Садао Сато применяются специальные окклюзионные накладки из композитных материалов. Они необходимы для формирования правильного смыкания зубов, а также перепрограммирования мышц. Обратитесь к нашим специалистам для эффективного ортодонтического лечения без хирургического вмешательства.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Что влияет на результат лечения</h2>
                <p class="text mb-80">Любое ортодонтическое лечение предполагает постоянный контакт доктора и пациента. Только так можно добиться быстрого и долгосрочного результата.</p>
                <h3 class="h3 mb-40">Самое важное при лечении:</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">строгое соблюдение графика осмотров</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">регулярная ежедневная гигиена, а раз в 3–4 месяца — профессиональная чистка</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">использование эластиков на протяжении всего периода исправления прикуса</p>
                    </div>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-30">Выравнивание зубов по методике Садао Сато занимает 1,5–2 года, в зависимости от сложности случая. В результате вы получаете ровные зубы без дополнительных операций. Записывайтесь на прием в стоматологию FamilySmile для эффективного устранения патологий прикуса.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
