<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Исправление прикуса без брекетов");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Эффективно преображаем улыбку без использования брекетов</h1>
            </div>
            <p class="text mb-30">Мечтаете скорректировать положение зубов и обрести красивую, здоровую улыбку? Но при этом не хотите устанавливать брекеты? Тогда обратитесь за помощью к опытным ортодонтам стоматологической клиники FamilySmile. Они подберут для вас оптимальный способ выравнивания зубов и исправления прикуса. Мы устраним любые несовершенства зубного ряда и восстановим здоровье даже без фиксации брекетов.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="main-inner">
            <p class="text">Провести ортодонтическое лечение необходимо не только для создания безупречной улыбки, но и для крепкого здоровья. Ведь неровные зубы и неправильный прикус влекут за собой серьезные проблемы. Появляются воспаление десен, повышенная чувствительность зубов, асимметрия лица, головные боли, проблемы с пищеварительной системой. Поэтому не откладывайте визит к врачу. Тем более что выровнять зубы взрослым можно и без установки брекетов. Для этого у нас есть трейнеры и элайнеры.</p>
        </div>
    </div>
</section>

<section class="section section-margin_top">
    <div class="container">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Трейнеры</h2>
        </div>
    </div>
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <p class="text mb-50">Это двухчелюстные силиконовые капы с ячейками для каждого зуба. Их действие направлено и на зубы, и на мышцы челюстно-лицевой области. Носить конструкцию нужно 2 часа днем, а потом надевать на ночь. Трейнеры исправляют небольшие дефекты:</p>
            <div class="dots">
                <div class="dots-item">
                    <p class="text dots__text">легкую скученность зубов</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">логопедические проблемы</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">наклон одного или двух зубов</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">гипертонус жевательных мышц</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <p class="text mb-40">Для устранения более сложных случаев мы применяем элайнеры. Какой способ  выравнивания зубов без брекетов  подойдет именно вам, сможете узнать на консультации у нашего специалиста.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>

<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>

<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper pl-0-xs pl-90-xl mt-80-xs mt-0-xl">
            <h2 class="h2 mb-40">Элайнеры</h2>
            <p class="text mb-50">Это съемные капы, которые изготавливаются по вашим индивидуальным слепкам из прозрачного материала. Такие конструкции понравятся тем, кто работает с аудиторией и заботится о своем безупречном виде. Ведь элайнеры практически незаметны. Капа рассчитана на 2 недели, за это время она выполняет свою программу по перемещению зубов, затем надевается новая. Носить конструкции нужно постоянно, однако снимать их можно во время приема пищи и чистки зубов. После лечения элайнерами ваша улыбка преобразится, и вы почувствуете себя намного увереннее.</p>
            <blockquote class="blockquote mb-50">Элайнеры необходимы при повороте или наклоне зуба, небольшой скученности зубов, увеличенных межзубных промежутках, недостатке места для имплантации и протезирования. Также капы используются для закрепления результата после лечения брекетами.</blockquote>
        </div>
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h3 class="h3 mb-40">Почему стоит выбрать элайнеры</h3>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Незаметное лечение</p>
                    <p class="text dots__text">Индивидуальные капы полупрозрачные, они позволяют сохранить эстетику улыбки на протяжении всего лечения.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Абсолютная биосовместимость</p>
                    <p class="text dots__text">Элайнеры изготавливаются из гипоаллергенного материала, который подходит всем.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Удобное ношение</p>
                    <p class="text dots__text">Конструкции практически не ощущаются, их также можно снять во время приема пищи или перед важным событием.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Качественный результат</p>
                    <p class="text dots__text">Капы оказывают мягкое, но точное действие, они отлично справляются с ортодонтическими проблемами.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>

<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-30">Лечение без брекетов в стоматологической клинике FamilySmile — это уникальная возможность преобразить свою улыбку. Наши опытные и внимательные врачи подберут для вас съемный аппарат, чтобы ваши зубы стали ровными, а прикус — правильным. Запишитесь на прием — восстановите здоровье и измените свой образ в лучшую сторону!</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
