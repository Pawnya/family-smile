<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Удаление зубов с целью последующей имплантации");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Надежное восстановление зуба после бережного удаления</h1>
                </div>
                <p class="text mb-30">Если зуб уже не поддается лечению, то необходимо удаление. Мы поможем восстановить утрату в тот же день. Опытный хирург стоматологической клиники FamilySmile аккуратно извлечет зуб, после чего установит имплантат с коронкой. Ваш комфорт при приеме пищи и улыбке вернется в ближайшее время после операции.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text">Имплантация — оптимальный способ восстановления зубов. Коронка на имплантате по ощущениям практически не отличается от родного зуба. Функция и эстетика полностью восстанавливаются на долгие годы. А если установить имплантат сразу после удаления зуба, то максимально сохраняется здоровье зубов и челюстей. </p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Почему имплантация сразу <br>после удаления — самый <br>предпочтительный вариант</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Комфортно</p>
                        <p class="text dots__text">За один прием проходит две операции: удаление зуба и установка имплантата.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Быстро</p>
                        <p class="text dots__text">Период приживления нового «корня» зуба сокращается на несколько месяцев.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Эстетично</p>
                        <p class="text dots__text">Можно сразу закрепить коронку, поэтому улыбка останется красивой.</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <p class="text mb-50">Для установки имплантата должно быть достаточно костной ткани, поэтому важно соблюсти два условия. Во-первых, удаление должно быть очень бережным, чтобы не повредить кость. Во-вторых, не откладывать надолго имплантацию. Если вам удалили зуб более 6 месяцев назад, то костной ткани может стать уже слишком мало для фиксации имплантата. В таком случае потребуется дополнительная операция по ее наращиванию. </p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Современные технологии <br>для удаления зубов <br>и имплантации</h2>
                <p class="text mb-50">Для проведения хирургических операций, а также имплантации, синус-лифтинга и костной пластики в нашей клинике есть ультразвуковой хирургический аппарат Piezon Master Surgery (компания EMS, Швейцария). Оборудование позволяет проводить удаление зуба очень аккуратно, не повреждая мягкие ткани. С помощью точно направленной вибрации ультразвука зуб разделяется на несколько частей и бережно извлекается.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Удаление зубов аппаратом Piezon <br>Master Surgery</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
                <h2 class="h2 mb-40">Подготовка к удалению зуба с целью последующей имплантации</h2>
                <p class="text pr-0-xs pr-70-xl">Самый важный этап подготовки к имплантации — диагностика. Обязательно проводится обследование на компьютерном томографе, который создает снимки в формате 3D. Доктор может рассмотреть любую область при многократном увеличении с разных ракурсов. Пользуясь снимком, он оценивает объем и плотность костной ткани, расположение корней зуба, чтобы спланировать операцию.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mt-40 mb-50">В стоматологической клинике FamilySmile имеется современный диагностический кабинет, где есть все необходимое оборудование. Цифровой томограф KaVo Pan Exam Plus 3D (Германия) позволяет получать снимки высокой четкости, обеспечивая самое информативное обследование для успешной имплантации.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/4.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Как проходит удаление <br>зубов с последующей <br>имплантацией</h3>
            </div>
        </div>
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Обезболивание</p>
                        <p class="text dots__text">Доктор обеспечивает ваш комфорт с помощью инъекции эффективного анестетика.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Удаление зуба</p>
                        <p class="text dots__text">Хирург аккуратно разделяет зуб на части и бережно извлекает их, максимально сохраняя костную ткань.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Фиксация имплантата</p>
                        <p class="text dots__text">Доктор надежно закрепляет в костной ткани имплантат.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Протезирование</p>
                        <p class="text dots__text">На верхушке имплантата прочно фиксируется временная коронка, которую позже заменяют на постоянную.</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-50-xs mt-25-xl">
                <blockquote class="blockquote">В нашей клинике применяют только проверенные системы имплантатов — SIC (Швейцария) и BioHorizons (США). Имплантаты хорошо приживаются, абсолютно биосовместимы и надежны. Это позволяет нам быть уверенными в качестве и долговечности выполненных работ.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <p class="text mb-80">Пока имплантат приживается, мы изготовим для вас прочный и эстетичный зубной протез. Материал вы выберете сами. У нас есть эстетичные керамика E.max и диоксид циркония, а также надежная металлокерамика. Более подробно о протезировании на имплантатах вы можете узнать <a href="/services/protezirovanie/protezirovanie-na-implantakh/">здесь</a>. </p>
                <p class="text mb-50">Если по показаниям придется удалить родной зуб, мы восстановим ваш комфорт очень быстро. Операция в стоматологической клинике FamilySmile пройдет совершенно безболезненно и очень аккуратно. Обратитесь к нашему специалисту за консультацией по поводу имплантации сразу после удаления зуба. Мы поможем вам сохранить удобство при приеме пищи и эстетику улыбки.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/5.png" alt="block5" class="block__img">
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
