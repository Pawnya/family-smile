<?
$aMenuLinks = Array(
	Array(
		"Бережное удаление зубов любой сложности",
		"/services/khirurgiya/berezhnoe-udalenie-zubov-lyuboy-slozhnosti/",
		Array(),
		Array("INFO"=>"Аккуратное извлечение зуба для скорейшего восстановления"),
		""
	),
	Array(
		"Удаление зубов с целью последующей имплантации",
		"/services/khirurgiya/udalenie-zubov-s-tselyu-posleduyushchey-implantatsii/",
		Array(),
		Array("INFO"=>"Бережное извлечение зуба, обеспечивающее легкость установки имплантата"),
		""
	),
	Array(
		"Зубосохраняющие операции",
		"/services/khirurgiya/zubosokhranyayushchie-operatsii/",
		Array(),
		Array("INFO"=>"Ампутация корня или его части для сохранения функциональности зуба"), 
		""
	)
);
?>
