<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Зубосохраняющие операции");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Безболезненное удаление кисты для сохранения зуба</h1>
            </div>
            <p class="text mb-30">Раньше при возникновении больших воспалительных образований на корне удаляли зуб. Сегодня у нас есть возможность сохранить здоровье и целостность зубного ряда, не прибегая к крайним мерам. Вместо удаления врачи проводят резекцию корня зуба — аккуратно и безболезненно отсекают воспаленную часть. Узнайте о процедуре подробнее, записавшись на консультацию к специалистам стоматологической клиники FamilySmile.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl pr-0-xs pr-40-xl">
            <h2 class="h2 mb-40">Воспаление, с которым необходимо бороться</h2>
            <p class="text">В результате инфекции в тканях пародонта может возникнуть киста. Если ее вовремя не удалить, то она увеличивается, и возникает опасность потерять один или несколько зубов. Как правило, такой зуб не болит, других симптомов не наблюдается. Проблему можно выявить только на снимке, поэтому в стоматологической клинике FamilySmile мы советуем проходить рентгенодиагностику перед стоматологическим лечением. Компьютерная томография показывает даже незначительные воспаления зубочелюстной системы.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <blockquote class="blockquote mb-50">Чтобы сохранить зуб, врачи FamilySmile полностью удаляют кисту. Вместе с ней отсекается и небольшая часть верхушки корня. Также врач обязательно проводит лечение каналов, чтобы полностью исключить повторное появление новообразования.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Резекция верхушки корня зуба позволяет:</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Сохранить целостность зубного ряда</p>
                    <p class="text dots__text">После удаления кисты зуб будет спасен и прослужит долго.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Держать кость в тонусе</p>
                    <p class="text dots__text">Когда зуб на месте, костная ткань поддерживается и сохраняет свой объем.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Обеспечивать здоровье организма</p>
                    <p class="text dots__text">Когда зубы на месте, пища пережевывается тщательно, а пищеварительная система работает правильно.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <blockquote class="blockquote mb-50">Удаление воспаления вместе с частью зуба — радикальный метод устранения кисты, но без него не обойтись. Операция занимает не более часа. И восстановление проходит за несколько дней.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Что помогает нам эффективно проводить резекцию</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">3D-диагностика</p>
                    <p class="text dots__text">Объемный снимок показывает образование и помогает врачу точно провести его удаление.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Компьютерная анестезия</p>
                    <p class="text dots__text">Специальный аппарат контролирует введение анестетика, вы даже не почувствуете инъекцию.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Точность под микроскопом</p>
                    <p class="text dots__text">Seiler Evolution XR6 (США) увеличивает оперируемую область до 30 раз, и врач полностью устраняет инфекцию.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Современные инструменты</p>
                    <p class="text dots__text">С уникальным пьезоскальпелем операция занимает не более часа.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <blockquote class="blockquote mb-50">В клинике мы используем пьезоскальпель — ультразвуковой инструмент для щадящего удаления. Он воздействует только на костную ткань и не травмирует мягкие ткани. Удаление кисты проходит аккуратно и быстро.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">В стоматологической клинике FamilySmile опытные врачи деликатно и совершенно безболезненно удалят кисту на корне зуба. Если раньше вас беспокоила боль, то она пройдет. К вам вернутся комфортные ощущения от приема пищи. Ждем вас в нашей клинике!</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
