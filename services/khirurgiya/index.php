<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Хирургия");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Безболезненные операции <br>для сохранения здоровья ваших зубов</h1>
                </div>
                <p class="text mb-30">Большинство стоматологических проблем можно решить с помощью терапевтического лечения, однако в некоторых случаях все-таки требуется хирургическое вмешательство. Специалисты клиники FamilySmile проводят операции, которые необходимы для устранения поврежденных и восстановления утраченных зубов. Обратитесь к нашим врачам для комфортного и эффективного хирургического лечения.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text">Наши стоматологи-хирурги с многолетним опытом помогут вам даже в сложном случае. Перед операцией они проводят обследование на компьютерном томографе, чтобы выявить особенности ваших зубов, состояние костной ткани. Такая диагностика обеспечивает создание грамотного плана и успешный результат операции.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Какие операции мы проводим</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h3 class="h3 mb-40">В каких случаях <br>необходимо хирургическое <br>вмешательство</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">Разрушение коронки зуба</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Перелом корня</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Перелом зуба</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Атрофия кости</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Утрата зубов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Наличие кисты на корне зуба</p>
                    </div>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block block_top">
            <div class="block-wrapper block-wrapper_tleft">
                <p class="text">В каждом случае используется свой способ устранения проблемы. При повреждении корня или его части проводятся зубосохраняющие операции. Если нужно восстановить зуб сразу после удаления, то доктор проведет одномоментную имплантацию. Врач определяет вид хирургического вмешательства после осмотра, сбора анамнеза и диагностики.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Чтобы сохранить эстетику и функциональность зубного ряда, в некоторых случаях мы проводим одномоментную имплантацию. За один визит доктор аккуратно удаляет зуб, фиксирует имплантат и временную коронку.</blockquote>
                <a href="/services/implantatsiya/odnomomentnaya-implantatsiya/" class="btn">Узнать подробнее</a>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества прохождения <br>хирургических операций в нашей <br>клинике</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-xl">
                <p class="text mb-50">Мы проводим зубосохраняющие операции при появлении образований на корне зуба. Для этого доктор устраняет кариес и его осложнения, а затем аккуратно удаляет верхушку корня вместе с образованием. После процедуры врач восстанавливает анатомию зуба пломбировочным материалом или керамической вкладкой. Обратитесь к нашим специалистам — спасаем зубы даже в сложных случаях!</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2 mb-40">Что делать после операции</h2>
            </div>
        </div>
        <div class="container block block_top">
            <div class="block-wrapper block-wrapper_tleft">
                <p class="text">Чтобы реабилитация после хирургического вмешательства заняла меньше времени, вам нужно выполнять все рекомендации врача. Например, в течение недели не следует посещать баню, сауну, принимать горячую ванну. Также на период восстановления нужно отказаться от алкоголя, курения, острой и грубой пищи, ограничить физические нагрузки.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Наши специалисты проведут любую операцию с комфортом для вас, а после нее расскажут, как ускорить процесс восстановления. Записывайтесь в стоматологию FamilySmile для безопасных и эффективных процедур.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
