<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гнатология");
?>
<link rel="stylesheet" href="main.min.css">
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Восстанавливаем функциональность зубочелюстной системы</h1>
                </div>
                <p class="text mb-30">Строение каждого зуба уникально. Его поверхность включает в себя до 22 непохожих элементов, которые выполняют свою роль. Когда зуб разрушен или удален, зубочелюстная система начинает работать неправильно, что влечет за собой функциональные нарушения. FamilySmile — единственная на сегодняшний день клиника в Смоленске, которая проводит лечение с учетом принципов гнатологии. Наши врачи восстанавливают ваше здоровье так, как было задумано природой. </p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="main-inner">
                <p class="text">Гнатология изучает работу каждого элемента зубочелюстной системы: мышц, связок, височно-нижнечелюстных суставов (ВНЧС) — и оценивает их взаимодействие. Врачи FamilySmile не смотрят на проблему однобоко. В нашем организме все взаимосвязано, поэтому важно видеть общую картину состояния здоровья. Гнатолог определит первопричину дефектов и восстановит гармоничную работу всех систем организма.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Гнатологическое лечение решает следующие проблемы:</h2>
                <div class="dots mb-60">
                    <div class="dots-item">
                        <p class="text dots__text">Сколы на зубах и коронках</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Повышенную стираемость зубов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Скрежет зубами (бруксизм)</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Щелчки и хруст в суставе челюсти</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Боль, затруднения открывания рта</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Неправильное смыкание челюстей</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Шейные и головные боли</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Заболевания десен</p>
                    </div>
                </div>
                <p class="text mb-50">Неслучайно мы первыми в городе стали применять гнатологические принципы лечения. Нам важно решить проблему не только сейчас, но и обеспечить вам здоровое, счастливое будущее. За качественной помощью в сложных случаях не обязательно ехать в столицу: профессионалы своего дела из клиники FamilySmile найдут решение вашей проблемы.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title mb-70">
                <h2 class="h2 tac mb-30">Найти причину заболевания — значит <br>помочь</h2>
                <p class="text mb-70">Прежде чем начать лечение, важно провести комплексное обследование и доскональный анализ работы каждого органа зубочелюстной системы, в том числе и в их взаимосвязи друг с другом. Только так можно выявить малейшие нарушения и чрезмерную нагрузку отдельных элементов, а затем составить грамотный план лечения.</p>
            </div>
            <div class="section-title">
                <h3 class="h3">Методы диагностики гнатологических <br>нарушений</h3>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper pb-0-xs pb-50-xl">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <blockquote class="blockquote mb-50">Лечение принесет пользу вашему здоровью только в результате комплексной диагностики. Когда врач знает все нюансы строения и работы зубочелюстной системы, то составляет эффективный план устранения проблемы. Настоящая помощь не терпит спешки: необходимо шаг за шагом изучать и планировать, поскольку на кону самое важное — ваше здоровье. </blockquote>
                            <div class="btn-wrapper">
                                <?$APPLICATION->IncludeComponent(
                                    "family-smile:form.result.new",
                                    "inline",
                                    Array(
                                        "WEB_FORM_ID"            => "1",
                                        "AJAX_OPTION_STYLE"      => "Y",
                                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                                        "USE_EXTENDED_ERRORS"    => "Y",
                                        "SEF_MODE"               => "N",
                                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                        "CACHE_TYPE"             => "N",
                                        "CACHE_TIME"             => "3600",
                                        "LIST_URL"               => "",
                                        "EDIT_URL"               => "",
                                        "SUCCESS_URL"            => "",
                                        "CHAIN_ITEM_TEXT"        => "",
                                        "CHAIN_ITEM_LINK"        => "",
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-50-xs mt-0-xl">
                <blockquote class="blockquote blockquote_tab mb-50">Лечение принесет пользу вашему здоровью только в результате комплексной диагностики. Когда врач знает все нюансы строения и работы зубочелюстной системы, то составляет эффективный план устранения проблемы. Настоящая помощь не терпит спешки: необходимо шаг за шагом изучать и планировать, поскольку на кону самое важное — ваше здоровье. </blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Начинаем менять вашу жизнь</h2>
                <p class="text mb-50">Для лечения нарушений работы зубочелюстной системы врачи клиники FamilySmile применяют индивидуальные шины. Они состоят из прозрачной биопластмассы, поэтому практически не видны на зубах. К тому же носить их необходимо преимущественно ночью, пока вы спите, а днем — только во время эмоционального напряжения. Шины возвращают мышечный тонус, нормализуют положение нижней челюсти и восстанавливают правильный прикус.</p>
                <blockquote class="blockquote mb-50">Гнатологическая помощь — это надежный фундамент для последующего ортодонтического и ортопедического лечения. Врач-гнатолог изучает строение и функционирование органов зубочелюстной системы, при необходимости проводит их коррекцию. Восстановление или перемещение зубов в дальнейшем проходит правильно — с учетом изменений и индивидуальных особенностей. Поэтому результат лечения сохраняется на долгие годы, возвращая вам здоровье и комфорт.</blockquote>
            </div>
            <div class="block-img order-1">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества лечения и восстановления зубов в клинике FamilySmile</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/4.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Побеждайте вместе с FamilySmile! Нейромышечные капы для спортсменов</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <p class="text mb-40">Чем отличается нейромышечная капа от обычной? Она создается с учетом ваших индивидуальных особенностей и обеспечивает правильное позиционирование нижней челюсти, а также оптимальную работу сустава.</p>
                <p class="text">Доказано, что организм тратит много энергии на позиционирование нижней челюсти, если она находится не в том положении. Нейромышечная капа обеспечивает исключительно правильное положение челюсти. В результате энергия будет затрачена на более важные вещи — поддержание баланса тела и координацию. С помощью капы вы станете более ловким и собранным. По статистике, эффективность повышается на 30–40%. В клинике FamilySmile мы изготовим для вас профессиональные капы, которые помогут вам достигать еще больших успехов в спорте.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <blockquote class="blockquote mb-40">Врачи клиники FamilySmile придерживаются принципов гнатологии, так как наша задача — правильная работа каждого элемента зубочелюстной системы. Поэтому лечение, восстановление и перемещение зубов проходят с учетом всех индивидуальных особенностей. Записывайтесь на лечение, которое меняет жизнь в лучшую сторону!</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<script defer src="main.js"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
