<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Протезирование");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Восстановление здоровья <br>и эстетики улыбки современными коронками </h1>
                </div>
                <p class="text mb-30">Вам нужно надежно восстановить зуб и вы хотите быть уверены в качестве протезирования?  Обращайтесь в стоматологическую клинику FamilySmile! Мы поможем вернуть комфорт при приеме пищи и красоту улыбки. Наши специалисты создадут протез, который сохранит ваше здоровье и обеспечит правильную работу зубочелюстной системы.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Виды протезирования зубов<br>в стоматологической клинике FamilySmile </h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 mb-40">Почему важно восстановить зубы</h2>
                <p class="text mb-30">В нашем организме все взаимосвязано, и потеря даже одного зуба ведет к нарушению работы всей зубочелюстной системы. Меняется распределение жевательной нагрузки, что влияет на здоровье всего организма и внешний вид. Исправить ситуацию поможет протез, который полностью восстановит правильное смыкание зубов. Именно такие коронки и конструкции создают специалисты FamilySmile.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Выгоды своевременного<br>протезирования зубов</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-xl">
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Каким должен быть<br>протез</h3>
                <p class="text mb-50">Ответ несложный: протез должен быть долговечным, эстетичным и сохраняющим здоровье. В стоматологической клинике FamilySmile изготовят конструкцию, которая эффективно восстановит правильное взаимодействие зубов, височно-нижнечелюстных суставов и жевательных мышц. Для этого наши специалисты — ортопед, гнатолог и зубной техник — работают в тесном сотрудничестве. Они учтут индивидуальные особенности челюстей, чтобы коронка прослужила как можно дольше. А благодаря современным материалам протез будет неотличим от настоящих зубов.</p>
                <div class="btn-wrapper mb-80">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
                <h3 class="h3 mb-40">Чем занимается<br>гнатолог</h3>
                <p class="text mb-50">Работа зубочелюстной системы зависит от многих факторов: от осанки, правильного дыхания, наличия вредных привычек. Но есть и обратная связь: нарушение функционирования височно-нижнечелюстного сустава может стать, например, причиной головной боли или вызвать другие малоприятные последствия. Все эти взаимодействия хорошо знает редкий специалист — стоматолог-гнатолог, который обязательно участвует в протезировании зубов.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Методы диагностики для протезирования<br>зубов в стоматологии FamilySmile</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <blockquote class="blockquote">Стоматология FamilySmile — единственная на сегодня в Смоленске клиника, которая специализируется на гнатологии и проблемах с височно-нижнечелюстным суставом. Часто бывает, что доктора из других стоматологий рекомендуют пациентам обратиться к нам для точной диагностики и протезирования, восстановления правильного смыкания зубов (окклюзии) и лечения бруксизма (непроизвольного скрежета зубов).</blockquote>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-50-xs mt-0-xl">
                <blockquote class="blockquote blockquote_tab mb-30">Стоматология FamilySmile — единственная на сегодня в Смоленске клиника, которая специализируется на гнатологии и проблемах с височно-нижнечелюстным суставом. Часто бывает, что доктора из других стоматологий рекомендуют пациентам обратиться к нам для точной диагностики и протезирования, восстановления правильного смыкания зубов (окклюзии) и лечения бруксизма (непроизвольного скрежета зубов).</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Система CAD/CAM</h2>
                <p class="text mb-50">Для изготовления протезов мы используем компьютерное моделирование CAD/CAM. Доктор обрабатывает зуб и снимает слепки, которые сканируются системой. Данные отправляются в специальную программу, где воссоздается виртуальный прототип ваших челюстей. На нем моделируется будущая коронка или конструкция с учетом мельчайших изгибов и впадинок на поверхности зубов. Модель протеза поступает на фрезерный станок, на котором вытачивается с точностью до микрона.</p>
                <p class="text mb-50">Опытные врачи, уникальные технологии, современные материалы и комплексный подход — безусловные преимущества стоматологической клиники FamilySmile. Мы эффективно восстановим здоровье ваших зубов. Коронки и конструкции, созданные с учетом функциональных особенностей зубочелюстной системы, сделают вашу жизнь комфортнее на долгие годы.</p>
                <div class="btn-wrapper mb-50-xs mb-0-xl">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
