<?
$aMenuLinks = Array(
	Array(
		"Коронки на основе диоксида циркония и безметалловая керамика",
		"/services/protezirovanie/koronki-na-osnove-dioksida-tsirkoniya-i-bezmetallovaya-keramika/",
		Array(),
		Array("INFO"=>"Современные материалы — это оптимальное сочетание прочности и эстетики для создания безупречной улыбки"),
		""
	),
	Array(
		"Металлокерамические коронки",
		"/services/protezirovanie/metallokeramicheskie-koronki/",
		Array(),
		Array("INFO"=>"Проверенная временем методика создания надежных конструкций для протезирования боковых зубов"),
		""
	),
	Array(
		"Микропротезирование вкладками",
		"/services/protezirovanie/mikroprotezirovanie-vkladkami/",
		Array(),
		Array("INFO"=>"Точное и долговечное восстановление естественного рельефа сильно разрушенного зуба"),
		""
	),
	Array(
		"Съемное и частичное съемное протезирование",
		"/services/protezirovanie/semnoe-i-chastichno-semnoe-protezirovanie/",
		Array(),
		Array("INFO"=>"Доступный способ вернуть жевательную функцию при множественной утрате зубов"),
		""
	),
	Array(
		"Протезирование на имплантах",
		"/services/protezirovanie/protezirovanie-na-implantakh/",
		Array(),
		Array("INFO"=>"Передовая технология надежного и комфортного восстановления всего зубного ряда"), 
		""
	)
);
?>
