<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Протезирование на имплантах в т.ч. all-on-6");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Надежное и эстетичное <br>восстановление зубов </h1>
                </div>
                <p class="text mb-30">Утрата зуба создает много неудобств: приходится менять рацион питания, отказываться от любимых блюд, прятать улыбку. Жевательная нагрузка распределяется неправильно, что со временем вызывает нежелательные последствия для здоровья. В стоматологии FamilySmile мы поможем восстановить ваше прежнее качество жизни. Благодаря протезированию зубов с опорой на имплантаты к вам вернется комфорт при приеме пищи, а улыбка снова станет неотразимой.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container">
            <div class="section-title">
                <h2 class="h2 tac">Почему протезирование с помощью <br>имплантации — лучший выбор</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="block-wrapper mt-30-xs mt-0-md">
                <blockquote class="blockquote ml-0-xs ml-90-xl">Имплантаты служат надежной опорой для различных зубных протезов. Благодаря имплантации можно восстановить один зуб эстетичной коронкой, несколько зубов — с помощью мостовидного протеза, а весь зубной ряд — условно-съемной конструкцией.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Полное восстановление <br>зубов за один день <br>по технологии «все на 6»</h2>
                <p class="text mb-80">При полной утрате зубов вариантов протезирования немного, а точнее, всего два. Один из них — полный съемный протез, который держится за счет вакуума. Такое крепление крайне ненадежно, и в любой момент протез может соскользнуть. Более современный и удобный вариант — это закрепление протеза на имплантатах.</p>
                <blockquote class="blockquote mb-50">Несколько лет назад швейцарские стоматологи разработали методику быстрого восстановления всего зубного ряда с помощью всего шести имплантатов. Понадобится только одна операция, чтобы установить надежные крепления для протеза. Уже в ближайшие дни после имплантации «все на 6» вы сможете принимать пищу и улыбаться, не беспокоясь, что протез соскользнет.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Как проходит протезирование «все на 6»</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Подготовка</p>
                                <p class="text box__text">После диагностики на цифровом томографе KaVo Pan Exam Plus 3D (Германия) определяются участки кости для надежной фиксации имплантатов. На трехмерных снимках хорошо видно структуру и объем костной ткани.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-2.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Имплантация</p>
                                <p class="text box__text">Уникальность технологии в том, что нет необходимости в костной пластике. Несколько имплантатов устанавливаются в переднем отделе, а остальные — по бокам, под углом 45 градусов, что способствует правильному распределению нагрузки.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-3.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Временный протез</p>
                                <p class="text box__text">На имплантатах фиксируется ортопедическая конструкция. В первые дни желательно есть мягкую пищу, которую вскоре можно будет заменить на любимые блюда. А вот улыбка станет красивой сразу.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-50">После приживления имплантатов мы заменим временную конструкцию на полный условно-съемный протез. Изделие будет изготовлено в нашей собственной зуботехнической лаборатории с учетом индивидуальных особенностей движения вашей челюсти. Надеть и снять конструкцию сможет только доктор во время контрольных посещений. Вам нужно будет регулярно чистить новые зубы обычной щеткой.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Несъемное <br>протезирование <br>на имплантатах</h3>
                <p class="text">Имплантаты служат надежной опорой для несъемных постоянных коронок и мостовидных протезов. Несъемные конструкции на имплантате обеспечивают максимально возможный комфорт по ощущениям. Они не отличаются от настоящих зубов. Полностью возвращая функцию, коронки на имплантате смотрятся естественно и эстетично.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/4.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Современные материалы <br>для протезирования, которые <br>мы используем</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Керамика E.max</p>
                        <p class="text dots__text">Прочный материал обеспечивает безупречную эстетику улыбки. Керамика отражает свет и обладает полупрозрачностью настоящей эмали. Среди множества ее оттенков можно подобрать цвет, который идеально вам подойдет.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Диоксид циркония</p>
                        <p class="text dots__text">Светлый материал по структуре похож на зубы, но прочнее их в 2 раза. Из диоксида циркония вытачивается каркас, который покрывается керамикой под цвет зубов. Подходит для восстановления любой части зубного ряда.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Металлокерамика</p>
                        <p class="text dots__text">Металлическая основа покрывается керамикой. Из-за металла конструкция непрозрачна, поэтому такая коронка — оптимальный вариант для протезирования только жевательных зубов.</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-50-xs mt-25-xl">
                <blockquote class="blockquote">Наша зуботехническая лаборатория оснащена системой компьютерного моделирования CAD/CAM. Программа на основе слепков создает виртуальный прототип вашей зубочелюстной системы. Коронка моделируется и «примеряется» на цифровом макете. Учитываются малейшие изгибы зубов для создания их идеального смыкания. После этого изделие автоматически вытачивается на специальном станке.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Восстановление <br>смыкания зубов <br>при помощи <br>зубных протезов</h2>
                <p class="text mb-40">Мы следим за правильным взаимодействием зубов, челюстей и мышц, чтобы сохранить ваше здоровье на долгие годы. Для этого изучаем траекторию движения челюстей вверх-вниз, влево-вправо и работу мышц с помощью специальных устройств — артикулятора, аксиографа, миографа. Полученные данные обязательно учитываются при изготовлении зубных протезов. Ортопед и гнатолог совместно работают над созданием безупречной конструкции, которая обеспечит вам комфорт и прослужит много лет.</p>
                <a href="/services/gnatologiya/" class="link link_section mb-40-xs mb-80-xl">Подробнее</a>
                <p class="text mb-50">Опытные специалисты FamilySmile учтут все индивидуальные особенности вашей зубочелюстной системы, чтобы создать безупречные ортопедические конструкции и надежно зафиксировать их на имплантатах.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/5.png" alt="block5" class="block__img">
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
