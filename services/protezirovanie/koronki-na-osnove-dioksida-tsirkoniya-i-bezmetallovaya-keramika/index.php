<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Коронки на основе диоксида циркония и безметалловая керамика");
?>
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Эстетичное <br>восстановление зубов</h1>
                </div>
                <p class="text mb-30">Хотите восстановить утраченный зуб так, чтобы коронка не отличалась от настоящего зуба? Самый эстетичный способ — установить протез из современных сверхпрочных материалов. В стоматологической клинике FamilySmile для этого используют диоксид циркония и безметалловую керамику. Коронка полностью копирует природный зуб и по цвету, и по форме. Благодаря особой прочности материалов конструкция прослужит долгие годы. К вам вернется ежедневный комфорт и эстетика улыбки.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <blockquote class="blockquote mb-80">Перед протезированием необходимо пройти профессиональную гигиену полости рта. Затем врач проведет компьютерную томографию, чтобы получить полную картину состояния зубов и десен.</blockquote>
                <h2 class="h2 mb-40">Безметалловая<br>керамика</h2>
                <p class="text mb-80">Керамические коронки идеально имитируют свойства эмали: полупрозрачность, преломление света, естественный блеск. Безметалловая керамика обладает большой палитрой оттенков, поэтому доктор подберет подходящий цвет. Материал позволяет воссоздать даже прозрачный режущий край зуба. Абсолютная эстетика керамики идеально подходит для передних зубов.</p>
                <blockquote class="blockquote mb-80">Кроме обычной, в клинике FamilySmile используют немецкую керамику E.max. Она изготовлена на основе дисиликата лития и отличается особой прочностью. Так как материал способен выдерживать большую нагрузку, его используют для коронок, виниров и вкладок на передние и жевательные зубы.</blockquote>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 mb-40">Диоксид циркония</h2>
                <p class="text">Диоксид циркония — это полупрозрачный материал белого цвета. Изготовленные из него протезы прочные и износостойкие, как металл. Поэтому в клинике FamilySmile их чаще используют для восстановления боковых зубов. Обычно коронки из диоксида циркония покрывают керамикой, чтобы придать им естественный внешний вид.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества протезов из диоксида <br>циркония и безметалловой керамики</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <blockquote class="blockquote">Коронки из диоксида циркония и керамики изготавливаются в клинике FamilySmile с помощью компьютерной системы CAD/CAM. Врач делает слепок челюсти и на компьютере моделирует будущий протез. С точностью до микрона цифровой фрезер вытачивает коронку из выбранного вами материала. Она подходит идеально и выглядит естественно, как настоящий зуб.</blockquote>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="btn-wrapper mt-30-xs mt-0-md">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
            <div class="box-bq mt-50-xs mt-0-xl">
                <blockquote class="blockquote blockquote_tab mb-30">Коронки из диоксида циркония и керамики изготавливаются в клинике FamilySmile с помощью компьютерной системы CAD/CAM. Врач делает слепок челюсти и на компьютере моделирует будущий протез. С точностью до микрона цифровой фрезер вытачивает коронку из выбранного вами материала. Она подходит идеально и выглядит естественно, как настоящий зуб.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Этапы протезирования</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Тщательная диагностика</p>
                                <p class="text box__text">3D-томография позволяет провести качественное исследование состояния вашей челюсти.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-2.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Снятие слепков</p>
                                <p class="text box__text">Зубы обтачивают и изготавливают гипсовую модель челюсти.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-3.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Компьютерная обработка</p>
                                <p class="text box__text">Слепок сканируют, и на его основе создается прототип коронки.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-4.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Моделирование протеза</p>
                                <p class="text box__text">Программа максимально точно с учетом прикуса копирует коронку, повторяя естественную форму зуба.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-5.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Фрезеровка протеза</p>
                                <p class="text box__text">Из заготовки выбранного вами материала с максимальной точностью вытачивается коронка.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-6.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Обжигание в печи</p>
                                <p class="text box__text">После примерки протез покрывают керамикой для идеальной гладкости, блеска и отправляют в печь на обжиг, а затем фиксируют.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-50">Созданный в нашей клинике протез точно повторяет форму и цвет вашего зуба. Он прослужит долго, потому что плотно прилегает к десне и под него не попадает пища. После установки коронки полностью восстанавливаются все функции зубочелюстной и пищеварительной систем. А это здоровье всего организма, уверенность в себе и красота улыбки. Запишитесь на прием в клинику FamilySmile, мы будем рады вам помочь.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
