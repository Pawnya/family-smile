<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Съемное и частичное съемное протезирование");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Надежные конструкции <br>для восстановления зубов</h1>
            </div>
            <p class="text mb-30">Утратили зубы и теперь не можете открыто улыбаться, с комфортом принимать пищу? Врачи стоматологической клиники FamilySmile помогут избавиться от таких проблем. Специалисты подберут оптимальную конструкцию съемного протеза, исходя из медицинских показаний, и обязательно учтут ваши пожелания. С ним вы будете чувствовать себя комфортно и уверенно. Записывайтесь на прием, мы с радостью позаботимся о вас и вернем функциональность вашим зубам.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="main-inner">
            <div class="section-title">
                <h2 class="h2 mb-40">Для чего необходимо <br>восстанавливать зубы</h2>
            </div>
            <p class="text">При отсутствии зубов не только нарушается эстетика улыбки, но и возникают проблемы с пищеварительной системой. Кроме того, появляются стираемость эмали, нечеткая речь, преждевременные морщины. Избежать таких последствий помогают бюгельные, нейлоновые и акриловые съемные протезы.</p>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block">
        <div class="block-wrapper pr-0-xs pr-100-md">
            <h2 class="h2 mb-40">Бюгельный протез</h2>
            <p class="text">Он применяется при отсутствии нескольких  зубов  на разных сторонах челюсти. Протез состоит из прочного металлического основания и пластмассовой части. Конструкция не закрывает небо — сохраняются все вкусовые ощущения, а значит, вы сможете полноценно принимать пищу. Также изделие равномерно распределяет нагрузку на челюсть, что обеспечивает правильное функционирование височно-нижнечелюстного сустава. Съемный бюгельный протез крепится к опорным зубам с помощью специальных фиксаторов.</p>
        </div>
        <div class="block-wrapper ">
            <h3 class="h3 mt-40-xs mt-0-xl mb-50">Виды креплений бюгельной конструкции</h3>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Кламмеры</p>
                    <p class="text dots__text">Металлические крючки обхватывают опорные зубы, поэтому протез крепко держится.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Замки</p>
                    <p class="text dots__text">Специальные аккуратные крепления, одна часть которых находится на опорном зубе, другая — внутри съемной конструкции. Крепление не видно и используется в эстетически значимых зонах.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Коронки</p>
                    <p class="text dots__text">На зубы фиксируются металлические коронки, затем на них устанавливается протез.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl mt-80-xs mt-0-xl order-2">
            <blockquote class="blockquote mt-0-xs mt-60-xl">Специалисты клиники FamilySmile — настоящие профессионалы. Они повышают свою квалификацию, участвуют в различных конференциях и проходят курсы. Наши врачи с заботой и пониманием отнесутся к вашему случаю, ответят на все волнующие вопросы о протезировании и составят грамотный план лечения.</blockquote>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl order-1">
            <h2 class="h2 mb-40">Преимущества бюгельного протеза</h2>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="dots__title">Прочная фиксация</p>
                    <p class="text dots__text">Конструкция абсолютно неподвижна благодаря надежным креплениям.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Полный комфорт</p>
                    <p class="text dots__text">Бюгельный протез легкий и аккуратный — вы быстро к нему привыкнете.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Эстетичная улыбка</p>
                    <p class="text dots__text">Замки и коронки незаметны — улыбайтесь открыто и уверенно.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Простой уход</p>
                    <p class="text dots__text">Чистить изделие можно самостоятельно, без обращения к врачу.</p>
                </div>
            </div>
            <p class="text mb-50">Бюгельный протез не смещается во время приема пищи или общения, выглядит эстетично. С ним вы будете чувствовать себя комфортно в любой ситуации. Приходите на консультацию к нашему ортопеду, он ознакомит вас со всеми особенностями и ценой съемной конструкции.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper order-2">
            <h2 class="h2 mb-40">Нейлоновый протез</h2>
            <p class="text mb-50">Он подходит при частичной потере зубов. Конструкция состоит из гибкого нейлона, который легко адаптируется под зубной ряд. Изделие  прочно крепится к опорным зубам специальными крючками из пластмассы под цвет десны. Протез легко снять для проведения гигиены. Служит нейлоновая конструкция более 3 лет и при этом не деформируется.</p>
        </div>
        <div class="block-img order-1">
            <img src="images/3.png" alt="block4" class="block__img">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper pr-0-xs pr-60-md order-2">
            <h2 class="h2 mb-50">Акриловая конструкция</h2>
            <p class="text mb-50">Это самый бюджетный способ восстановления всех зубов на челюсти. Съемный протез состоит из твердого акрила, который не меняет свою форму. Конструкция держится за счет эффекта присасывания к небу, полностью перекрывая его.</p>
            <p class="text">Если же вы хотите с комфортом принимать пищу и ощущать новые зубы как родные, то рекомендуем вам <a href="/services/protezirovanie/protezirovanie-na-implantakh/">протезирование на имплантах по технологии All-on-4 или All-on-6</a>. Эстетичная конструкция крепко фиксируется на опоры и служит десятилетия.</p>
        </div>
        <div class="block-img order-1">
            <img src="images/4.png" alt="block4" class="block__img">
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Не знаете, какой способ восстановления зубов подходит именно вам? Приходите в стоматологическую клинику FamilySmile, и наши врачи помогут сделать правильный выбор. Они подберут оптимальную конструкцию, чтобы вернуть вам комфорт и красивую улыбку. Ждем вас на приеме.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
