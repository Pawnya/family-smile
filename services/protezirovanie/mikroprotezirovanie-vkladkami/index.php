<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Микропротезирование вкладками");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Точное и долговечное <br>восстановление жевательных зубов </h1>
                </div>
                <p class="text mb-30">Зуб хочется сохранить как можно дольше. Но что делать, если он сильно разрушен? Реставрация пломбой в таком случае уже не будет надежной и долговечной, а для протезирования коронкой потребуется глубокая обработка оставшихся тканей. В стоматологии FamilySmile есть методика, которая позволит вернуть вашему зубу естественный рельеф, максимально сохранив его, и гарантирует стабильный многолетний результат. Это керамическая вкладка.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md">
                <h2 class="h2 mb-40">Что такое керамическая <br>вкладка</h2>
                <p class="text mb-40-xs mb-0-xl">Вкладку изготавливают в зуботехнической лаборатории с применением цифровых технологий.  Она представляет собой утраченную часть зуба и воссоздается с точностью до микрона. Высококачественная керамика обладает свойствами, благодаря которым вкладка полноценно восполняет разрушенные ткани зуба. Керамическая вкладка прочна, надежна и эстетична.</p>
            </div>
            <div class="block-wrapper">
                <blockquote class="blockquote mt-60">Когда зуб разрушен более чем наполовину, вкладка — оптимальный вариант. Для ее установки требуется минимальная обработка зуба, поэтому в отдельных случаях можно обойтись без удаления нерва. А по долговечности такая реставрация превосходит даже самые качественные пломбировочные материалы. Срок службы вкладки может превышать 15 лет!</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества керамической <br>вкладки</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md">
                <h2 class="h2 mb-40">Правильный прикус — <br>важное условие <br>сохранения вашего <br>здоровья</h2>
                <p class="text mb-80">Потеря даже малой части зуба постепенно приводит к нарушению смыкания зубов и процесса пережевывания пищи. Перераспределение жевательной нагрузки со временем вызывает деформацию височно-нижнечелюстных суставов. Поспешите в стоматологию FamilySmile, и мы поможем избежать более серьезных последствий: беспричинных головных и шейных болей, шума в ушах, проблем со спиной.</p>
            </div>
            <div class="block-wrapper">
                <blockquote class="blockquote mt-40-xs mt-100-xl mb-50">В стоматологической клинике FamilySmile врачи работают в команде. Восстановлением зубов, помимо терапевта и ортопеда, обязательно занимается еще и стоматолог-гнатолог. Это специалист, который помогает создать правильное смыкание зубов с учетом индивидуальных особенностей вашей зубочелюстной системы.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Современные <br>технологии для <br>реставрации зубов</h2>
                <p class="text mb-50">Врачи проведут необходимое обследование на специальном оборудовании: 3D-томограф обеспечивает самую информационную рентгенодиагностику, артикулятор и аксиограф позволяют высчитать траекторию движения челюсти, миограф исследует работу жевательных мышц. Благодаря столь подробному изучению вашей зубочелюстной системы создается вкладка, идеально вписывающаяся в ваш зубной ряд. Она сохраняет правильную функциональность всей системы. Для максимально точного вытачивания вкладки используется система моделирования CAD/CAM. </p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Как устанавливается <br>керамическая вкладка</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Снятие слепков</p>
                        <p class="text dots__text">Доктор подготовит зуб и снимет слепки, данные с которых сканер системы CAD/CAM передаст в программу. </p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Моделирование</p>
                        <p class="text dots__text">Программа создает прототип челюстей, по которому проектируется виртуальная модель вкладки.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Корректировка</p>
                        <p class="text dots__text">Врач может многократно увеличить изображение и внести изменения, учесть каждый, даже мельчайший изгиб.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Изготовление</p>
                        <p class="text dots__text">Изображение поступит на автоматизированный фрезерный станок, где будет выточено из блока цельной керамики.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Установка</p>
                        <p class="text dots__text">На повторном приеме врач надежно зафиксирует вкладку на зубе с помощью специального композитного материала.</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <blockquote class="blockquote mt-50-xs mt-0-xl mb-80">Реставрация керамической вкладкой проходит более удобно, чем пломбирование зубов. Никакого долгого сидения в кресле стоматолога с широко открытым ртом! Всего два посещения — и ваш зуб восстановлен. Основная часть работы будет выполнена в зуботехнической лаборатории.</blockquote>
                <p class="text mb-50">Не откладывайте лечение и восстановление зубов. Обращайтесь в стоматологию FamilySmile — мы вернем зубу функциональность и эстетику с помощью керамической вкладки.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
