<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Металлокерамические коронки");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Классический способ восстановления зубов, проверенный временем</h1>
            </div>
            <p class="text mb-30">При отсутствии одного или даже нескольких зубов вы можете испытывать дискомфорт, поскольку стараетесь жевать только на одной стороне. Такая привычка со временем становится причиной нарушения прикуса. Специалисты стоматологии FamilySmile знают, как исправить эту ситуацию. Они восстановят зуб с помощью надежных металлокерамических коронок.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top section-overflow">
    <div class="container block block_top">
        <div class="block-wrapper pr-0-xs pr-95-md">
            <blockquote class="blockquote mb-80">Металлокерамика более 20 лет применяется для восстановления зубов. Этот способ считается традиционным. Металлокерамические коронки обладают высокой прочностью и отлично выдерживают ежедневную нагрузку.</blockquote>
            <h2 class="h2 mb-40">Преимущества металлокерамических коронок</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="text dots__text">Повторение анатомии. Конструкция в точности повторяет форму разрушенного или отсутствующего зуба.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Долговечность. При правильном домашнем уходе и регулярной профессиональной гигиене коронки прослужат более 7 лет.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Сохранение цвета. Керамика, которой покрыта металлическая основа, не впитывает красители и не темнеет со временем.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Износостойкость. Благодаря металлической основе коронка почти в два раза тверже собственных зубов и выдерживает жевательную нагрузку.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper">
            <p class="text mb-40">Металлокерамикой можно восстановить один или несколько зубов. Коронки устанавливаются как на зубы, так и на имплантаты. Металлокерамические конструкции менее заметны, чем металлические, однако они не пропускают свет, поэтому выглядят темнее своих зубов. При восстановлении в зоне улыбки наши специалисты рекомендуют обратить внимание на более современные способы протезирования — керамические коронки и конструкции на основе диоксида циркония.</p>
            <a href="/services/protezirovanie/" class="link link_section mb-80">Подробнее</a>
            <blockquote class="blockquote mb-50">На первом приеме мы проводим диагностику зубов и десен, собираем анамнез. Только после этого доктор подбирает оптимальный способ восстановления зубов. Запишитесь на прием, сделайте первый шаг к восстановлению функциональности зубного ряда и преображению вашей улыбки.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top section-overflow">
    <div class="container block block_top">
        <div class="block-wrapper pr-0-xs pr-95-md">
            <h2 class="h2 mb-40">Почему стоит обратиться к нам</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="text dots__text">Комплексная диагностика. На первом приеме мы исследуем зубы и десны с помощью компьютерного томографа. За минуту доктор получает всю необходимую информацию.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Высокая точность. Коронки изготавливаются в лаборатории по слепкам ваших зубов. Готовая конструкция повторяет анатомию зуба и полностью заменяет его.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Наглядные изменения. Мы ведем фотопротокол на протяжении всего периода лечения. По этим снимкам вы увидите, насколько изменилась улыбка.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Дискомфорт при общении и приеме пищи — это временная неприятность, которую можно легко исправить. Приходите в стоматологию FamilySmile, чтобы восстановить функциональность зубного ряда металлокерамическими коронками.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
