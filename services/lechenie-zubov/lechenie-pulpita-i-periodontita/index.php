<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лечение пульпита и периодонтита");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Спасаем зубы при<br>осложнении кариеса</h1>
            </div>
            <p class="text mb-30">Болит зуб? Срочно обращайтесь за помощью в стоматологию FamilySmile! Возможно, у вас осложнение кариеса, которое может привести к потере зуба, — пульпит или периодонтит. Наши специалисты проведут эффективную диагностику и лечение. Они примут все необходимые меры, чтобы сохранить и восстановить зуб.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper pl-0-xs pl-80-xl">
            <blockquote class="blockquote mb-50">Приоритет докторов FamilySmile — сохранение зуба даже в самой сложной ситуации. Для этого клиника оснащена передовыми технологиями, которые помогают не только добиваться высоких результатов в лечении кариеса и его осложнений, но и обеспечивают вам максимальный комфорт.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Почему стоит лечить зубы в клинике FamilySmile</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Эффективная диагностика</p>
                    <p class="text dots__text">Проведем самое информативное обследование — трехмерную томографию на оборудовании KaVo Pan Exam Plus 3D (Германия). Доктор отчетливо увидит строение даже самых тонких каналов зуба, оценит состояние окружающих тканей для проведения успешного лечения.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Гарантия комфорта</p>
                    <p class="text dots__text">Компьютерная анестезия QuickSleeper 5 (Франция) настолько аккуратная, что вы даже не почувствуете укол. Она обеспечит надежное обезболивание на протяжении всей процедуры.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Бережное лечение</p>
                    <p class="text dots__text">Чтобы удалить кариозные ткани без сверления, используем водно-абразивную систему Velopex Aquacut Quattro (Великобритания). Поданная под большим давлением на пораженный участок смесь воды и специального порошка буквально «вымывает» кариес.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Ювелирная точность</p>
                    <p class="text dots__text">Для максимального сохранения здоровых тканей и эффективного лечения   применим микроскоп Seiler Evolution XR6 (Германия). Под многократным увеличением врач успешно обрабатывает даже тончайшие каналы и проводит эстетичную реставрацию зуба.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top section-overflow">
    <div class="container block block_top pb-10">
        <div class="block-wrapper pr-0-xs pr-95-md order-2">
            <h2 class="h2 mb-40">Что такое пульпит и периодонтит</h2>
            <p class="text mb-80">Практически каждый человек знает, что такое кариес. Под действием кислот в зубном налете эмаль постепенно разрушается, что без лечения приводит к более серьезным последствиям. При дальнейшем распространении инфекция может добраться до нерва и вызвать его воспаление — пульпит. А если по каналам зуба она проникнет до верхушек корней, то может возникнуть другое осложнение — периодонтит, или воспаление костных тканей.</p>
            <blockquote class="blockquote mb-50">Периодонтит — следствие несвоевременного лечения  пульпита. Если оставить эту проблему без внимания, то повышается риск потерять зуб. Поэтому лучше пройти диагностику и лечение как можно раньше. Регулярные профилактические осмотры в стоматологии FamilySmile помогут выявить и устранить кариес на ранней стадии.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
        <div class="block-wrapper block-wrapper_tleft">
            <h2 class="h2 mb-40">Признаки осложнений кариеса</h2>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Появилась постоянная или периодическая боль в зубе.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Зуб реагирует на высокую или низкую температуру пищи и напитков, на сладкое.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Вечером и ночью болезненность усиливается.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Боль может пульсировать или простреливать, не всегда понятен ее источник.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Дискомфорт усиливается при нажатии на зуб или накусывании.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Возник отек или покраснение десны.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper pl-0-xs pl-80-xl">
            <blockquote class="blockquote mb-50">Если у вас есть хоть один из перечисленных симптомов, незамедлительно обратитесь в стоматологическую клинику FamilySmile. Наши врачи устранят боль, проведут качественное лечение и спасут зуб от удаления.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h3 class="h3 mb-50">Как проходит лечение<br>пульпита и периодонтита</h3>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Снимаем боль</p>
                    <p class="text dots__text">Делаем компьютерную анестезию, которая проходит совершенно безболезненно.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Надеваем коффердам</p>
                    <p class="text dots__text">Эластичный латексный платок защищает рабочую поверхность от влаги, чтобы качественно пройти каналы и более надежно зафиксировать пломбу.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Лечим зуб</p>
                    <p class="text dots__text">Под микроскопом удаляем кариозные ткани, обрабатываем антисептическим составом и пломбируем каналы.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Восстанавливаем поверхность</p>
                    <p class="text dots__text">С ювелирной точностью воссоздаем каждый мельчайший изгиб зуба.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>

<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Реставрация зубов после лечения</h2>
            <p class="text">Очень важно после лечения вернуть зубам прежнюю природную форму, чтобы обеспечить правильное смыкание зубных рядов. Тогда вся зубочелюстная система будет работать правильно. Поэтому наши врачи уделяют особое внимание восстановлению естественного рельефа зубов — максимально точно воспроизводятся бугорки, впадины и резцовый край.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <blockquote class="blockquote mb-50">Провести реставрацию зуба можно композитной пломбой или керамической вкладкой при сильном разрушении. Реставрация нанокомпозитами Filtek (США) и Charisma Opal (Германия) производится под микроскопом сразу после лечения. А вкладка создается в лаборатории при помощи цифровых технологий.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Обращайтесь за квалифицированной помощью в стоматологическую клинику FamilySmile. Мы сохраним, вылечим  и надежно восстановим ваш зуб даже в случае осложнений кариеса — пульпита и периодонтита. Он прослужит вам еще много лет!</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
