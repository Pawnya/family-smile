<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лечение кариеса");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Успешное лечение кариеса<br>на любой стадии</h1>
            </div>
            <p class="text mb-30">Как показывает статистика, кариес — одно из самых распространенных заболеваний полости рта. Причем в любом возрасте. Чтобы избежать развития осложнений, лечить кариес необходимо своевременно. В стоматологической клинике FamilySmile проводят качественное лечение, основанное на современных технологиях и методиках. Мы вернем здоровье вашим зубам и восстановим их природную красоту.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.jpg" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="main-inner">
            <div class="section-title">
                <h2 class="h2 mb-40">Ставим правильный диагноз</h2>
            </div>
            <p class="text">Лечение начинается с тщательной диагностики. Наша клиника оснащена компьютерным томографом. Он создает объемное изображение челюсти в формате 3D, благодаря которому доктор получает максимум информации. В некоторых случаях для контроля на различных этапах лечения проводится обследование на визиографе. Он создает прицельные снимки одного-двух зубов.</p>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.jpg" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Этапы, которые проходит кариес</h2>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Начальный</p>
                    <p class="text dots__text">На эмали появилось светлое или темное пятно, которое можно вылечить с помощью укрепляющих препаратов, без сверления. Пятно исчезает, эмаль восстанавливается, и укрепляется ее структура.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Поверхностный</p>
                    <p class="text dots__text">В слоях эмали образуется небольшая полость, которую врач аккуратно обрабатывает, после чего накладывает на этом месте небольшую пломбу.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Средний</p>
                    <p class="text dots__text">Кариес проник вглубь зуба и затронул дентин, появляется реакция на холодную и горячую пищу. Специалист эффективно устранит кариес и восстановит зуб пломбировочным материалом.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Глубокий</p>
                    <p class="text dots__text">Инфекция затронула все ткани зуба и почти добралась до пульпы. Ощущается острая боль при приеме пищи. В этом случае доктор заполняет глубокую кариозную полость биоактивным заменителем дентина Biodentine.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <blockquote class="blockquote mb-50">Кариес занимает первое место среди заболеваний полости рта. Лучший способ его обнаружить — раз в полгода проходить профилактические осмотры в клинике FamilySmile. Важно оперативно выявить проблему и приступить к лечению. Мы позаботимся о красоте и здоровье ваших зубов.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top section-overflow">
    <div class="container block block_top">
        <div class="block-wrapper pr-0-xs pr-95-md">
            <h2 class="h2 mb-40">Лечение кариеса под<br>увеличительной техникой</h2>
            <p class="text mb-80">Чтобы сохранить ваши зубы, мы проводим <a href="/services/lechenie-zubov/lechenie-zubov-pod-mikroskopom/">лечение под микроскопом</a> или с использованием бинокуляров. Микроскоп с его многократным увеличением дает реальный шанс успешно вылечить даже осложнения кариеса, не прибегая к удалению зуба. Доктор отлично видит проблемные участки и обрабатывает их. При этом здоровые ткани не затрагиваются, а пломбирование проводится точно. Если можно обойтись без микроскопа, то специалист проводит лечение под бинокулярами.</p>
        </div>
        <div class="block-wrapper block-wrapper_tleft">
            <h3 class="h3 mb-40">Преимущества лечения<br>кариеса в клинике<br>FamilySmile</h3>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Современная диагностика</p>
                    <p class="text dots__text">Правильно поставленный диагноз помогает назначить необходимое лечение и спасти ваш зуб.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Увеличительная техника</p>
                    <p class="text dots__text">Микроскоп и бинокуляры повышают качество лечения, что продлевает зубу жизнь.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Аппликационная анестезия</p>
                    <p class="text dots__text">Замораживающий гель наносится на десну, после чего вы даже не почувствуете укол.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Лечение без сверления</p>
                    <p class="text dots__text">Система Aquacut отлично справляется с начальной стадией кариеса.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Зарубежный композитный материал</p>
                    <p class="text dots__text">Применяем естественные пломбировочные материалы Filtek (США), Charisma Opal (Германия) для эстетичной реставрации зубов.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.jpg" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <h2 class="h2 mb-40">Осложнения, вызываемые кариесом</h2>
            <p class="text mb-30">Если кариес не лечить, то он может перейти в пульпит и периодонтит. На этом этапе поражается нерв зуба, а затем костная ткань. И в том и в другом случае необходимо чистить и пломбировать корневые каналы. Использование компьютерного томографа для точной диагностики и микроскопа для качественного лечения дает возможность спасти ваш зуб от удаления.</p>
            <p class="text mb-50">В стоматологической клинике FamilySmile позаботятся о вашем здоровье. Мы лечим все формы кариеса и восстанавливаем природную форму зубов.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
