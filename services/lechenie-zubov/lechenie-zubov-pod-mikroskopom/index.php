<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лечение зубов под микроскопом");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h2 class="h1 mb-30">Ювелирная точность при лечении зубов</h2>
                </div>
                <p class="text mb-30">При глубоком кариесе и его осложнениях, когда инфекция переходит дальше в зуб, воспаляются нерв и окружающие корень ткани. Речь идет уже не просто о восстановлении, но в первую очередь о сохранении зуба. В стоматологической клинике FamilySmile ваш зуб спасут даже в сложном случае, и все благодаря современным технологиям. Проводя лечение под микроскопом, доктор видит нужный участок с многократным увеличением. Он может максимально сохранить здоровые ткани и эффективно отреставрировать зуб. Ни одна деталь не будет упущена!</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-wrapper">
                <h2 class="h2 mb-40">В каких случаях наши<br>доктора используют<br>микроскоп</h2>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper block-wrapper_tleft">
                <div class="dots mb-60">
                    <div class="dots-item">
                        <p class="text dots__text">Устранение кариеса</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Лечение пульпита и периодонтита</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Художественная реставрация зуба</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Хирургическое лечение</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-20-xl">
                <blockquote class="blockquote">Мы считаем своей главной задачей сохранение ваших зубов и восстановление их здоровья на долгие годы. Наши специалисты спасают даже безнадежные зубы с осложнениями кариеса. И не последнюю роль в этом, помимо профессионализма стоматологов, играет высокотехнологичное оптическое оборудование.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Ваш комфорт<br>при лечении зубов<br>с микроскопом</h2>
                <p class="text mb-80">Время приема сокращается, если стоматолог применяет микроскоп. Благодаря отличной видимости доктор быстро выполняет нужные манипуляции. Кроме того, врач не будет нарушать ваше личное пространство. Микроскоп позволит ему находиться на комфортном для вас расстоянии.</p>
                <blockquote class="blockquote mb-50">В стоматологической клинике FamilySmile применяется микроскоп Seiler Evolution XR6. Высококачественный оптический прибор произведен в Германии. Микроскоп может двигаться в разных направлениях, чтобы врачу было удобно работать. Увеличение изображения возможно до 30 раз.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block1" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Зачем нужен микроскоп <br>при лечении зубов</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-xl">
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-img block-img_tleft">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Как мы лечим кариес</h3>
                <p class="text mb-40">Чем раньше обнаружен кариес, тем проще его вылечить. И в этом случае многократное увеличение при диагностике — просто находка! В клинике FamilySmile есть возможность вылечить кариес без сверления. Мы используем водно-пескоструйный аппарат Aquacut Quattro Velopex (Великобритания) для безболезненного и бесшумного удаления пораженных тканей. А микроскоп позволяет сделать процедуру еще и максимально точной.</p>
                <a href="/services/lechenie-zubov/lechenie-kariesa/" class="link link_section mb-80">Подробнее</a>
                <blockquote class="blockquote">
                    Комфорт при лечении зубов в стоматологии FamilySmile вам обеспечит современная система компьютерной анестезии QuickSleeper (Франция). Введение препарата происходит совершенно безболезненно благодаря особой тонкой игле и медленной подаче.  Скорость введения анестетика контролирует компьютер.
                </blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества лечения зубов с микроскопом</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-50">Лечить зубы под микроскопом могут только опытные врачи, которые прошли специальные курсы, имеют большую практику и нужные навыки. В клинике FamilySmile работают именно такие специалисты. Доверив нам здоровье своих зубов, вы можете быть совершенно уверены в положительном результате. Лечение с микроскопом — это гарантия эффективного восстановления и сохранения зубов.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
