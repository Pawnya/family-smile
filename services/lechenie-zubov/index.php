<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лечение зубов");
?>
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Проводим безболезненное<br>лечение любой сложности</h1>
                </div>
                <p class="text mb-30">Современные методы лечения зубов достигли такого уровня, что положительный результат сохраняется надолго. Но только при условии, что вы своевременно обратитесь к врачу. Тогда вы защитите себя не только от возможного появления кариеса, но и от потери зуба. В стоматологической клинике FamilySmile есть все необходимое, чтобы вылечить кариес и его осложнения, сохранить здоровье и красоту ваших зубов. Наши врачи справляются с самыми трудными случаями.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">В стоматологической клинике<br>FamilySmile проводят:</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Почему важно<br>своевременно лечить<br>зубы</h2>
                <p class="text mb-40">В организме все взаимосвязано, поэтому на общее самочувствие влияет и состояние зубов. Важно знать, что если с ними возникли проблемы, то затягивать с этим не стоит. Зубы необходимо лечить вовремя. Легче предотвратить кариес на начальном этапе, чем бороться с его осложнениями — пульпитом и периодонтитом.</p>
                <p class="text mb-80">Врачи стоматологической клиники FamilySmile рекомендуют взять себе за правило раз в полгода проходить профилактический осмотр у специалиста. Тогда мы сможем оперативно выявить проблему и сразу приступить к лечению. Не забывайте, что зубы выполняют важную роль не только в пережевывании пищи. Здоровые, крепкие зубы — это еще и красивая улыбка.</p>
                <blockquote class="blockquote mb-50">Как только вы заметили, что на зубе появилось пятно или начал меняться цвет эмали, то сразу обращайтесь к врачу. А еще лучше регулярно 2 раза в год проходите профессиональную гигиену полости рта. Тогда проблем с зубами практически не будет. Эта простая профилактическая процедура защитит их от кариеса.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image3" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Кариес проходит<br>несколько стадий</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Начальная стадия</p>
                        <p class="text dots__text">На эмали появляется небольшое светлое или темное пятно. Можно вылечить без сверления и укола. С этим отлично справляется система Aquacut, с помощь которой проблемный участок промывается водой с абразивными частицами. Совершенно безопасный и эффективный способ лечения.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Поверхностный кариес</p>
                        <p class="text dots__text">На этом этапе появляется небольшая полость, которая располагается пока только в эмали. Восстановить ее естественным образом уже нельзя, и врач, аккуратно обработав зуб, фиксирует небольшую пломбу.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Средний и глубокий кариес</p>
                        <p class="text dots__text">Речь идет о более глубоком проникновении инфекции под эмаль. Врач проводит тщательную обработку полости зуба, устраняя поврежденные ткани, после чего эстетично восстанавливает форму зуба.</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <h3 class="h3 mb-40">Осложнения после кариеса</h3>
                <p class="text mb-30">Без лечения кариес переходит в следующую форму — пульпит и периодонтит. На этой стадии поражаются нервно-сосудистая система зуба и окружающая его костная ткань. В обоих случаях необходимо чистить и пломбировать каналы зуба. После диагностики доктор проводит лечение под микроскопом, что повышает шансы сохранить ваш зуб.</p>
                <p class="text mb-50">Если вы почувствовали боль или дискомфорт, позвоните нам. В стоматологической клинике FamilySmile вам быстро и качественно вылечат зубы.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества лечения зубов в стоматологической клинике FamilySmile</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-30">Еще один его незаменимый помощник — профессиональная фотодиагностика.  На первичном приеме врач фотографирует вас в разных ракурсах, затем при помощи специального программного обеспечения выявляет отклонения в симметрии и пропорциях лица и показывает результат вам на мониторе или телевизоре для наглядной демонстрации. В дальнейшем эти фотографии помогают составить более детальный и комплексный план лечения без повторного приглашения пациента в клинику.</p>
                <p class="text mb-50">В стоматологической клинике FamilySmile есть все необходимое для диагностики и качественного лечения зубов. Мы спасем ваши зубы от удаления!</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
