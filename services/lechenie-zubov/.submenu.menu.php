<?
$aMenuLinks = Array(
	Array(
		"Лечение кариеса",
		"/services/lechenie-zubov/lechenie-kariesa/",
		Array(),
		Array("INFO"=>"Врач устранит кариес, а затем восстановит природную форму зуба композитными материалами."),
		""
	),
	Array(
		"Лечение пульпита и периодонтита",
		"/services/lechenie-zubov/lechenie-pulpita-i-periodontita/",
		Array(),
		Array("INFO"=>"У нас есть необходимое оборудование, чтобы тщательно обработать канал и обеспечить зубу вторую жизнь."),
		""
	),
	Array(
		"Лечение зубов под микроскопом",
		"/services/lechenie-zubov/lechenie-zubov-pod-mikroskopom/",
		Array(),
		Array("INFO"=>"Многократное увеличение до 40 раз позволяет проводить лечение максимально ювелирно."),
		""
	)
);
?>
