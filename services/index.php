<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Услуги");
?>
<link rel="stylesheet" href="main.min.css">
<!-- <script defer src="main.js"></script> -->
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Полный комплекс<br>стоматологических услуг<br>для всей семьи</h1>
                </div>
                <p class="text mb-30">Стоматологическая клиника FamilySmile создана, чтобы заботиться о вашем здоровье, красоте и комфорте. Наши специалисты решают сложнейшие стоматологические задачи, используя свой опыт, современное оборудование и материалы премиального класса. Мы убеждены, что качественная медицинская помощь способна изменить вашу жизнь к лучшему, поэтому работаем во всех направлениях стоматологии.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Услуги</h2>
            </div>
            <? $bq = 'Врачи клиники FamilySmile стремятся не просто решить проблему сегодня. Для нас важно вернуть вам полноценное здоровье, комфорт повседневной жизни и привлекательность улыбки. Над составлением плана лечения работает целая команда врачей. И все необходимые услуги мы оказываем в одном месте. Запишитесь на прием, мы вам обязательно поможем.';?>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services-main",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "2",
                            "CHILD_MENU_TYPE" => "submenu",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                    <div class="box-slide swiper-slide">
                        <div class="box-item box-item_last">
                            <blockquote class="blockquote mb-50">
                                <?=$bq;?>
                            </blockquote>
                            <?$APPLICATION->IncludeComponent(
                                "family-smile:form.result.new",
                                "inline",
                                Array(
                                    "WEB_FORM_ID"            => "1",
                                    "AJAX_OPTION_STYLE"      => "Y",
                                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                                    "USE_EXTENDED_ERRORS"    => "Y",
                                    "SEF_MODE"               => "N",
                                    "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                    "CACHE_TYPE"             => "N",
                                    "CACHE_TIME"             => "3600",
                                    "LIST_URL"               => "",
                                    "EDIT_URL"               => "",
                                    "SUCCESS_URL"            => "",
                                    "CHAIN_ITEM_TEXT"        => "",
                                    "CHAIN_ITEM_LINK"        => "",
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mb-90-xs mb-0-xl mt-50-xs mt-0-md">
                <blockquote class="blockquote blockquote_tab mb-30">
                    <?=$bq;?>
                </blockquote>
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
