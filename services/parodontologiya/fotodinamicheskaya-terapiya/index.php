<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Фотодинамическая терапия");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Заметное улучшение состояния десен после первого визита</h1>
            </div>
            <p class="text mb-30">Приятная улыбка, свободное общение, комфорт во время приема пищи — все это начинается со здоровья десен. Они служат надежной опорой для зубов, поэтому требуют особого подхода. Фотодинамическая терапия в клинике FamilySmile бережно, но эффективно устраняет главную причину воспалений — бактерии. Улучшение состояния десен вы заметите уже после первой процедуры.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="main-inner">
            <h2 class="h2 mb-40">Технологии вашего здоровья</h2>
            <p class="text">Врачи клиники FamilySmile используют в работе современный аппарат FotoSan. Его работа основана на мягком воздействии светового луча, активирующего специальный гель. Он наносится на десну или закладывается в пародонтальный карман. Высвобождается кислород, который разрушает бактериальную биопленку и уничтожает вредные микроорганизмы. Благодаря аппарату FotoSan лечение проходит комфортно и безболезненно.</p>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h3 class="h3 mb-40">Применение FotoSan</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Лечение заболеваний десен</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Устранение стоматита</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Профилактика и снятие воспаления вокруг имплантатов</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Дезинфекция каналов перед пломбированием</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Лечение глубокого кариеса</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-90-xl">
            <blockquote class="blockquote mb-50">Фотодинамическая терапия заменяет лечение антибиотиками. Микроорганизмы постепенно вырабатывают сопротивляемость к препаратам, особенно если прервать лечение без согласования с врачом. FotoSan уничтожает бактерии мгновенно, поэтому они просто не успевают повысить устойчивость к гелю. Воспаления устраняются в самые сжатые сроки без побочных эффектов.</blockquote>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Как проходит лечение аппаратом FotoSan</h2>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Специальный гель наносится тонким слоем на десну или закладывается в пародонтальный карман.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Свет лазера активирует препарат — выделяется активный кислород, уничтожающий микроорганизмы изнутри.</p>
                </div>
            </div>
            <p class="text">Гель воздействует исключительно на микроорганизмы и не затрагивает здоровые ткани. Поэтому фотодинамическая терапия абсолютно безопасна и подходит всем.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <blockquote class="blockquote mb-50">Фотодинамическая терапия наиболее эффективна в комплексе с аппаратом Vector. Он бережно очищает пародонтальные карманы от зубных отложений. А FotoSan закрепляет эффект лечения. Сочетание двух методик устраняет воспаление и не допускает дальнейшего прогрессирования заболевания. Выполняйте все рекомендации врача FamilySmile, и вы забудете о проблемах с деснами.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Преимущества фотодинамической терапии  </h2>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Лечение не имеет противопоказаний или побочных эффектов.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Аппарат эффективно устраняет все известные бактерии.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Улучшение состояния десен вы почувствуете после первой процедуры.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Лечение проходит бесконтактно, бескровно и безболезненно.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <p class="text mb-50">Лечение аппаратом FotoSan останавливает воспалительные процессы. К вам возвращается комфорт повседневной жизни: устраняются кровоточивость, болезненность десен и запах изо рта. Вы можете свободно улыбаться, общаться и принимать в пищу все, что хочется.</p>
            <p class="text mb-60">Мы заботимся о вас, поэтому применяем самые современные технологии. Для нас очень важно, чтобы каждый день вы чувствовали себя комфортно и были спокойны за свое здоровье. Фотодинамическая терапия в клинике FamilySmile — это быстрый, эффективный и безболезненный вариант лечения десен. Запишитесь на прием. Мы будем рады вам помочь. </p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
