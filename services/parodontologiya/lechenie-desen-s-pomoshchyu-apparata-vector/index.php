<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лечение десен с помощью аппарата Vector");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Щадящее лечение десен</h1>
                </div>
                <p class="text mb-30">Налет может не только скапливаться на поверхности зубов, но и перемещаться под десну при отсутствии регулярной гигиены. Начинается воспалительный процесс. Безболезненно устранить очаги воспаления и предотвратить дальнейшее поражение тканей помогает ультразвуковой аппарат Vector Paro («Вектор»). Специалисты стоматологической клиники FamilySmile применяют его в комплексе с другими методиками лечения, возвращая здоровье вашим деснам.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 mb-40">Вернем вам крепкие десны  </h2>
                <p class="text">Нерегулярная гигиена полости рта — это частая причина возникновения заболеваний десен. В клинике FamilySmile с помощью современного оборудования мы возвращаем здоровье мягким тканям. Однако важно помнить, что запущенные болезни десен нуждаются в постоянных профилактических мероприятиях, чтобы поддерживать оптимальное состояние пародонта.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Как избежать заболеваний десен</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <p class="text mt-60 mb-50">Наш стоматолог поможет вам снять воспаление и вернуть здоровье мягким тканям. Он объяснит, как правильно чистить зубы, и подберет для вас индивидуальные средства гигиены.</p>
                            <div class="btn-wrapper">
                                <?$APPLICATION->IncludeComponent(
                                    "family-smile:form.result.new",
                                    "inline",
                                    Array(
                                        "WEB_FORM_ID"            => "1",
                                        "AJAX_OPTION_STYLE"      => "Y",
                                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                                        "USE_EXTENDED_ERRORS"    => "Y",
                                        "SEF_MODE"               => "N",
                                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                        "CACHE_TYPE"             => "N",
                                        "CACHE_TIME"             => "3600",
                                        "LIST_URL"               => "",
                                        "EDIT_URL"               => "",
                                        "SUCCESS_URL"            => "",
                                        "CHAIN_ITEM_TEXT"        => "",
                                        "CHAIN_ITEM_LINK"        => "",
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-30-xs mt-0-md">
                <p class="text mb-50">Наш стоматолог поможет вам снять воспаление и вернуть здоровье мягким тканям. Он объяснит, как правильно чистить зубы, и подберет для вас индивидуальные средства гигиены.</p>
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/2.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-90-xl">
                <h2 class="h2 mb-40">Vector Paro («Вектор») для лечения заболеваний десен</h2>
                <p class="text">Когда десны поражены, меняется не только их цвет, но и структура: они становятся рыхлыми. А при пародонтите наблюдается даже образование зубодесневых карманов. Бактериальный налет и камни скапливаются уже глубоко под десной, вызывая ее отслоение от зуба. В этом случае, чтобы удалить налет и бактерии, врач использует аппарат немецкого производства Vector. С его помощью мы лечим заболевания десен, сохраняя зубы от расшатывания и выпадения.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Vector Paro щадяще воздействует на мягкие ткани. Аппарат подает направленную струю жидкости с абразивными частицами. Под воздействием ультразвуковой волны эти частицы приходят в движение и деликатно устраняют налет и бактерии. Также они полируют поверхность корней, что помогает деснам плотнее прилегать к зубам. </blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Почему мы доверяем лечение десен аппарату Vector («Вектор»)</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Особенности проведения процедуры </h2>
                <p class="text mb-80">Перед очищением зубодесневых карманов в клинике FamilySmile мы проводим профессиональную гигиену полости рта. Специалист удаляет зубной налет с каждого зуба и из межзубных промежутков. Процедура помогает подготовиться к основному этапу лечения десен. Чтобы восстановить здоровье пародонта, мы должны ликвидировать все очаги воспаления.</p>
                <blockquote class="blockquote mb-50">Vector-терапию необходимо повторить через 10–14 дней, а затем через 3 месяца прийти на контрольный прием для определения прогресса. Поддерживающий курс лечения десен рекомендуется проходить 1 раз в 6 месяцев.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block block_top">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-100-xl">
                <p class="text">Аппарат Vector Paro справляется со сложными поддесневыми отложениями. Если другие ультразвуковые устройства проникают на глубину до 5 мм, то Vector Paro устраняет бактерии и налет на глубине до 11 мм. Сегодня это самый совершенный метод лечения заболеваний пародонта, также он эффективно используется для профилактики воспалений при имплантации и протезировании.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <p class="text mb-50">Проходите регулярные осмотры у пародонтолога в стоматологической клинике FamilySmile. Чем раньше будет обнаружено заболевание десен, тем эффективнее пройдет лечение. Мы не только вернем здоровье мягким тканям, но и сохраним устойчивость ваших зубов.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
