<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Плазмолифтинг");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Эффективное восстановление здоровья десен</h1>
                </div>
                <p class="text mb-30">Десны требуют особо внимательного подхода. Они являются надежной опорой для зубов, делая улыбку красивой и здоровой. Приходите в стоматологию FamilySmile, если обнаружили кровоточивость или воспаление десен. Наши врачи составят для вас курс комплексной реабилитации пародонта с применением плазмолифтинга. Это передовая методика восстановления десен на клеточном уровне за счет собственных ресурсов организма.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text">Плазмолифтинг — это инъекции плазмы, полученной из вашей собственной крови. Плазма богата тромбоцитами, которые содержат в себе факторы роста и играют важную роль в заживлении и регенерации поврежденных мягких тканей. Процедура проводится в ходе комплексного курса лечения. Она эффективна уже после первого применения и абсолютно безопасна, поскольку используется родной биоматериал.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">В каких случаях помогает плазмолифтинг</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-50-xs mt-0-xl">
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Как проходит подготовка к плазмолифтингу </h2>
                <p class="text mb-80">Мы внимательны к каждому случаю. Приходите к нам на прием и расскажите, что вас беспокоит. Наши врачи побеседуют с вами, проведут подробное обследование и составят комплексный план лечения. От их профессионального взгляда не ускользнет ни одна деталь. </p>
                <blockquote class="blockquote mb-50">Врач сделает забор небольшого количества вашей крови, поместит ее в современную центрифугу EBA-200 (Германия). В специальных условиях кровь делится на фракции. Образуется плазма, богатая тромбоцитами. Затем врач делает инъекции равномерно по всей десне. Уже в первые секунды начинаются активные процессы восстановления тканей.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">Преимущества плазмолифтинга для здоровья десен</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Эффективно</p>
                        <p class="text dots__text">Курс лечения восстанавливает деснам крепкую структуру и здоровый цвет.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Надежно</p>
                        <p class="text dots__text">Плазмолифтинг возвращает десне правильную форму и способствует долговечности службы имплантатов, коронок.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Комфортно</p>
                        <p class="text dots__text">Инъекции практически неощутимы и в большинстве случаев не требуют анестезии.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Быстро</p>
                        <p class="text dots__text">Нужно не более 30 минут, чтобы провести забор крови, поместить ее в центрифугу, извлечь плазму и сделать инъекцию.</p>
                    </div>
                </div>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <p class="text mb-60">Плазмолифтинг не просто останавливает процесс воспаления в деснах, а способствует активному восстановлению формы, структуры и цвета десны. </p>
                <p class="text mb-50">Приходите в стоматологию FamilySmile на лечение десен к врачу-пародонтологу, кандидату медицинских наук. Он составит для вас курс лечебных процедур, которые эффективно помогут в вашем случае. С помощью плазмолифтинга наш специалист повысит иммунитет ваших десен к различным заболеваниям, а также ускорит восстановление тканей. Ждем вас на приеме с понедельника по субботу в удобное вам время.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
