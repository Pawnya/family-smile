<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пародонтология");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Проведем комплексное лечение десен </h1>
                </div>
                <p class="text mb-30">В клинике FamilySmile мы с особой деликатностью восстанавливаем здоровье десен. Наши врачи проводят комплексное лечение мягких тканей с помощью современного оборудования и устраняют даже запущенные воспаления десен практически без боли. Записывайтесь на прием к специалистам стоматологической клиники FamilySmile. Щадящие процедуры оставят у вас положительные впечатления от посещения пародонтолога.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Для оздоровления десен мы применяем следующие методики:</h2>
            </div>
            <? $bq = 'Хирургическое вмешательство в нашей клинике проводится под многоуровневым контролем с заботой о вашем здоровье. Самой тщательной обработке подвергаются и рабочие инструменты, и помещение.';?>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                    <div class="box-slide swiper-slide">
                        <div class="box-item box-item_last">
                            <blockquote class="blockquote mb-50">
                                <?=$bq;?>
                            </blockquote>
                            <?$APPLICATION->IncludeComponent(
                                "family-smile:form.result.new",
                                "inline",
                                Array(
                                    "WEB_FORM_ID"            => "1",
                                    "AJAX_OPTION_STYLE"      => "Y",
                                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                                    "USE_EXTENDED_ERRORS"    => "Y",
                                    "SEF_MODE"               => "N",
                                    "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                    "CACHE_TYPE"             => "N",
                                    "CACHE_TIME"             => "3600",
                                    "LIST_URL"               => "",
                                    "EDIT_URL"               => "",
                                    "SUCCESS_URL"            => "",
                                    "CHAIN_ITEM_TEXT"        => "",
                                    "CHAIN_ITEM_LINK"        => "",
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq">
                <blockquote class="blockquote blockquote_tab mb-30">
                    <?=$bq;?>
                </blockquote>
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества лечения воспалений десен в клинике FamilySmile</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <blockquote class="blockquote mt-40">Мы заботимся о вашем благополучии, поэтому в клинике FamilySmile действует строгий многоуровневый контроль безопасности. Мы тщательно следим за стерильностью всех помещений и инструментария.</blockquote>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner">
                <div class="box-bq">
                    <blockquote class="blockquote mt-60-xs mt-0-xl mb-50">Мы заботимся о вашем благополучии, поэтому в клинике FamilySmile действует строгий многоуровневый контроль безопасности. Мы тщательно следим за стерильностью всех помещений и инструментария.</blockquote>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Наше оборудование и плюсы новейших методик лечения  </h2>
                <p class="text mb-80">Восстановление десен может занимать длительное время. Передовые технологии помогают нам сократить этот период. Вы вернетесь к прежней жизни — без дискомфорта и боли.</p>
                <h3 class="h3 mb-40">В лечении мы применяем:</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Аппарат FotoSan</p>
                        <p class="text dots__text">Устройство «пробуждает» ионы кислорода, которые эффективно устраняют все виды бактерий.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Vector Paro</p>
                        <p class="text dots__text">Разрушает биопленку и отложения даже на корне зуба, значительно уменьшая пародонтальные карманы.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Hettich ЕВА-200</p>
                        <p class="text dots__text">Универсальная центрифуга отделяет плазму крови — десны восстанавливаются собственными ресурсами организма.</p>
                    </div>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 mb-40">Лечим десны по показаниям: <br>основные симптомы заболеваний</h2>
                <p class="text mb-50">Десны изменили свой цвет, и появилась кровоточивость. При чистке зубов чувствуется дискомфорт. Все это — возможные признаки воспаления мягких тканей. Также явными симптомами заболеваний служат оголение шеек зубов и расшатывание. Это означает, что пришло время обратиться к пародонтологу. Запишитесь на консультацию к специалистам клиники FamilySmile. Мы проведем обследование десен и назначим эффективный курс их восстановления.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
