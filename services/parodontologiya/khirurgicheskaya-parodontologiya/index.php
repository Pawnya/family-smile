<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Хирургическая пародонтология");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Эффективная хирургическая помощь при заболеваниях десен</h1>
            </div>
            <p class="text mb-30">Многие приходят к стоматологу только для лечения зубов и совсем не обращают внимания на состояние своих десен. Однако отсутствие своевременного лечения пародонта приводит к тяжелым формам заболеваний, при которых можно лишиться зубов. Ведь именно десны удерживают зубы от выпадения. Если консервативные методы неэффективны, на помощь приходит хирургия. Специалисты стоматологической клиники FamilySmile остановят воспаление, восстановят десневой контур и вернут тканям пародонта здоровый вид.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <blockquote class="blockquote mb-40">Основная причина воспаления десен — зубной налет, который твердеет и травмирует нежные ткани пародонта. Полностью удалить налет возможно только с помощью специального оборудования. Посещайте клинику FamilySmile дважды в год для профилактического осмотра и проведения профессиональной гигиены полости рта, чтобы сохранить здоровье десен.</blockquote>
            <p class="text">На ранней стадии можно вылечить практически любое заболевание пародонта. Но если оно прогрессирует, то остановить его сможет только хирург. Наши врачи владеют различными методиками лечения десен любой сложности.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-90-xl mt-50-xs mt-0-xl">
            <h3 class="h3 mb-40">Что такое пародонтальный карман</h3>
            <p class="text">Налет со временем минерализируется и становится зубным камнем. Он повреждает десну, которая воспаляется и становится рыхлой. Постепенно десна отходит от зуба, а налет проникает все глубже. Так образуется пародонтальный карман. Появляются болезненность и дискомфорт. Специалисты клиники FamilySmile помогут решить эту проблему.</p>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block">
        <div class="section-title">
            <h2 class="h2 mb-60">Хирургическая помощь для здоровья ваших десен в клинике FamilySmile</h2>
        </div>
    </div>
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h3 class="h3 mb-40">Лоскутные операции</h3>
            <p class="text mb-60">Лоскутные операции позволяют улучшить состояние десен при пародонтите и увеличить ее объем, укрепить костную ткань, уменьшить или устранить подвижность зубов и кровоточивость десен. Последние приобретают более эстетичный вид, контур становится ровнее.</p>
            <h3 class="h3 mb-40">Открытый кюретаж</h3>
            <p class="text">Если пародонтальный карман достиг глубины более 5 мм, то очистить его можно только с помощью операции кюретажа. Кюрета — это инструмент, которым удаляется налет. Метод эффективен даже при тяжелых осложнениях, например при отслоении десны от зуба.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h3 class="h3 mb-40">Как проходит открытый кюретаж</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Проводим обезболивание эффективным анестетиком.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Делаем аккуратный небольшой надрез и отслаиваем десну.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Выполняем чистку с помощью кюреты, ультразвука и других инструментов.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Полируем корень зуба, связки при этом не затрагиваются.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Закрываем десну, накладываем аккуратные швы.</p>
                </div>
            </div>
            <p class="text mb-50">Чтобы значительно уменьшить период восстановления и сохранить зубодесневые связки, мы используем методику <a href="/services/parodontologiya/plazmolifting/">плазмолифтинга</a>. При помощи этой процедуры мы успешно восстанавливаем костные структуры и слизистые поверхности, значительно уменьшаем альвеолярные карманы, сокращаем шатание, устраняем кровоточивость и минимизируем воспалительный процесс.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h3 class="h3 mb-50">Остеопластика</h3>
            <p class="text mb-60">Методика применяется, если пародонтальный карман достиг 8 мм и началась атрофия кости.   При этом зубы стали подвижными, увеличился риск их выпадения. Процедура похожа на открытый кюретаж, только в полость карманов вводится препарат, стимулирующий рост костной ткани.</p>
            <h3 class="h3 mb-40">Закрытие рецессии десны</h3>
            <p class="text">Если ткань десны сильно убыла и оголилась шейка зуба, то мы проведем трансплантацию мягких тканей. Методика позволяет укрепить десну и вернуть ей естественный контур и объем. Специалисты стоматологической клиники FamilySmile помогут восстановить эстетику вашей улыбки.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h3 class="h3 mb-40">Как проходит операция</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Проводим местную анестезию для вашего комфорта</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Осторожно отсекаем лоскут от десны из угла челюсти или верхнего неба</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Пересаживаем лоскут на нужное место, накладываем швы</p>
                </div>
            </div>
            <h3 class="h3 mb-40">Резекция десневого кармана</h3>
            <p class="text mb-50">Цель операции — удалить поврежденную десну, которая отслоилась от зуба. Образовавшийся десневой карман является следствием воспаления и одновременно способствует прогрессированию заболевания. Под десну попадают частицы пищи и бактерии, которые еще больше разрушают пародонт. Остановить этот процесс может только отсечение пораженных тканей и тщательное удаление налета. Операция пройдет совершенно безболезненно, и в скором времени вы почувствуете облегчение. Источник заболевания будет устранен, воспаление и отечность пройдут.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/4.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-50">Гингивэктомия</h2>
            <p class="text mb-60">Если ткань пародонта патологически разрослась вследствие воспаления, то проводится иссечение края  десны  и удаление лишних тканей. Операция применяется, чтобы предотвратить развитие воспалительного процесса. Хирург проведет эту довольно быструю операцию очень бережно, заживление наступит в короткие сроки.</p>
            <h3 class="h3 mb-40">Гингивопластика</h3>
            <p class="text mb-50">Цель процедуры — выровнять высоту десен и придать им эстетичный вид. Лишние ткани просто удаляются. Ваша улыбка станет привлекательнее, десна приобретет аккуратный вид.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <p class="text mb-50">Хирургическая пародонтология в стоматологической клинике FamilySmile позволяет улучшить здоровье десен и сохранить устойчивость зубов, а также решить эстетические проблемы даже при осложнениях заболеваний пародонта. Приходите в нашу клинику, и мы поможем вашим деснам снова обрести здоровый вид!</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
