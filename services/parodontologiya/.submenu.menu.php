<?
$aMenuLinks = Array(
    Array(
        "Фотодинамическая терапия",
        "/services/parodontologiya/fotodinamicheskaya-terapiya/",
        Array(),
        Array("INFO"=>"С помощью аппарата FotoSan мы проводим профилактику, а также лечение гингивита и пародонтита."),
        ""
    ),
	Array(
		"Лечение десен с помощью аппарата Vector",
		"/services/parodontologiya/lechenie-desen-s-pomoshchyu-apparata-vector/",
		Array(),
		Array("INFO"=>"Специалисты клиники эффективно очищают пародонтальные карманы, устраняя даже сильное воспаление."),
		""
	),
	Array(
		"Плазмолифтинг",
		"/services/parodontologiya/plazmolifting/",
		Array(),
		Array("INFO"=>"Процедура помогает оздоровить десны собственными ресурсами организма на финальном этапе их лечения."),
		""
	),
	Array(
		"Хирургическая пародонтология",
		"/services/parodontologiya/khirurgicheskaya-parodontologiya/",
		Array(),
		Array("INFO"=>"В сложных случаях, чтобы поддержать здоровье мягких тканей, необходима помощь хирурга."),
		""
	)
);
?>
