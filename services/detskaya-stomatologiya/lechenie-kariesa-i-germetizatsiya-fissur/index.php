<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лечение кариеса и герметизация фиссур");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Лечим кариес по новым технологиям — без боли и шума</h1>
            </div>
            <p class="text mb-30">Молочные и постоянные зубы у детей очень уязвимы. Одним из предрасполагающих факторов развития кариеса является неудовлетворительная гигиена полости рта. Ребенок не может самостоятельно убрать щеткой весь налет, и в результате образуется кариес. В стоматологической клинике FamilySmile мы советуем не ждать образования кариеса, а предотвращать его, посещая профилактические осмотры. Если кариес проник сквозь эмаль, тогда мы проводим лечение. Благодаря современным технологиям и оборудованию оно протекает полностью безболезненно, а от общения со стоматологом у ребенка остаются только положительные впечатления.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Первое знакомство с детским стоматологом</h2>
            <p class="text">Мы знаем, как сложно бывает ребенку успокоиться перед лечением, поэтому для начала советуем приводить детей на знакомство с доктором. Они пообщаются, посчитают и почистят зубки — врач завоюет доверие маленького пациента. После такого посещения можно быть уверенным, что на следующем приеме лечение пройдет в спокойной и комфортной атмосфере.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h2 class="h2 mb-40">Лечить кариес совсем не страшно</h2>
            <p class="text">Наши постоянные маленькие пациенты не боятся лечить зубы. Они знают, что в клинике FamilySmile к ним относятся внимательно, по-доброму. Мы применяем передовое стоматологическое оборудование, благодаря которому анестезия проводится безболезненно, а само лечение проходит без шума.</p>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Этапы лечения кариеса в клинике FamilySmile</h2>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Готовимся к инъекции. Врач наносит на десну ребенка охлаждающий гель. Он делает ее нечувствительной для укола, и ребенок ничего не почувствует.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Проводим анестезию. У нас применяется компьютерное обезболивание. Благодаря ему гарантируется абсолютно комфортное проведение анестезии.</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Безболезненно лечим. С помощью жидкости с абразивными частицами бесшумный аппарат Aquacut Quattro бережно удаляет кариозные ткани.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <blockquote class="blockquote mb-50">Почему инъекция у нас полностью безболезненна? Все дело в том, что стоматолог применяет аппарат QuickSleeper 5. Специальная программа контролирует подачу обезболивающего препарата и скорость вращения иглы. Дети совершенно не ощущают боли! Тем более что место инъекции мы предварительно замораживаем гелем.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block mb-40">
        <div class="block-wrapper block-wrapper_tleft">
            <h2 class="h2">Эффективно лечим кариес на любой стадии</h2>
        </div>
    </div>
    <div class="container block">
        <div class="block-wrapper pr-0-xs pr-40-md">
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Начальный, в виде белого пятна</p>
                    <p class="text dots__text">Начальный кариес поражает эмаль незаметно. Это могут быть не только темные, но и белые пятна. Для лечения такого кариеса врач наносит на эмаль укрепляющие составы. Они содержат кальций и фтор, которые восстанавливают и укрепляют эмаль.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Средний и глубокий кариес</p>
                    <p class="text dots__text">Если вовремя не вылечить начальный кариес, то он проникает дальше эмали — в дентин. С помощью Aquacut Quattro доктор деликатно удаляет кариозные поражения и восстанавливает зуб эстетичным пломбировочным материалом.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Пульпит и периодонтит</p>
                    <p class="text dots__text">Кариес может проникнуть дальше дентина, в пульпу — к нервным окончаниям, а также к тканям, удерживающим зуб. Врач в этом случае удаляет воспаленный нерв, а сильно разрушенный зуб реставрирует подобранным в цвет композитным материалом.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
            <p class="text mb-50">В клинике мы сберегаем даже сильно разрушенные зубы от преждевременного удаления. Для этого врач устанавливает на них специальные детские коронки. Они обеспечивают нужную нагрузку на челюсти и сохраняют правильность их смыкания. Кроме того, обеспечивают надежную герметизацию реставрации и гарантируют сохранение зуба до физиологической смены. С коронками зубы ребенка остаются крепкими и служат долго.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/4.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_center">
        <div class="block-wrapper pr-0-xs pr-40-md">
            <h2 class="h2 mb-40">Защищаем тонкую эмаль от поражения кариесом</h2>
            <p class="text mb-50">Кариес молочных и постоянных зубов у детей вызывает налет. Он активно скапливается на эмали и особенно в межзубных промежутках. В стоматологической клинике FamilySmile мы убираем весь налет и предотвращаем возникновение кариеса безболезненными и эффективными процедурами:</p>
            <div class="dots">
                <div class="dots-item">
                    <p class="dots__title">Чисткой Air-Flow</p>
                    <p class="text dots__text">С помощью аппарата Air-Flow врач обрабатывает поверхность каждого зуба. Поток воды с абразивными частицами аккуратно устраняет мягкий налет.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Чисткой специальными пастами</p>
                    <p class="text dots__text">С помощью особых составов эмаль тщательно полируется. На гладких зубах налет практически не задерживается.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Фторированием эмали</p>
                    <p class="text dots__text">После того как налет удален, врач наносит на зубы фтор и кальций. Минералы питают эмаль и надолго укрепляют ее. Также они запечатывают микротрещины и снижают чувствительность зубов.</p>
                </div>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
            <p class="text mb-50">Молочные зубы, а также постоянные в первые 2 года после своего появления особенно уязвимы. Глубокие впадины на жевательной поверхности задерживают остатки пищи, и тонкую эмаль быстро поражает кариес. В клинике FamilySmile мы сохраняем здоровье коренных зубов, покрывая углубления (фиссуры) безопасным герметиком.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Приходите в семейную стоматологию FamilySmile! Заботливые доктора безболезненно вылечат кариес и научат ребенка правильно ухаживать за своими зубами. Игрушки помогают нам объяснить детям, почему необходимо ухаживать за зубками. Мы не только сохраняем здоровье зубного ряда, но и формируем у детей позитивный опыт общения с врачами-стоматологами.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
