<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Удаление детских зубов и пластика уздечки");
?>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Хирургическая помощь для здоровой улыбки малыша</h1>
            </div>
            <p class="text mb-30">Следить за здоровьем полости рта у детей очень важно. Состояние зубов напрямую влияет на пищеварение и развитие организма в целом. Поэтому молочные зубы желательно сохранять до физиологической смены. А если это не удалось? Тогда нужно правильно удалить зуб.<br> Еще одна частая проблема может затруднять прием пищи и формирование речи — врожденное нарушение строения губ и языка. Детские врачи стоматологической клиники FamilySmile позаботятся о здоровье вашего ребенка. Мы безболезненно проведем удаление зуба и пластику уздечек губ или языка.</p>
            <div class="btn-wrapper btn-wrapper_col">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
                <a href="#" class="link link_section mt-30">Подробнее</a>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/1.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Заботимся о ваших детях</h2>
            <p class="text mb-40">Некоторых детей очень непросто уговорить прийти на осмотр врача. И для родителей лечение зубов малыша кажется очень сложной задачей. В стоматологической клинике FamilySmile учитывается психология ребенка. Любое лечение начинается с предварительного знакомства. Ребенок должен привыкнуть к обстановке, почувствовать себя в безопасности. Доктор старается с ним подружиться.</p>
            <p class="text">Сначала проводится обычный осмотр. Потом, чтобы ваш малыш привык к манипуляциям врача, можно провести несложную безболезненную процедуру, например почистить зубной налет. И только когда установились доверительные отношения, доктор приступит к более сложному лечению.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h3 class="h3 mb-40">Показания к удалению зуба у детей</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">Молочные зубы мешают росту постоянных</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Острое воспаление зуба или десны, которое невозможно вылечить</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Механическое разрушение зуба</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">Обширный кариес, который может затронуть зачаток коренного зуба</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/2.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">К чему приводит преждевременное удаление молочных зубов</h2>
            <p class="text mb-60">Если зуб был потерян раньше времени, то соседние зубы начинают сдвигаться. И когда приходит пора прорезаться постоянному зубу, для него просто не оказывается достаточно места. Очень часто в дальнейшем приходится проходить ортодонтическое лечение: носить пластинки или брекеты. При удалении молочных зубов в стоматологической клинике FamilySmile учитывается этот момент.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h3 class="h3 mb-40">Как проходит удаление зубов у детей</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="dots__title">При физиологической смене зубов (с 5 до 14 лет)</p>
                    <p class="text dots__text">Такое удаление самое простое и проходит очень быстро. Как правило, зуб уже расшатан, а корни атрофированы. На десну наносится обезболивающий гель, и зуб моментально извлекается.</p>
                </div>
                <div class="dots-item">
                    <p class="dots__title">Преждевременная потеря зуба</p>
                    <p class="text dots__text">Корни зуба еще не рассосались, поэтому требуется более серьезная анестезия. В десну вводится безопасный для ребенка препарат, и доктор удаляет зуб. В дальнейшем мы установим на это место конструкцию, которая удержит соседние зубы от смещения.</p>
                </div>
            </div>
            <blockquote class="blockquote mb-40">У специалистов стоматологической клиники FamilySmile большой опыт работы с детьми. Мы находим подход к каждому ребенку. Удаление зуба пройдет быстро и безболезненно, в форме игры. Очень важно сохранить положительный настрой ребенка, чтобы не вызвать страха перед лечением зубов на всю жизнь. Доктор постарается, чтобы у малыша осталось впечатление, что стоматолог — это добрая Зубная фея, которой можно доверить свои зубы. Для отвлечения внимания малыша над креслом есть телевизор с увлекательными мультфильмами.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/3.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h2 class="h2 mb-40">Патологии уздечки губ и языка: как распознать</h2>
            <p class="text mb-40">Если с самого раннего возраста малыш плохо ест и в дальнейшем часто капризничает при приеме пищи, то это повод задуматься. Чем раньше вы покажете ребенка специалисту, тем лучше. Если уздечка губ в длину менее 5 мм, а уздечка языка меньше 3 мм или закреплена неправильно, нужно провести пластику.</p>
            <p class="text">Наши детские стоматологи в игровой форме проведут осмотр. Чтобы свести психологический дискомфорт для ребенка к минимуму, доктор «договорится» с малышом об операции.</p>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h3 class="h3 mb-40">Если слишком короткая уздечка губ:</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">формируется неправильный прикус</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">оголяются шейки зубов и возникают зубодесневые карманы</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">появляется щербинка между передними зубами</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">нарушается произношение звуков</p>
                </div>
            </div>
            <blockquote class="blockquote mb-40">Патологии уздечки — одна из самых частых проблем в детской стоматологии. Встречаются они почти у 10% детей. Причем взрослые иногда даже не подозревают о проблеме. Профилактический осмотр в клинике FamilySmile поможет вам получить полную информацию о стоматологическом здоровье вашего малыша!</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container">
        <div class="block-img block-img_full">
            <img src="images/4.png" alt="Full image" class="block__img_full">
        </div>
    </div>
</section>
<section class="section section-margin_top">
    <div class="container block block_top">
        <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
            <h3 class="h3 mb-40">Если нарушено строение уздечки языка:</h3>
            <div class="dots mb-60">
                <div class="dots-item">
                    <p class="text dots__text">младенцу трудно сосать молоко</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">нарушается жевательная функция</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">повышается слюноотделение</p>
                </div>
                <div class="dots-item">
                    <p class="text dots__text">неизбежны логопедические проблемы</p>
                </div>
            </div>
            <blockquote class="blockquote mb-40">Провести пластику уздечек  губ и языка можно даже в младенческом возрасте. Лучше не затягивать с лечением. Оптимально провести операцию до того, как начали прорезываться постоянные зубы.</blockquote>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
        <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
            <h2 class="h2 mb-40">Как проходит пластика уздечек</h2>
            <p class="text">Малыш будет психологически подготовлен к коррекции уздечки. Внимание ребенка сосредоточится либо на игровых моментах, либо на мультфильмах. Операция очень быстрая, поэтому обычно проходит под местной анестезией. Аккуратно и бережно, с ювелирной точностью доктор подрезает уздечку. При необходимости накладываются швы, после чего место надреза обрабатывается антисептиком. И вы вместе с малышом пойдете домой, где нужно будет соблюдать несложные рекомендации врача.</p>
        </div>
    </div>
</section>
<section class="section section-margin_top section-margin_bottom">
    <div class="container">
        <div class="main-inner">
            <p class="text mb-50">Мы не сомневаемся, что вы заботитесь о здоровом росте своих детей, и всегда рады вам в этом помочь. Обращайтесь в стоматологическую клинику FamilySmile, если хотите быть уверены в здоровье зубов ребенка. Мы проведем качественную операцию по пластике уздечки или удалению зуба, чтобы ваш малыш принимал пищу без проблем, правильно произносил звуки и гармонично развивался.</p>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
