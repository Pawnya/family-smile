<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Исправление прикуса у детей");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Выравниваем зубы для здоровья и эстетики детской улыбки</h1>
                </div>
                <p class="text mb-30">В организме человека все взаимосвязано. Поэтому зубы играют важную роль в развитии ребенка: от них зависит, как сформируется речь, будет ли правильным пищеварение. Кроме того, зубочелюстная система влияет на осанку и правильное дыхание. Чтобы ваш малыш гармонично развивался и рос здоровым, приводите его на прием к ортодонту в стоматологическую клинику FamilySmile для диагностики прикуса. Доктор выявит любые нарушения и проведет эффективное лечение.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 tac mb-40">Правильный прикус для гармоничного развития ребенка</h2>
                <p class="text mb-30">По мере взросления вашего ребенка красивые ровные зубы будут приобретать все большее значение для его самооценки и психологического комфорта. Неэстетичная улыбка влечет неуверенность в себе и может влиять на коммуникабельность. Но красота в вопросе прикуса не самый важный момент. Неровные зубы сказываются не только на внешнем виде, но и на здоровье.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Последствия нарушения прикуса</h2>
                <div class="dots mb-80">
                    <div class="dots-item">
                        <p class="text dots__text">Логопедические проблемы </p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Трудности при пережевывании пищи и дыхании</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Повышенное стирание эмали</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Множественный кариес и ранняя потеря зубов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Нарушения осанки</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Асимметрия, изменение овала и черт лица</p>
                    </div>
                </div>
                <h3 class="h3 mb-30">Почему происходит нарушение прикуса</h3>
                <p class="text">Самая частая причина неправильного прикуса — нарушение работы мышц челюстей и лица. К этому приводят детские привычки, искусственное вскармливание, нарушение строения уздечек языка и губ, дыхание ртом при частых ОРВИ. Влияет на прикус и преждевременная потеря молочных зубов.<br> Если малыш часто болеет, плохо кушает или любит сосать пустышку, проконсультируйтесь с ортодонтом. Многие проблемы можно предотвратить при ранней диагностике.</p>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2">Первое посещение ортодонта</h2>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md pr-90-xl">
                <p class="text mb-80">Проконсультироваться с ортодонтом стоматологической клиники FamilySmile оптимально, как только прорезались молочные зубы. Доктор проверит, правильно ли развивается челюстно-лицевая система ребенка, и расскажет, что нужно делать для профилактики нарушений роста зубов. Костная ткань малыша только формируется, она пластична и легко поддается воздействию. Поэтому чем младше пациент, тем проще скорректировать развитие прикуса.</p>
                <blockquote class="blockquote">Первое посещение стоматологической клиники FamilySmile начинается со знакомства. Ребенку необходимо время на адаптацию к новому месту, чтобы он почувствовал себя комфортно и в безопасности. Доктора нашей стоматологии особое внимание уделяют психологическому подходу к малышу. Если у ребенка сложится положительное впечатление, он спокойно посидит в кресле и в дальнейшем не будет бояться стоматологов.</blockquote>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-80-xs mt-0-xl">
                <h3 class="h3 mb-30">Способы профилактики проблем с прикусом у малышей </h3>
                <div class="dots mb-50">
                    <div class="dots-item">
                        <p class="dots__title">Миотерапия</p>
                        <p class="text dots__text">Чтобы восстановить функции мышц лица, наши врачи назначают своим пациентам специальную гимнастику с 4 лет. Родителям нужно обеспечить регулярное выполнение упражнений. Этот метод назначается для исправления нарушений при молочном прикусе как основной, а при смене зубов — как дополнительный к ношению аппаратов.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Вестибулярные пластинки STOPPI и MUPPY (Германия)</p>
                        <p class="text dots__text">Эти профилактические средства — эффективная замена соске-пустышке или пальцу во рту, на которую ваш ребенок легко согласится. Пластинки способствуют восстановлению работы мышц, и в результате развитие зубов и челюстей нормализируется. Коррекция проходит легко и комфортно.</p>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/4.png" alt="block4" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-80-xl">
                <h2 class="h2 mb-40">Исправление нарушений работы мышц челюсти трейнерами</h2>
                <p class="text mb-80">Трейнер — это мягкая двучелюстная капа, которая надевается в ночное время и всего на 2 часа днем. Трейнер помогает установить правильное взаимодействие мышц лица (губ, щек и языка) и зубочелюстной системы. Аппарат устраняет нарушения работы мышц и корректирует рост зубов.</p>
                <h3 class="h3 mb-30">Трейнеры помогут:</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">отучить ребенка от пустышки или тянуть в рот пальчик</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">приучить малыша дышать только через нос</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">нормализировать положение языка</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">исправить нарушения прикуса</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Наши трейнеры от компании Myofunctional Research Co (Австралия)</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-md">
                <p class="text mb-50">Благодаря мягкости материала трейнеры очень бережно воздействуют на зубочелюстную систему. За ними легко ухаживать: после каждого использования достаточно почистить обычной зубной щеткой. Капы гипоаллергенны, не натирают десны и не повреждают эмаль.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Мягкая коррекция прикуса в период смены зубов</h2>
                <p class="text mb-80">Пластинки — съемные конструкции, которые применяются для исправления прикуса, выравнивания зубов, расширения неба, ускорения или замедления роста челюсти. Они состоят из цветной пластиковой основы, металлической проволоки и различных элементов: крючков, винтов, пружин. Иногда на пластинке фиксируется искусственный зуб для сохранения места, если молочный зуб был утерян раньше естественной смены на постоянный. </p>
                <blockquote class="blockquote mb-50">Пластинки применяются для исправления нарушений в период смены зубов (с 5 до 11 лет). Они оказывают постоянное небольшое давление, которого вполне достаточно, чтобы повлиять на активно формирующиеся костные ткани челюсти. </blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/5.png" alt="block5" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Какие бывают пластинки</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-md">
                <p class="text mb-50">Пластинки носят круглосуточно, снимая только во время гигиены полости рта или приема пищи. Для эффективного лечения необходимо следить, чтобы ребенок исправно носил аппарат. Для этого вы можете придумать для него какое-то поощрение. Каждый месяц маленькому пациенту нужно будет посещать ортодонта клиники FamilySmile для регулировки механизмов. </p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_top">
            <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
                <h2 class="h2 mb-40">Лечение нарушений прикуса у подростков</h2>
                <p class="text">Лечение брекетами — надежный способ выравнивания зубов ребенка, когда у него практически сформировался постоянный прикус, примерно с 11 лет. Брекеты — это несъемные аппараты, которые меняют положение зубов благодаря непрерывному воздействию на костную ткань челюсти. </p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-80-xl">
                <blockquote class="blockquote mb-50">В стоматологической клинике FamilySmile вылечить нарушение прикуса можно без ущерба для эстетики. Стеснительные подростки, безусловно, оценят такую возможность. У нас есть варианты: малозаметные керамические брекеты и похожие на драгоценное украшение сапфировые брекеты.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/6.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-90-xl">
                <h2 class="h2 mb-40">Комплексная диагностика для результата на долгие годы</h2>
                <p class="text mb-60">Во время обследования доктор изучает анатомические особенности строения лицевого скелета, измеряет углы соотношения черепа, челюстей и зубов. Он определяет вектор и величину нагрузки, необходимой для перемещения зубов, а также прогнозирует результаты.</p>
                <p class="text">Позаботьтесь о гармоничном развитии вашего малыша! Обращайтесь в стоматологическую клинику FamilySmile для профилактики и коррекции нарушений прикуса. Мы поможем устранить причину неправильного развития зубов и челюстей ребенка, проведем бережное лечение во время смены прикуса и поможем подросткам обрести улыбку мечты эстетичным способом.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Чтобы поставить верный диагноз, врач проводит диагностику по «золотому стандарту» ортодонтии. В обследование входят: трехмерная компьютерная томография, телерентгенография, кондилография (аксиография), миография, создание контрольно-диагностических моделей челюстей. Точные данные помогают составить эффективный план лечения и спрогнозировать успешный результат. </blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
