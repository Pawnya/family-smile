<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Детская стоматология");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Позаботимся о здоровой и красивой улыбке вашего ребенка </h1>
                </div>
                <p class="text mb-30">Здоровье зубов — важная составляющая здоровья всего организма. Следить за состоянием полости рта нужно начинать, как только появился первый молочный зуб. Но как объяснить это ребенку? В стоматологии FamilySmile работают профессиональные детские стоматологи, которые не только проведут бережное лечение, но и научат малыша не бояться и правильно следить за своими зубками.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Услуги детского отделения стоматологии FamilySmile</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Первое знакомство</h2>
                <p class="text mb-80">Именно первое впечатление от посещения клиники влияет на отношение вашего ребенка к стоматологам в дальнейшем. Вспомните себя в детстве: боялись ли вы лечить зубы? У многих этот страх не проходит даже в зрелом возрасте. Чтобы ваш ребенок в дальнейшем спокойно относился к лечению зубов, к нему необходимо найти психологический подход с первого посещения. Для этого в стоматологической клинике FamilySmile сначала проходит знакомство с доктором.</p>
                <blockquote class="blockquote mb-50">Ваш поход с ребенком к стоматологу клиники FamilySmile превратится в увлекательное путешествие. Малышу будет интересно узнать, как чистит зубы герой его любимого мультфильма. А также наша Зубная фея посчитает зубки маленького пациента и подробно расскажет родителям о состоянии здоровья его полости рта.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Как проходит подготовка к лечению зубов у детей</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-md">
                <p class="text mb-50">Подготовка к лечению зубов необходима, чтобы сформировать у ребенка положительное впечатление от посещения стоматологии. Спокойный малыш, который доверяет доктору, без труда позволит осмотреть и полечить зубки. А в будущем, когда ребенок вырастет, он станет охотнее заботиться о своих зубах.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2 mb-40">Плюсы раннего посещения детской стоматологии</h2>
                <p class="text mb-80">Первое посещение стоматологии следует запланировать, когда малышу исполнится 6–9 месяцев, даже если ничего не беспокоит. Именно в этом возрасте прорезываются первые детские зубки. Доктор стоматологической клиники FamilySmile проверит, все ли в порядке, чтобы вовремя скорректировать рост зубов и челюстей. Важно своевременно выявить нарушения развития уздечки губ или языка (которые, по статистике, у 10% малышей), чтобы не возникли осложнения с прикусом, речью, питанием.</p>
                <h3 class="h3 mb-40">Показания для срочного визита в стоматологию FamilySmile</h3>
                <div class="dots mb-50">
                    <div class="dots-item">
                        <p class="text dots__text">Капризы ребенка при приеме пищи</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Появление творожистого налета или высыпаний на слизистой полости рта у ребенка</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Потемнение эмали</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Разрушенность зуба</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Жалобы на зубную боль</p>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/4.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-90-xl">
                <h2 class="h2 mb-40">Лечение детских зубов</h2>
                <p class="text">Чем раньше начать лечение зуба, тем лучше. Кариес на детских зубах развивается очень быстро. Если молочные зубы начали разрушаться, наши детские специалисты сделают все возможное, чтобы их сохранить до смены на постоянные. Мы проведем лечение кариеса и восстановим зуб пломбой или детскими коронками. А если это невозможно, аккуратно удалим зуб без боли.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Большинство проблем с зубами возникает из-за скопления зубного налета. Кислоты, содержащиеся в нем, разрушают детскую эмаль и вызывают кариес. В домашних условиях полностью очистить налет невозможно. Чтобы сохранить здоровье зубов, каждые полгода необходимо проходить профессиональную гигиену полости рта в стоматологии FamilySmile.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md order-2">
                <h3 class="h3 mb-40">Почему важно сохранить молочные зубы до смены прикуса</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Для правильного развития челюстей</p>
                        <p class="text dots__text">Ранняя потеря молочного зуба приводит к смещению соседних зубов. В итоге постоянному зубу просто не хватает места для прорезывания, и он вырастает неправильно. Чтобы этого не случилось, даже если зуб пришлось удалить, мы установим специальный держатель места.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Для хорошей дикции</p>
                        <p class="text dots__text">Отсутствие зубов не позволяет ребенку четко выговаривать некоторые звуки, например шипящие. Неправильная дикция мешает обучению. Такие дети медленнее учатся читать.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Для комфортного пищеварения</p>
                        <p class="text dots__text">Чтобы все питательные вещества полноценно усвоились, пища должна быть хорошо измельчена. Даже при недостатке одного зуба ребенку неудобно жевать, что сказывается на пищеварении.</p>
                    </div>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/5.png" alt="block4" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества лечения вашего ребенка в нашей клинике</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <blockquote class="blockquote mt-25">Обратитесь как можно раньше к нашему ортодонту. Доктор проведет осмотр и определит, правильно ли развивается зубочелюстная система малыша. В детском возрасте исправление неровных зубов наиболее эффективно. Ведь костная ткань челюсти только формируется и скорректировать ее развитие намного проще. Ваш ребенок может избежать лечения брекетами, если вы позаботитесь о красоте и здоровье его улыбки в раннем возрасте.</blockquote>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq">
                <blockquote class="blockquote mt-60-xs mt-0-xl mb-50">Обратитесь как можно раньше к нашему ортодонту. Доктор проведет осмотр и определит, правильно ли развивается зубочелюстная система малыша. В детском возрасте исправление неровных зубов наиболее эффективно. Ведь костная ткань челюсти только формируется и скорректировать ее развитие намного проще. Ваш ребенок может избежать лечения брекетами, если вы позаботитесь о красоте и здоровье его улыбки в раннем возрасте.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-30">Стоматология FamilySmile сочетает лучшие семейные традиции. Детские специалисты найдут подход к вашему малышу и избавят от страха лечения зубов. Здоровая и красивая улыбка маленьких и взрослых пациентов — наша цель и лучшая награда! Ждем вас на приеме.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
