<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Одномоментная имплантация");
?>
<link rel="stylesheet" href="main.min.css">
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Качественное восстановление <br>зубов всего за 1 день</h1>
                </div>
                <p class="text mb-30">Одномоментная имплантация — это эффективный способ восстановить красоту и функциональность зубов в короткие сроки. Что это значит? Удаление сильно разрушенного зуба, фиксация имплантата и протезирование проходят всего за один визит в клинику. Вы сможете открыто улыбаться и полноценно принимать пищу уже на следующий день. Опытные врачи стоматологии FamilySmile проводят процедуру аккуратно и безболезненно, используя современное оборудование и надежные материалы.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Плюсы имплантации зубов <br>за один день</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <p class="text mt-30">Чтобы провести одномоментную имплантацию, необходимо удалить разрушенный зуб очень точно. Для этого наши специалисты применяют пьезоскальпель Piezon Master Surgery (EMS, Швейцария). Врач с помощью инструмента разрезает зуб на части. Прибор действует только на твердые ткани и не затрагивает мягкие. Такой подход позволяет установить имплантат уже в этот же день. <a href="/services/khirurgiya/udalenie-zubov-s-tselyu-posleduyushchey-implantatsii/">Узнать более подробно об удалении зуба</a> и его восстановлении вы можете на соответствующей странице.</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-50-xs mt-0-xl">
                <p class="text">Чтобы провести одномоментную имплантацию, необходимо удалить разрушенный зуб очень точно. Для этого наши специалисты применяют пьезоскальпель Piezon Master Surgery (EMS, Швейцария). Врач с помощью инструмента разрезает зуб на части. Прибор действует только на твердые ткани и не затрагивает мягкие. Такой подход позволяет установить имплантат уже в этот же день. <a href="/services/khirurgiya/udalenie-zubov-s-tselyu-posleduyushchey-implantatsii/">Узнать более подробно об удалении зуба</a> и его восстановлении вы можете на соответствующей странице.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h3 class="h3 mb-30">Как проходит одномоментная имплантация зубов</h3>
                <div class="dots mb-50">
                    <div class="dots-item">
                        <p class="dots__title">Обезболивание</p>
                        <p class="text dots__text">Используем безопасную анестезию, чтобы во время операции вы ничего не почувствовали.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Удаление</p>
                        <p class="text dots__text">Врач деликатно удаляет зуб по частям с помощью пьезоскальпеля.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Имплантация</p>
                        <p class="text dots__text">Специалист устанавливает опору для протеза — титановый корень, используя хирургический шаблон.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Протезирование</p>
                        <p class="text dots__text">На имплантат сразу фиксируется временная эстетичная коронка.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Реставрация</p>
                        <p class="text dots__text">Постоянная ортопедическая конструкция крепится через 4–6 месяцев, после приживления искусственного корня.</p>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <p class="text">В стоматологической клинике FamilySmile есть собственная зуботехническая лаборатория. Она оснащена современным оборудованием — цифровой системой CAD/CAM. Благодаря ей ортопедическая конструкция в точности повторяет анатомию родных зубов. Готовое изделие получается прочным и эстетичным, ведь мы используем керамику E.maх, диоксид циркония и металлокерамику. Также мы специализируемся на гнатологическом подходе — применяем артикулятор, аксиограф, электромиограф для исследования нарушений височно-нижнечелюстного сустава и изготовления удобных коронок.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества имплантации <br>за один день в клинике FamilySmile</h2>
            </div>
            <div class="inf swiper-container mb-0">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper pb-0-xs pb-50-xl">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="inf-item inf-item_bq">
                            <p class="text mt-30 mb-50">Мы ценим ваше время, комфорт и здоровье, поэтому качественно восстановим зуб с помощью одномоментной имплантации. Процедура пройдет бережно и всего за 1 день. Запишитесь на прием в стоматологическую клинику FamilySmile, доверьтесь нашим профессионалам.</p>
                            <div class="btn-wrapper">
                                <?$APPLICATION->IncludeComponent(
                                    "family-smile:form.result.new",
                                    "inline",
                                    Array(
                                        "WEB_FORM_ID"            => "1",
                                        "AJAX_OPTION_STYLE"      => "Y",
                                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                                        "USE_EXTENDED_ERRORS"    => "Y",
                                        "SEF_MODE"               => "N",
                                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                                        "CACHE_TYPE"             => "N",
                                        "CACHE_TIME"             => "3600",
                                        "LIST_URL"               => "",
                                        "EDIT_URL"               => "",
                                        "SUCCESS_URL"            => "",
                                        "CHAIN_ITEM_TEXT"        => "",
                                        "CHAIN_ITEM_LINK"        => "",
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="box-bq mt-50-xs mt-0-xl">
                <p class="text mb-50">Мы ценим ваше время, комфорт и здоровье, поэтому качественно восстановим зуб с помощью одномоментной имплантации. Процедура пройдет бережно и всего за 1 день. Запишитесь на прием в стоматологическую клинику FamilySmile, доверьтесь нашим профессионалам.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<script defer src="main.js"></script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
