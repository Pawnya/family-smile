<?
$aMenuLinks = Array(
	Array(
		"Классическая имплантация",
		"/services/implantatsiya/klassicheskaya-implantatsiya/",
		Array(),
		Array("INFO"=>"Двухэтапная операция, при которой коронка фиксируется после полного приживления имплантата"),
		""
	),
	Array(
		"Одномоментная имплантация",
		"/services/implantatsiya/odnomomentnaya-implantatsiya/",
		Array(),
		Array("INFO"=>"Удаление разрушенного зуба, фиксация имплантата и коронки за один визит"),
		""
	),
	Array(
		"Синус-лифтинг и костная пластика",
		"/services/implantatsiya/sinus-lifting-i-kostnaya-plastika/",
		Array(),
		Array("INFO"=>"Восстановление объема костной ткани для установки имплантата"), 
		""
	)
);
?>
