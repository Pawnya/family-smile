<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Синус-лифтинг и костная пластика");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Создадим условия для <br>предстоящей имплантации</h1>
                </div>
                <p class="text mb-30">Имплантация является оптимальным способом восстановления утраченных зубов. Но для фиксации искусственного корня необходим достаточный объем кости. Врачи клиники FamilySmile создают все условия для имплантации с помощью синус-лифтинга и костной пластики. Мы обязательно вернем вам комфорт и красивую улыбку.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 mb-35">На пути к красоте и комфорту</h2>
                <p class="text">Потеря зуба лишает кость жевательной нагрузки, по этой причине она начинает постепенно убывать. Организм экономит внутренние ресурсы и просто прекращает ее питание. Поэтому мы рекомендуем восстановить зуб в течение полугода после удаления, ведь для фиксации имплантата важны объем и высота кости. Если обследование выявило недостаток костной ткани, мы проведем процедуру ее восстановления.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Костная пластика в клинике <br>FamilySmile</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-md">
                <p class="text mb-50">Спланировать костную пластику в клинике FamilySmile помогает компьютерный томограф KaVo Pan Exam Plus 3D (Германия). Это единственный способ оценить объем кости. Трехмерный снимок можно изучить во всех деталях и с любого ракурса. Врач составит оптимальный план восстановления костного объема и будущей имплантации.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Тонкий подход</h2>
                <p class="text mb-80">Особого мастерства от хирурга требует воссоздание костного объема в области пазух носа, или синусов. Процедура так и называется — синус-лифтинг. У пазух заимствуется место для будущего имплантата. Мембрана гайморовой пазухи бережно приподнимается, получившееся пространство заполняется костным материалом. В некоторых случаях имплантация может проводиться в тот же день, что и костная пластика.</p>
                <h3 class="h3 mb-40">Типы синус-лифтинга</h3>
                <div class="dots mb-50">
                    <div class="dots-item">
                        <p class="dots__title">Открытый синус-лифтинг</p>
                        <p class="text dots__text">Проводится при нехватке кости более 4 мм. В стенке пазухи формируется окно, через которое подается костный материал.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Закрытый синус-лифтинг</p>
                        <p class="text dots__text">Через ложе, подготовленное для имплантации, подается костный материал для восстановления до 3 мм костной ткани.</p>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container block block_top">
            <div class="block-wrapper block-wrapper_tleft">
                <blockquote class="blockquote">Достаточный объем кости — это важное условие для проведения имплантации. Без него просто невозможно зафиксировать искусственный корень и вернуть вам красивую улыбку. В клинике FamilySmile костная пластика планируется на основе тщательной диагностики, с учетом дальнейшего лечения. Имплантат прослужит вам долгие годы и станет надежной опорой для протеза.</blockquote>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl  pr-0-xs pr-120-xl mt-80-xs mt-0-xl">
                <p class="text mb-50">Врачи клиники FamilySmile выбирают только лучшее для вас. Синус-лифтинг и костная пластика создают все условия для имплантации. Мы вернем вам повседневный комфорт и красивую улыбку. Запишитесь на прием, мы будем рады вам помочь.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
