<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Классическая имплантация");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Вернем красоту и здоровье улыбки <br>эффективным способом </h1>
                </div>
                <p class="text mb-30">Что для вас важно при восстановлении утраченного зуба? Наверняка удобство, эстетика и долговечный результат. Идеально, если при этом максимально сохраняются здоровые зубы и комфорт при приеме пищи. У нас есть такая методика! В стоматологии FamilySmile помогут вернуть жевательную функцию и красоту улыбки с помощью классической имплантации. Приходите на консультацию, чтобы надежно восстановить зубы с бессрочной гарантией.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Почему имплантация — оптимальный способ восстановления зубов</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper inf-wrapper_left swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">С чего начинается <br>имплантация</h2>
                <p class="text mb-80">Чтобы спланировать успешную имплантацию, необходимо полное обследование зубочелюстной системы. Всестороннюю диагностику в стоматологии FamilySmile мы проводим на компьютерном томографе KaVo Pan Exam Plus 3D (Германия). С помощью трехмерных снимков высокого качества хирург-имплантолог оценит структуру кости, ее размеры и особенности. Для установки имплантата должно быть достаточно костной ткани.</p>
                <blockquote class="blockquote mb-50">Что происходит, если долго не восстанавливать зуб? Кость атрофируется, потому что остается без нагрузки. Можно провести аналогию с мышцами, которые также теряют тонус, если не напрягаются. Уже через 6 месяцев после удаления зуба объем кости уменьшается. Чтобы нарастить костную ткань до нужного размера, применяется костная пластика или синус-лифтинг.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/2.png" alt="block2" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Классическая имплантация проходит в 3 этапа</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Костная пластика или синус-лифтинг</p>
                                <p class="text box__text">При необходимости проведем дополнительную операцию, чтобы увеличить объем кости под имплантат.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-2.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Установка имплантата</p>
                                <p class="text box__text">С помощью специального оборудования надежно зафиксируем имплантат.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-3.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Протезирование</p>
                                <p class="text box__text">Через 4–6 месяцев установим постоянную ортопедическую конструкцию, которая вернет вам комфорт.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner">
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/3.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pl-0-xs pl-90-xl">
                <h2 class="h2 mb-40">Передовые технологии <br>для хирургических <br>операций</h2>
                <p class="text">Для костной пластики мы используем инновационное оборудование. Piezon Master Surgery (Швейцария) — ультразвуковой хирургический прибор, который обеспечивает идеально точный разрез кости. Аппарат не воздействует на мягкие ткани, поэтому операция проходит максимально аккуратно. Для установки имплантата, а также обеспечения безопасности и стерильности у нас есть физиодиспенсер Implantmed (Австрия). Он охлаждает и очищает место разреза с помощью физраствора.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-0-xl">
                <blockquote class="blockquote mb-50">Успешность классической имплантации в стоматологии FamilySmile во многом обусловлена надежными системами SIC (Швейцария) и BioHorizons (США). Мы применяем эти имплантаты много лет и уверены в их качестве. Они имеют покрытие, которое гарантирует биосовместимость и приживаемость имплантатов.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества классической имплантации<br> в стоматологии FamilySmile</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/4.png" alt="block4" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Здоровье <br>и красота улыбки</h3>
                <p class="text mb-50">Восстановление правильного взаимодействия зубов, суставов челюсти и жевательных мышц — наша специализация. С помощью имплантации и высокоточных коронок мы обеспечиваем идеальное смыкание зубов. Для протезов мы применяем современные материалы — керамику E.max и диоксид циркония, сочетающие высокую эстетику и прочность. Также применяем и классические материалы — металлокерамику.</p>
                <p class="text mb-50">Внимательное отношение, уникальные технологии, опытные врачи и комплексный подход сделают ваше лечение в стоматологии FamilySmile не только эффективным, но и максимально комфортным. Мы поможет вам обрести здоровье и эстетику улыбки!</p>
            </div>
            <div class="btn-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:form.result.new",
                    "inline",
                    Array(
                        "WEB_FORM_ID"            => "1",
                        "AJAX_OPTION_STYLE"      => "Y",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS"    => "Y",
                        "SEF_MODE"               => "N",
                        "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                        "CACHE_TYPE"             => "N",
                        "CACHE_TIME"             => "3600",
                        "LIST_URL"               => "",
                        "EDIT_URL"               => "",
                        "SUCCESS_URL"            => "",
                        "CHAIN_ITEM_TEXT"        => "",
                        "CHAIN_ITEM_LINK"        => "",
                        )
                    );
                ?>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
