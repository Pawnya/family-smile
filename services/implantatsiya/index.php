<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Имплантация");
?>
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Надежный способ <br>восстановления зубов </h1>
                </div>
                <p class="text mb-30">Имплантация зубов — наиболее надежный способ восстановления эстетики и функциональности. Имплантат с коронкой полностью заменит утраченный зуб и прослужит вам десятки лет. Конструкция эстетично выглядит и по ощущениям очень похожа на собственный зуб. Обратитесь в стоматологию FamilySmile за современным восстановлением зубов.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Методики имплантации зубов</h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:menu",
                        "services",
                        Array(
                            "ROOT_MENU_TYPE" => "submenu",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "top",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => ""
                        )
                    );?>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/2.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3">В каких случаях необходима <br>имплантация зубов</h3>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <div class="dots mb-80">
                    <div class="dots-item">
                        <p class="dots__title">Утрата одного зуба</p>
                        <p class="text dots__text">Титановое основание позволяет воссоздать зуб без обтачивания соседних.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Отсутствие нескольких зубов подряд</p>
                        <p class="text dots__text">Их восстанавливают одной конструкцией на двух имплантатах.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Нежелание носить съемные протезы</p>
                        <p class="text dots__text">Конструкция на имплантатах надежно фиксируется и не смещается во время приема пищи.</p>
                    </div>
                </div>
                <p class="text">Имплантация — это первый этап восстановления функциональности зубного ряда. Впоследствии на титановый корень крепится протез, и вся эта конструкция полностью заменяет утраченный зуб. Коронки на имплантатах не смещаются во время приема пищи, а также отлично выдерживают ежедневную жевательную нагрузку.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-25">
                <blockquote class="blockquote mt-40 mb-50">При установке нескольких имплантатов мы используем хирургические шаблоны. Они создаются индивидуально, по слепкам вашей челюсти. Хирургический шаблон — это накладка на зубной ряд с отверстиями-направляющими под фиксацию имплантатов. Благодаря шаблонам операция проходит быстро и максимально точно.</blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Преимущества имплантации <br>в нашей клинике</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-md">
                <p class="text mb-50">Имплантация — обязательная операция для нормализации работы всего организма. Отсутствие даже одного зуба затрудняет прием пищи, становится причиной появления речевых дефектов и проблем с пищеварением. Поэтому установку имплантата желательно провести как можно скорее, так как после утраты зуба кость на его месте постепенно теряет объем.</p>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-img block-img_tleft">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <blockquote class="blockquote mt-20 mb-80">Наши специалисты рекомендуют заняться восстановлением в течение полугода, пока не начался процесс атрофии кости. Однако и после этого срока мы поможем вам вернуть отсутствующие зубы. Проведем костную пластику, и для установки имплантата будет достаточно места.</blockquote>
                <h2 class="h2 mb-40">Почему имплантация — <br>лучший способ <br>восстановления зубов</h2>
                <p class="text mb-50">Установка имплантата — единственная методика восстановления зуба без обточки соседних. После фиксации титанового корня прекращается убыль костной ткани, правильно распределяется жевательная нагрузка, нормализуется работа зубочелюстной системы. Вы снова сможете с комфортом принимать пищу, пропадут неприятные ощущения в суставе.<br> Отсутствие зубов можно легко исправить. Обратитесь в стоматологию FamilySmile для восстановления. После имплантации и установки коронки вы снова сможете свободно улыбаться и с комфортом принимать пищу.</p>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
