<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Диагностика");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.js"></script>
<div id="app">
    <section class="section section-padding_inner">
        <div class="section-layer">
            <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
        </div>
        <div class="container main-container main-container_center">
            <div class="main-inner">
                <?$APPLICATION->IncludeComponent(
                    "family-smile:breadcrumb",
                    "family-breadcrumbs",
                    Array(
                        "START_FROM" => "0",
                        "PATH"       => "",
                        "SITE_ID"    => "s1"
                    )
                );?>
                <div class="section-title">
                    <h1 class="h1 mb-30">Качественная диагностика — залог успешного лечения и сохранения здоровья </h1>
                </div>
                <p class="text mb-30">Чтобы сохранить здоровье зубов и десен, необходимо посещать стоматолога дважды в год. Многие заболевания начинаются незаметно, и только своевременное обследование может их выявить. Стоматологическая клиника FamilySmile оснащена самым передовым оборудованием. Мы сможем выявить любую проблему, чтобы оказать эффективную помощь.</p>
                <div class="btn-wrapper btn-wrapper_col">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                    <a href="#" class="link link_section mt-30">Подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/1.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <h2 class="h2 tac mb-40">Компьютерная томография для всесторонней диагностики</h2>
                <p class="text mb-30">В стоматологической клинике FamilySmile установлен современный цифровой томограф KaVo Pan Exam Plus 3D (Германия), который создает снимки в формате 3D. Это самый информативный способ обследования зубочелюстной системы: врач может увеличивать изображение и поворачивать его на 360°. Диагностика длится примерно 30 секунд, и за это время создается трехмерный снимок зубов высокого качества.</p>
                <p class="text mb-30">Компьютерная томография совершенно безопасна: лучевая нагрузка сравнима с 4-часовым перелетом на самолете. Снимок хранится на компьютере и может быть выдан вам на любом носителе.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Для чего нужна компьютерная томография челюсти:</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="inf in infographics">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + inf.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ inf.title }}</p>
                            <p class="text">{{ inf.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
            <div class="main-inner mt-30-xs mt-0-xl">
                <p class="text mb-50">Томография дает информации больше, чем десятки рентгеновских снимков. Всего за одно короткое обследование вы узнаете все о состоянии своих зубов и челюстей. Это очень выгодно, потому что отпадет необходимость дополнительных исследований и появится возможность выявить заболевания на ранней стадии, когда их проще всего вылечить.</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/2.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_top">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-90-xl">
                <h2 class="h2 mb-40">Панорамный снимок</h2>
                <p class="text mb-80">Панорамный снимок — это 2D-снимок верхнего и нижнего зубного ряда в анфас. В некоторых случаях достаточно данного вида диагностики, чтобы правильно выстроить процесс лечения. Обследование занимает несколько секунд.</p>
                <blockquote class="blockquote">Для более полной и тщательной диагностики специалисты стоматологической клиники FamilySmile всегда рекомендуют пройти компьютерную томографию. Ранние стадии заболеваний не всегда видны на плоском снимке, в отличие от трехмерной проекции. Но если упустить момент для своевременного лечения, то можно потерять зуб.</blockquote>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-80-xs mt-80-xl">
                <h3 class="h3 mb-40">В каких случаях используется панорамный снимок для диагностики  </h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="dots__title">Проблемные зубы</p>
                        <p class="text dots__text">Хорошо видны зубы, которые не могут прорезаться или неправильно расположены. </p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Киста зуба или челюсти</p>
                        <p class="text dots__text">Можно оценить степень поражения, локацию и размеры новообразования.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Процессы в костной ткани</p>
                        <p class="text dots__text">На снимке видна атрофия костной ткани при пародонтите, а также воспалительные процессы.</p>
                    </div>
                    <div class="dots-item">
                        <p class="dots__title">Оценка состояния челюстей</p>
                        <p class="text dots__text">Диагностика проблем височно-нижнечелюстного сустава.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_top">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Прицельный <br>снимок зуба</h2>
                <p class="text mb-80">Контроль лечения зубов на всех этапах мы осуществляем с помощью быстрого прицельного снимка на визиографе. Обследование охватывает 1–2 зуба. В стоматологической клинике FamilySmile есть собственный рентген-кабинет, поэтому вам не нужно будет никуда идти для диагностики. Мы сделаем снимок на высокоточном визиографе GENDEX EXPERT DC. Результат сразу же появится на экране. </p>
                <blockquote class="blockquote mb-50">Уникальность визиографа — в его максимальной безопасности. Датчик визиографа настолько чувствителен, что для его активации требуется минимальная доза рентгеновских лучей. </blockquote>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/3.png" alt="block3" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/4.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block block_center">
            <div class="block-wrapper block-wrapper_tleft pr-0-xs pr-90-xl">
                <h2 class="h2 mb-40">Телерентгенография</h2>
                <p class="text">Телерентгенограмма — это снимок всего черепа в прямой и боковой проекции. Он незаменим для ортодонтического лечения. На снимке хорошо видны и мягкие, и костные ткани. Врач оценивает с помощью этой диагностики размеры и расположение зубов, костей челюстей и лица по отношению друг к другу. </p>
            </div>
            <div class="block-wrapper pl-0-xs pl-80-xl mt-60-xs mt-25-xl">
                <blockquote class="blockquote">Чем раньше ваш ребенок пройдет телерентгенографию, тем лучше для его здоровья. Если выявить возможные нарушения прикуса на ранней стадии, то лечение пройдет быстрее и эффективнее. Детские зубы более податливы к перемещению.</blockquote>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-img block-img_tleft">
                <img src="images/5.png" alt="block5" class="block__img">
            </div>
            <div class="block-wrapper block-wrapper_tleft">
                <h3 class="h3 mb-40">Что показывает <br>телерентгенография</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">Разновидность прикуса и его нарушения</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Положение челюстей относительно друг друга </p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Возможную асимметрию зубных рядов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Состояние дыхательных органов</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Развитие шейного отдела позвоночника</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-overflow">
        <div class="container block block_center">
            <div class="block-wrapper pr-0-xs pr-95-md order-2">
                <h2 class="h2 mb-40">Диагностика кариеса и состояния тканей зуба </h2>
                <p class="text mb-50">Для эффективного лечения зуба доктор должен знать точное расположение кариозных полостей, видеть даже микротрещины в эмали. Не менее важно после лечения восстановить поверхность зуба с ювелирной точностью. Необходимо герметично покрыть зуб пломбой, воссоздать его естественный рельеф для идеального смыкания челюстей и предотвратить вторичный кариес. </p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="block-img order-1">
                <img src="images/6.png" alt="block6" class="block__img">
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-padding_top section-padding_bottom">
        <div class="section-layer">
            <img src="/images/section-layer2.png" alt="" class="section-layer__img section-layer__img_left">
        </div>
        <div class="container main-container main-container_center">
            <div class="section-title">
                <h2 class="h2 tac">Для диагностики и лечения кариеса мы используем</h2>
            </div>
            <div class="inf swiper-container">
                <div class="inf-wrapper swiper-wrapper">
                    <div class="swiper-slide" v-for="reason in reasons">
                        <div class="inf-item">
                            <div class="inf-img">
                                <img :src="'/images/icon/' + reason.icon + '.svg'" alt="inf">
                            </div>
                            <p class="subtitle inf__title">{{ reason.title }}</p>
                            <p class="text">{{ reason.text }}</p>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_inf">
                    <div class="pagination pagination_inf swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container block mb-40">
            <div class="block-wrapper block-wrapper_tleft">
                <h2 class="h2">Диагностика состояния височно-нижнечелюстного сустава</h2>
            </div>
        </div>
        <div class="container block">
            <div class="block-wrapper pr-0-xs pr-40-md">
                <p class="text mb-60">Здоровье височно-нижнечелюстного сустава имеет большое влияние на ваше общее самочувствие. Нарушения работы сустава — очень распространенное явление и, по статистике, находится на третьем месте после кариеса и пародонтита. И это неудивительно: сустав функционирует практически ежеминутно — при разговоре, приеме пищи и зевоте.</p>
                <p class="text">Заболевания сустава могут возникнуть по самым разным причинам: из-за стресса и чрезмерных нагрузок, неправильного смыкания зубов, травм, нарушения развития костной ткани. Чтобы максимально точно диагностировать проблему и составить эффективный план лечения, в нашей клинике помимо МРТ и 3D-томографии используются другие прогрессивные методы обследования.</p>
            </div>
            <div class="block-wrapper pl-0-xs pl-90-xl mt-30-xs mt-0-xl">
                <h3 class="h3 mb-40">Нарушение работы височно-нижнечелюстного сустава приводит к неприятным последствиям:</h3>
                <div class="dots">
                    <div class="dots-item">
                        <p class="text dots__text">Головные боли, спазмы лицевых мышц</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Щелчки и болезненность при жевании</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Перенапряжение шейных мышц и нарушение осанки</p>
                    </div>
                    <div class="dots-item">
                        <p class="text dots__text">Шум в ушах и головокружение</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="block-img block-img_full">
                <img src="images/7.png" alt="Full image" class="block__img_full">
            </div>
        </div>
    </section>
    <section class="section section-margin_top">
        <div class="container">
            <div class="section-title">
                <h2 class="h2">Способы диагностики работы височно-нижнечелюстного сустава  </h2>
            </div>
            <div class="box box_menu swiper-container">
                <div class="box-wrapper swiper-wrapper">
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-1.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Артикулятор</p>
                                <p class="text box__text">Благодаря специальному механическому прибору доктор оценивает индивидуальные особенности движения нижней челюсти в различных направлениях. Современный, полностью регулируемый артикулятор REFERENCE SL GAMMA DENTAL используется для определения нарушений работы сустава, а также создания подходящих конструкций при протезировании для равномерной нагрузки и правильного смыкания зубов.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-2.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Электронная аксиография</p>
                                <p class="text box__text">Электронный аксиограф CADIAX 4 GAMMA DENTAL записывает в цифровом формате траектории движения нижней челюсти. Он состоит из лицевой дуги с датчиками, которые передают информацию на компьютер. Устройство позволяет оценить симметричность и синхронность работы левого и правого сустава. Данные потом переносятся на артикулятор для составления эффективного плана лечения.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-slide swiper-slide">
                        <div class="box-item">
                            <div class="box-img box-img_menu">
                                <img src="images/1-3.png" alt="box-menu1" class="box__img">
                            </div>
                            <div class="box-desc">
                                <p class="box__title">Электромиографическое исследование</p>
                                <p class="text box__text">Самая объективная и информативная диагностика нервной системы и мышц нижней челюсти проводится с помощью четырехканального адаптивного электромиографа «Синапсис». Прибор определяет электрические импульсы мышц в различном положении: при смыкании челюстей, жевании. Он используется также для определения результатов лечения в динамике.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-container pagination-container_box">
                    <div class="pagination pagination_box swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-margin_top section-margin_bottom">
        <div class="container">
            <div class="main-inner">
                <p class="text mb-30">Правильно поставленный диагноз многократно увеличивает успех лечения. Современные средства диагностики в стоматологической клинике FamilySmile нацелены на получение объемной и точной информации о ваших зубах. Приходите к нам на обследование для сохранения вашего стоматологического здоровья на долгие годы!</p>
                <div class="btn-wrapper">
                    <?$APPLICATION->IncludeComponent(
                        "family-smile:form.result.new",
                        "inline",
                        Array(
                            "WEB_FORM_ID"            => "1",
                            "AJAX_OPTION_STYLE"      => "Y",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS"    => "Y",
                            "SEF_MODE"               => "N",
                            "VARIABLE_ALIASES"       => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
                            "CACHE_TYPE"             => "N",
                            "CACHE_TIME"             => "3600",
                            "LIST_URL"               => "",
                            "EDIT_URL"               => "",
                            "SUCCESS_URL"            => "",
                            "CHAIN_ITEM_TEXT"        => "",
                            "CHAIN_ITEM_LINK"        => "",
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
