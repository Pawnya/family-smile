$(function() {
    new Vue({
        el: '#app',
        name: 'app',
        data: {
            infographics: [
                {
                    icon: 'implant',
                    title: 'Планирование имплантации',
                    text: 'Только томография позволяет оценить объем и состояние костной ткани для надежного крепления имплантата.'
                },
                {
                    icon: 'hiryrgiya',
                    title: 'Удаление проблемных зубов',
                    text: 'Доктор видит положение многокорневых зубов, в том числе лежачие «восьмерки», и точно планирует операцию.'
                },
                {
                    icon: 'terapiya',
                    title: 'Эффективное терапевтическое лечение',
                    text: 'На томограмме отлично видны любые скрытые кариозные полости и анатомически сложные каналы зубов.'
                },
                {
                    icon: 'ortopediya',
                    title: 'Составление плана лечения у ортодонта',
                    text: 'Для правильного расчета силы давления конструкций нужны точные данные о состоянии костной ткани, положении зубов и корней.'
                },
                {
                    icon: 'kista',
                    title: 'Выявление кистозных новообразований',
                    text: 'В начале заболевания киста видна только на снимке — исследование помогает распознать и точно локализировать проблему.'
                },
                {
                    icon: 'gnatologiya',
                    title: 'Оценка состояния костей при травмах',
                    text: 'На томограмме видна костная ткань челюсти, височно-нижнечелюстной сустав, а также кости всего лица.'
                }
            ],
            reasons: [
                {
                    icon: 'lampa',
                    title: 'Специальная лампа с насадками valo translume ultradent',
                    text: 'Насадки двух цветов (зеленая и оранжевая) позволяют без рентгена определить скрытый кариес, оценить прилегание пломбы, состояние протезов, обнаружить штифт, микротрещины в эмали.'
                },
                {
                    icon: 'accuracy',
                    title: 'Микроскоп SEILER EVOLUTION XR6',
                    text: 'Прибор позволяет доктору рассмотреть самые мельчайшие детали, что делает его незаменимым при многих манипуляциях. Врач отлично видит каналы зуба, он может обеспечить полное прилегание пломбы и воссоздать даже мельчайший изгиб вашего зуба.'
                },
                {
                    icon: 'diagnostic',
                    title: 'Безопасный визиограф',
                    text: 'Аппарат за доли секунды создает снимок 1–2 зубов. Он помогает доктору определить степень поражения кариесом и спланировать дальнейшее лечение. Визиограф также применяется как контрольный этап при сложном вмешательстве.'
                }
            ]
        }
    });
});
