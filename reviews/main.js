$(function() {
    $('#review-add').submit(function(){
        var formData = $(this).serialize(),
            reg      = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
            mail = document.querySelector('.reviews__input_mail').value;

        if (reg.test(mail) && $('.reviews__label input').prop('checked') !== false) {
            $.ajax({
                type: "POST",
                url: '/reviews/add-review.php',
                data: formData,
                success: function (data) {
                    // Вывод текста результата отправки
                    $('.overlay-reviews').removeClass('hidden');
                    $('.overlay').removeClass('hidden');
                    $('.reviews-form').addClass('hidden');
                    setTimeout(function() {
                        $('.overlay-reviews').addClass('hidden');
                        $('.overlay').addClass('hidden');
                        $('.reviews-form').removeClass('hidden');
                    }, 5000);
                },
                error: function (jqXHR, text, error) {
                    // Вывод текста ошибки отправки
                    $('.overlay-reviews').removeClass('hidden');
                    $('.overlay').removeClass('hidden');
                    $('.reviews-form').addClass('hidden');
                    $('.reviews-form__title').addClass('hidden');
                    $('.reviews-form__title_2').removeClass('hidden');
                    setTimeout(function () {
                        $('.reviews-form__title').removeClass('hidden');
                        $('.reviews-form__title_2').addClass('hidden');
                        $('.overlay-reviews').addClass('hidden');
                        $('.overlay').addClass('hidden');
                        $('.reviews-form').removeClass('hidden');
                    }, 5000);
                }
            });
        } else {
            alert('Заполните все поля и дайте согласие на обработку персональных данных');
        }
        return false;
    });

    $('.reviews__check').click(function() {
        var inputCheck = $('.reviews__label input');
        var check = $(this);
        check.toggleClass('check');
        if (check.hasClass('check')) {
            inputCheck.prop('checked', true);
        } else {
            inputCheck.prop('checked', false);
        }
    });

    $('input[name="phone"]').attr('type', 'tel').mask("+7 999 999 99 99");

    $('.reviews-form__close_x').click(function () {
        $('.overlay-reviews').addClass('hidden');
        $('.overlay').addClass('hidden');
        $('.reviews-form').removeClass('hidden');
    });
})
