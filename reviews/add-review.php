<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST['name']) && !empty($_POST['description'])) {

        CModule::IncludeModule('iblock');

        //Погнали
        $el = new CIBlockElement;
        $iblock_id = 6;
        $PROP = array();
        $PROP[PHONE] = $_POST['phone'];
        $name = strip_tags($_POST['name']);
        $arParams = array("replace_space"=>"-","replace_other"=>"-");
        $trans = Cutil::translit($name,"ru",$arParams);

        //Основные поля элемента
        $fields = array(
            "DATE_CREATE"     => date("d.m.Y H:i:s"),
            "CREATED_BY"      => $GLOBALS['USER']->GetID(),
            "IBLOCK_ID"       => $iblock_id,
            "PROPERTY_VALUES" => $PROP,
            "NAME"            => strip_tags($_POST['name']),
            "ACTIVE"          => "N",
            "PREVIEW_TEXT"    => strip_tags($_POST['description']),
            "CODE"            => $trans
        );


        //Результат в конце отработки
        if ($ID = $el->Add($fields))
        {
            echo '<center>

            Спасибо за отправку вашего отзыва!

            </center>';
        } else {
            echo '<center>

            <b>Ошибка. Отзыв не отправлен!</b>

            </center>';
        }
    } else {
        http_response_code(403);
        echo "Попробуйте еще раз";
    }
}
?>
