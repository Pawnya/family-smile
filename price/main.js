$(function() {
    var btn = document.querySelectorAll('.price__btn');

    btn.forEach(function(item) {
        item.addEventListener('click', function() {
            document.querySelectorAll('.price-list').forEach(function(elem) {
                elem.classList.remove('active');
            });
            btn.forEach(function(el) {
                el.classList.remove('price__btn_active');
            });
            this.parentElement.nextElementSibling.classList.add('active');
            this.classList.add('price__btn_active');

        })
    });
});
