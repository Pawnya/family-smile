<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Цены");
?>
<link rel="stylesheet" href="main.min.css">
<script defer src="main.min.js"></script>
<section class="section section-padding_inner">
    <div class="section-layer">
        <img src="/images/inner-layer.png" alt="section-layer2" class="section-layer__img">
    </div>
    <div class="container main-container main-container_center">
        <div class="main-inner">
            <?$APPLICATION->IncludeComponent(
                "family-smile:breadcrumb",
                "family-breadcrumbs",
                Array(
                    "START_FROM" => "0",
                    "PATH"       => "",
                    "SITE_ID"    => "s1"
                )
            );?>
            <div class="section-title">
                <h1 class="h1 mb-30">Цены</h1>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent(
            "family-smile:news.list",
            "price",
            Array(
                "DISPLAY_DATE"                    => "Y",
                "DISPLAY_NAME"                    => "Y",
                "DISPLAY_PICTURE"                 => "Y",
                "DISPLAY_PREVIEW_TEXT"            => "Y",
                "AJAX_MODE"                       => "N",
                "IBLOCK_TYPE"                     => "content",
                "IBLOCK_ID"                       => "price",
                "NEWS_COUNT"                      => "20",
                "SORT_BY1"                        => "ACTIVE_FROM",
                "SORT_ORDER1"                     => "DESC",
                "SORT_BY2"                        => "SORT",
                "SORT_ORDER2"                     => "ASC",
                "FILTER_NAME"                     => "",
                "FIELD_CODE"                      => Array("ID"),
                "PROPERTY_CODE"                   => Array("DESCRIPTION"),
                "CHECK_DATES"                     => "Y",
                "DETAIL_URL"                      => "",
                "PREVIEW_TRUNCATE_LEN"            => "",
                "ACTIVE_DATE_FORMAT"              => "d.m.Y",
                "SET_TITLE"                       => "N",
                "SET_BROWSER_TITLE"               => "N",
                "SET_META_KEYWORDS"               => "Y",
                "SET_META_DESCRIPTION"            => "Y",
                "SET_LAST_MODIFIED"               => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
                "ADD_SECTIONS_CHAIN"              => "Y",
                "HIDE_LINK_WHEN_NO_DETAIL"        => "Y",
                "PARENT_SECTION"                  => "",
                "PARENT_SECTION_CODE"             => "",
                "INCLUDE_SUBSECTIONS"             => "Y",
                "CACHE_TYPE"                      => "A",
                "CACHE_TIME"                      => "3600",
                "CACHE_FILTER"                    => "Y",
                "CACHE_GROUPS"                    => "Y",
                "DISPLAY_TOP_PAGER"               => "Y",
                "DISPLAY_BOTTOM_PAGER"            => "Y",
                "PAGER_TITLE"                     => "Цены",
                "PAGER_SHOW_ALWAYS"               => "Y",
                "PAGER_TEMPLATE"                  => "",
                "PAGER_DESC_NUMBERING"            => "Y",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL"                  => "Y",
                "PAGER_BASE_LINK_ENABLE"          => "Y",
                "SET_STATUS_404"                  => "Y",
                "SHOW_404"                        => "Y",
                "MESSAGE_404"                     => "",
                "PAGER_BASE_LINK"                 => "",
                "PAGER_PARAMS_NAME"               => "arrPager",
                "AJAX_OPTION_JUMP"                => "N",
                "AJAX_OPTION_STYLE"               => "Y",
                "AJAX_OPTION_HISTORY"             => "N",
                "AJAX_OPTION_ADDITIONAL"          => ""
            )
        );?>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/include/reviews.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
